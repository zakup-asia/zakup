<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Быстрый старт. Размещение интерактивной карты на странице</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
    <script type="text/javascript">
        ymaps.ready(init);

        function init() {
            var myGeolocation = document.getElementById('MyCity');
            // Данные о местоположении, определённом по IP
            var geolocation = ymaps.geolocation,
            // координаты
                coords = [geolocation.latitude, geolocation.longitude],
                myMap = new ymaps.Map('map', {
                    center: coords,
                    zoom: 10
                });
            var geo = geolocation.city;
            var tochka = new ymaps.Placemark(
                coords,
                {
                    // В балуне: страна, город, регион.
                    balloonContentHeader: geolocation.country,
                    balloonContent: geolocation.city,
                    balloonContentFooter: geolocation.region
                }
            )

            myMap.geoObjects.add(tochka);

            //В итоге нам нужно парсить вот это, оно содержит всё нам необходимое.
            console.log(geolocation);
            console.log(geolocation['country'], geolocation['region'], geolocation['city']);
            console.log(geo);
        }
    </script>
</head>

<body>
    <div id="map" style="width: 1px; height: 1px; display: block"></div>
</body>

</html>