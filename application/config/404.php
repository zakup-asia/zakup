 <section class="two-col-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="two-col">
                        <div class="profile-menu" id="profile-menu">

                        </div><!-- /.profile-menu -->
                        <div class="megamenu-left" style="display: block" >
                            <ul class="megamenu-items-wrapper hidden-sm hidden-xs">
                                <?php foreach($section->result_array() as $row) : ?>
                                    <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/product/findProducts/?searchText=<?php echo $row['name_section']?>"><?php echo $row['name_section']?></a>
                                        <div class="hidden-item">
                                            <div>
                                                <?php foreach ($category->result_array() as $row2) :?>
                                                    <div class="hidden-menu-column">
                                                        <?php if($row2['id_section_one'] == $row['id_section_one'] ):?>
                                                            <a class="menu-parent-item" href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/product/findProducts/?searchText=<?php echo $row2['name_category']?>"><?php echo $row2['name_category']?></a>
                                                            <?php foreach ($subcategory->result_array() as $row3) :?>

                                                                <?php if($row3['id_category'] == $row2['id_category']):?>
                                                                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/product/findProducts/?searchText=<?php echo $row3['name_subcategory']?>" class="menu-cat-item"><?php echo $row3['name_subcategory']?></a>
                                                                <?php endif;?>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                <?php endforeach;?>
                                            </div>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>

                        <div class="megamenu-right" style="height: 445px">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12 col-bg-12">
										<h1>ZAKUP.ASIA</h1>
										<br>
										<img src="/../../images/404.png" alt="404"/>
										<br>
                                        <h1><b>404</b></h1>
										<h3>ошибка</h3>
										<h5>Вы перешли по неправильной ссылке или страница была удалена</h5>
										<br>
										<a href="http://zakup.asia/" title="вернуться на главную" class="clear-order text-center">вернуться на главную</a>
                                    </div>
                                </div>   
                        </div><!-- /.megamenu-right-->
                    </div><!-- /.two-col -->
                </div>
            </div>
        </div><!-- /.container -->
    </section>

    <section class="main-tab">
        <div class="tab-line"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="tabs-wrapper">
                        <a id="All" href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/suppliers/getAllSuppliers" class="all-actions"><span>Все поставщики</span></a>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#providers" aria-controls="providers" role="tab" data-toggle="tab" onclick="AllCompanies()">
                                    <span class="hidden-xs">Поставщики</span>
                                    <span class="visible-xs-inline">Поставщики</span>
                                    <span>&nbsp;</span>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#actions" aria-controls="actions" role="tab" data-toggle="tab" onclick="AllAction()">
                                    <span class="hidden-xs">Акции</span>
                                    <span class="visible-xs-inline">Акции</span>
                                    <span>&nbsp;</span>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#news" aria-controls="news" role="tab" data-toggle="tab" onclick="AllNews()">
                                    <span class="hidden-xs">Новости и статьи</span>
                                    <span class="visible-xs-inline">Новости</span>
                                    <span>&nbsp;</span>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#products" aria-controls="products" role="tab" data-toggle="tab" onclick="AllProducts()">
                                    <span class="hidden-xs">Товары</span>
                                    <span class="visible-xs-inline">Товары</span>
                                    <span>&nbsp;</span>
                                </a>
                            </li>
                        </ul>


                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane" id="actions">
                                <?php //TODO акции ?>
                                <?php foreach($stock->result_array() as $row): ?>
                                    <div class="best-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="best-action">
                                                    <?php if($row['path'] != null):?>
                                                        <img width="72" src="/../../images/mini/<?php echo $row['path']?>" alt="">
                                                    <?php endif; ?>
                                                    <?php if($row['path'] == null):?>
                                                        <img width="72" src="/../../images/best-action.jpg<?php echo $row['path']?>" alt="">
                                                    <?php endif; ?>
                                                    <div class="action-desc">
                                                        <p><?php echo $row['short_description_stock']?></p>
                                                        <p class="action-company">
                                                            <span><?php echo $row['name']?></span>
                                                            <span> до <?php echo $row['end_stock']?></span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- .best-actions -->
                                <?php endforeach;?>
                            </div>
                            <div role="tabpanel" class="tab-pane active" id="providers">
                                <div class="best-providers">
                                    <div class="row">
                                        <!-- Tab panes -->
                                        <?php foreach ($companies->result_array() as $row) :?>
                                            <div class="col-md-6">
                                                <div class="best-provider block-shadow">
                                                    <?php if($row['path'] != null):?>
                                                        <img width="72" src="/../../images/mini/<?php echo $row['path']?>" alt="">
                                                    <?php endif; ?>
                                                    <?php if($row['path'] == null):?>
                                                        <img width="72" src="/../../images/best-action.jpg<?php echo $row['path']?>" alt="">
                                                    <?php endif; ?>
                                                    <div class="provider-desc">
                                                        <p>
                                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/suppliers/getSupplier/<?php echo $row['id_company'] ?>">
                                                                <?php echo $row['name']?>
                                                            </a>
                                                        </p>
                                                        <div class="provider-cats">
                                                            <?php if($row['short_description'] != null):?>
                                                                <p><?php echo $row['short_description']?></p>
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="provider-info">
                                                            <?php if($row['payment_methods'] != null):?>
                                                                <span class="small">Способы оплаты:</span>
                                                                <span class="small"><?php echo $row['payment_methods'] ?></span>
                                                            <?php endif; ?>
                                                            <br>
                                                            <?php if($row['min_amount'] != null):?>
                                                                <span class="small">Мин. сумма заказа:</span>
                                                                <span class="small"><?php echo $row['min_amount'].' '?></span>
                                                            <?php endif; ?>
                                                            <br>
                                                            <?php if($row['shipping'] != null):?>
                                                                <span class="small">Доставка:</span>
                                                                <span class="small"><?php if($row['shipping'] == '1') echo 'есть'; else echo 'нет'  ?></span>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div><!-- .best-actions -->
                            </div>
                            <div role="tabpanel" class="tab-pane" id="news">
                                <div class="best-actions">
                                    <div class="row">
                                        <?php //TODO новости ?>
                                        <?php foreach($news->result_array() as $row):?>
                                            <div class="col-md-6">
                                                <div class="best-action">
                                                    <?php if($row['path'] != null):?>
                                                        <img width="72" src="/../../images/mini/<?php echo $row['path']?>" alt="">
                                                    <?php endif; ?>
                                                    <?php if($row['path'] == null):?>
                                                        <img width="72" src="/../../images/best-action.jpg<?php echo $row['path']?>" alt="">
                                                    <?php endif; ?>
                                                    <div class="action-desc">
                                                        <p><?php echo $row['short_discription_new']?></p>
                                                        <p class="action-company">
                                                            <span><?php echo $row['name']?></span>
                                                            <span><?php echo $row['data_new']?></span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach;?>
                                    </div>
                                </div><!-- .best-actions -->
                            </div><!-- /.tab-content  -->
                            <div role="tabpanel" class="tab-pane" id="products">
                                <div class="best-providers">
                                    <div class="row">
                                        <?php foreach($products->result_array() as $row): ?>
                                            <div class="col-md-6">
                                                <div class="best-action">
                                                    <?php if($row['path'] != null): ?>
                                                        <img width="72" src="/../../images/mini/<?php echo $row['path'] ?>">
                                                    <?php endif;?>
                                                    <?php if($row['path'] == null): ?>
                                                        <img width="72" src="/../../images/best-action.jpg" alt="">
                                                    <?php endif; ?>
                                                    <div class="action-desc">
                                                        <p>
                                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/product/getProduct/<?php echo $row['id_product']?>/<?php echo $row['id_company'] ?>">
                                                                <?php echo $row['name_product']?>
                                                            </a>
                                                        </p>
                                                        <p class="action-company">
                                                            <span>
                                                                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/suppliers/getSupplier/<?php echo $row['id_company'] ?>">
                                                                    <?php echo $row['name']?>
                                                                </a>
                                                            </span>
                                                            <span>&nbsp;</span>

                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div><!-- .best-actions -->
                            </div>
                        </div>	<!-- /.tabs-wrapper-->
                    </div>
                </div>
            </div>
    </section>

    <section class="main-informer">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="main-summa">
                        <p>Сегодня на Zakup.asia совершили</p>
                        <span><?php echo rand(20, 40);?> заказов</span>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="main-subscribe">
                        <p>Хотите получать <span>выгодные предложения</span> и первыми узнавать о <span>новых акциях</span> и <span>скидках</span> лучших поставщиков? Подписывайтесь на рассылку!</p>
                        <form class="form-inline">
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Введите Ваш E-mail">
                            </div>
                            <div class="form-group">
                                <input type="tel" class="form-control" placeholder="Введите Ваш телефон">
                            </div>
                            <button type="submit" class="btn">Подписаться</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>




<script>
    function AllProducts(){
        var all = document.getElementById('All');
        all.setAttribute('href', '<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/' ?>index.php/Products/findProducts/?searchText=');
        all.innerHTML = '<span>Все товары</span>';
    }

    function AllCompanies(){
        var all = document.getElementById('All');
        all.setAttribute('href', '<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/' ?>index.php/Suppliers/getAllSuppliers');
        all.innerHTML = '<span>Все компании</span>';
    }

    function AllAction(){
        var all = document.getElementById('All');
        all.setAttribute('href', '#');
        all.innerHTML = '<span>Все акции</span>';
    }

    function AllNews(){
        var all = document.getElementById('All');
        all.setAttribute('href', '#');
        all.innerHTML = '<span>Все новости</span>';
    }

</script>