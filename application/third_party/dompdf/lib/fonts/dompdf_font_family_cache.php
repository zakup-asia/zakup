<?php return array (
  'sans-serif' => array(
    'normal' => DOMPDF_FONT_DIR . 'Helvetica',
    'bold' => DOMPDF_FONT_DIR . 'Helvetica-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Helvetica-BoldOblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
    'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
    'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
    'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
    'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => DOMPDF_FONT_DIR . 'Courier',
    'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => DOMPDF_FONT_DIR . 'Helvetica',
    'bold' => DOMPDF_FONT_DIR . 'Helvetica-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Helvetica-BoldOblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => DOMPDF_FONT_DIR . 'ZapfDingbats',
    'bold' => DOMPDF_FONT_DIR . 'ZapfDingbats',
    'italic' => DOMPDF_FONT_DIR . 'ZapfDingbats',
    'bold_italic' => DOMPDF_FONT_DIR . 'ZapfDingbats',
  ),
  'serif' => array(
    'normal' => DOMPDF_FONT_DIR . 'times',
    'bold' => DOMPDF_FONT_DIR . 'timesbd',
    'italic' => DOMPDF_FONT_DIR . 'timesi',
    'bold_italic' => DOMPDF_FONT_DIR . 'timesbi',
  ),
  'monospace' => array(
    'normal' => DOMPDF_FONT_DIR . 'Courier',
    'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => DOMPDF_FONT_DIR . 'Courier',
    'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
  ),
  'glyphicons halflings' => array(
    'normal' => DOMPDF_FONT_DIR . '8af77db7dc6bf362e3e5019f7bca758f',
  ),
  'ubuntu' => array(
    'normal' => DOMPDF_FONT_DIR . '5f3cf0b7176c4cb56b2b7c2e82d18f13',
  ),
  'ubuntu-medium' => array(
    'normal' => DOMPDF_FONT_DIR . 'eee26978677ea055a03d337a9b970dd2',
  ),
  'ubuntu-bold' => array(
    'normal' => DOMPDF_FONT_DIR . '90e3627f18a78314c21f02e2125f2439',
  ),
  'ubuntu-light' => array(
    'normal' => DOMPDF_FONT_DIR . '96f576a6805c7cc59e0033b040e41c30',
  ),
  'ubuntu-lightitalic' => array(
    'normal' => DOMPDF_FONT_DIR . '5ec67103fb521e96edd0242c5003299f',
  ),
  'ubuntu-italic' => array(
    'normal' => DOMPDF_FONT_DIR . 'd83611fd146aef83585d61757ac67efd',
  ),
) ?>