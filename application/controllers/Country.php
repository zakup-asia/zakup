<?php
class Country extends CI_Controller{

    private $server = null;

    public function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url', 'cookie'));
        $this->load->library('form_validation');
        if ($_SERVER['SERVER_PORT'] != 80){
            $this->server = $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'];
        } else {
            $this->server = $_SERVER['SERVER_NAME'];
        }
    }

    public function getAllCountry(){
        $sql = "SELECT * FROM country ORDER BY id_country";
        $country = $this->db->query($sql);
        $allCountry = array();
        $thisCountry = array();
        foreach($country ->result_array() as $notRow){
            $thisCountry['id_country'] = $notRow['id_country'];
            $thisCountry['name_country'] = $notRow['name_country'];
            $thisCountry['mask_country'] = $notRow['mask_country'];
            array_push($allCountry,$thisCountry);
        }
        print json_encode($allCountry);
    }

    //Получить все ригионы в данной стране
    public function getAllRegions($id_country = null){
        $sql = "SELECT * FROM region LEFT OUTER JOIN country USING (id_country) WHERE id_country = "
               .$this->db->escape($id_country)." ORDER BY id_region";
        $region = $this->db->query($sql);
        $allRegion = array();
        $thisRegion = array();
        foreach($region ->result_array() as $notRow){
            $thisRegion['id_region'] = $notRow['id_region'];
            $thisRegion['name_region'] = $notRow['name_region'];
            $thisRegion['mask_region'] = $notRow['mask_region'];
			$thisRegion['id_country'] = $notRow['id_country'];
            array_push($allRegion,$thisRegion);
        }
        print json_encode($allRegion);
    }

    //Получить все города в данном регионе
    public function getAllCity($id_region = null){
        $sql = "SELECT * FROM city WHERE id_region = ".$this->db->escape($id_region)." ORDER BY id_city";
        $city = $this->db->query($sql);
        $allcity = array();
        $thisCity = array();
        foreach($city ->result_array() as $notRow){
            $thisCity['id_city'] = $notRow['id_city'];
            $thisCity['name_city'] = $notRow['name_city'];
            $thisCity['mask_sity'] = $notRow['mask_sity'];
            array_push($allcity,$thisCity);
        }
        print json_encode($allcity);
    }

    //Получаем все города из определёной страны
    public function getAllCityFromCountry(){
        $sql = "SELECT * FROM country LEFT OUTER JOIN region USING (id_country) LEFT OUTER JOIN city USING (id_region) WHERE
               id_city is not NUll AND name_country = ".$this->db->escape($this->input->get('name_country'))." ORDER BY id_country";
        $city = $this->db->query($sql);
        $allCity = array();
        $thisCity = array();
        foreach($city->result_array() as $row){
            $thisCity['id_city'] = $row['id_city'];
            $thisCity['name_city'] = $row['name_city'];
            array_push($allCity, $thisCity);
        }
        print json_encode($allCity);
    }
	
	public function getCityName(){
		$isActiveSupplier = "";
		if($this->session->userdata('id_company') != null){
			$isActiveSupplier = ",(SELECT show_company FROM user LEFT OUTER JOIN company USING(id_user) WHERE goal = TRUE AND id_user = ".$this->db->escape($this->session->userdata('id_user')).") AS show_company";
		}
        if ($this->input->get('mask_city') == 'zakup.'){
            $sql = "SELECT *".$isActiveSupplier." FROM country WHERE name_country = 'Казахстан' ORDER BY name_country";
            $city = $this->db->query($sql);
            $allCity = array();
            $thisCity = array();
            foreach ($city->result_array() as $row){
                $thisCity['name_city'] = $row['name_country'];
				if($this->session->userdata('id_company') != null)
					$thisCity['show_company'] = $row['show_company'];
				array_push($allCity,$thisCity);
            }
            print json_encode($allCity);
        } else{
            $sql = "SELECT *".$isActiveSupplier." FROM city WHERE mask_sity =".$this->db->escape($this->input->get('mask_city'))." ORDER BY id_city";
            $city = $this->db->query($sql);
			$sql = "SELECT *".$isActiveSupplier." FROM region WHERE mask_region = ".$this->db->escape($this->input->get('mask_city'))." ORDER BY id_region";
            $region = $this->db->query($sql);
			$allCity = array();
            $thisCity = array();
            foreach ($city->result_array() as $row){
				if ($row['name_city'] != null) {
					$thisCity['name_city'] = $row['name_city'];
					$thisCity['id_region'] = $row['id_region'];
					if($this->session->userdata('id_company') != null)
						$thisCity['show_company'] = $row['show_company'];
					array_push($allCity,$thisCity);
				}
            }
			foreach ($region->result_array() as $row){
				if ($row['name_region'] != null) {
					$thisCity['name_city'] = $row['name_region'];
					$thisCity['id_region'] = null;
					if($this->session->userdata('id_company') != null)
						$thisCity['show_company'] = $row['show_company'];
					array_push($allCity,$thisCity);
				}
            }
            print json_encode($allCity);
        }
    }
	
	public function compare(){
        $sql = "SELECT COUNT(*) AS count, name_city, mask_sity FROM city WHERE name_city =".$this->db->escape($this->input->get('city'));
        $compare = $this->db->query($sql);
        foreach($compare->result_array() as $row){
            if ($row['count'] != '0'){
                echo $row['mask_sity'];
            }
        }
    }
	
}