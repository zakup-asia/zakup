<?php
class AdminPanel extends CI_Controller{

    private $server = null;

    //TODO один суперадмин, который может всё и возможность создавать админов, похоже будет сложная система
    //Сегодня работа с админкой, её отображением и т.д.
    //Ещё нужны функции логина и логаута
    //TODO проверка админ ли это или нет
    public function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url', 'cookie'));
        $this->load->library('form_validation');
        if ($_SERVER['SERVER_PORT'] != 80){
            $this->server = $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'];
        } else {
            $this->server = $_SERVER['SERVER_NAME'];
        }
    }

    //Тут мы будем выводить все возможные действия для нашего админа
    public function index($searchText = null){
        $cookie = get_cookie('id');
        if ($cookie == null){
            $cookie = array(
                'name'   => 'id',
                'value'  => $this->session->userdata('__ci_last_regenerate'),
                'expire' => '604800'
            );
            set_cookie($cookie);
        } else {
            //Вот тут обновляем.
        }
        $hide = 'hide';
        $this->load->view('header');
        $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
        $this->load->view('admin/default');
        $this->load->view('footer');
    }

    //Выбрать все секции
    public function getAllSection($searchText = null){
        $hide = 'hide';
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null){
            $sql = "SELECT * FROM section_one";
            $section_one = $this->db->query($sql);
            $this->load->view('header');
            $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
            $this->load->view('admin/sections', array('section_one' => $section_one));
            $this->load->view('footer');
        } else echo 'Залогиньтесь';
    }
	
    //Выбрать все секции
    public function changeOrder($searchText = null){
        $hide = 'hide';
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null){
            $this->load->view('header');
            $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
            $this->load->view('admin/order', array());
            $this->load->view('footer');
        } else echo 'Залогиньтесь';
    }	

    //Добавить секцию
    public function addSection($searchText = null){
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $name1 = $this->translit($this->input->post('name_section'));
            $sql = "INSERT INTO `section_one`(`name_section`, `name_section_translit`) VALUES (".$this->db->escape($this->input->post('name_section')).",".$this->db->escape($name1).")";
            $section_one = $this->db->query($sql);
            redirect('http://'.$this->server.'/AdminPanel/getAllSection', 'refresh');
        } else echo 'Залогиньтесь';
    }

    //Редактировать секцию
    public function editSection($id_section_one = null, $searchText = null){
        $hide = 'hide';
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "SELECT * FROM section_one WHERE id_section_one = ".$this->db->escape($id_section_one);
            $section = $this->db->query($sql);
            $this->form_validation->set_rules('name_section', 'название категории', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header');
                $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
                $this->load->view('admin/edit_section', array('id_section' => $id_section_one, 'section' => $section));
                $this->load->view('footer');
            } else {
                $name1 = $this->translit($this->input->post('name_section'));
                $sql = "UPDATE `section_one` SET `name_section` = " . $this->db->escape($this->input->post('name_section')) .
                       ", `name_section_translit` = ".$this->db->escape($name1)." WHERE `id_section_one` = ".$this->db->escape($this->input->post('id_section_one'));
                $this->db->query($sql);
                redirect('http://'.$this->server.'/AdminPanel/getAllSection', 'refresh');
            }
        } else echo 'Залогиньтесь';
    }

    //Удалить секцию
    public function deleteSection($id_section_one){
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "DELETE FROM `section_one`  WHERE `id_section_one` =".$this->db->escape($id_section_one);
            $this->db->query($sql);
            redirect('http://'.$this->server.'/AdminPanel/getAllSection', 'refresh');
        } else echo 'Залогиньтесь';
    }

    //Выбрать все категории
    public function getAllCategory($searchText = null){
        $hide = 'hide';
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "SELECT * FROM section_one";
            $section_one = $this->db->query($sql);
            $sql = "SELECT * FROM category LEFT OUTER JOIN section_one USING (id_section_one)";
            $category = $this->db->query($sql);
            $this->load->view('header');
            $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
            $this->load->view('admin/categories', array('categories' => $category, 'section' => $section_one));
            $this->load->view('footer');
        } else echo 'Залогиньтесь';
    }

    //Добавить категорию
    public function addCategory(){
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $name = $this->translit($this->input->post('name_category'));
            $sql = "INSERT INTO `category`(`name_category`, `id_section_one`, `name_category_translit`) VALUES ("
                   .$this->db->escape($this->input->post('name_category')).",".$this->db->escape($this->input->post('id_section_one'))
                   .",".$this->db->escape($name).")";
            $this->db->query($sql);
            redirect('http://'.$this->server.'/AdminPanel/getAllCategory', 'refresh');
        } else echo 'Залогиньтесь';
    }

    //Добовление категории из секции
    public function addCategoryForS($id_section_one = null){
        $hide = 'hide';
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $this->form_validation->set_rules('name_category', 'название категории', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header');
                $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
                $this->load->view('admin/addCategory', array('id_section' => $id_section_one));
                $this->load->view('footer');
            } else {
                $name = $this->translit($this->input->post('name_category'));
                $sql = "INSERT INTO `category`(`name_category`, `id_section_one`, `name_category_translit`) VALUES ("
                       .$this->db->escape($this->input->post('name_category')).","
                       .$this->db->escape($this->input->post('id_section')).",".
                       $this->db->escape($name).")";
                $this->db->query($sql);
                redirect('http://'.$this->server.'/AdminPanel/getAllCategory', 'refresh');
            }
        }
    }

    //Редактировать категорию
    public function editCategory($id_category = null){
        $hide = 'hide';
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "SELECT * FROM category WHERE id_category = ".$this->db->escape($id_category);
            $category = $this->db->query($sql);
            $this->form_validation->set_rules('name_category', 'название категории', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header');
                $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
                $this->load->view('admin/edit_category', array('id_category' => $id_category, 'category' => $category));
                $this->load->view('footer');
            } else {
                $name = $this->translit($this->input->post('name_category'));
                $sql = "UPDATE `category` SET `name_category` = " . $this->db->escape($this->input->post('name_category')) .
                       ", `name_category_translit` = ".$this->db->escape($name).
                       " WHERE `id_category` = " . $this->db->escape($this->input->post('id_category'));
                $this->db->query($sql);
                redirect('http://'.$this->server.'/AdminPanel/getAllCategory', 'refresh');
            }
        } else echo 'Залогиньтесь';
    }

    //Удалить категорию
    public function deleteCategory($id_category){
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "DELETE FROM `category` WHERE `id_category` = ".$this->db->escape($id_category);
            $this->db->query($sql);
            redirect('http://'.$this->server.'/AdminPanel/getAllCategory', 'refresh');
        } else echo 'Залогиньтесь';
    }

    //Выбрать все подкатегории
    public function getAllSubcategory(){
        $hide = 'hide';
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "SELECT * FROM category";
            $category = $this->db->query($sql);
            $sql = "SELECT * FROM subcategory LEFT OUTER JOIN category USING (id_category)";
            $subcategory = $this->db->query($sql);
            $this->load->view('header');
            $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
            $this->load->view('admin/subcategory', array('subcategory' => $subcategory, 'category'=> $category));
            $this->load->view('footer');
        } else echo 'Залогиньтесь';
    }

    //Добавить подкатегорию
    public function addSubcategory(){
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $name = $this->translit($this->input->post('name_subcategory'));
            $sql = "INSERT INTO `subcategory`(`name_subcategory`, `id_category`, `name_subcategory_translit`) VALUES ("
                   .$this->db->escape($this->input->post('name_subcategory')).",".
                   $this->db->escape($this->input->post('id_category')).",".
                   $this->db->escape($name).")";
            $this->db->query($sql);
            redirect('http://'.$this->server.'/AdminPanel/getAllSubcategory', 'refresh');
        } else echo 'Залогиньтесь';
    }

    //Добавление подкатегории из категории
    public function addSubcategoryForC($id_category = null){
        $hide = 'hide';
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $this->form_validation->set_rules('name_subcategory', 'название категории', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header');
                $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
                $this->load->view('admin/addSubcategory', array('id_category' => $id_category));
                $this->load->view('footer');
            } else {
                $name = $this->translit($this->input->post('name_subcategory'));
                $sql = "INSERT INTO `subcategory`(`name_subcategory`, `id_category`, `name_subcategory_translit`) VALUES (".
                       $this->db->escape($this->input->post('name_subcategory')).","
                       .$this->db->escape($this->input->post('id_category')).",".$this->db->escape($name).")";
                $this->db->query($sql);
                redirect('http://'.$this->server.'/AdminPanel/getAllSubcategory', 'refresh');
            }
        }
    }

    //Редактировать подкатегорию
    public function editSubcategory($id_subcategory = null){
        $hide = 'hide';
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "SELECT * FROM subcategory WHERE id_subcategory = ".$this->db->escape($id_subcategory);
            $subcategory = $this->db->query($sql);
            $this->form_validation->set_rules('name_subcategory', 'название категории', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header');
                $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
                $this->load->view('admin/edit_subcategory', array('id_subcategory' => $id_subcategory, 'subcategory' => $subcategory));
                $this->load->view('footer');
            } else {
                $name = $this->translit($this->input->post('name_subcategory'));
                $sql = "UPDATE `subcategory` SET `name_subcategory` = ".$this->db->escape($this->input->post('name_subcategory')).
                       ", `name_subcategory_translit` = ".$this->db->escape($name).
                       " WHERE `id_subcategory` = " . $this->db->escape($this->input->post('id_subcategory'));
                $this->db->query($sql);
                redirect('http://'.$this->server.'/AdminPanel/getAllSubcategory', 'refresh');
            }
        } else echo 'Залогиньтесь';
    }

    //Удалить подкатегорию
    public function deleteSubcategory($id_subcategory){
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "DELETE FROM `subcategory` WHERE `id_subcategory` = ".$this->db->escape($id_subcategory);
            $this->db->query($sql);
            redirect('http://'.$this->server.'/AdminPanel/getAllSubcategory', 'refresh');
        } else echo 'Залогиньтесь';
    }

    //Выбрать все страны
    public function getAllCountry(){
        $hide = 'hide';
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "SELECT * FROM country LEFT OUTER JOIN currency USING(id_currency)";
            $countries = $this->db->query($sql);
            $sql = "SELECT * FROM currency";
            $currency = $this->db->query($sql);
            $this->load->view('header', array('keywords' => null, 'description' => null));
            $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
            $this->load->view('admin/countries', array('countries' => $countries, 'currency' => $currency));
            $this->load->view('footer');
        } else echo 'Залогиньтесь';
    }

    //Добавить страну и пустые слайдеры к стране.
    public function addCountry($name_country = null, $id_currency = null){
        if($this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null){
            $sql = "INSERT INTO `country`(`name_country`, `id_currency`, `mask_country`) VALUES (".
                   $this->db->escape($this->input->post('name_country')).", ".$this->db->escape($this->input->post('id_currency')).
                   ", ".$this->db->escape($this->input->post('mask_country')).")";
            $this->db->query($sql);
            $sql = "INSERT INTO `sliders` (`id_image`, `name_slide`, `id_country`) VALUES
                   (NULL, 'Первый слайд', (SELECT id_country FROM country WHERE name_country = ".$this->db->escape($this->input->post('name_country')).")),
                   (NULL, 'Второй слайд', (SELECT id_country FROM country WHERE name_country = ".$this->db->escape($this->input->post('name_country')).")),
                   (NULL, 'Третий слайд', (SELECT id_country FROM country WHERE name_country = ".$this->db->escape($this->input->post('name_country')).")),
                   (NULL, 'Четвёртый слайд', (SELECT id_country FROM country WHERE name_country = ".$this->db->escape($this->input->post('name_country')).")),
                   (NULL, 'Пятый слайд', (SELECT id_country FROM country WHERE name_country = ".$this->db->escape($this->input->post('name_country')).")),                    (NULL, 'Шестой слайд', (SELECT id_country FROM country WHERE name_country = ".$this->db->escape($this->input->post('name_country'))."));";
            $this->db->query($sql);
            redirect('http://'.$this->server.'/AdminPanel/getAllCountry', 'refresh');
        } else echo 'Залогиньтесь';
    }

    //Редактировать страну
    public function editCountry($id_country = null){
        $hide = 'hide';
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "SELECT * FROM currency";
            $currency = $this->db->query($sql);
            $sql = "SELECT * FROM country WHERE id_country = ".$this->db->escape($id_country);
            $country = $this->db->query($sql);
            $this->form_validation->set_rules('name_country', 'название страны', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header', array('keywords' => null, 'description' => null));
                $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
                $this->load->view('admin/edit_country', array('id_country' => $id_country, 'country' => $country, 'currency' => $currency));
                $this->load->view('footer');
            } else {
                $sql = "UPDATE `country` SET `name_country` = ".$this->db->escape($this->input->post('name_country')).
                       ",`id_currency` = ".$this->db->escape($this->input->post('id_currency')).
                       ", `mask_country` = ".$this->db->escape($this->input->post('mask_country'))." WHERE `id_country` = "
                       .$this->db->escape($this->input->post('id_country'));
                $this->db->query($sql);
                redirect('http://'.$this->server.'/AdminPanel/getAllCountry', 'refresh');
            }
        } else echo 'Залогиньтесь';
    }

    //Удалить страну
    public function deleteCountry($id_country){
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "DELETE FROM `country` WHERE `id_country` = ".$this->db->escape($id_country);
            $this->db->query($sql);
            redirect('http://'.$this->server.'/AdminPanel/getAllCountry', 'refresh');
        } else echo 'Залогиньтесь';
    }

    //Выбрать все регион
    public function getAllRegion(){
        $hide = 'hide';
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "SELECT * FROM country";
            $country = $this->db->query($sql);
            $sql = "SELECT * FROM region LEFT OUTER JOIN country USING (id_country)";
            $region = $this->db->query($sql);
            $this->load->view('header', array('keywords' => null, 'description' => null));
            $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
            $this->load->view('admin/regions', array('region' => $region, 'country' => $country));
            $this->load->view('footer');
        } else echo 'Залогиньтесь';
    }

    //Добавить регион, и пустые слайдеры для региона.
    public function addRegion(){
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "INSERT INTO `region`(`name_region`, `id_country`, `mask_region`) VALUES (".$this->db->escape($this->input->post('name_region')).
                   ",".$this->db->escape($this->input->post('id_country')).", ".$this->db->escape($this->input->post('mask_region')).")";
            $this->db->query($sql);
            $sql = "INSERT INTO `sliders` (`id_image`, `name_slide`, `id_region`) VALUES
                   (NULL, 'Первый слайд', (SELECT id_region FROM region WHERE name_region = ".$this->db->escape($this->input->post('name_region')).")),
                   (NULL, 'Второй слайд', (SELECT id_region FROM region WHERE name_region = ".$this->db->escape($this->input->post('name_region')).")),
                   (NULL, 'Третий слайд', (SELECT id_region FROM region WHERE name_region = ".$this->db->escape($this->input->post('name_region')).")),
                   (NULL, 'Четвёртый слайд', (SELECT id_region FROM region WHERE name_region = ".$this->db->escape($this->input->post('name_region')).")),
                   (NULL, 'Пятый слайд', (SELECT id_region FROM region WHERE name_region = ".$this->db->escape($this->input->post('name_region')).")),
                   (NULL, 'Шестой слайд', (SELECT id_region FROM region WHERE name_region = ".$this->db->escape($this->input->post('name_region'))."));";
            $this->db->query($sql);
            redirect('http://'.$this->server.'/AdminPanel/getAllRegion', 'refresh');
        } else echo 'Залогиньтесь';
    }

    //Редактировать регион
    public function editRegion($id_region = null){
        $hide = 'hide';
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "SELECT * FROM country";
            $country = $this->db->query($sql);
            $sql = "SELECT * FROM region WHERE id_region = ".$this->db->escape($id_region);
            $region = $this->db->query($sql);
            $this->form_validation->set_rules('name_region', 'название региона', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header', array('keywords' => null, 'description' => null));
                $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
                $this->load->view('admin/edit_region', array('id_region' => $id_region, 'country' => $country, 'region' => $region));
                $this->load->view('footer');
            } else {
                $sql = "UPDATE `region` SET `name_region` = " . $this->db->escape($this->input->post('name_region')) .
                       ",`id_country` = " . $this->db->escape($this->input->post('id_country')) .
                       ", `mask_region` = ".$this->db->escape($this->input->post('mask_region')).
                       " WHERE `id_region` = " . $this->db->escape($this->input->post('id_region'));
                $this->db->query($sql);
                redirect('http://'.$this->server.'/AdminPanel/getAllRegion', 'refresh');
            }
        } else echo 'Залогиньтесь';
    }

    //Удалить регион
    public function deleteRegion($id_region){
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "DELETE FROM `region` WHERE `id_region` = ".$this->db->escape($id_region);
            $this->db->query($sql);
            redirect('http://'.$this->server.'/AdminPanel/getAllRegion', 'refresh');
        } else echo 'Залогиньтесь';
    }

    //Выбрать все города
    public function getAllCity(){
        $hide = 'hide';
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "SELECT * FROM city LEFT OUTER JOIN region USING(id_region) LEFT OUTER JOIN country USING(id_country)";
            $cities = $this->db->query($sql);
            $sql = "SELECT * FROM country";
            $country = $this->db->query($sql);
            $sql = "SELECT * FROM region";
            $region = $this->db->query($sql);
            $this->load->view('header', array('keywords' => null, 'description' => null));
            $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
            $this->load->view('admin/cities', array('cities' => $cities, 'country' => $country, 'region' => $region));
            $this->load->view('footer');
        } else echo 'Залогиньтесь';
    }

    //Добавить город, и пустые слайдеры для города.
    public function addCity(){
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "INSERT INTO `city`(`name_city`, `id_region`, `mask_sity`) VALUES (".$this->db->escape($this->input->post('name_city')).",".
                   $this->db->escape($this->input->post('id_region')).", ".$this->db->escape($this->input->post('mask_sity')).")";
            $this->db->query($sql);
            $sql = "INSERT INTO `sliders` (`id_image`, `name_slide`, `id_city`) VALUES
                   (NULL, 'Первый слайд', (SELECT id_region FROM city WHERE name_city = ".$this->db->escape($this->input->post('name_city')).")),
                   (NULL, 'Второй слайд', (SELECT id_region FROM city WHERE name_city = ".$this->db->escape($this->input->post('name_city')).")),
                   (NULL, 'Третий слайд', (SELECT id_region FROM city WHERE name_city = ".$this->db->escape($this->input->post('name_city')).")),
                   (NULL, 'Четвёртый слайд', (SELECT id_region FROM city WHERE name_city = ".$this->db->escape($this->input->post('name_city')).")),
                   (NULL, 'Пятый слайд', (SELECT id_region FROM city WHERE name_city = ".$this->db->escape($this->input->post('name_city')).")),
                   (NULL, 'Шестой слайд', (SELECT id_region FROM city WHERE name_city = ".$this->db->escape($this->input->post('name_city'))."));";
            $this->db->query($sql);
            redirect('http://'.$this->server.'/AdminPanel/getAllCity', 'refresh');
        } else echo 'Залогиньтесь';
    }

    //Редактировать город
    public function editCity($id_city = null){
            $hide = 'hide';
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "SELECT * FROM country";
            $country = $this->db->query($sql);
            $sql = "SELECT * FROM region";
            $region = $this->db->query($sql);
            $sql = "SELECT * FROM city LEFT OUTER JOIN region USING (id_region) LEFT OUTER JOIN country USING (id_country) WHERE id_city = ".$this->db->escape($id_city);
            $city = $this->db->query($sql);
            $this->form_validation->set_rules('name_city', 'название города', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header', array('keywords' => null, 'description' => null));
                $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
                $this->load->view('admin/edit_city', array('id_city' => $id_city, 'country' => $country, 'region' => $region, 'city' => $city));
                $this->load->view('footer');
            } else {
                $sql = "UPDATE `city` SET `name_city` = " . $this->db->escape($this->input->post('name_city')) .
                    ",`id_region`= " . $this->db->escape($this->input->post('id_region')).
                    ", `mask_sity` =".$this->db->escape($this->input->post('mask_sity')).
                    " WHERE `id_city` = " . $this->db->escape($this->input->post('id_city'));
                if (!$this->db->query($sql)) {
                    echo 'Error database';
                }
                redirect('http://'.$this->server.'/AdminPanel/getAllCity', 'refresh');
            }
        } else echo 'Залогиньтесь';
    }

    //Удалить город
    public function deleteCity($id_city){
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "DELETE FROM `city` WHERE `id_city` = ".$this->db->escape($id_city);
            if (!$this->db->query($sql)) {
                echo 'Error database';
            }
            redirect('http://'.$this->server.'/AdminPanel/getAllCity', 'refresh');
        } else echo 'Залогиньтесь';
    }

    // Получить все валюты
    public function getCurrency(){
        $hide = 'hide';
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "SELECT * FROM currency";
            $currency = $this->db->query($sql);
            $this->load->view('header', array('keywords' => null, 'description' => null));
            $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
            $this->load->view('admin/currency', array('currency' => $currency));
            $this->load->view('footer');
        } else echo 'Залогиньтесь';
    }

    // Добавить валюту
    public function addCurrency(){
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "INSERT INTO `currency`(`name_currency`, `icon_currency`) VALUES (".$this->db->escape($this->input->post('name_currency')).",".
                   $this->db->escape($this->input->post('icon_currency')).")";
            if (!$this->db->query($sql)) {
                echo 'Error database';
            }
            redirect('http://'.$this->server.'/AdminPanel/getCurrency', 'refresh');
        } else echo 'Залогиньтесь';
    }

    //Добавление валюты, если её нет, хотя мы должны сначала создать валюту, а затем уже страну. Да и страну сейчас без валюты нереальносоздать
    public function addCurrencyFromC($id_country = null){
        $hide = 'hide';
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $this->form_validation->set_rules('name_subcategory', 'название валюты', 'required');
            $this->form_validation->set_rules('name_subcategory', 'короткое название', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header', array('keywords' => null, 'description' => null));
                $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
                $this->load->view('admin/addCurrency', array('id_category' => $id_country));
                $this->load->view('footer');
            } else {
                $sql = "INSERT INTO `currency`(`name_currency`, `icon_currency`) VALUES (".$this->db->escape($this->input->post('name_currency')).",".
                       $this->db->escape($this->input->post('icon_currency')).")";
                $this->db->query($sql);
                $sql = "UPDATE country SET id_currency = (SELECT id_currency FROM currency WHERE name_currency = ".
                       $this->db->escape($this->input->post('name_currency')).") WHERE id_country = ".
                       $this->db->escape($this->input->post('id_country'));
                $this->db->query($sql);
                redirect('http://'.$this->server.'/AdminPanel/getAllSubcategory', 'refresh');
            }
        }
    }

    // Обовить валюту
    public function updateCurrency($id_currency = null){
        $hide = 'hide';
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "SELECT * FROM currency WHERE id_currency = ".$this->db->escape($id_currency);
            $currency = $this->db->query($sql);
            $this->form_validation->set_rules('name_currency', 'название валюты', 'required');
            $this->form_validation->set_rules('icon_currency', 'короткое название', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header', array('keywords' => null, 'description' => null));
                $this->load->view('headerMenu', array('hide' => 'hide', 'searchText' => null));
                $this->load->view('admin/edit_currency', array('id_currency' => $id_currency, 'currency' => $currency));
                $this->load->view('footer');
            } else {
                $sql = "UPDATE `currency` SET `name_currency` = ".$this->db->escape($this->input->post('name_currency')).
                       ",`icon_currency` = ".$this->db->escape($this->input->post('icon_currency'))." WHERE `id_currency` = "
                       .$this->db->escape($this->input->post('id_currency'));
                if (!$this->db->query($sql)) {
                    echo 'Error database';
                }
                redirect('http://'.$this->server.'/AdminPanel/getCurrency', 'refresh');
            }
        } else echo 'Залогиньтесь';
    }

    // Удалить ввалюту
    public function deleteCurrency($id_currency){
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "DELETE FROM `currency` WHERE `id_currency` = ".$this->db->escape($id_currency);
            $this->db->query($sql);
            redirect('http://'.$this->server.'/AdminPanel/getCurrency', 'refresh');
        } else echo 'Залогиньтесь';
    }

    public function addSliders(){
        if ($this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null){
            $id_region = null;
            $id_city = null;
            $id_country = null;
            if($this->input->post('id_city') != null){
                $id_city = $this->input->post('id_city');
            }
            if($this->input->post('id_region') != null){
                $id_region = $this->input->post('id_region');
            }
            if($this->input->post('id_country') != null){
                $id_country = $this->input->post('id_country');
            }
            $sql = "INSERT INTO `sliders` (`id_image`, `name_slide`, `id_city`, `id_region`, `id_country`) VALUES
                   (NULL, 'Первый слайд', ".$this->db->escape($id_city).", ".$this->db->escape($id_region)." , ".$this->db->escape($id_country)."),
                   (NULL, 'Второй слайд', ".$this->db->escape($id_city).", ".$this->db->escape($id_region)." , ".$this->db->escape($id_country)."),
                   (NULL, 'Третий слайд', ".$this->db->escape($id_city).", ".$this->db->escape($id_region)." , ".$this->db->escape($id_country)."),
                   (NULL, 'Четвёртый слайд', ".$this->db->escape($id_city).", ".$this->db->escape($id_region)." , ".$this->db->escape($id_country)."),
                   (NULL, 'Пятый слайд', ".$this->db->escape($id_city).", ".$this->db->escape($id_region)." , ".$this->db->escape($id_country)."),
                   (NULL, 'Шестой слайд', ".$this->db->escape($id_city).", ".$this->db->escape($id_region)." , ".$this->db->escape($id_country).");";
            $this->db->query($sql);
            redirect('http://'.$this->server.'/AdminPanel/getSliders', 'refresh');
        }
    }

    public function deleteSliders(){
        if ($this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null){
            if($this->input->post('id_city') != null) $sql = "DELETE FROM `sliders` WHERE id_city = ".$this->input->post('id_city');
            if($this->input->post('id_region') != null) $sql = "DELETE FROM `sliders` WHERE id_region = ".$this->input->post('id_region');
            if($this->input->post('id_country') != null) $sql = "DELETE FROM `sliders` WHERE id_country = ".$this->input->post('id_country');
            $this->db->query($sql);
            redirect('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/getSliders', 'refresh');
        }
    }

    //Получаем все места для слайдов. На главной странице тоже будут получать какие слайды у нас есть.
    public function getSliders($hide = null){
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "SELECT sliders.id_slide, sliders.id_country, sliders.id_city, sliders.id_image, sliders.name_slide, images.path, city.name_city, city.mask_sity, country.name_country, country.id_currency, country.mask_country, sliders.id_region, region.name_region FROM sliders LEFT OUTER JOIN country USING (id_country) LEFT OUTER JOIN images USING (id_image) LEFT OUTER JOIN region USING (id_region) LEFT OUTER JOIN city USING (id_city)";
            $sliders = $this->db->query($sql);
            $sql = "SELECT * FROM city ORDER BY id_city";
            $city = $this->db->query($sql);
            $sql = "SELECT * FROM region ORDER BY id_region";
            $region = $this->db->query($sql);
            $sql = "SELECT * FROM country ORDER BY id_country";
            $country = $this->db->query($sql);
            $this->load->view('header', array('keywords' => null, 'description' => null));
            $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
            $this->load->view('admin/sliders', array('sliders' => $sliders, 'city' => $city, 'region' => $region, 'country' => $country));
            $this->load->view('footer');
        } else echo 'Залогиньтесь';
    }

    //Установить слайдер 1
    public function updateSlider(){
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "SELECT * FROM sliders LEFT OUTER JOIN images USING(id_image) WHERE id_slide = ".$this->db->escape($this->input->post('id_slide'));
            $slide = $this->db->query($sql);
            $sql = "UPDATE `sliders` SET `id_image` = (SELECT id_image FROM images WHERE path = "
                   .$this->db->escape($this->input->post('path')).") WHERE `id_slide` = ".$this->db->escape($this->input->post('id_slide'));
            $this->db->query($sql);
            foreach($slide->result_array() as $row){
                if($row['path'] != $this->input->post('path')){
                    $filename = $_SERVER['DOCUMENT_ROOT'] . '/images/mini/' . $row['path'];
                    @unlink($filename);
                    $filename = $_SERVER['DOCUMENT_ROOT'] . '/images/upload/' . $row['path'];
                    @unlink($filename);
                    $sql = "DELETE FROM `images` WHERE id_image = ".$this->db->escape($row['id_image']);;
                    $this->db->query($sql);
                }
            }
            redirect('http://'.$this->server.'/AdminPanel/getSliders', 'refresh');
        } else echo 'Залогиньтесь';
    }

    //Получить все компании
    public function getCompanies($hide = null){
        if( $this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) {
            $sql = "SELECT * FROM company LEFT OUTER JOIN `user` USING (id_user) LEFT OUTER JOIN contact_information USING (id_company)
                   LEFT OUTER JOIN images USING(id_image) LEFT OUTER JOIN city USING (id_city)WHERE goal = TRUE";
            $companies = $this->db->query($sql);
            $this->load->view('header', array('keywords' => null, 'description' => null));
            $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
            $this->load->view('admin/companies', array('companies' => $companies));
            $this->load->view('footer');
        }
    }

    //Зайти в админ панель
    public function loginAdmin(){
        if($this->input->post('nickname') && $this->input->post('password')){
            $nickname = $this->input->post('nickname');
            $password = $this->input->post('password');
            $admin_nickname = '123';
            $admin_password = '123';
            if($nickname == $admin_nickname && $password == $admin_password){
                $newdata = array('admin' => 'true');
                $this->session->set_userdata($newdata);
                redirect('http://'.$this->server.'/AdminPanel/', 'refresh');
            }
        } else echo ' Не ввели данные или ошиблись';
    }

    //Выйти из панели
    public function logoutAdmin(){
        $this->session->unset_userdata('admin');
        redirect('http://'.$this->server.'/AdminPanel/', 'refresh');
    }

    //Получаем все поступившие нам заказы
    public function getAllOrders(){
        $sql = "SELECT * FROM cart LEFT OUTER JOIN orders USING (user) LEFT OUTER JOIN company USING (id_company) WHERE cart.id_order = orders.id_order  AND  cart.id_order is not NULL AND (order_status =
               'Просмотрен' OR order_status = 'Не просмотрен' OR order_status = 'Принят') GROUP BY orders.id_order ORDER BY
               order_date DESC";
        $orders = $this->db->query($sql);
        $hide = null;
        $this->load->view('header', array('keywords' => null, 'description' => null));
        $this->load->view('headerMenu', array('hide' => $hide, 'searchText' => null));
        $this->load->view('admin/orders', array('orders' => $orders));
        $this->load->view('footer');
    }

    //Получаем всю историю заказов
    public function getOrdersHistory(){
        $sql = "SELECT * FROM cart LEFT OUTER JOIN orders USING (user) LEFT OUTER JOIN company USING (id_company) WHERE cart.id_order = orders.id_order  AND  cart.id_order is not NULL AND (order_status =
               'Доставлен' OR order_status = 'Отказан') GROUP BY orders.id_order ORDER BY
               order_date DESC";
        $orders = $this->db->query($sql);
        $hide = null;
        $this->load->view('header', array('keywords' => null, 'description' => null));
        $this->load->view('headerMenu', array('hide' => $hide,'searchText' => null));
        $this->load->view('admin/ordersHistory', array('orders' => $orders));
        $this->load->view('footer');
    }
	
    public function setTest(){
        $sql = "UPDATE `company` SET `is_test`= TRUE WHERE `id_company`=".$this->db->escape($this->input->get('id_company'));
        $this->db->query($sql);
        redirect('http://'.$this->server.'/AdminPanel/getCompanies', 'refresh');
    }

    public function unsetTest(){
        $sql = "UPDATE `company` SET `is_test`= FALSE WHERE `id_company`=".$this->db->escape($this->input->get('id_company'));
        $this->db->query($sql);
        redirect('http://'.$this->server.'/AdminPanel/getCompanies', 'refresh');
    }
	
	public function randomZakaz(){
		$sql = "UPDATE `random` SET `random`= ".$this->db->escape(rand(20, 40));
        $this->db->query($sql);
		redirect('http://'.$this->server.'/AdminPanel/', 'refresh');
	}

    //2 варианта решения
    //1 ты делаешь транслит и потом обратно переводишь в запросе
    //2 при заполнении товара ты сохраняешь отдельные данные для транслита

    //Очевидны плюсы второго варианта, ведь тогда тебе не нужно постоянно вызывать эту функцию и тратить ресурсы своего сервера.
    //она будет вызвана только один раз.

    function translit($s) {
        $s = (string) $s; // преобразуем в строковое значение
        $s = strip_tags($s); // убираем HTML-теги
        $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
        $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
        $s = trim($s); // убираем пробелы в начале и конце строки
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
        $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
        $s = str_replace(" ", "-", $s); // заменяем пробелы знаком минус
        return $s; // возвращаем результат
    }

    //TODO потом закрыть или удалить эти методы.
    //Добоавляем имена на транслите для секций
    private function forSection(){
        $sql = "SELECT * FROM section_one";
        $suppliers = $this->db->query($sql);
        foreach ($suppliers->result_array() as $row){
            if ($row['name_section'] != null){
                $name1 = $this->translit($row['name_section']);
                $sql = "UPDATE `section_one` SET `name_section_translit`=".$this->db->escape($name1).
                       " WHERE `id_section_one`=".$this->db->escape($row['id_section_one']);
                $this->db->query($sql);
            }
        }
    }

    //Добоавляем имена на транслите для категорий
    private function forCategory(){
        $sql = "SELECT * FROM category";
        $suppliers = $this->db->query($sql);
        foreach ($suppliers->result_array() as $row){
            if ($row['name_category'] != null ){
                $name1 = $this->translit($row['name_category']);
                $sql = "UPDATE `category` SET `name_category_translit`=".$this->db->escape($name1).
                       " WHERE `id_category`=".$this->db->escape($row['id_category']);
                $this->db->query($sql);
            }
        }
    }

    //Добоавляем имена на транслите для субкатегорий
    private function forSubcategory(){
        $sql = "SELECT * FROM subcategory";
        $suppliers = $this->db->query($sql);
        foreach ($suppliers->result_array() as $row){
            if ($row['name_subcategory'] != null){
                $name1 = $this->translit($row['name_subcategory']);
                $sql = "UPDATE `subcategory` SET `name_subcategory_translit`=".$this->db->escape($name1).
                       " WHERE `id_subcategory`=".$this->db->escape($row['id_subcategory']);
                $this->db->query($sql);
            }
        }
    }

    //Создаём слайдеры для регионов
    private function forSliderRegion(){
        $sql = "SELECT * FROM region LEFT OUTER JOIN sliders USING (id_region)";
        $region = $this->db->query($sql);
        foreach ($region->result_array() as $row){
            if ($row['id_slide'] == null){
                $sql = "INSERT INTO `sliders` (`id_image`, `name_slide`, `id_region`) VALUES
                       (NULL, 'Первый слайд', ".$this->db->escape($row['id_region'])."),
                       (NULL, 'Второй слайд', ".$this->db->escape($row['id_region'])."),
                       (NULL, 'Третий слайд', ".$this->db->escape($row['id_region'])."),
                       (NULL, 'Четвёртый слайд', ".$this->db->escape($row['id_region'])."),
                       (NULL, 'Пятый слайд', ".$this->db->escape($row['id_region'])."),
                       (NULL, 'Шестой слайд', ".$this->db->escape($row['id_region']).");";
                $this->db->query($sql);
            }
        }
    }

    //Создаём слайдеры для городов
    private function forSliderCity(){
        $sql = "SELECT * FROM city LEFT OUTER JOIN sliders USING (id_city)";
        $city = $this->db->query($sql);
        foreach ($city->result_array() as $row){
            if ($row['id_slide'] == null){
                $sql = "INSERT INTO `sliders` (`id_image`, `name_slide`, `id_city`) VALUES
                       (NULL, 'Первый слайд', ".$this->db->escape($row['id_city'])."),
                       (NULL, 'Второй слайд', ".$this->db->escape($row['id_city'])."),
                       (NULL, 'Третий слайд', ".$this->db->escape($row['id_city'])."),
                       (NULL, 'Четвёртый слайд', ".$this->db->escape($row['id_city'])."),
                       (NULL, 'Пятый слайд', ".$this->db->escape($row['id_city'])."),
                       (NULL, 'Шестой слайд', ".$this->db->escape($row['id_city']).");";
                $this->db->query($sql);
            }
        }
    }
}