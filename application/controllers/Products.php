<?php
class Products extends CI_Controller{

    //TODO меньше запросов, меньше.

    public function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url', 'cookie'));
        $this->load->library('form_validation');
    }

    //Просмотр всех своих продуктов
    public function myProducts($searchText = null){
        if($this->session->userdata('id_user') == null | $this->session->userdata('id_company') == null){
            redirect('http://'.$_SERVER['SERVER_NAME'].'/index.php/user/login/', 'refresh');
        } else {
            $sql = "SELECT * FROM product LEFT OUTER JOIN images USING (id_image) WHERE id_company = ".$this->db->escape($this->session->userdata('id_company'))."  AND deleted IS FALSE ORDER BY `position`";
            $products = $this->db->query($sql);
            $sql = "SELECT icon_currency FROM company LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN country
                   USING (id_country) LEFT OUTER JOIN currency USING (id_currency) WHERE id_company =".$this->db->escape($this->session->userdata('id_company'));
            $currency = $this->db->query($sql);
            $this->load->view('header');
            $this->load->view('headerMenu', array('hide' => null, 'searchText' => null));
			$this->load->view('leftMenu', array('active'=>'products'));
            $this->load->view('myProducts',array('products' => $products, 'currency' => $currency));
            $this->load->view('footerMyProducts');
        }
    }

    //TODO запрос для выборки доступных разделов, категорий, подкатегорий, стран.
    //Добавление продукта
    public function addProduct($searchText = null){
        if ($this->session->userdata('id_user') == null | $this->session->userdata('id_company') == null) {
            redirect('http://'.$_SERVER['SERVER_NAME'].'/index.php/user/login/', 'refresh');
        } else {
            $sql = "SELECT * FROM section_one";
            $info = $this->db->query($sql);
            $sql = "SELECT * FROM category ";
            $info2 = $this->db->query($sql);
            $sql = "SELECT * FROM subcategory ";
            $info3 = $this->db->query($sql);
            $sql = "SELECT * FROM country ";
            $info4 = $this->db->query($sql);
            $sql = "SELECT icon_currency FROM company LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN country
                   USING (id_country) LEFT OUTER JOIN currency USING (id_currency) WHERE id_company =".$this->db->escape($this->session->userdata('id_company'));
            $currency = $this->db->query($sql);
            $this->form_validation->set_rules('name_product', 'название продукта', 'required|max_length[120]');
            //$this->form_validation->set_rules('id_section_one', 'id_section_one', 'required');
            //$this->form_validation->set_rules('id_category', 'категория', 'required');
            //$this->form_validation->set_rules('id_subcategory', 'подкатегория', 'required');
            $this->form_validation->set_rules('price', 'цена', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header');
                $this->load->view('headerMenu', array('hide' => null, 'searchText' => null));
				$this->load->view('leftMenu', array('active'=>'products'));
                $this->load->view('addProduct', array('info' => $info, 'info2' => $info2, 'info3' => $info3, 'info4' => $info4, 'currency' => $currency));
                $this->load->view('footer');
            } else {
                $sql = "INSERT INTO `product`(`id_company`, `name_product`, `id_section_one`, `id_category`, `id_subcategory`,
                       `name_country`, `price`, `in_stock`, `at_the_end`, `sample`, `stock`, `id_image`, `barcode`, `product_meta`) VALUES (".
                       $this->db->escape($this->session->userdata('id_company')).",".
                       $this->db->escape($this->input->post('name_product')).",".
                       $this->db->escape($this->input->post('id_section_one')).",".
                       $this->db->escape($this->input->post('id_category')).",".
                       $this->db->escape($this->input->post('id_subcategory')).",".
                       $this->db->escape($this->input->post('name_country')).",".
                       $this->db->escape($this->input->post('price')).",".
                       $this->db->escape($this->input->post('in_stock')).",".
                       $this->db->escape($this->input->post('at_the_end')).",".
                       $this->db->escape($this->input->post('sample')).",".
                       $this->db->escape($this->input->post('stock')).",".
                       "(SELECT id_image FROM images WHERE path = ".$this->db->escape($this->input->post('path')).")".",".
					   $this->db->escape($this->input->post('barcode')).",".
                       $this->db->escape($this->input->post('product_meta')).")";
                if (!$this->db->query($sql)) {
                    echo 'Error database';
                }
                redirect('http://'.$_SERVER['SERVER_NAME'].'/index.php/Products/myProducts', 'refresh');
            }
        }
    }

    //TODO выборка информации о продукте и прочем
    //Редактирование информации о продукте
    public function editProduct($searchText = null){
        if ($this->session->userdata('id_user') == null | $this->session->userdata('id_company') == null) {
            redirect('http://'.$_SERVER['SERVER_NAME'].'/index.php/user/login/', 'refresh');
        } else {
            $sql = "SELECT * FROM section_one";
            $info = $this->db->query($sql);
            $sql = "SELECT * FROM category ";
            $info2 = $this->db->query($sql);
            $sql = "SELECT * FROM subcategory ";
            $info3 = $this->db->query($sql);
            $sql = "SELECT * FROM product LEFT OUTER JOIN images USING (id_image) WHERE id_product = ".$this->db->escape($this->input->get_post('id_product', true));
            $product = $this->db->query($sql);
            $sql = "SELECT icon_currency FROM company LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN country
                   USING (id_country) LEFT OUTER JOIN currency USING (id_currency) WHERE id_company =".$this->db->escape($this->session->userdata('id_company'));
            $currency = $this->db->query($sql);
            $this->form_validation->set_rules('name_product', 'название продукта', 'required|max_length[120]');
            //$this->form_validation->set_rules('id_section_one', 'id_section_one', 'required');
            //$this->form_validation->set_rules('id_category', 'категория', 'required');
            //$this->form_validation->set_rules('id_subcategory', 'подкатегория', 'required');
            $this->form_validation->set_rules('price', 'цена', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header');
                $this->load->view('headerMenu', array('hide' => null, 'searchText' => null));
				$this->load->view('leftMenu', array('active'=>'products'));
                $this->load->view('editProduct', array('info' => $info, 'info2' => $info2, 'info3' => $info3, 'product' => $product, 'currency' => $currency));
                $this->load->view('footer');
            } else {
                $sql = "UPDATE `product` SET `name_product`= ".$this->db->escape($this->input->post('name_product')).
                       ",`id_section_one`= ".$this->db->escape($this->input->post('id_section_one')).
                       ",`id_category`= ".$this->db->escape($this->input->post('id_category')).
                       ",`id_subcategory`= ".$this->db->escape($this->input->post('id_subcategory')).
                       ",`name_country`= ".$this->db->escape($this->input->post('name_country')).
                       ",`price`= ".$this->db->escape($this->input->post('price')).
                       ",`in_stock`= ".$this->db->escape($this->input->post('in_stock')).
                       ",`at_the_end`= ".$this->db->escape($this->input->post('at_the_end')).
                       ",`sample`= ".$this->db->escape($this->input->post('sample')).
                       ",`stock`= ".$this->db->escape($this->input->post('stock')).
                       ", `id_image` = "."(SELECT id_image FROM images WHERE path =".$this->db->escape($this->input->post('path')).")".
                       ", `product_meta` = ".$this->db->escape($this->input->post('product_meta')).
					   ", `barcode` = ".$this->db->escape($this->input->post('barcode')).
                       " WHERE id_product = ".$this->db->escape($this->input->post('id_product')).
                       " AND id_company = ".$this->db->escape($this->session->userdata('id_company'));
                if (!$this->db->query($sql)) {
                    echo 'Error database';
                }
                redirect('http://'.$_SERVER['SERVER_NAME'].'/index.php/Products/myProducts', 'refresh');
            }
        }
    }

    //TODO при удалении продукта, если есть картинка её тоже удаляем
    //Удалени продукта
    public function deleteProduct(){
        if($this->session->userdata('id_company') == null && $this->input->get_post('id_product') == null){
            redirect('http://'.$_SERVER['SERVER_NAME'].'/index.php/user/login/', 'refresh');
        } else {
            $sql = "UPDATE `product` SET `deleted` = TRUE  WHERE id_product = ".$this->db->escape($this->input->get_post('id_product'))
                   ." AND id_company =".$this->db->escape($this->session->userdata('id_company'));
            if (!$this->db->query($sql)) {
                echo 'Error database';
            }
            redirect('http://'.$_SERVER['SERVER_NAME'].'/index.php/Products/myProducts', 'refresh');
        }
    }

    //Получить все продукты
    public function getAllProducts(){

    }

    //Получить конкретный продукт
    public function getProduct($id_product = null, $id_company = null, $searchText = null){
        $sql = "SELECT * FROM section_one";
        $section = $this->db->query($sql);
        $sql = "SELECT * FROM category";
        $category = $this->db->query($sql);
        $sql = "SELECT * FROM subcategory";
        $subcategory = $this->db->query($sql);
        if ($this->session->userdata('id_user') != null ){
            $sql = "SELECT *,(SELECT COUNT(*) FROM favorites WHERE id_product = ".$this->db->escape($id_product).
                   " AND favorites.id_user = ".$this->db->escape($this->session->userdata('id_user'))."), company.id_company
                   FROM product LEFT OUTER JOIN images USING (id_image) LEFT OUTER JOIN company USING (id_company)
                   LEFT OUTER JOIN favorites USING (id_product) WHERE id_product = ".$this->db->escape($id_product);
            $product = $this->db->query($sql);
        } else {
            $sql = "SELECT * FROM product LEFT OUTER JOIN images USING (id_image) LEFT OUTER
                   JOIN company USING (id_company) WHERE id_product = " . $this->db->escape($id_product);
            $product = $this->db->query($sql);
        }
        $sql = "SELECT icon_currency FROM company LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN country
               USING (id_country) LEFT OUTER JOIN currency USING (id_currency) WHERE id_company = ".$this->db->escape($id_company);
        $currency = $this->db->query($sql);
        $this->load->view('header');
        $this->load->view('headerMenu', array('hide' => 'hide', 'searchText' => null));
        $this->load->view('viewProduct', array('product' => $product, 'currency' => $currency, 'section' => $section, 'category' => $category, 'subcategory' => $subcategory));
        $this->load->view('footer');
    }

    //В данный момент используем OR, это не очень хорошо, возможно нужно переложить все функции на поле product_meta, тогда можно будет
    //Опимизировать запрос и он будет отрабатывать быстрее.
    //Поиск продуктов
    public function findProducts(){
        $city = $this->city();
        $sql = "SELECT * FROM section_one";
        $section = $this->db->query($sql);
        $sql = "SELECT * FROM category";
        $category = $this->db->query($sql);
        $sql = "SELECT * FROM subcategory";
        $subcategory = $this->db->query($sql);
        $searchText = $this->input->get('searchText');
        $sql = "SELECT product.id_product, product.id_image, images.path, product.id_company, product.name_product, company.name, contact_information.id_region, contact_information.id_country, contact_information.id_city, product.name_country, product.price, product.at_the_end, product.in_stock FROM product LEFT OUTER JOIN images USING (id_image) LEFT OUTER JOIN company USING (id_company) LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN country USING (id_country) LEFT OUTER JOIN region USING (id_region)  LEFT OUTER JOIN city USING (id_city) WHERE ".$city." AND (name_product LIKE '%".$this->input->get('searchText')."%' OR product_meta LIKE '%".$this->input->get('searchText')."%') AND company.show_company = TRUE AND company.show_company = TRUE AND deleted = FALSE";
        $products = $this->db->query($sql);
        $sql = "SELECT COUNT(*) AS `count` FROM product LEFT OUTER JOIN company USING (id_company) LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN country USING (id_country) LEFT OUTER JOIN region USING (id_region)  LEFT OUTER JOIN city USING (id_city) WHERE ".$city." AND name_product LIKE '%"
               .$this->input->get('searchText')."%' AND company.show_company = TRUE ";
        $count_product = $this->db->query($sql);
        $this->load->view('header');
        $this->load->view('headerMenu',array('hide' => 'hide', 'searchText' => $searchText));
        $this->load->view('search', array('products' => $products, 'count' => $count_product, 'section' => $section, 'category' => $category, 'subcategory' => $subcategory));
        $this->load->view('footer');
    }

   private function city(){
        $city = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
        $a=mb_strstr($city,".",true);
        if($a) $city=$a.".";
        if($city == 'zakup.'){
             return 'country.name_country = "Казахстан"';
        }
        return  "(city.mask_sity = ".$this->db->escape($city)." OR region.mask_region = ".$this->db->escape($city).")";
    }
	
	public function changePosition(){
        $sql = "UPDATE product SET position = case ".$this->input->post('position');
        if ($this->db->query($sql))
            echo 'Позиции сохранены';
        else
            echo 'You have a prolem';
    }
}