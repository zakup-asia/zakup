<?php
class StockNews  extends CI_Controller{

    //TODO один суперадмин, который может всё и возможность создавать админов, похоже будет сложная система
    //Сегодня работа с админкой, её отображением и т.д.
    //Ещё нужны функции логина и логаута
    //TODO проверка админ ли это или нет
    public function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url', 'cookie'));
        $this->load->library('form_validation');
    }

    //Добавить новость
    public function addNew(){
        if ($this->session->userdata('id_user') == null | $this->session->userdata('id_company') == null) {
            redirect(SITE_NAME.'index.php/user/login/', 'refresh');
        } else {
            $this->form_validation->set_rules('name_new', 'название новости', 'required');
            $this->form_validation->set_rules('description_new', 'описание новости', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header');
                $this->load->view('headerMenu', array('hide' => 'hide', 'searchText' => null));
                $this->load->view('addNew', array());
                $this->load->view('footer');
            } else {
                $sql = 'INSERT INTO `news`( `name_new`, `short_discription_new`, `description_new`, `id_image`, `id_company`) VALUES ('
                       .$this->db->escape($this->input->post('name_new')).', '.$this->db->escape($this->input->post('short_discription_new')).
                       ', '.$this->db->escape($this->input->post('description_new')).', '.$this->db->escape($this->input->post('id_image')).')';
                if (!$this->db->query($sql)){
                    echo 'Error database';
                }
                redirect(SITE_NAME.'index.php/Products/myProducts', 'refresh');
            }
        }


    }

    //Просмотреть новость
    public function getNew(){
        $sql = 'SELECT * FROM news LEFT OUTER JOIN images USING (id_image) WHERE id_new = '.$this->db->escape($this->input->get('id_new'));
        $this->db->query($sql);
    }

    //Смотрим свои собственные новости
    public function getMyNews(){
        $sql = 'SELECT * FROM news LEFT OUTER JOIN images USING (id_image) WHERE id_company = '.$this->db->escape($this->session->userdata('id_company'));
        $news = $this->db->query($sql);
        $this->load->view('header');
        $this->load->view('headerMenu', array('hide' => 'hide', 'searchText' => null));
        $this->load->view('myNews', array('news' => $news));
        $this->load->view('footer');
    }

    //Отредактировать новость
    public function editNew(){
        $sql = 'UPDATE `news` SET `name_new`='.$this->db->escape($this->input->post('name_new')).
               ', `short_discription_new`='.$this->db->escape($this->input->post('short_discription_new')).
               ', `description_new`='.$this->db->escape($this->input->post('description_new')).
               ', `id_image`= '.$this->db->escape($this->input->post('id_image')).' WHERE
               `id_new`='.$this->db->escape($this->input->get('id_new'));
        $this->db->query($sql);
    }

    //Удалить новость
    public function deleteNew(){
        $sql = 'DELETE FROM `news` WHERE `id_new`='.$this->db->escape($this->input->get('id_new'));
        $this->db->query($sql);
    }

    //Добавить акцию
    public function addStock(){

    }

    //Получить
    public function getStock(){

    }

    //Отредактировать акцию
    public function editStock(){

    }

    //Удалить акцию
    public function deleteStock(){

    }
}