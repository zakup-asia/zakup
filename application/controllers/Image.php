<?php
class Image extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url', 'cookie'));
        $this->load->library('form_validation');
    }

    public function uploadImage(){
        $this->form_validation->set_rules('file', 'Страница пользователя', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('load');
        } else {
            $this->load->plugin('fileupload');
            $file = fileupload('/../../images');
            echo json_encode(array('status'=>'ok','file'=>$file));
        }
    }

    function do_upload(){
        $config['upload_path'] = './images/upload/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']	= '2048';
        $config['max_width']  = '2000';
        $config['max_height']  = '2000';
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload()) {
            $error = array('error' => $this->upload->display_errors());
            echo json_encode($error);
        }
        else {
            $data = array('upload_data' => $this->upload->data());
            $file_name = array();
            foreach($data as $item){
                $file_name['file_name'] = $item['file_name'];
            }
            $sql = "INSERT INTO `images` (path) VALUES("
                .$this->db->escape($file_name['file_name']).")";
            $this->db->query($sql);
            $config['image_library'] = 'gd2';
            $config['source_image']	= './images/upload/'.$file_name['file_name'];
            $config['new_image'] = './images/mini/'.$file_name['file_name'];
            $config['create_thumb'] = FALSE;
            $config['maintain_ratio'] = TRUE;
            $config['width']	= 200;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            print json_encode($file_name);
        }
    }

    //Удаляем загруженную картинку
    function deleteImage($path = null){
        $sql = "DELETE FROM `images` WHERE path = ("
            .$this->db->escape($path).")";
        if($this->db->query($sql)){
            $filename = './images/mini/'.$path;
            unlink($filename);
            $filename = './images/upload/'.$path;
            unlink($filename);
        } else echo 'Ошибка';
    }
}