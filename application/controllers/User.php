<?php
class User extends CI_Controller{

    private $server = null;
    
    public function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url', 'cookie'));
        $this->load->library('form_validation');
        if ($_SERVER['SERVER_PORT'] != 80){
            $this->server = $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'];
        } else {
            $this->server = $_SERVER['SERVER_NAME'];
        }
    }

    //TODO необходимо добавить ко всем запросам с выборками информации город
    //TODO нужно проверять куку на наличие, если есть, мы должгны обновить её.
    //Главная страница, наверное её стоит вынести куда-нибудь, желательно в другое место
    public function index($searchText = null){
        $cookie = get_cookie('id');
        if ($cookie == null){
            $cookie = array(
                'name'   => 'id',
                'value'  => $this->session->userdata('__ci_last_regenerate'),
                'expire' => '604800'
            );
            set_cookie($cookie);
        } else {
            //Вот тут обновляем.
        }
        $sliderCity = $this->sliderCity();
        $city = $this->city();
        //SELECT * FROM sliders LEFT OUTER JOIN images USING (id_image) LEFT OUTER JOIN city USING (id_city) WHERE mask_sity =
        $sql = "SELECT sliders.id_slide,sliders.name_slide, sliders.id_image, images.path, sliders.id_city, sliders.id_region, sliders.id_country FROM sliders LEFT OUTER JOIN images USING (id_image) LEFT OUTER JOIN country USING (id_country) LEFT OUTER JOIN region USING (id_region) LEFT OUTER JOIN city USING (id_city) WHERE ".$sliderCity;
        $sliders = $this->db->query($sql);
        $sql = "SELECT company.name_translit,product.id_product, product.id_image, images.path, product.id_company, product.name_product, company.name, contact_information.id_region, contact_information.id_country, contact_information.id_city FROM product LEFT OUTER JOIN images USING (id_image) LEFT OUTER JOIN company USING (id_company) LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN country USING (id_country) LEFT OUTER JOIN region USING (id_region)  LEFT OUTER JOIN city USING (id_city) WHERE  ".$city." AND show_company = TRUE AND deleted = FALSE ORDER BY id_product LIMIT 0,8 ";
        $products = $this->db->query($sql);
        $sql = "SELECT company.name_translit, company.id_company, company.name, company.short_description, company.id_image, images.path, conditions.payment_methods, conditions.min_amount, conditions.shipping, city.mask_sity, region.mask_region, contact_information.id_region, contact_information.id_city, contact_information.id_country FROM company LEFT OUTER JOIN images USING (id_image) LEFT OUTER JOIN conditions USING (id_company) LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN country USING (id_country) LEFT OUTER JOIN region USING (id_region) LEFT OUTER JOIN city USING (id_city) WHERE show_company = TRUE AND ".$city." ORDER BY id_company LIMIT 0,32";
        $companies = $this->db->query($sql);
		$sql = "SELECT news.id_new, news.id_image, news.description_new, images.path, news.name_new, news.id_company, contact_information.id_region, contact_information.id_country, contact_information.id_city  FROM news LEFT OUTER JOIN images USING (id_image) LEFT OUTER JOIN company USING (id_company)LEFT OUTER JOIN contact_information USING (id_company)  LEFT OUTER JOIN country USING (id_country) LEFT OUTER JOIN region USING (id_region)  LEFT OUTER JOIN city USING (id_city) WHERE ".$city." AND show_company = TRUE  ORDER BY id_company LIMIT 0,8";
        $news = $this->db->query($sql);
        $sql = "SELECT company.name, stock.end_stock, stock.id_stock, stock.id_image, stock.description_stock, images.path, stock.name_stock, stock.id_company, contact_information.id_region, contact_information.id_country, contact_information.id_city  FROM stock LEFT OUTER JOIN images USING (id_image) LEFT OUTER JOIN company USING (id_company) LEFT OUTER JOIN contact_information USING (id_company)  LEFT OUTER JOIN country USING (id_country) LEFT OUTER JOIN region USING (id_region)  LEFT OUTER JOIN city USING (id_city) WHERE ".$city." AND show_company = TRUE  ORDER BY id_company LIMIT 0,8 ";
        $stock = $this->db->query($sql);
        $sql = "SELECT * FROM section_one ORDER BY id_section_one";
        $section = $this->db->query($sql);
        $sql = "SELECT * FROM category ORDER BY id_category";
        $category = $this->db->query($sql);
        $sql = "SELECT * FROM subcategory ORDER BY id_subcategory";
        $subcategory = $this->db->query($sql);
		$sql = "SELECT * FROM random ORDER BY random";
		$random = $this->db->query($sql);
        $this->load->view('header', array('keywords' => null, 'description' => null));
        $this->load->view('headerMenu', array('hide' => null, 'searchText' => $searchText));
        $this->load->view('index',array('sliders' => $sliders, 'products'=> $products, 'companies' => $companies, 'section' => $section, 'category' => $category, 'subcategory' => $subcategory, 'cookie' => $cookie, 'city' => $city , 'news' => $news, 'stock' => $stock, 'random' => $random));
        $this->load->view('footer');
    }

    //TODO редирект
    //TODO автоматическое создание компании, и всего остольного.
    //Функция логина
    public function login(){
        $this->form_validation->set_rules('email', 'email адрес', 'required');
        $this->form_validation->set_rules('password', 'пароль', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('header', array('keywords' => null, 'description' => null));
            $this->load->view('login');
            $this->load->view('footer');
        } else {
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $this->logout();
            $user = $this->getByEmail($email);
            $sql = "SELECT * FROM `company` WHERE `id_user`= (".$this->db->escape($user['id_user']).")";
            if(!$this->db->query($sql)){
                $sql = "INSERT INTO `company`(`id_user`) VALUES (".$this->db->escape($user['id_user']).")";
                if(!$this->db->query($sql)){
                    $us_pass = $user['password'];
                    if ($user == null) return false;
                    if ($us_pass == $password){
                        $newdata = array(
                            'id_user' => $user['id_user'],
                            'email'     => $user['email'],
                            'id_company' => $user['id_company'],
                            'name_translit' => $user['name_translit'],
                            'goal' => $user['goal'],
                            'name_role' => $user['role'],
                            'show' => $user['show_company'],
                            'path' => $user['path'],
                            'logged_in' => TRUE
                        );
                        $this->session->set_userdata($newdata);
                    }
                    redirect('http://'.$this->server.'/', 'refresh');
                }
            }
            $us_pass = $user['password'];
            if ($user == null) return false;
            if ($us_pass == $password){
                $newdata = array(
                    'id_user' => $user['id_user'],
                    'email'     => $user['email'],
                    'id_company' => $user['id_company'],
                    'name_translit' => $user['name_translit'],
                    'goal' => $user['goal'],
                    'name_role' => $user['role'],
                    'show' => $user['show_company'],
                    'path' => $user['path'],
                    'logged_in' => TRUE
                );
                $this->session->set_userdata($newdata);
            }
            redirect('http://'.$this->server.'/', 'refresh');
        }
    }

    //Не помню для чего я это делал D
    public function loginMain($email, $password){
        $user = $this->getByEmail($email);
        $us_pass = $user['password'];
        if ($user == null) return false;
        if ($us_pass == $password){
            $newdata = array(
                'id_user' => $user['id_user'],
                'email' => $user['email'],
                'id_company' => $user['id_company'],
                'name_translit' => $user['name_translit'],
                'goal' => $user['goal'],
                'name_role' => $user['role'],
                'logged_in' => TRUE
            );
            $this->session->unset_userdata('goal');
            $this->session->set_userdata($newdata);
        }
        redirect('http://'.$this->server.'/user/main/', 'refresh');
    }

    //TODO вынести правила в отдельный файл конфигурации
    //Функция регистрации нового пользователя
    public function registration(){
        $this->form_validation->set_rules('email', 'email адрес', 'required|is_unique[user.email]');
        $this->form_validation->set_rules('password', 'пароль', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('header', array('title' => 'Регистрация', 'keywords' => null, 'description' => null, 'title' => 'Регистрация'));
            $this->load->view('registration');
            $this->load->view('footer');
        } else {
            $email = $this->input->post('email');
            $password = $this->input->post('password');
			$goal = $this->input->post('goal');
            if(!$this->new_user($email, $password)) echo 'Ошибка';
            $this->logout();
            $user = $this->getByEmail($email);
			if ($goal == '1'){	
				$sql = "INSERT INTO `company`(`id_user`,`goal`) VALUES (".$this->db->escape($user['id_user']).", ".$this->db->escape($goal).")";
				$this->db->query($sql);
				//TODO вот это
				$this->emailAboutRegistrationSupplier($email, $goal);
			}	
			if ($goal == '0'){	
				$sql = "INSERT INTO `company`(`id_user`,`goal`) VALUES (".$this->db->escape($user['id_user']).", ".$this->db->escape($goal).")";
				$this->db->query($sql);
				//TODO вот это
				$this->emailAboutRegistrationSupplier($email, $goal);
			}
            $sql = "SELECT * FROM `company` WHERE `id_user`= (".$this->db->escape($user['id_user']).")";
            if(!$this->db->query($sql)){
                $sql = "INSERT INTO `company`(`id_user`) VALUES (".$this->db->escape($user['id_user']).")";
                if(!$this->db->query($sql)){
                    $us_pass = $user['password'];
                    if ($user == null) return false;
                    if ($us_pass == $password){
                        $newdata = array(
                            'id_user' => $user['id_user'],
                            'email' => $user['email'],
                            'id_company' => $user['id_company'],
                            'name_translit' => $user['name_translit'],
                            'name_role' => $user['role'],
                            'show' => $user['show_company'],
                            'goal' => $goal,
                            'path' => $user['path'],
                            'logged_in' => TRUE
                        );
                        $this->session->set_userdata($newdata);
                    }
                    redirect('http://'.$this->server.'/user/main/', 'refresh');
                }
            }
            $us_pass = $user['password'];
            if ($user == null) return false;
            if ($us_pass == $password){
                $newdata = array(
                    'id_user' => $user['id_user'],
                    'email' => $user['email'],
                    'id_company' => $user['id_company'],
                    'name_translit' => $user['name_translit'],
                    'name_role' => $user['role'],
                    'show' => $user['show_company'],
                    'goal' => $goal,
                    'path' => $user['path'],
                    'logged_in' => TRUE
                );
                $this->session->set_userdata($newdata);
            }
            redirect('http://'.$this->server.'/user/main/', 'refresh');
        }
    }

    //Функция разлогирования, убираем все данные, которые помещаем при логине
    public function logout($log = null){
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('id_user');
        $this->session->unset_userdata('id_company');
        $this->session->unset_userdata('admin');
        $this->session->unset_userdata('goal');
        $this->session->unset_userdata('name_role');
        $this->session->unset_userdata('show_company');
        $this->session->unset_userdata('name_translit');
        $this->session->unset_userdata('path');
        if ($log != null) redirect('http://'.$this->server, 'refresh');

    }

    //Функция разлогирования
    public function logoutFromSite(){
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('id_user');
        $this->session->unset_userdata('id_company');
        $this->session->unset_userdata('goal');
        $this->session->unset_userdata('name_role');
        $this->session->unset_userdata('show_company');
        $this->session->unset_userdata('name_translit');
        $this->session->unset_userdata('path');
    }

    //получаем пользователя по емайл
    private function getByEmail($email){
        $sql = "SELECT * FROM `user` LEFT OUTER JOIN `company` USING (id_user) LEFT OUTER JOIN roles USING (id_role)
               LEFT OUTER JOIN images USING (id_image) WHERE email = (".$this->db->escape($email).")";
        $query = $this->db->query($sql);
        $object = array();
        $image = array();
        foreach ($query->result_array() as $row){
            $object['id_user'] = $row['id_user'];
            $object['password'] = $row['password'];
            $object['email'] = $row['email'];
            $object['id_company'] = $row['id_company'];
            $object['name_translit'] = $row['name_translit'];
            $object['role'] = $row['name_role'];
            $object['goal'] = $row['goal'];
            $object['show_company'] = $row['show_company'];
            $image['path'] = $row['path'];
        }
        if ($image['path'] == null){
            $object['path'] = 'profile-img-small.png';
        }
        if ($image['path'] != null){
            $object['path'] = $image['path'];
        }
        return $object;
    }

    //Выполняет запрос к базе данных, возвращает true или false, если запрос на добовление удачен/нет
    private function new_user($email, $password){
        $sql = "INSERT INTO `user` (email, password) VALUES(".$this->db->escape($email).",".$this->db->escape($password).")";
        $query = $this->db->query($sql);
        $this->emailMe($email, $password);
        return $query;
    }

    //Основная информация
    public function Main($searchText = null){
        if($this->session->userdata('id_user') == null){
            redirect('http://'.$this->server.'/user/login/', 'refresh');
        } else {
            $sql = "SELECT * FROM `user` LEFT OUTER JOIN `company` USING (id_user) LEFT OUTER JOIN `images` USING (id_image) WHERE user.id_user =".$this->db->escape($this->session->userdata('id_user'));
            $company = $this->db->query($sql);
            $check = array();
            foreach ($company->result_array() as $row) {
                $check['id_company'] = $row['id_company'];
            }
            if($check['id_company'] != null) {
                $this->session->set_userdata($check);
                $this->form_validation->set_rules('name', 'Название компании', 'required');
                $this->form_validation->set_rules('short_description', 'Маленькое описание компании', 'required');
                $this->form_validation->set_rules('description', 'Описание компании', 'required');
                if ($this->form_validation->run() == FALSE) {
                    $this->load->view('header', array('keywords' => null, 'description' => null));
                    $this->load->view('headerMenu', array('title' => 'Основная информация', 'hide'=> null, 'searchText' => $searchText));
					$this->load->view('leftMenu', array('active'=>'main'));
                    $this->load->view('main', array('company' => $company));
                    $this->load->view('footer');
                } else {
                    $nameTranslit = $this->translit($this->input->post('name'));
                    $sql = "UPDATE `company` SET `name` = ".$this->db->escape($this->input->post('name')).",
                           `short_description` = ".$this->db->escape($this->input->post('short_description')).",
                           `description` = ".$this->db->escape($this->input->post('description')).
                           ",`type_of_activity` = ".$this->db->escape($this->input->post('type-activity')).
                           ",`is_seller` = ".$this->db->escape($this->input->post('is_seller')).
                           ",`workday1from`= ".$this->db->escape($this->input->post('workday1from')).
                           ",`workday1to` = ".$this->db->escape($this->input->post('workday1to')).
                           ",`workday2from` = ".$this->db->escape($this->input->post('workday2from')).
                           ",`workday2to` = ".$this->db->escape($this->input->post('workday2to')).
                           ",`workday3from` = ".$this->db->escape($this->input->post('workday3from')).
                           ",`workday3to` = ".$this->db->escape($this->input->post('workday3to')).
                           ",`workday4from` = ".$this->db->escape($this->input->post('workday4from')).
                           ",`workday4to` = ".$this->db->escape($this->input->post('workday4to')).
                           ",`workday5from` = ".$this->db->escape($this->input->post('workday5from')).
                           ",`workday5to` = ".$this->db->escape($this->input->post('workday5to')).
                           ",`workday6from` = ".$this->db->escape($this->input->post('workday6from')).
                           ",`workday6to` = ".$this->db->escape($this->input->post('workday6to')).
                           ",`workday7from` = ".$this->db->escape($this->input->post('workday7from')).
                           ",`workday7to` = ".$this->db->escape($this->input->post('workday7to')).
                           ",`id_image` = "."(SELECT id_image FROM images WHERE path =".$this->db->escape($this->input->post('path')).")".
                           ",`is_manufacturer` = ".$this->db->escape($this->input->post('is_manufacturer')).
                           ",`is_importer` = ".$this->db->escape($this->input->post('is_importer')).
                           ",`is_supplier` = ".$this->db->escape($this->input->post('is_supplier')).
                           ",`goal` = ".$this->db->escape($this->session->userdata('goal'))//.
                           //", `name_translit` = ".$this->db->escape($this->input->post('name_translit'))
                           ." WHERE id_user = ".$this->db->escape($this->session->userdata('id_user'));
                    $this->session->unset_userdata('goal');
                    $array = array('goal' => 1, 'show' => 0, 'name_translit' => $nameTranslit);
                    $this->session->set_userdata($array);
                    if (!$this->db->query($sql)) {
                        echo 'Ошибка базы данных';
                    }
					//Вот тут проверку сделать. Кто это и сделать другую функцию.
                    $this->loginMain($this->session->userdata('email'), $this->session->userdata('password'));
                }
            } else {
                $this->form_validation->set_rules('name', 'Название компании', 'required');
                $this->form_validation->set_rules('short_description', 'Маленькое описание компании', 'required');
                $this->form_validation->set_rules('description', 'Описание компании', 'required');
                if ($this->form_validation->run() == FALSE) {
                    $this->load->view('header', array('title' => 'Основная информация', 'keywords' => null, 'description' => null));
                    $this->load->view('headerMenu', array('hide' => null,'searchText' => $searchText));
					$this->load->view('leftMenu', array('active'=>'main'));
                    $this->load->view('main',  array('company' => $company));
                    $this->load->view('footer');
                } else {
                    $sql = "INSERT INTO `company`(`id_user`, `name`, `short_description`, `description`, `id_image`, `type_of_activity`,
                           `is_seller`, `workday1from`, `workday1to`, `workday2from`, `workday2to`, `workday3from`, `workday3to`,
                           `workday4from`, `workday4to`, `workday5from`, `workday5to`, `workday6from`, `workday6to`, `workday7from`,
                           `workday7to`, `is_manufacturer`, `is_importer`, `is_supplier`, `goal`) VALUES (".$this->db->escape($this->session->userdata('id_user')).",".
                           $this->db->escape($this->input->post('name')).",".
                           $this->db->escape($this->input->post('short_description')).",".
                           $this->db->escape($this->input->post('description')).",".
                           "(SELECT id_image FROM images WHERE path =".$this->db->escape($this->input->post('path')).")".",".
                           $this->db->escape($this->input->post('type_of_activity')).",".
                           $this->db->escape($this->input->post('is_seller')).",".
                           $this->db->escape($this->input->post('workday1from')).",".
                           $this->db->escape($this->input->post('workday1to')).",".
                           $this->db->escape($this->input->post('workday2from')).",".
                           $this->db->escape($this->input->post('workday2to')).",".
                           $this->db->escape($this->input->post('workday3from')).",".
                           $this->db->escape($this->input->post('workday3to')).",".
                           $this->db->escape($this->input->post('workday4from')).",".
                           $this->db->escape($this->input->post('workday4to')).",".
                           $this->db->escape($this->input->post('workday5from')).",".
                           $this->db->escape($this->input->post('workday5to')).",".
                           $this->db->escape($this->input->post('workday6from')).",".
                           $this->db->escape($this->input->post('workday6to')).",".
                           $this->db->escape($this->input->post('workday7from')).",".
                           $this->db->escape($this->input->post('workday7to')).",".
                           $this->db->escape($this->input->post('is_manufacturer')).",".
                           $this->db->escape($this->input->post('is_importer')).",".
                           $this->db->escape($this->input->post('is_supplier')).",".
                           $this->db->escape($this->session->userdata('goal')).","//.
                           //$this->db->escape($this->translit($this->input->post('name')))
                        .")";
                        $this->session->unset_userdata('goal');
                        $array = array('goal' => 0, 'show' => 0);
                        $this->session->set_userdata($array);
                    if (!$this->db->query($sql)) {
                        echo 'Ошибка базы данных';
                    }
                    $this->emailAboutRegistrationSupplier($this->session->userdata('email'), $this->input->post('goal'));
                    redirect('http://'.$this->server.'/user/main/', 'refresh');
                }
            }
        }
    }

    //TODO запрос для получения уже введённой информации
    //Опции
    public function Options($searchText = null){
        if($this->session->userdata('id_user') == null){
            redirect('http://'.$this->server.'/user/login/', 'refresh');
        } else {
            //Проверка формы
            $this->form_validation->set_rules('id_user', 'Страница пользователя', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header', array('title' => 'Избранное', 'keywords' => null, 'description' => null));
                $this->load->view('headerMenu', array('searchText' => $searchText));
                $this->load->view('options');
                $this->load->view('footer');
            } else {
            }
        }
    }

    //TODO как вариант можно сделать ajax запрос, который будет пинать три нахиш функции на сервере,
    //они же в свою очередь будут заполнять данные, таким образом мы сможем по одному клику заполнять три формы
    //TODO запрос для получения уже введённой информации
    //Условия заказа
    public function Conditions($searchText = null){
        $sql = "SELECT COUNT(*) AS count FROM alert_email WHERE id_company = ".$this->db->escape($this->session->userdata('id_company'));
        $alert = $this->db->query($sql);
        $sql = "SELECT * FROM `alert_email` WHERE id_company = ".$this->db->escape($this->session->userdata('id_company'));
        foreach ($alert->result_array() as $row){
            if ($row['count'] == '0'){
                $emails = null;
            } else {
                $emails = $this->db->query($sql);
            }
        }
        if($this->session->userdata('id_user') == null){
            redirect('http://'.$this->server.'/user/login/', 'refresh');
        } else if($this->session->userdata('id_company') == null){
            redirect('http://'.$this->server.'/user/main/', 'refresh');
        } else {
            $sql = "SELECT * FROM `company` LEFT OUTER JOIN `conditions` USING (id_company) LEFT OUTER JOIN `user` USING (id_user)
                   LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN country USING (id_country) LEFT OUTER JOIN
                   currency USING (id_currency)  WHERE company.id_company =  ".$this->db->escape($this->session->userdata('id_company'));
            $conditions = $this->db->query($sql);
            $check = array();
            foreach ($conditions->result_array() as $row) {
                $check['id_condition'] = $row['id_condition'];
            }
            if($check['id_condition'] != null) {
                $this->form_validation->set_rules('min_amount', 'минимальная сумма заказа', 'required');
                if ($this->form_validation->run() == FALSE) {
                    $this->load->view('header', array('title' => 'Условия заказа', 'keywords' => null, 'description' => null));
                    $this->load->view('headerMenu', array('hide' => null, 'searchText' => $searchText));
					$this->load->view('leftMenu', array('active'=>'main'));
                    $this->load->view('conditions', array('conditions' => $conditions, 'emails' => $emails));
                    $this->load->view('footer');
                } else {
                    $sql = "UPDATE `conditions` SET `min_amount`=".$this->db->escape($this->input->post('min_amount')).
                           ",`pickup`=".$this->db->escape($this->input->post('pickup')).
                           ",`сonsignment`=".$this->db->escape($this->input->post('сonsignment')).
                           ",`payment_methods`=".$this->db->escape($this->input->post('payment_methods')).
                           ",`nds`=".$this->db->escape($this->input->post('nds')).
                           ",`id_nds`=".$this->db->escape($this->input->post('id_nds')).
                           ",`shipping`=".$this->db->escape($this->input->post('shipping')).
                           ",`free_shipping`=".$this->db->escape($this->input->post('free_shipping')).
                           ",`free_shipping_from`=".$this->db->escape($this->input->post('free_shipping_from')).
                           ",`cost_shipping`=".$this->db->escape($this->input->post('cost_shipping')).
                           ",`shippingday1from`=".$this->db->escape($this->input->post('shippingday1from')).
                           ",`shippingday1to`=".$this->db->escape($this->input->post('shippingday1to')).
                           ",`shippingday2from`=".$this->db->escape($this->input->post('shippingday2from')).
                           ",`shippingday2to`=".$this->db->escape($this->input->post('shippingday2to')).
                           ",`shippingday3from`=".$this->db->escape($this->input->post('shippingday3from')).
                           ",`shippingday3to`=".$this->db->escape($this->input->post('shippingday3to')).
                           ",`shippingday4from`=".$this->db->escape($this->input->post('shippingday4from')).
                           ",`shippingday4to`=".$this->db->escape($this->input->post('shippingday4to')).
                           ",`shippingday5from`=".$this->db->escape($this->input->post('shippingday5from')).
                           ",`shippingday5to`=".$this->db->escape($this->input->post('shippingday5to')).
                           ",`shippingday6from`=".$this->db->escape($this->input->post('shippingday6from')).
                           ",`shippingday6to`=".$this->db->escape($this->input->post('shippingday6to')).
                           ",`shippingday7from`=".$this->db->escape($this->input->post('shippingday7from')).
                           ",`shippingday7to`=".$this->db->escape($this->input->post('shippingday7to')).
                           ", email =".$this->db->escape($this->input->post('email')).
                           ", storing =".$this->db->escape($this->input->post('storing')).
                           ", is_test =".$this->db->escape($this->input->post('is_test')).
                           " WHERE `id_company` = ".$this->db->escape($this->session->userdata('id_company'));
                    if(!$this->db->query($sql)){
                        echo 'Error database';
                    }
					//TODO подумать о том, как сохранять изменения для емайлов
                    $sql = "UPDATE `alert_email` SET `company_email`=".$this->db->escape($this->input->post('email0'))." WHERE `id_alert_email`=".$this->db->escape($this->input->post('id_alert_email'));
                    $this->db->query($sql);
                    redirect('http://'.$this->server.'/user/conditions/', 'refresh');
                }
            } else {
                $this->form_validation->set_rules('min_amount', 'минимальная сумма заказа', 'required');
                if ($this->form_validation->run() == FALSE) {
                    $this->load->view('header', array('title' => 'Условия заказа', 'keywords' => null, 'description' => null));
                    $this->load->view('headerMenu', array('hide' => null, 'searchText' => $searchText));
					$this->load->view('leftMenu', array('active'=>'main'));
                    $this->load->view('conditions', array('conditions' => $conditions, 'emails' => $emails));
                    $this->load->view('footer');
                } else {
                    $sql = "INSERT INTO `conditions`(`id_company`, `min_amount`, `pickup`, `сonsignment`,
                           `payment_methods`, `nds`, `id_nds`, `shipping`, `free_shipping`, `free_shipping_from`, `cost_shipping`
                           , `shippingday1from`, `shippingday1to`, `shippingday2from`, `shippingday2to`,`shippingday3from`, `shippingday3to`,
                           `shippingday4from`, `shippingday4to`, `shippingday5from`, `shippingday5to`, `shippingday6from`,`shippingday6to`, `shippingday7from`,
                           `shippingday7to`, `email`, `storing`, `is_test`)
                           VALUES (".$this->db->escape($this->session->userdata('id_company')).",".
                           $this->db->escape($this->input->post('min_amount')).",".
                           $this->db->escape($this->input->post('pickup')).",".
                           $this->db->escape($this->input->post('сonsignment')).",".
                           $this->db->escape($this->input->post('payment_methods')).",".
                           $this->db->escape($this->input->post('nds')).",".
                           $this->db->escape($this->input->post('id_nds')).",".
                           $this->db->escape($this->input->post('shipping')).",".
                           $this->db->escape($this->input->post('free_shipping')).",".
                           $this->db->escape($this->input->post('free_shipping_from')).",".
                           $this->db->escape($this->input->post('cost_shipping')).
                           ",".$this->db->escape($this->input->post('shippingday1from')).
                           ",".$this->db->escape($this->input->post('shippingday1to')).
                           ",".$this->db->escape($this->input->post('shippingday2from')).
                           ",".$this->db->escape($this->input->post('shippingday2to')).
                           ",".$this->db->escape($this->input->post('shippingday3from')).
                           ",".$this->db->escape($this->input->post('shippingday3to')).
                           ",".$this->db->escape($this->input->post('shippingday4from')).
                           ",".$this->db->escape($this->input->post('shippingday4to')).
                           ",".$this->db->escape($this->input->post('shippingday5from')).
                           ",".$this->db->escape($this->input->post('shippingday5to')).
                           ",".$this->db->escape($this->input->post('shippingday6from')).
                           ",".$this->db->escape($this->input->post('shippingday6to')).
                           ",".$this->db->escape($this->input->post('shippingday7from')).
                           ",".$this->db->escape($this->input->post('shippingday7to')).
                           ",".$this->db->escape($this->input->post('email')).
                           ",".$this->db->escape($this->input->post('storing')).
                           ",".$this->db->escape($this->input->post('is_test')).")";
                    if(!$this->db->query($sql)){
                        echo 'Error database';
                    }
					$sql = "INSERT INTO `alert_email`(`id_company`, `company_email`) VALUES (".$this->db->escape($this->session->userdata('id_company')).", ".$this->db->escape($this->input->post('email0')).")";
					$this->db->query($sql);
                    redirect('http://'.$this->server.'/user/conditions/', 'refresh');
            }
            }
        }
    }

    //Расписание доставки?
    private function raspsanieDostavki(){
        $sql = "SELECT * FROM `company` LEFT OUTER JOIN `delivery_schedule` USING (id_company) WHERE company.id_company ="
            .$this->db->escape($this->session->userdata('id_company'));
        $coditions = $this->db->query($sql);
        $check = array();
        foreach ($coditions->result_array() as $row) {
            $check['id_delivery_schedule'] = $row['id_delivery_schedule'];
        }
        if($check['id_delivery_schedule'] != null) {

        } else {

        }
    }

    //Реквизиты
    public function Requisites($searchText = null){
        if($this->session->userdata('id_user') == null | $this->session->userdata('id_company') == null){
            redirect('http://'.$this->server.'/user/main/', 'refresh');
        } else {
            $sql = "SELECT * FROM `company` LEFT OUTER JOIN `requisites` USING (id_company) WHERE company.id_company ="
                   .$this->db->escape($this->session->userdata('id_company'));
            $requisites = $this->db->query($sql);
            $check = array();
            foreach ($requisites->result_array() as $row) {
                $check['id_requisite'] = $row['id_requisite'];
            }
            if($check['id_requisite'] != null) {
                $this->form_validation->set_rules('type_company', 'тип компании', 'required');
                if ($this->form_validation->run() == FALSE) {
                    $this->load->view('header', array('title' => 'Реквизиты', 'keywords' => null, 'description' => null));
                    $this->load->view('headerMenu', array('hide' => null, 'searchText' => $searchText));
					$this->load->view('leftMenu', array('active'=>'main'));
                    $this->load->view('requisites', array('requisites' => $requisites));
                    $this->load->view('footer');
                } else {
                    $sql = "UPDATE `requisites` SET `name_company`=".$this->db->escape($this->input->post('name_company')) .
                           ",`type_company`=" . $this->db->escape($this->input->post('type_company')) .
                           ",`FIO_director`=" . $this->db->escape($this->input->post('FIO_director')) .
                           ",`business_address`=" . $this->db->escape($this->input->post('business_address')) .
                           ",`IIN`=" . $this->db->escape($this->input->post('IIN')) .
                           ",`KPP`=" . $this->db->escape($this->input->post('KPP')) .
                           ",`bank`=" . $this->db->escape($this->input->post('bank')) .
                           ",`account`=" . $this->db->escape($this->input->post('account')) .
                           ",`BIK`=" . $this->db->escape($this->input->post('BIK')) .
                           " WHERE id_company = ".$this->db->escape($this->session->userdata('id_company'));
                    if (!$this->db->query($sql)) {
                        echo 'Ошибка базы данных';
                    }
                    redirect('http://'.$this->server.'/user/requisites/', 'refresh');
                }
            } else {
                $this->form_validation->set_rules('type_company', 'тип компании', 'required');
                if ($this->form_validation->run() == FALSE) {
                    $this->load->view('header', array('title' => 'Реквизиты', 'keywords' => null, 'description' => null));
                    $this->load->view('headerMenu', array('hide' => null, 'searchText' => $searchText));
					$this->load->view('leftMenu', array('active'=>'main'));
                    $this->load->view('requisites', array('requisites' => $requisites));
                    $this->load->view('footer');
                } else {
                    $sql = "INSERT INTO `requisites`(`id_company`, `name_company`, `type_company`, `FIO_director`,
                           `business_address`, `IIN`, `KPP`, `bank`, `account`, `BIK`) VALUES (" .
                           $this->db->escape($this->session->userdata('id_company')).",".
                           $this->db->escape($this->input->post('name_company')).",".
                           $this->db->escape($this->input->post('type_company')).",".
                           $this->db->escape($this->input->post('FIO_director')).",".
                           $this->db->escape($this->input->post('business_address')).",".
                           $this->db->escape($this->input->post('IIN')).",".
                           $this->db->escape($this->input->post('KPP')).",".
                           $this->db->escape($this->input->post('bank')).",".
                           $this->db->escape($this->input->post('account')).",".
                           $this->db->escape($this->input->post('BIK')).")";
                    if (!$this->db->query($sql)) {
                        echo 'Ошибка базы данных';
                    }
                    redirect('http://'.$this->server.'/user/requisites/', 'refresh');
                }
            }
        }
    }

    //TODO запрос для получения уже введённой информации
    //Сотрудники
    //public function Workers(){
    //    if($this->session->userdata('id_user') == null){
    //        redirect(SITE_NAME.'index.php/user/login/', 'refresh');
    //    } else {
    //        //Проверка формы
    //        $this->form_validation->set_rules('id_user', 'Страница пользователя', 'required');
    //        if ($this->form_validation->run() == FALSE) {
    //            $this->load->view('header');
    //            $this->load->view('headerMenu');
    //           $this->load->view('workers');
    //            $this->load->view('footer');
    //        } else {
    //        }
    //    }
    //}

    //Логин и пароль
    public function LoginAndPassword ($searchText = null){
        if($this->session->userdata('id_user') == null){
            redirect('http://'.$this->server.'/user/login/', 'refresh');
        } else {
            $sql = "SELECT * FROM `user` LEFT OUTER JOIN company USING(id_user) WHERE id_user = ".$this->db->escape($this->session->userdata('id_user'));
            $user = $this->db->query($sql);
            $this->form_validation->set_rules('email', 'емайл', 'required');
            $this->form_validation->set_rules('password', 'пароль', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header', array('title' => 'Логин и пароль', 'keywords' => null, 'description' => null));
                $this->load->view('headerMenu', array('hide' => null, 'searchText' => $searchText));
				$this->load->view('leftMenu', array('active'=>'main'));
                $this->load->view('loginAndPassword', array('user' => $user));
                $this->load->view('footer');
            } else {
                $sql = "UPDATE `user` SET email =".$this->db->escape($this->input->post('email')).
                       ", password =".$this->db->escape($this->input->post('password')).
                       " WHERE id_user =".$this->db->escape($this->session->userdata('id_user'));
                if($this->db->query($sql)){
                    $this->emailAndPassword($this->session->userdata('email'), $this->input->post('email'), $this->input->post('password'));
                    $this->session->unset_userdata('email');
                    $arr = array('email' => $this->input->post('email'));
                    $this->session->set_userdata($arr);
                }
                redirect('http://'.$this->server.'/user/LoginAndPassword/', 'refresh');
            }
        }
    }

    //Расписание доставки можно отправлять с помощью запроса ajax, правда непонятно зачем мы его везде пихаем D.
    //TODO в данный момент восприниматься будет корректно только один телефон домашний и один сотовый, подумай над этим, а точнее
    //TODO разберись как добовляются они с помощью бутстрапа или же делай свои с помощью javascript, а ещё с email такая же проблема
    //TODO валидация форм в отдельный файл можно, почему ты раньше этого не сделал?
    //TODO похоже придётся переделывать полностью номера, с ними что-то странное замутили.
    //Зачем нашим пользователям нас обходить, если мы зарабатываем не на них? Цена будет такая же, а вот удобства при попытке
    //избежать нас будет намного меньше.
    //При обновлении стоит ставить cascade, а при удалении set null
    //Делаем выборку доступных стран и получаем информацию о фирме.
    //Контакты
    public function contactInformation($searchText = null){
        if($this->session->userdata('id_user') == null){
            redirect('http://'.$this->server.'/user/login/', 'refresh');
        } else if($this->session->userdata('id_company') == null){
            redirect('http://'.$this->server.'/user/main/', 'refresh');
        } else {
            $sql = "SELECT * FROM country ";
            $info4 = $this->db->query($sql);
            $sql = "SELECT * FROM `company` LEFT OUTER JOIN `contact_information` USING (id_company) LEFT OUTER JOIN `region`
                   USING (id_region) LEFT OUTER JOIN `city` USING (id_city) WHERE company.id_company ="
                   .$this->db->escape($this->session->userdata('id_company'));
            $contactInformation = $this->db->query($sql);
            $check = array();
            foreach ($contactInformation->result_array() as $row) {
                $check['id_contact'] = $row['id_contact'];
            }
            if($check['id_contact'] != null) {
                $this->form_validation->set_rules('id_region', 'регион', 'required');
                $this->form_validation->set_rules('id_country', 'страна', 'required');
                $this->form_validation->set_rules('id_city', 'город', 'required');
                if ($this->form_validation->run() == FALSE) {
                    $this->load->view('header', array('title' => 'Контактная информация', 'keywords' => null, 'description' => null));
                    $this->load->view('headerMenu', array('hide' => null, 'searchText' => $searchText));
					$this->load->view('leftMenu', array('active'=>'main'));
                    $this->load->view('contacts', array('info4' => $info4, 'contactInformation' => $contactInformation));
                    $this->load->view('footer');
                } else {
                    $sql = "UPDATE `contact_information` SET `id_region`=".$this->db->escape($this->input->post('id_region')).
                           ",`indexs`=".$this->db->escape($this->input->post('indexs')).
                           ",`home_phone0`=".$this->db->escape($this->input->post('home_phone0')).
                           ",`home_phone1`=".$this->db->escape($this->input->post('home_phone1')).
                           ",`home_phone2`=".$this->db->escape($this->input->post('home_phone2')).
                           ",`mobile_phone0`=".$this->db->escape($this->input->post('mobile_phone0')).
                           ",`mobile_phone1`=".$this->db->escape($this->input->post('mobile_phone1')).
                           ",`mobile_phone2`=".$this->db->escape($this->input->post('mobile_phone2')).
                           ",`email1`=".$this->db->escape($this->input->post('email1')).
                           ",`email2`=".$this->db->escape($this->input->post('email2')).
                           ",`email3`=".$this->db->escape($this->input->post('email3')).
                           ",`id_country`=".$this->db->escape($this->input->post('id_country')).
                           ",`id_city`=".$this->db->escape($this->input->post('id_city')).
                           ",`actual_address`=".$this->db->escape($this->input->post('actual_address')).
                           " WHERE `id_company` =" .$this->db->escape($this->session->userdata('id_company'));
                    if (!$this->db->query($sql)) {
                        echo 'Ошибка базы данных';
                    }
                    redirect('http://'.$this->server.'/user/contactInformation/', 'refresh');
                }
            } else {
                $this->form_validation->set_rules('id_region', 'регион', 'required');
                $this->form_validation->set_rules('id_country', 'страна', 'required');
                $this->form_validation->set_rules('id_city', 'город', 'required');
                if ($this->form_validation->run() == FALSE) {
                    $this->load->view('header', array('title' => 'Контактная информация', 'keywords' => null, 'description' => null));
                    $this->load->view('headerMenu', array('hide' => null, 'searchText' => $searchText));
					$this->load->view('leftMenu', array('active'=>'main'));
                    $this->load->view('contacts', array('info4' => $info4, 'contactInformation' => $contactInformation));
                    $this->load->view('footer');
                } else {
                    $sql = "INSERT INTO `contact_information`(`id_company`, `id_region`, `indexs`, `home_phone0`,
                           `home_phone1`, `home_phone2`, `mobile_phone0`, `mobile_phone1`, `mobile_phone2`, `email1`, `email2`,
                           `email3`, `id_country`, `id_city`, `actual_address`) VALUES (
                           ".$this->db->escape($this->session->userdata('id_company')).
                           ", ".$this->db->escape($this->input->post('id_region')).
                           ", ".$this->db->escape($this->input->post('indexs')).
                           ", ".$this->db->escape($this->input->post('home_phone0')).
                           ", ".$this->db->escape($this->input->post('home_phone1')).
                           ", ".$this->db->escape($this->input->post('home_phone2')).
                           ", ".$this->db->escape($this->input->post('mobile_phone0')).
                           ", ".$this->db->escape($this->input->post('mobile_phone1')).
                           ", ".$this->db->escape($this->input->post('mobile_phone2')).
                           ", ".$this->db->escape($this->input->post('email1')).
                           ", ".$this->db->escape($this->input->post('email2')).
                           ", ".$this->db->escape($this->input->post('email3')).
                           ", ".$this->db->escape($this->input->post('id_country')).
                           ", ".$this->db->escape($this->input->post('id_city')).
                           ", ".$this->db->escape($this->input->post('actual_address')).")";
                    if (!$this->db->query($sql)) {
                        echo 'Ошибка базы данных';
                    }
                    redirect('http://'.$this->server.'/user/contactInformation/', 'refresh');
                }
            }
        }
    }

    public function getCountry(){
        $this->load->view('location');
    }

    //TODO данные функции только для администратора
    //Даём пользователю права администратора
    public function setAdmin(){
        if($this->session->userdata('admin') != null) {
            $sql = 'UPDATE `user` SET `id_role` = 1 WHERE id_user = '.$this->db->escape($this->input->get('id_user'));
            $this->db->query($sql);
            redirect('http://'.$this->server.'/user/getAllUsers', 'refresh');
        }
    }

    //Забираем права администратора
    public function unsetAdmin(){
        if($this->session->userdata('admin') != null) {
            $sql = 'UPDATE `user` SET `id_role` = 2 WHERE id_user = '.$this->db->escape($this->input->get('id_user'));
            $this->db->query($sql);
            redirect('http://'.$this->server.'/user/getAllUsers', 'refresh');
        }
    }

    public function deleteUser(){
        if($this->session->userdata('admin') != null) {
            $sql = 'DELETE FROM `user` WHERE id_user = '.$this->db->escape($this->input->get('id_user'));
            $this->db->query($sql);
            redirect('http://'.$this->server.'/user/getAllUsers', 'refresh');
        }
    }

    //Получаем всех пользователей
    public function getAllUsers($searchText = null){
        if($this->session->userdata('admin') != null) {
            $sql = "SELECT * FROM `user` LEFT OUTER JOIN roles USING (id_role) LEFT OUTER JOIN company USING (id_user) WHERE goal = FALSE OR goal is null";
            $object = $this->db->query($sql);
            $this->load->view('header', array('keywords' => null, 'description' => null, 'title' => 'Профиль'));
            $this->load->view('headerMenu', array('hide'=>'hide','searchText' => $searchText));
            $this->load->view('admin/users', array('object' => $object));
            $this->load->view('footer');
        }
    }

    //TODO по возможности добавить ссылку на заказ
    //Отправляем сообщение о том, что мы получили какой-то заказ
    public function emailAndPassword($emailOld, $emailNew, $passwordNew){
		if ($this->is_email($emailOld) && $this->is_email($emailNew)){
			$this->load->library('email');
			//Задает адрес электронной почты и имя лица-отправителя:
			$this->email->from('info@zakup.asia', 'Администрация');
			//Устанавливает адрес для ответа, если не была указана информация в функции "from". Пример:
			$this->email->reply_to('you@your-site.com', 'Ваше Имя');
			//Задает адрес(а) получателя (получателей). Может быть указан один адрес электронной почты, список через запятую или массив:
			$list = array($emailOld, $emailNew);
			$this->email->to($list);
			//Устанавливает тему письма
			$this->email->subject('Вы сменили емайл или пароль?');
			//Устанавливает текст сообщения
			$this->email->message('
				Ваш логин - '.$emailNew.'
				Ваш пароль -'.$passwordNew.'
				Сайт - http://zakup.asia
				Если вы не меняли пароль или емайл, сообщите администрации сайта.
				');
			//Отправка самого письма
			$this->email->send();
		}
    }

    //TODO возможно нужна ссылка на профиль пользователя
    //Оповещаем нас о регистрации нового пользователя
    public function emailAboutRegistration($email, $is_seller){
		if ($this->is_email($email)){
			$this->load->library('email');
			//Задает адрес электронной почты и имя лица-отправителя:
			$this->email->from('center@zakup.asia', 'Администрация');
			//Устанавливает адрес для ответа, если не была указана информация в функции "from". Пример:
			$this->email->to('info@zakup.asia');
			$this->email->subject('Зарегистрирован новый пользователь');
			//Устанавливает текст сообщения
			$this->email->message('
			   Зарегистрирован новый пользователь - '.$email.'
			   Сайт - http://zakup.asia
			 ');
			$this->email->send();
		}
    }

	public function emailMe($email, $password){
		if ($this->is_email($email)){
			$this->load->library('email');
			//Задает адрес электронной почты и имя лица-отправителя:
			$this->email->from('info@zakup.asia', 'Администрация');
			//Устанавливает адрес для ответа, если не была указана информация в функции "from". Пример:
			$this->email->reply_to('you@your-site.com', 'Ваше Имя');
			//Задает адрес(а) получателя (получателей). Может быть указан один адрес электронной почты, список через запятую или массив:
			$this->email->to($email);
			//Устанавливает тему письма
			$this->email->subject('Регистрация на сайте');
			//Устанавливает текст сообщения
			$this->email->message('
				Ваш логин - '.$email.'
				Ваш пароль -'.$password.'
				Сайт - http://zakup.asia
				');
			//Отправка самого письма
			$this->email->send();
		}
    }
	
	public function emailAboutRegistrationSupplier($email, $is_seller){
			if ($is_seller == true){
				$this->load->library('email');
				//Задает адрес электронной почты и имя лица-отправителя:
				$this->email->from('center@zakup.asia', 'Администрация');
				//Устанавливает адрес для ответа, если не была указана информация в функции "from". Пример:
				$this->email->to('info@zakup.asia');
				$this->email->subject('Зарегистрирован новый продавец');
				//Устанавливает текст сообщения
				$this->email->message('
				   Зарегистрирован новый продавец - '.$email.'
				   Сайт - http://zakup.asia
				 ');
				$this->email->send();
			} else {
				$this->load->library('email');
				//Задает адрес электронной почты и имя лица-отправителя:
				$this->email->from('center@zakup.asia', 'Администрация');
				//Устанавливает адрес для ответа, если не была указана информация в функции "from". Пример:
				$this->email->to('info@zakup.asia');
				$this->email->subject('Зарегистрирован новый пользователь');
				//Устанавливает текст сообщения
				$this->email->message('
				   Зарегистрирован новый пользователь - '.$email.'
				   Сайт - http://zakup.asia
				 ');
				$this->email->send();
			}
    }
	
    //Удаляем почту для оповещений через ajax
    public function deleteAlertEmail(){
        $sql = "DELETE FROM `alert_email` WHERE id_alert_email = ".$this->db->escape($this->input->get('id_alert_email'));
        if ($this->db->query($sql)){
            echo 'true';
        } else echo 'false';
    }

    //Устанавливаем маску города/региона/страны
    private function city(){
        $city = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
        $a=mb_strstr($city,".",true);
        if($a) $city=$a.".";
		//Добавили
        if($city == 'zakup.' | $city == 'www.'){
             return 'country.name_country = "Казахстан"';
        }
        return  "(city.mask_sity = ".$this->db->escape($city)." OR region.mask_region = ".$this->db->escape($city).")";
    }

    //Устанавливаем маску для слайдеров города/региона/страны
    private function sliderCity(){
        $city = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
        $a=mb_strstr($city,".",true);
        if($a) $city=$a.".";
		//Добавили
		if($city == 'www.'){
            return 'country.name_country = "Казахстан"';
        }
		return   "(city.mask_sity = ".$this->db->escape($city)." OR region.mask_region = ".$this->db->escape($city)." OR country.mask_country =".$this->db->escape($city).")";
    }

    private function city2(){
        $city = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
        $a=mb_strstr($city,".",true);
        if($a) $city=$a.".";
        if($city == 'zakup.'){
            $city = '';
        }
        return $city;
    }

    public function insertAlertEmail(){
        $sql = "INSERT INTO `alert_email`(`id_company`, `company_email`) VALUES ("
            . $this->db->escape($this->input->get('id_company')) . ", " . $this->db->escape($this->input->get('company_email')) . ")";
        if ($this->db->query($sql)) {
            echo 'true';
        } else
            echo 'false';
    }

	//Пример использования: if ( !is_email($url) ) echo 'E-mail not valid';
    //Проверка емайла на валидность
    private function is_email($email) {
        return preg_match("/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)*\.([a-zA-Z]{2,6})$/", $email);
    }

    private function translit($s) {
        $s = (string) $s; // преобразуем в строковое значение
        $s = strip_tags($s); // убираем HTML-теги
        $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
        $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
        $s = trim($s); // убираем пробелы в начале и конце строки
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
        $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
        $s = str_replace(" ", "-", $s); // заменяем пробелы знаком минус
        return $s; // возвращаем результат
    }

    //проверяем уникальность url
    public function checkName($name = null){
        if ($name != null){
            $sql = "SELECT COUNT(*) AS count FROM company WHERE name_translit = ".$this->db->escape($name);
            $result = $this->db->query($sql);
            foreach( $result->result_array() as $row){
                if ($row["count"] == 1){
                    echo '<div class="css-error">Url компании не уникален</div>';
                } else {
                    $sql = "UPDATE `company` SET `name_translit`=".$this->db->escape($name)." WHERE id_company =".$this->db->escape($this->session->userdata('id_company'));
                    $this->db->query($sql);
                    echo '<div class="css-error" style="background-color: green">Url можно использовать</div>';
                }
            }
        } else {
            echo '<div class="css-error">Поле Url компании обязательно</div>';
        }
    }

}