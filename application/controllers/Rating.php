<?php

class Rating extends CI_Controller{

    private $server = null;

    //TODO один суперадмин, который может всё и возможность создавать админов, похоже будет сложная система
    //TODO проверка админ ли это или нет
    public function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url', 'cookie'));
        $this->load->library('form_validation');
        if ($_SERVER['SERVER_PORT'] != 80){
            $this->server = $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'];
        } else {
            $this->server = $_SERVER['SERVER_NAME'];
        }
    }

    //TODO возможно лучше просто вставлять в базу данных запись, если же
    //Устанавливаем рейтинг и затем считаем рейтинг компании, получается два запроса. Возможно мы должны возвращить итоговый рейтинг
    public function setRating(){
        $sql = "SELECT COUNT(*) AS `count` FROM rating WHERE id_company =".$this->db->escape($this->input->get('id_company'))." AND
        id_user = ".$this->session->userdata('id_user');
        $check = $this->db->query($sql);
        foreach ($check->result_array() as $row){
            $check = $row['count'];
        }
        if($check == 1){
            $rating1 = null;
            $sql = "UPDATE `rating` SET `company_rating` = ".$this->db->escape($this->input->get('company_rating')).
                " WHERE `id_user` = ".$this->session->userdata('id_user')." AND `id_company` = ".$this->db->escape($this->input->get('id_company'));
            $this->db->query($sql);
            //TODO запрос-вычисление сумму делим на количество и получаем рейтинг, который потом вставляем в бд компании.
            $sql = "SELECT AVG(company_rating) AS AVGrating FROM rating WHERE id_company = ".$this->db->escape($this->input->get('id_company'))
                ." ORDER BY company_rating";
            $rating = $this->db->query($sql);
            foreach($rating->result_array() as $row){
                $sql = "UPDATE `company` SET `company_rating`= ".$this->db->escape($row['AVGrating'])." WHERE `id_company` = "
                    .$this->db->escape($this->input->get('id_company'));
                $rating1 = round($row['AVGrating'], 1);
            }
            $this->db->query($sql);
            echo $rating1;
        }
        if ($check == 0){
            $rating1 = null;
            $sql = "INSERT INTO `rating`(`id_user`, `id_company`, `company_rating`) VALUES (".$this->session->userdata('id_user').",".
                $this->db->escape($this->input->get('id_company')).",".$this->db->escape($this->input->get('company_rating')).")";
            $this->db->query($sql);
            //TODO запрос-вычисление сумму делим на количество и получаем рейтинг, который потом вставляем в бд компании.
            $sql = "SELECT AVG(company_rating) AS AVGrating FROM rating WHERE id_company = ".$this->db->escape($this->input->get('id_company'))
                ." ORDER BY company_rating";
            $rating = $this->db->query($sql);
            foreach($rating->result_array() as $row){
                $sql = "UPDATE `company` SET `company_rating`= ".$this->db->escape($row['AVGrating'])." WHERE `id_company` = "
                    .$this->db->escape($this->input->get('id_company'));
                $rating1 = round($row['AVGrating'], 1);
            }
            $this->db->query($sql);
            echo $rating1;
        }
    }
}