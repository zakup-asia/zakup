<?php
class Favorites extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url', 'cookie'));
        $this->load->library('form_validation');
        if ($_SERVER['SERVER_PORT'] != 80){
            $this->server = $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'];
        } else {
            $this->server = $_SERVER['SERVER_NAME'];
        }
    }

    //Добавляем в избранное
    public function setFavorite(){
        if ($this->input->get('id_product') != null){
            $sql = 'INSERT INTO `favorites`( `id_user`, `id_product`) VALUES ('
                   .$this->db->escape($this->session->userdata('id_user')).', '
                   .$this->db->escape($this->input->get('id_product')).')';
            if($this->db->query($sql)){
                echo '<a class="favoriteProduct favoriteThis" onclick="unfavoriteProduct(\''.$this->input->get('id_product').'\')">Из избранного</a>';
            } else {
                echo 'false';
            }
        }
        if ($this->input->get('id_company') != null){
            $sql = 'INSERT INTO `favorites`( `id_user`, `id_company`) VALUES ('
                   .$this->db->escape($this->session->userdata('id_user')).', '
                   .$this->db->escape($this->input->get('id_company')).')';
            if($this->db->query($sql)){
                echo '<a class="favoriteThis" onclick="unFavorite(\''.$this->input->get('id_company').'\')">Из избранного</a>';
            } else {
                echo 'false';
            }
        }
    }

    //Убираем из избранного
    public function unsetFavorite(){
        if ($this->input->get('id_product') != null){
            $sql = 'DELETE FROM `favorites` WHERE id_user = '.$this->db->escape($this->session->userdata('id_user')).
                   ' AND id_product = '.$this->db->escape($this->input->get('id_product'));
            if($this->db->query($sql)){
                echo '<a class="favoriteProduct favoriteThis" onclick="favoriteProduct(\''.$this->input->get('id_product').'\')">В избранное</a>';
            } else {
                echo 'false';
            }
        }
        if ($this->input->get('id_company') != null){
            $sql = 'DELETE FROM `favorites` WHERE id_user = '.$this->db->escape($this->session->userdata('id_user')).
                   ' AND id_company = '.$this->db->escape($this->input->get('id_company'));
            if($this->db->query($sql)){
                echo '<a class="favoriteThis" onclick="favorite(\''.$this->input->get('id_company').'\')">В избранное</a>';
            } else {
                echo 'false';
            }
        }
    }

    //Проверяем в избранном ли это
    public function checkFavorite(){
        if($this->input->get('id_product') != null){

        }
        if ($this->input->get('id_company') != null){
            $sql = "SELECT COUNT(*) AS `count` FROM favorites WHERE id_user = ".$this->db->escape($this->session->userdata('id_user')).
                " AND id_company = ".$this->db->escape($this->input->get('id_company'));
            $object = $this->db->query($sql);
            foreach($object->result_array() as $row){
                if ($row['count'] != '0'){
                    echo '<a class="favoriteThis" onclick="unFavorite(\''.$this->input->get('id_company').'\')">Из избранного</a>';
                } else if ($row['count'] == '0') {
                    echo '<a class="favoriteThis" onclick="favorite(\''.$this->input->get('id_company').'\')">В избранное</a>';
                }
            }
        }
    }


    //Получить избранные продукты
    public function getFavoritesProducts(){
        $sql = "SELECT * FROM section_one";
        $info = $this->db->query($sql);
        $sql = "SELECT * FROM category ";
        $info2 = $this->db->query($sql);
        $sql = "SELECT * FROM subcategory ";
        $info3 = $this->db->query($sql);
        $sql = 'SELECT * FROM `favorites` LEFT OUTER JOIN product USING (id_product) LEFT OUTER JOIN images USING (id_image)
                WHERE id_product IS NOT NULL AND favorites.id_user = '
               .$this->db->escape($this->session->userdata('id_user'));
        $favorites = $this->db->query($sql);
        $searchText = null;
        $this->load->view('header', array('title' => 'Избранные продукты', 'keywords' => null, 'description' => null));
        $this->load->view('headerMenu', array('hide' => null, 'searchText' => null));
		$this->load->view('leftMenu', array('active'=>'favorites'));
        $this->load->view('favorites_products', array('favorites' => $favorites, 'info' => $info, 'info2' => $info2, 'info3' => $info3));
        $this->load->view('footer');
    }

    //Получить избранные компании
    public function getFavoritesCompanies(){
        $sql = "SELECT * FROM section_one";
        $section = $this->db->query($sql);
        $sql = "SELECT * FROM category";
        $category = $this->db->query($sql);
        $sql = "SELECT * FROM subcategory";
        $subcategory = $this->db->query($sql);
        $sql = 'SELECT * FROM `favorites` LEFT OUTER JOIN company USING (id_company)
               LEFT OUTER JOIN images USING (id_image) LEFT OUTER JOIN conditions USING (id_company) WHERE id_company IS NOT NULL
               AND favorites.id_user = '.$this->db->escape($this->session->userdata('id_user'));
        $favorites = $this->db->query($sql);
        $searchText = null;
        $this->load->view('header', array('title' => 'Избранные компании', 'keywords' => null, 'description' => null));
        $this->load->view('headerMenu', array('hide' => null, 'searchText' => null));
		$this->load->view('leftMenu', array('active'=>'favorites'));
        $this->load->view('favorites_companies', array('favorites' => $favorites, 'section' => $section, 'category' => $category, 'subcategory' => $subcategory ));
        $this->load->view('footer');
    }

    //Получить все из избранного
    public function getFavorites(){
        $sql = 'SELECT * FROM `favorites`';
        $favorites = $this->db->query($sql);
        $searchText = null;
        $this->load->view('header', array('title' => 'Избранное', 'keywords' => null, 'description' => null));
        $this->load->view('headerMenu', array('hide' => null, 'searchText' => null));
		$this->load->view('leftMenu', array('active'=>'favorites'));
        $this->load->view('favorites', array('$favorites' => $favorites));
        $this->load->view('footer');
    }
}

//TODO добавить возможность удаления из простмотра фаворитов