<?php
class StockAndNews  extends CI_Controller{

    private $server = null;

    //TODO один суперадмин, который может всё и возможность создавать админов, похоже будет сложная система
    //TODO проверка админ ли это или нет
    public function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url', 'cookie'));
        $this->load->library('form_validation');
        if ($_SERVER['SERVER_PORT'] != 80){
            $this->server = $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'];
        } else {
            $this->server = $_SERVER['SERVER_NAME'];
        }
    }

    //Добавить новость
    public function addNew(){
        if ($this->session->userdata('id_user') == null | $this->session->userdata('id_company') == null) {
            redirect('http://'.$this->server.'/user/login/', 'refresh');
        } else {
            $this->form_validation->set_rules('name_new', 'название новости', 'required');
            $this->form_validation->set_rules('description_new', 'описание новости', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header', array('title'=> 'Добавить новость', 'keywords' => null, 'description' => null));
                $this->load->view('headerMenu', array('hide' => 'hide', 'searchText' => null));
				$this->load->view('leftMenu', array('active'=>'news'));
                $this->load->view('addNew', array());
                $this->load->view('footer');
            } else {
                $sql = 'INSERT INTO `news`( `name_new`, `short_discription_new`, `description_new`, `id_image`, `id_company`) VALUES ('
                       .$this->db->escape($this->input->post('name_new')).', '.$this->db->escape($this->input->post('short_discription_new')).
                       ', '.$this->db->escape($this->input->post('description_new')).', (SELECT id_image FROM images WHERE path ='
                       .$this->db->escape($this->input->post('path')).'),'.$this->db->escape($this->input->post('id_company')).')';
                if (!$this->db->query($sql)){
                    echo 'Error database';
                }
                redirect('http://'.$this->server.'/StockAndNews/getMyNews', 'refresh');
            }
        }
    }

    //Просмотреть новость
    public function getNew(){
        $sql = 'SELECT * FROM news LEFT OUTER JOIN images USING (id_image) WHERE id_new = '.$this->db->escape($this->input->get('id_new'));
        $this->db->query($sql);
    }

    //Смотрим свои собственные новости
    public function getMyNews(){
        if($this->session->userdata('id_company') == null){
            redirect('http://'.$this->server.'/', 'refresh');
        } else {
            $sql = 'SELECT * FROM news LEFT OUTER JOIN images USING (id_image) WHERE id_company = '.$this->db->escape($this->session->userdata('id_company'));
            $news = $this->db->query($sql);
            $this->load->view('header', array('title'=> 'Мои новости', 'keywords' => null, 'description' => null));
            $this->load->view('headerMenu', array('hide' => 'hide', 'searchText' => null));
			$this->load->view('leftMenu', array('active'=>'news'));
            $this->load->view('myNews', array('news' => $news));
            $this->load->view('footer');
        }
    }

    //Отредактировать новость
    public function editNew(){
        if ($this->session->userdata('id_user') == null | $this->session->userdata('id_company') == null) {
            redirect('http://'.$this->server.'/user/login/', 'refresh');
        } else {
            $sql = "SELECT * FROM news LEFT OUTER JOIN images USING (id_image) WHERE id_new = ".$this->db->escape($this->input->get('id_new'));
            $new = $this->db->query($sql);
            $this->form_validation->set_rules('name_new', 'название продукта', 'required');
            $this->form_validation->set_rules('description_new', 'название продукта', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header', array('title'=> 'Отредактировать новость', 'keywords' => null, 'description' => null));
                $this->load->view('headerMenu', array('hide' => null, 'searchText' => null));
				$this->load->view('leftMenu', array('active'=>'news'));
                $this->load->view('editNew', array('new' => $new));
                $this->load->view('footer');
            } else {
                $sql = 'UPDATE `news` SET `name_new`='.$this->db->escape($this->input->post('name_new')).
                    ', `short_discription_new`='.$this->db->escape($this->input->post('short_discription_new')).
                    ', `description_new`='.$this->db->escape($this->input->post('description_new')).
                    ', `id_image`= '.$this->db->escape($this->input->post('id_image')).' WHERE
               `id_new`='.$this->db->escape($this->input->post('id_new'));
                if (!$this->db->query($sql)) {
                    echo 'Error database';
                }
                redirect('http://'.$this->server.'/StockAndNews/getMyNews', 'refresh');
            }
        }
    }

    //Удалить новость
    public function deleteNew(){
        $sql = 'DELETE FROM `news` WHERE `id_new`='.$this->db->escape($this->input->get('id_new'));
        $this->db->query($sql);
        redirect('http://'.$this->server.'/StockAndNews/getMyNews', 'refresh');
    }

    //Получить все новости
    public function getAllNews(){

    }

    //TODO примерно тоже, что и с новостями, только тут должна быть возможность присоединять призы, скидки и товары, к которым эта акция относится.
    //Добавить акцию
    public function addStock(){
        if ($this->session->userdata('id_user') == null | $this->session->userdata('id_company') == null) {
            redirect('http://'.$this->server.'/user/login/', 'refresh');
        } else {
            $this->form_validation->set_rules('name_stock', 'название новости', 'required');
            $this->form_validation->set_rules('description_stock', 'описание новости', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header', array('title'=> 'Добавить акцию', 'keywords' => null, 'description' => null));
                $this->load->view('headerMenu', array('hide' => 'hide', 'searchText' => null));
				$this->load->view('leftMenu', array('active'=>'stock'));
                $this->load->view('addStock', array());
                $this->load->view('footer');
            } else {
                $sql = 'INSERT INTO `stock`(`id_company`, `name_stock`, `description_stock`, `id_image`, `begin_stock`, `end_stock`, `short_description_stock`) VALUES ('
                       .$this->db->escape($this->session->userdata('id_company')).', '.$this->db->escape($this->input->post('name_stock')).
                       ', '.$this->db->escape($this->input->post('description_stock')).', (SELECT id_image FROM images WHERE path ='
                       .$this->db->escape($this->input->post('path')).'),'.$this->db->escape($this->input->post('begin_stock')).
                       ', '.$this->db->escape($this->input->post('end_stock')).', '.$this->db->escape($this->input->post('short_description_stock')).')';
                if (!$this->db->query($sql)){
                    echo 'Error database';
                }
                redirect('http://'.$this->server.'/StockAndNews/getMyStock', 'refresh');
            }
        }
    }

    //Получить акцию
    public function getStock(){

    }

    //Отредактировать акцию
    public function editStock(){
        if ($this->session->userdata('id_user') == null | $this->session->userdata('id_company') == null) {
            redirect('http://'.$this->server.'/user/login/', 'refresh');
        } else {
            $sql = "SELECT * FROM stock LEFT OUTER JOIN images USING (id_image) WHERE id_stock = ".$this->db->escape($this->input->get('id_stock'));
            $stock = $this->db->query($sql);
            $this->form_validation->set_rules('name_stock', 'название новости', 'required');
            $this->form_validation->set_rules('description_stock', 'описание новости', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('header', array('title'=> 'Отредактировать акцию', 'keywords' => null, 'description' => null));
                $this->load->view('headerMenu', array('hide' => null, 'searchText' => null));
				$this->load->view('leftMenu', array('active'=>'stock'));
                $this->load->view('editStock', array('stock' => $stock));
                $this->load->view('footer');
            } else {
                $sql = 'UPDATE `news` SET `name_new`='.$this->db->escape($this->input->post('name_new')).
                    ', `short_discription_new`='.$this->db->escape($this->input->post('short_discription_new')).
                    ', `description_new`='.$this->db->escape($this->input->post('description_new')).
                    ', `id_image`= '.$this->db->escape($this->input->post('id_image')).' WHERE
               `id_new`='.$this->db->escape($this->input->post('id_new'));
                if (!$this->db->query($sql)) {
                    echo 'Error database';
                }
                redirect('http://'.$this->server.'/StockAndNews/getMyNews', 'refresh');
            }
        }
    }

    //Удалить акцию
    public function deleteStock(){
        $sql = 'DELETE FROM `stock` WHERE `id_stock`='.$this->db->escape($this->input->get('id_stock'));
        $this->db->query($sql);
        redirect('http://'.$this->server.'/StockAndNews/getMyStock', 'refresh');
    }

    //Получаем свои акции
    public function getMyStock(){
        if($this->session->userdata('id_company') == null){
            redirect('http://'.$this->server.'/', 'refresh');
        } else {
            $sql = 'SELECT * FROM stock LEFT OUTER JOIN images USING (id_image) WHERE id_company = '.$this->db->escape($this->session->userdata('id_company'));
            $stock = $this->db->query($sql);
            $this->load->view('header', array('title'=> 'Мои акции', 'keywords' => null, 'description' => null));
            $this->load->view('headerMenu', array('hide' => 'hide', 'searchText' => null));
			$this->load->view('leftMenu', array('active'=>'stock'));
            $this->load->view('myStock', array('stock' => $stock));
            $this->load->view('footer');
        }
    }

    //Получить все акции
    public function getAllStock(){

    }
}