<?php
class Suppliers extends CI_Controller{

    private $server = null;

    //TODO меньше запросов, меньше.
    public function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url', 'cookie'));
        $this->load->library('form_validation');
        if ($_SERVER['SERVER_PORT'] != 80){
            $this->server = $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'];
        } else {
            $this->server = $_SERVER['SERVER_NAME'];
        }
    }

    //Получить всех поставщиков
    public function getAllSuppliers(){
        $city = $this->city();
        $sql = "SELECT * FROM section_one ORDER BY id_section_one";
        $section = $this->db->query($sql);
        $sql = "SELECT * FROM category ORDER BY id_category";
        $category = $this->db->query($sql);
        $sql = "SELECT * FROM subcategory ORDER BY id_subcategory";
        $subcategory = $this->db->query($sql);
        $sql = "SELECT company.id_company, company.name, company.short_description, company.id_image, images.path,
               conditions.payment_methods, conditions.min_amount, conditions.shipping, city.mask_sity, region.mask_region,
               contact_information.id_region, contact_information.id_city, contact_information.id_country FROM company LEFT OUTER
               JOIN images USING (id_image) LEFT OUTER JOIN conditions USING (id_company) LEFT OUTER JOIN contact_information USING
               (id_company) LEFT OUTER JOIN country USING (id_country) LEFT OUTER JOIN region USING (id_region) LEFT OUTER JOIN city
               USING (id_city) WHERE show_company = TRUE AND ".$city." ORDER BY id_company";
        $suppliers = $this->db->query($sql);
        $sql = "SELECT COUNT(*) AS `count` FROM company LEFT OUTER JOIN contact_information USING (id_company)LEFT OUTER JOIN
               country USING (id_country) LEFT OUTER JOIN region USING (id_region) LEFT OUTER JOIN city USING (id_city) WHERE "
               .$city." AND company.show_company = TRUE ORDER BY id_company";
        $count = $this->db->query($sql);
        $this->load->view('header', array('title'=> 'Поставщики', 'keywords' => null, 'description' => null));
        $this->load->view('headerMenu', array('hide' => 'hide', 'searchText' => null));
        $this->load->view('suppliers', array('suppliers' => $suppliers, 'count' => $count, 'section' => $section, 'category' => $category, 'subcategory' => $subcategory ));
        $this->load->view('footer');
    }
	
	
	public function getSuppliersPrice($suppliers_id) {
		
		error_reporting(0);
		$city = $this->city();
		
		require_once("PHPExcel/Classes/PHPExcel.php");
		
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')->setSize(12);
        $objRichText = new PHPExcel_RichText();
        $objRichText->createText("Прайс компании ".$company[0]['name']."");
        $objRichText = new PHPExcel_RichText();
		
		
        $sql = "SELECT * FROM company WHERE id_company = ".$this->db->escape($suppliers_id);
        $company0 = $this->db->query($sql);	
	    $company = $company0->result_array();	


        $objRichText->createText("Прайс компании ".$company[0]['name']."");
		
        $city = $this->city();


        $sql = "SELECT * FROM product LEFT OUTER JOIN images USING (id_image) LEFT OUTER JOIN company USING (id_company) WHERE id_company = ".$this->db->escape($suppliers_id)." AND deleted = FALSE ORDER BY `position`";
        $products0 = $this->db->query($sql);		
	    $products = $products0->result_array();

            $sql = "SELECT icon_currency FROM company LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN country
                   USING (id_country) LEFT OUTER JOIN currency USING (id_currency) WHERE id_company =".$this->db->escape($suppliers_id);
            $currency = $this->db->query($sql);		
		
		$icon_currency = null;
		foreach($currency->result_array() as $row2){

			if($row2['icon_currency'] != null){
				$icon_currency = array();
				$icon_currency['icon_currency'] = $row2['icon_currency'];
			}
		}		
		

			
$style_header = array(
	//Шрифт
	'font'=>array(
		'bold' => true,
		'size' => 12
	),
//Выравнивание
	'alignment' => array(
		'horizontal' => PHPExcel_STYLE_ALIGNMENT::HORIZONTAL_CENTER,
		'vertical' => PHPExcel_STYLE_ALIGNMENT::VERTICAL_CENTER,
	)


);

$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($style_header);			
			
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);			
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);			
			
			$objPHPExcel->getActiveSheet()->setCellValue('A1','Изображение');
			$objPHPExcel->getActiveSheet()->setCellValue('B1','Наименование товара');
			$objPHPExcel->getActiveSheet()->setCellValue('C1','Цена');
			$objPHPExcel->getActiveSheet()->setCellValue('D1','Валюта');		
		
		 if ($products != null) {
			$i = 2;
            foreach ($products0->result_array() as $row) {


			$objDrawing = new PHPExcel_Worksheet_Drawing();
			
			$objDrawing->setName(''.$row['name_product'].'');
			$objDrawing->setDescription(''.$row['name_product'].'');
			$objDrawing->setPath('images/upload/'.$row['path'].'');
			
		
			// set resize to false first

			
			// set width later
			
			
			$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			
			$objDrawing->setCoordinates('A'.$i);
			$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(100);
			$objDrawing->setOffsetX(10);
			$objDrawing->setOffsetY(20);
			
			$objDrawing->setResizeProportional(true);
			$objDrawing->setWidth(50);
			
			$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
						

			$objPHPExcel->getActiveSheet()->setCellValue('B'.$i,$row['name_product']);
			$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$i,$row['price']);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->setCellValue('D'.$i,$icon_currency['icon_currency']);
			$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);	
			$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);			
									
			

									
            $i++;
		 }
	}
		

	
		
        $filename = "products_price_of_".$suppliers_id.".xls";
        $objPHPExcel->getActiveSheet()->setTitle('Прайс компании '.$company[0]['name'].'');
        $objPHPExcel->setActiveSheetIndex(0);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;filename=\"".$filename."\";");
        header("Cache-Control: max-age=0");
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
	
	}

    //TODO подумать как узнавать есть у нас эта компания в избранном или нет
    //Получить данные о конкртеном поставщике
    public function getSupplier($id_company, $searchText = null){
        if($id_company == null){
            echo 'Не ввели данные';
        } else {
            $sql = "SELECT * FROM section_one ORDER BY id_section_one";
            $section = $this->db->query($sql);
            $sql = "SELECT * FROM category ORDER BY id_category";
            $category = $this->db->query($sql);
            $sql = "SELECT * FROM subcategory ORDER BY id_subcategory";
            $subcategory = $this->db->query($sql);
            if ($this->session->userdata('id_user') != null ){
                $sql = "SELECT *, (SELECT COUNT(*) FROM favorites WHERE id_company = ".$this->db->escape($id_company).
                       " AND favorites.id_user = ".$this->db->escape($this->session->userdata('id_user')).")
                       FROM company LEFT OUTER JOIN conditions USING(id_company) LEFT OUTER JOIN images USING (id_image)
                       LEFT OUTER JOIN requisites USING (id_company) LEFT OUTER JOIN contact_information USING (id_company)
                       LEFT OUTER JOIN city USING (id_city) LEFT OUTER JOIN country USING (id_country) WHERE id_company = ".$this->db->escape($id_company);
                $company = $this->db->query($sql);
            } else {
                $sql = "SELECT * FROM company LEFT OUTER JOIN conditions USING(id_company) LEFT OUTER JOIN images USING (id_image)
                   LEFT OUTER JOIN requisites USING (id_company) LEFT OUTER JOIN contact_information USING (id_company)
                   LEFT OUTER JOIN city USING (id_city) LEFT OUTER JOIN country USING (id_country)
                   WHERE id_company = ".$this->db->escape($id_company);
                $company = $this->db->query($sql);
            }
            $sql = "SELECT * FROM product LEFT OUTER JOIN images USING (id_image) LEFT OUTER JOIN company USING (id_company) WHERE id_company = ".$this->db->escape($id_company)." AND deleted = FALSE ORDER BY `position`";
            $product = $this->db->query($sql);
            $sql = "SELECT icon_currency FROM company LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN country
                   USING (id_country) LEFT OUTER JOIN currency USING (id_currency) WHERE id_company =".$this->db->escape($id_company);
            $currency = $this->db->query($sql);
            $sql = "SELECT COUNT(*) AS `count` FROM product WHERE id_company = ".$this->db->escape($id_company);
            $count_product = $this->db->query($sql);
            $sql = "SELECT name_region FROM company LEFT OUTER JOIN contact_information USING (id_company)LEFT OUTER JOIN region
                   USING (id_region) WHERE id_company = ".$this->db->escape($id_company);
            $region = $this->db->query($sql);
			$sql = "SELECT company.name, company.id_company, stock.name_stock, stock.description_stock, stock.id_image, stock.begin_stock, stock.end_stock, images.path, (SELECT COUNT(*) AS `count` FROM stock WHERE id_company = ".$this->db->escape($id_company)." ORDER BY id_company) AS `count` FROM company LEFT OUTER JOIN images USING (id_image)  LEFT OUTER JOIN stock USING (id_company)  WHERE id_company = ".$this->db->escape($id_company)."  ORDER BY id_company";
            $stock = $this->db->query($sql);
            $sql = "SELECT company.name, company.id_company, news.name_new, news.description_new, news.id_image, news.data_new, images.path, (SELECT COUNT(*) AS `count` FROM news WHERE id_company = ".$this->db->escape($id_company)." ORDER BY id_company) AS `count` FROM company LEFT OUTER JOIN images USING (id_image)  LEFT OUTER JOIN news USING (id_company)  WHERE id_company = ".$this->db->escape($id_company)."  ORDER BY news.data_new";
            $news = $this->db->query($sql);
            $this->load->view('header', array('keywords' => null, 'description' => null));
            $this->load->view('headerMenu', array('hide' => 'hide', 'searchText' => null));
            $this->load->view('company', array( 'suppliers' => $company, 'products' => $product, 'currency' => $currency, 'region' => $region, 'section' => $section, 'category' => $category, 'subcategory' => $subcategory, 'stock' => $stock, 'news' => $news, 'count_product' => $count_product));
            $this->load->view('footer');
        }
    }

    //получаем компанию используя транслит, также используется файл application/config/routes.php
    public function getSupplierByTranslit($nameTranslit){
        if($nameTranslit == null){
            echo 'Не ввели данные';
        } else {
            $sql = "SELECT * FROM section_one ORDER BY id_section_one";
            $section = $this->db->query($sql);
            $sql = "SELECT * FROM category ORDER BY id_category";
            $category = $this->db->query($sql);
            $sql = "SELECT * FROM subcategory ORDER BY id_subcategory";
            $subcategory = $this->db->query($sql);
            if ($this->session->userdata('id_user') != null ){
                $sql = "SELECT *, (SELECT COUNT(*) FROM favorites WHERE name_translit = ".$this->db->escape($nameTranslit).
                    " AND favorites.id_user = ".$this->db->escape($this->session->userdata('id_user')).") AS count
                       FROM company LEFT OUTER JOIN conditions USING(id_company) LEFT OUTER JOIN images USING (id_image)
                       LEFT OUTER JOIN requisites USING (id_company) LEFT OUTER JOIN contact_information USING (id_company)
                       LEFT OUTER JOIN city USING (id_city) LEFT OUTER JOIN country USING (id_country) WHERE name_translit = ".$this->db->escape($nameTranslit);
                $company = $this->db->query($sql);
            } else {
                $sql = "SELECT * FROM company LEFT OUTER JOIN conditions USING(id_company) LEFT OUTER JOIN images USING (id_image)
                   LEFT OUTER JOIN requisites USING (id_company) LEFT OUTER JOIN contact_information USING (id_company)
                   LEFT OUTER JOIN city USING (id_city) LEFT OUTER JOIN country USING (id_country)
                   WHERE name_translit = ".$this->db->escape($nameTranslit);
                $company = $this->db->query($sql);
            }
            $sql = "SELECT * FROM product LEFT OUTER JOIN images USING (id_image) LEFT OUTER JOIN company USING (id_company) WHERE name_translit = ".$this->db->escape($nameTranslit)." AND deleted = FALSE ORDER BY `position`";
            $product = $this->db->query($sql);
            $sql = "SELECT icon_currency FROM company LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN country
                   USING (id_country) LEFT OUTER JOIN currency USING (id_currency) WHERE name_translit =".$this->db->escape($nameTranslit);
            $currency = $this->db->query($sql);
            $sql = "SELECT COUNT(*) AS `count` FROM product LEFT OUTER JOIN company USING(id_company) WHERE name_translit = ".$this->db->escape($nameTranslit);
            $count_product = $this->db->query($sql);
            $sql = "SELECT name_region FROM company LEFT OUTER JOIN contact_information USING (id_company)LEFT OUTER JOIN region
                   USING (id_region) WHERE name_translit = ".$this->db->escape($nameTranslit);
            $region = $this->db->query($sql);
			$sql = "SELECT company.name_translit, company.name, company.id_company, stock.name_stock, stock.description_stock, stock.id_image, stock.begin_stock, stock.end_stock, images.path, (SELECT COUNT(*) AS `count` FROM stock LEFT OUTER JOIN company USING(id_company) WHERE name_translit =".$this->db->escape($nameTranslit)." ORDER BY id_company) AS `count` FROM company LEFT OUTER JOIN images USING (id_image)  LEFT OUTER JOIN stock USING (id_company) WHERE name_translit =".$this->db->escape($nameTranslit)."  ORDER BY id_company";
            $stock = $this->db->query($sql);
            $sql = "SELECT company.name_translit, company.name, company.id_company, news.name_new, news.description_new, news.id_image, news.data_new, images.path, (SELECT COUNT(*) AS `count` FROM news LEFT OUTER JOIN company USING(id_company) WHERE name_translit =".$this->db->escape($nameTranslit)." ORDER BY id_company) AS `count` FROM company LEFT OUTER JOIN images USING (id_image)  LEFT OUTER JOIN news USING (id_company)  WHERE name_translit =".$this->db->escape($nameTranslit)." ORDER BY news.data_new";
            $news = $this->db->query($sql);
			/*
            $sql = "SELECT company.name, company.id_company, stock.name_stock, stock.description_stock, stock.id_image, stock.begin_stock, stock.end_stock, images.path, (SELECT COUNT(*) AS `count` FROM stock WHERE name_translit = ".$this->db->escape($nameTranslit)." ORDER BY id_company) AS `count` FROM company LEFT OUTER JOIN images USING (id_image)  LEFT OUTER JOIN stock USING (id_company)  WHERE name_translit = ".$this->db->escape($nameTranslit)."  ORDER BY id_company";
            $stock = $this->db->query($sql);
            $sql = "SELECT company.name, company.id_company, news.name_new, news.description_new, news.id_image, news.data_new, images.path, (SELECT COUNT(*) AS `count` FROM news WHERE name_translit = ".$this->db->escape($nameTranslit)." ORDER BY id_company) AS `count` FROM company LEFT OUTER JOIN images USING (id_image)  LEFT OUTER JOIN news USING (id_company)  WHERE name_translit = ".$this->db->escape($nameTranslit)."  ORDER BY news.data_new";
            $news = $this->db->query($sql);
			*/
            //TODO вынести в отдельную функцию
            $search = array();
            $replace = array();
            $nameCompany = null;
            $description = null;
            $canonical = null;
            foreach($company->result_array() as $row){
                if(isset($row['description'])) $description = $row['description'];
                if(isset($row['name'])) {
                    $nameCompany = $row['name'];
                    array_push($replace, $row['name']);
                    array_push($search, 'ТОО Resto brothers');
                }
                if(isset($row['name_city'])) {
                    array_push($replace, $row['name_city']);
                    array_push($search, 'Караганда');
                }
                if(isset($row['name_translit'])) $canonical = 'http://'.$this->server.'/'.$row['name_translit'];
            }
            $keywords = "ТОО Resto brothers, Караганда, ТОО Resto brothers Караганда, Дистрибьютор, производитель, сайт поставщика, оптовый поставщик, компания ТОО Resto brothers, фирма ТОО Resto brothers, контакты ТОО Resto brothers, телефон ТОО Resto brothers";
            $keywords = str_replace($search, $replace, $keywords);

            $this->load->view('header', array('title'=> $nameCompany, 'keywords' => $keywords, 'description' => $description, 'canonical' => $canonical));
            $this->load->view('headerMenu', array('hide' => 'hide', 'searchText' => null));
            $this->load->view('company', array( 'suppliers' => $company, 'products' => $product, 'currency' => $currency, 'region' => $region, 'section' => $section, 'category' => $category, 'subcategory' => $subcategory, 'stock' => $stock, 'news' => $news, 'count_product' => $count_product));
            $this->load->view('footer');
        }
    }

    //Получить количество
    private function getCount(){
        $sql = "SELECT COUNT(*) AS `count` FROM `images`";
        $object = $this->db->query($sql);
        foreach ($object->result_array() as $row){
            return $row['count'];
        }
    }

    //TODO даннные функции доступны только админу теперь нужно сделать дополнительный where, чтобы проверять поле show
    //Скрыть компанию
    public function hideSupplier($id_company){
        if($this->session->userdata('admin') != null) {
            $sql = "UPDATE `company` SET `show_company`= FALSE WHERE `id_company`=".$this->db->escape($id_company);
            $this->db->query($sql);
            redirect('http://'.$this->server.'/index.php/AdminPanel/getCompanies', 'refresh');
        }
    }

    //Показать компанию
    public function showSupplier($id_company){
        if($this->session->userdata('admin') != null) {
            $sql = "UPDATE `company` SET `show_company`= TRUE WHERE `id_company`=".$this->db->escape($id_company);
            $this->db->query($sql);
			$this->emailAcceptSupplier($this->input->get('email'), $this->input->get('name_translit'));
            redirect('http://'.$this->server.'/index.php/AdminPanel/getCompanies', 'refresh');
        }
    }

    //TODO

    //Скрыть компанию аджакс
    public function hideSupplierAjax($id_company){
        if($this->session->userdata('admin') != null) {
            $sql = "UPDATE `company` SET `show_company`= FALSE WHERE `id_company`=".$this->db->escape($id_company);
            if($this->db->query($sql)){
                echo 'Нет';
            }
        }
    }

    //Показать компанию аджакс
    public function showSupplierAjax($id_company){
        if($this->session->userdata('admin') != null) {
            $sql = "UPDATE `company` SET `show_company`= TRUE WHERE `id_company`=".$this->db->escape($id_company);
            if($this->db->query($sql)){
                echo 'Да';
            }
        }
    }

    //Удаляем компанию
    public function deleteSupplier(){
        if($this->session->userdata('admin') != null) {
            $sql = "DELETE FROM `company` WHERE `id_company`=".$this->db->escape($this->input->get('id_company'));
            $this->db->query($sql);
            redirect('http://'.$this->server.'/index.php/AdminPanel/getCompanies', 'refresh');
        }
    }

    private function city(){
        $city = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
        $a=mb_strstr($city,".",true);
        if($a) $city=$a.".";
        if($city == 'zakup.' | $city == 'www.'){
             return 'country.name_country = "Казахстан"';
        }
        return  "(city.mask_sity = ".$this->db->escape($city)." OR region.mask_region = ".$this->db->escape($city).")";
    }
	
	 public function emailAcceptSupplier($email, $name_translit){
        $this->load->library('email');
        //Задает адрес электронной почты и имя лица-отправителя:
        $this->email->from('info@zakup.asia', 'Администрация');
        //Устанавливает адрес для ответа, если не была указана информация в функции "from". Пример:
        $this->email->reply_to('info@zakup.asia', 'Администрация');
        //Задает адрес(а) получателя (получателей). Может быть указан один адрес электронной почты, список через запятую или массив:
        $this->email->to($email);
        $this->email->subject('Ваша компания прошла модерацию');
        //Устанавливает текст сообщения
        $this->email->message('
           Поздравляем, ваша компания прошла модерацию и доступна на сайте по ссылке - http://zakup.asia/'.$name_translit.'
         ');
        $this->email->send();
    }

    private function translit($s) {
        $s = (string) $s; // преобразуем в строковое значение
        $s = strip_tags($s); // убираем HTML-теги
        $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
        $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
        $s = trim($s); // убираем пробелы в начале и конце строки
        $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
        $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
        $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
        $s = str_replace(" ", "-", $s); // заменяем пробелы знаком минус
        return $s; // возвращаем результат
    }

    private function forSuppliers(){
        $nameTranslit = null;
        $sql = "SELECT * FROM company LEFT OUTER
               JOIN images USING (id_image) LEFT OUTER JOIN conditions USING (id_company) LEFT OUTER JOIN contact_information USING
               (id_company) LEFT OUTER JOIN country USING (id_country) LEFT OUTER JOIN region USING (id_region) LEFT OUTER JOIN city
               USING (id_city) ORDER BY id_company";
        $suppliers = $this->db->query($sql);
        foreach ($suppliers->result_array() as $row){
            if ($row['name'] != null){
                $nameTranslit = $this->translit($row['name']);
                $sql = "UPDATE `company` SET `name_translit`=".$this->db->escape($nameTranslit)." WHERE `id_company`=".$this->db->escape($row['id_company']);
                $this->db->query($sql);
            }
        }
    }
}