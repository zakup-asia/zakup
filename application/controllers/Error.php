<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Error extends CI_Controller{
    private $server = null;

    public function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url', 'cookie'));
        $this->load->library('form_validation');
        if ($_SERVER['SERVER_PORT'] != 80){
            $this->server = $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'];
        } else {
            $this->server = $_SERVER['SERVER_NAME'];
        }
    }

    private function city(){
        $city = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
        $a=mb_strstr($city,".",true);
        if($a) $city=$a.".";
        if($city == 'zakup.'){
             return 'country.name_country = "Казахстан"';
        }
        return   "city.mask_sity = ".$this->db->escape($city);
    }

    function error_404(){
            $city = $this->city();
            $sql = "SELECT * FROM product LEFT OUTER JOIN images USING (id_image) LEFT OUTER JOIN company USING (id_company)
                   LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN city USING (id_city) LEFT OUTER JOIN country USING (id_country)
                   WHERE ".$city." AND show_company = TRUE ORDER BY id_product LIMIT 0,8 ";
            $products = $this->db->query($sql);
            $sql = "SELECT * FROM company LEFT OUTER JOIN images USING (id_image) LEFT OUTER JOIN conditions USING (id_company)
                   LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN city USING (id_city) LEFT OUTER JOIN country USING (id_country)
                   WHERE show_company = TRUE AND ".$city." ORDER BY id_company LIMIT 0,8 ";
            $companies = $this->db->query($sql);
            $sql = "SELECT * FROM news LEFT OUTER JOIN images USING (id_image) LEFT OUTER JOIN company USING (id_company)
                   LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN city USING (id_city) LEFT OUTER JOIN country USING (id_country) WHERE ".$city." AND show_company = TRUE  ORDER BY id_company LIMIT 0,8 ";
            $news = $this->db->query($sql);
            $sql = "SELECT * FROM stock LEFT OUTER JOIN images USING (id_image) LEFT OUTER JOIN company USING (id_company)
                   LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN city USING (id_city) LEFT OUTER JOIN country USING (id_country) WHERE ".$city." AND show_company = TRUE ORDER BY id_company LIMIT 0,8 ";
            $stock = $this->db->query($sql);
            $sql = "SELECT * FROM section_one ORDER BY id_section_one";
            $section = $this->db->query($sql);
            $sql = "SELECT * FROM category ORDER BY id_category";
            $category = $this->db->query($sql);
            $sql = "SELECT * FROM subcategory ORDER BY id_subcategory";
            $subcategory = $this->db->query($sql);
            $this->load->view('header');
            $this->load->view('headerMenu', array('hide' => null, 'searchText' => null));
            $this->load->view('404',array('products'=> $products, 'companies' => $companies, 'section' => $section, 'category' => $category, 'subcategory' => $subcategory, 'news' => $news, 'stock' => $stock));
            $this->load->view('footer');

        @$this->output->set_status_header('404');

        $this->data['title'] = "Страница не найдена";
        $this->data['content'] = '<h1>Страница не найдена.</h1> Посмотрите существующие страницы в меню, или перейдите <a href="http://'.$this->server.'">на главную.</a>';

    }
}