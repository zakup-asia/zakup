<?php
class Cart extends CI_Controller{

    private $server = null;
    private $sendCart = null;

    //TODO уменьшить количество запросов
    public function __construct(){
        parent::__construct();
        $this->load->helper(array('form', 'url', 'cookie'));
        $this->load->library('form_validation');
        $cookie = get_cookie('id');
        if ($cookie == null){
            $cookie = array(
                'name'   => 'id',
                'value'  => $this->session->userdata('__ci_last_regenerate'),
                'expire' => '604800'
            );
            set_cookie($cookie);
        } else {
            //Вот тут обновляем.
        }
        if ($_SERVER['SERVER_PORT'] != 80){
            $this->server = $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'];
        } else {
            $this->server = $_SERVER['SERVER_NAME'];
        }
    }

    //Какие есть варианты?
    // 1 Мы используем ajax для работы с корзиной. Добавление, удаление товара, обновление всей корзины и т.д.
    // 2 Мы используем стандартные средства, но к сожалению корзина - устаревший элемент и это не самая лучшая идея.
    // 3 Мы пишем свою корзину используя сессии, вот только насколько это будет правильно? Хороший вопрос.
    // Есть ли ещё хорошие варианты? Хороший вопрос, точно прям сейчас сказать не смогу.
    // Такс идея у каждого заказа есть id, во время одной сессии должно быть какое-то поле, которое будет определять, что всё это
    // относится к одному заказу.

    //Получаем корзину
    public function getCart(){
        if ($this->session->userdata('id_user') == null){
            $cookie = get_cookie('id');
        } else {
            $cookie = $this->session->userdata('id_user');
        }
        $sql = "SELECT * FROM cart LEFT OUTER JOIN company USING (id_company) LEFT OUTER JOIN contact_information USING
               (id_company) LEFT OUTER JOIN country USING (id_country) LEFT OUTER JOIN currency USING (id_currency) LEFT OUTER JOIN
               product USING (id_product)WHERE user = ".$this->db->escape($cookie)." AND id_order is NULL  ORDER BY cart.id_company;";
        $object = $this->db->query($sql);
        $allCart = array();
        $productCart = array();
        foreach($object ->result_array() as $notRow){
            $productCart['id_cart'] = $notRow['id_cart'];
            $productCart['id_product'] = $notRow['id_product'];
            $productCart['name_product'] = $notRow['name_product'];
            $productCart['id_company'] = $notRow['id_company'];
            $productCart['name'] = $notRow['name'];
            $productCart['icon_currency'] = $notRow['icon_currency'];
            $productCart['price'] = $notRow['price'];
            $productCart['price_cart'] = $notRow['price_cart'];
            $productCart['number_of_products'] = $notRow['number_of_products'];
            array_push($allCart,$productCart);
        }
        print json_encode($allCart);
    }

    //Проверяем есть ли товер у нашего пользователя, если он его уже добавлял увеличиваем количество.
    //Возможно было бы правильно тут, при первом добавлении устанавливать ид заказа.
    //Добовляем товар в корзину
    public function addToCart(){
        if ($this->session->userdata('id_user') == null){
            $cookie = get_cookie('id');
        } else {
            $cookie = $this->session->userdata('id_user');
        }
        $sql = "INSERT INTO `cart`(`user`, `id_product`, `id_company`, `price_cart`) VALUES (".
               $this->db->escape($cookie).", ".$this->db->escape($this->input->get('id_product')).", ".
               $this->db->escape($this->input->get('id_company')).",".(string)$this->db->escape($this->input->get('price')).")";
        if($this->db->query($sql)){
            echo 'true';
        } else echo 'false';
    }

    //Точнее состояние какого-то продукта в корзине.
    //Обновляем корзину
    public function updateCart(){
        if ($this->session->userdata('id_user') == null){
            $cookie = get_cookie('id');
        } else {
            $cookie = $this->session->userdata('id_user');
        }
        $sql = "UPDATE `cart` SET `user` = ".$this->db->escape($this->input->post()).",`id_product` = "
               .$this->db->escape($this->input->post()).",`number_of_products` = "
               .$this->db->escape($this->input->post()).",`id_order` = ".$this->db->escape($this->input->post()).",`id_company`
               = ".$this->db->escape($this->input->post()).",`price_cart` = ".$this->db->escape($this->input->post()).
               " WHERE `id_cart` = ".$this->db->escape($this->input->get('id_cart'));
        $this->db->query($sql);
    }

    //TODO как вытаскивать id_cart
    //Обновляем количество продуктов.
    public function updateProductNumber(){
        $sql = "UPDATE cart LEFT OUTER JOIN product USING (id_product) SET  `price_cart`= (price*"
               .$this->db->escape($this->input->get('number_of_products'))."), `number_of_products` = "
               .$this->db->escape($this->input->get('number_of_products'))." WHERE id_cart = "
               .$this->db->escape($this->input->get('id_cart'));
        $this->db->query($sql);
        echo json_encode($this->input->get('number_of_products'));
    }

    //Единиственное отличие - отсутствие редиректа, возможно возращение каких-то значений в дальнейшем.
    //Удаляем товар из корзины с помощью аджакса
    public function deleteFromCartAjax(){
        if ($this->session->userdata('id_user') == null){
            $cookie = get_cookie('id');
        } else {
            $cookie = $this->session->userdata('id_user');
        }
        $sql = "DELETE FROM `cart` WHERE user = ".$this->db->escape($cookie)." AND id_cart = "
            .$this->db->escape($this->input->get('id_cart'));
        if ($this->db->query($sql)){
            echo 'true';
        } else echo 'false';
    }

    //Удаляем товар из корзины
    public function deleteFromCart(){
        if ($this->session->userdata('id_user') == null){
            $cookie = get_cookie('id');
        } else {
            $cookie = $this->session->userdata('id_user');
        }
        $sql = "DELETE FROM `cart` WHERE user = ".$this->db->escape($cookie)." AND id_cart = "
               .$this->db->escape($this->input->get('id_cart'));
        $this->db->query($sql);
        redirect('http://'.$this->server.'/cart/sendOrder/'.$this->input->get('id_user'), 'refresh');
    }

    //Сейчас надо обдумать вот этот момент, это не самое лёгкое, что было и придётся попотеть. Сейчас тебе мешает то, что после удаления
    // товара фирма всё-равно остаётся и необходимо как-то либо удалять её, либо ещё что-то с ней делать. Это неприятно хотябы потому,
    // что иначе можно наделать пустых заказов или ещё чего и отправлять их фирмам. Они же явно довольны этим не будут. Да и очень заметно
    // это, так что надо подумать, надо подумать.
    // Тебе так и так надо будет удалять фирмы, возможно это твой шанс.
    // Возможно при выводе сделать какое-то скрытое поле, которое будет увеличиваться, когда товар добавляется, при удалении товара оно
    // будет уменьшаться и если это поле становится равно 0, значит удаляем элемент компании.

    //Мы можем с помощью одного запроса вставлять в базу несколько записей, посмотри дамп баз данных, там используется эта фича
    //таким образом мы сможем разделять заказы по фирмам.
    //получается вот так INSERT INTO `cart` (`id_cart`, `user`, `id_product`, `number_of_products`, `id_order`, `id_company`, `price_cart`) VALUES
    //(21, 3, 10, 1, NULL, 6, NULL),
    //(22, 3, 23, 1, NULL, 6, NULL);
    //Тут мы можем установить дату для записей с помощью условия WHERE id_cart >= и тут первый ид
    //TODO тут мы должны устанавливать дату и по ней уникально определять каждый заказ
    //TODO нужно подумать как мы будем получать ид заказа
    //TODO по идее при отправке заказа, мы должны подключиться к сервлету и отправить запись фирме, отправить запись в бд.
    //TODO Получить количество уникальных поставщиков
    //Отправляем заказ
    public  function sendOrder($id_user = null){
        $cart = null;
        $user= null;
        if ($this->session->userdata('id_user') == null){
            $cookie = get_cookie('id');
            $user = null;
        } else {
            $cookie = $this->session->userdata('id_user');
            $sql = "SELECT * FROM `user` LEFT OUTER JOIN company USING (id_user) LEFT OUTER JOIN contact_information USING (id_company)
                   WHERE id_user = ".$this->db->escape($this->session->userdata('id_user'))." ORDER BY id_user";
            $user = $this->db->query($sql);
        }
        $this->form_validation->set_rules('client_phone', 'телефон', 'required');
        if ($this->form_validation->run() == FALSE) {
            $sql = "SELECT * FROM section_one ORDER BY id_section_one";
            $section = $this->db->query($sql);
            $sql = "SELECT * FROM category ORDER BY id_category";
            $category = $this->db->query($sql);
            $sql = "SELECT * FROM subcategory ORDER BY id_subcategory";
            $subcategory = $this->db->query($sql);
            $sql = "SELECT COUNT(*) AS `count` FROM `cart` WHERE user = ".$this->db->escape($cookie)." AND id_order is NULL";
            $count = $this->db->query($sql);
            $sql = "SELECT COUNT(DISTINCT id_company) AS `count` FROM `cart` WHERE user = ".$this->db->escape($cookie);
            $company_count = $this->db->query($sql);
            $sql = "SELECT  SUM(price_cart) AS SUMMA FROM cart WHERE user = ".$this->db->escape($cookie)." AND id_order is NULL";
            $summa = $this->db->query($sql);
            //SUM(price) AS SUMMA FROM
            $sql = "SELECT * FROM cart LEFT OUTER JOIN company USING (id_company) LEFT OUTER JOIN contact_information USING
                   (id_company) LEFT OUTER JOIN country USING (id_country) LEFT OUTER JOIN currency USING (id_currency) LEFT OUTER JOIN
                   product USING (id_product) LEFT OUTER JOIN `user` USING (id_user)WHERE user = ".$this->db->escape($cookie)." AND
                   id_order is NULL ORDER BY cart.id_company;";
            $cart = $this->db->query($sql);
            $this->sendCart = $cart;
            $this->load->view('header', array('keywords' => null, 'description' => null, 'title' => 'Корзина'));
            $this->load->view('headerMenu', array('hide' => 'hide', 'searchText' => null));
            $this->load->view('cart', array('cart' => $cart, 'count' => $count, 'company_count' => $company_count, 'summa' => $summa,'section' => $section, 'category' => $category, 'subcategory' => $subcategory, 'user' => $user));
            $this->load->view('footer');
        } else {
            $sql = 'INSERT INTO `orders`(`user`, `address`, `data_shipping`, `comment`, `fio`, `phone`, `email` , `order_date`)
                   VALUES ('.$this->db->escape($cookie).','.$this->db->escape($this->input->post('address')).','
                   .$this->db->escape($this->input->post('data_shipping')).',' .$this->db->escape($this->input->post('comment')).','
                   .$this->db->escape($this->input->post('client_name')).','.$this->db->escape($this->input->post('client_phone')).','
                   .$this->db->escape($this->input->post('client_email')).', '.$this->db->escape($this->input->post('order_date')).')';
            $this->db->query($sql);
            //TODO ещё добавить ид пользователя, кроме поля order_date т.к. одно поле не особо уникально
            $sql = 'UPDATE `cart` SET `id_order`= (SELECT id_order FROM orders WHERE order_date = '
                   .$this->db->escape($this->input->post('order_date')).') WHERE user ='.$this->db->escape($cookie).
                   " AND id_order is NULL";
            $this->db->query($sql);
            //Отлично, теперь осталось только придумать как это рассылать по компаниям.
            $sql = 'SELECT *, (SELECT  SUM(price_cart) AS SUMMA FROM cart LEFT OUTER JOIN orders USING (id_order) WHERE orders.id_order
                   =(SELECT id_order FROM orders WHERE order_date = '.$this->db->escape($this->input->post('order_date'))
                   .')) as SUMMA FROM cart LEFT OUTER JOIN orders USING (id_order) LEFT OUTER JOIN company USING (id_company)
                   LEFT OUTER JOIN conditions USING (id_company) LEFT OUTER JOIN alert_email USING (id_company)  WHERE orders.id_order
                   =(SELECT id_order FROM orders WHERE order_date = '.$this->db->escape($this->input->post('order_date'))
                   .')  GROUP BY company_email ORDER BY id_company';
            $object = $this->db->query($sql);
            $company = null;
            $sql = "SELECT *,product.name_country FROM cart NATURAL JOIN orders NATURAL JOIN product LEFT OUTER JOIN company USING
                   (id_company) LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN country USING (id_country)
                   LEFT OUTER JOIN currency USING (id_currency) WHERE user =  ".$this->db->escape($cookie)." AND cart.id_order = orders.id_order AND
                   order_date =  ".$this->db->escape($this->input->post('order_date'));
            $cart = $this->db->query($sql);
            //Скорее всего будет отправлять сообщения о том, что нам пришёл какой-то заказ
            foreach($object->result_array() as $row){
                if($company != $row['name']){
                    $company = $row['name'];
                    $this->alertZakup($row['company_email'], $row['id_order'], $row['order_date'], $row['fio'], $cart, $row['comment'], $row['phone'], $row['address'], $row['SUMMA']);
                }
                //Тут мы должны будем проверять компанию и выбирать какие товары должны отправить им в сообщении
                //создать массив, в который мы будем помещать все товары заказанные у этой компании, при смене компании будем отчищать
                //массив и заново добавлять туда товары, которые относятся к новой компании.
                $this->emailIncomingOrder($row['company_email'], $row['id_order'], $row['order_date'], $row['fio'], $cart, $row['comment'], $row['phone'], $row['address'], $row['SUMMA'], $row['name']);
            }
            $sql = "SELECT * FROM section_one ORDER BY id_section_one";
            $section = $this->db->query($sql);
            $sql = "SELECT * FROM category ORdER BY id_category";
            $category = $this->db->query($sql);
            $sql = "SELECT * FROM subcategory ORDER BY id_subcategory";
            $subcategory = $this->db->query($sql);
            $this->load->view('header', array('keywords' => null, 'description' => null, 'title' => 'Заказ отправлен'));
            $this->load->view('headerMenu', array('hide' => 'hide', 'searchText' => null));
            $this->load->view('order_complete', array('section' => $section, 'category' => $category, 'subcategory' => $subcategory, 'user' => $user));
            $this->load->view('footer');
        }
    }
	
    public  function printOrder($id_user = null){
        if ($this->session->userdata('id_user') == null){
            $cookie = get_cookie('id');
            $user = null;
        } else {
            $cookie = $this->session->userdata('id_user');
            $sql = "SELECT * FROM `user` LEFT OUTER JOIN company USING (id_user) LEFT OUTER JOIN contact_information USING (id_company)
                   WHERE id_user = ".$this->db->escape($this->session->userdata('id_user'))." ORDER BY id_user";
            $user = $this->db->query($sql);
        }
		
            $sql = "SELECT * FROM section_one ORDER BY id_section_one";
            $section = $this->db->query($sql);
            $sql = "SELECT * FROM category ORdER BY id_category";
            $category = $this->db->query($sql);
            $sql = "SELECT * FROM subcategory ORDER BY id_subcategory";
            $subcategory = $this->db->query($sql);		
			
            $sql = "SELECT COUNT(*) AS `count` FROM `cart` WHERE user = ".$this->db->escape($cookie)." AND id_order is NULL";
            $count = $this->db->query($sql);
            $sql = "SELECT COUNT(DISTINCT id_company) AS `count` FROM `cart` WHERE user = ".$this->db->escape($cookie);
            $company_count = $this->db->query($sql);
            $sql = "SELECT  SUM(price_cart) AS SUMMA FROM cart WHERE user = ".$this->db->escape($cookie)." AND id_order is NULL";
            $summa = $this->db->query($sql);
            //SUM(price) AS SUMMA FROM
            $sql = "SELECT * FROM cart LEFT OUTER JOIN company USING (id_company) LEFT OUTER JOIN contact_information USING
                   (id_company) LEFT OUTER JOIN country USING (id_country) LEFT OUTER JOIN currency USING (id_currency) LEFT OUTER JOIN
                   product USING (id_product) WHERE user = ".$this->db->escape($cookie)." AND id_order is NULL ORDER BY cart.id_company;";
            $object = $this->db->query($sql);			

			$this->load->library('mypdf');

            $this->load->view('header', array('keywords' => null, 'description' => null, 'title' => 'Распечатка Вашего заказа'));
			
            $this->load->view('print_order', array('cart' => $object, 'count' => $count, 'company_count' => $company_count, 'summa' => $summa, 'section' => $section, 'category' => $category, 'subcategory' => $subcategory, 'user' => $user));
			
			$this->load->view('footer');

$company = null;
$product = null;
			
			
$html =
'<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Zakup.asia</title>
</head>
<body>
	<div class="wrapper">
		<div class="container">
			<div class="row">
			<h3>Содержимое Вашего заказа</h3><br>
<table width="100%" align="center" cellpadding="10" cellspacing="10" rules="all">';			
                                       foreach($object->result_array() as $row) {

                                            if($company != $row['name']) {
                                                $html .='<tr>';
                                                    $company = $row['name'];
                                                    $html .='<td colspan="3">
																<b>'.$row['name'].'</b>
															</td>
														</tr>
	<tr>
		<th>Наименование</th>
		<th>Количество</th>
		<th>Цена</th>
	</tr>														
														';
                                                    foreach($object->result_array() as $row2) {
                                                       if($row['name'] == $row2['name']) {
                                                            $html .='<tr>                                                                
																		<td>
																			'.$row2['name_product'].'
																		</td>
                                                                
                                                                    <td>
                                                                        '.$row2['number_of_products'].'
                                                                    </td>
                                                                    <td>
                                                                        '.$row2['price_cart'] .' Тг.
                                                                    </td>
                                                               
                                                            </tr>';
														}
													}
												
												}
											}													
												
$html .='	
				</table>									
			</div>
		</div>	
	</div>
</body>
</html>
';

        $this->dompdf->load_html($html);
        $this->dompdf->set_option('enable_css_float',true);
        $this->dompdf->set_paper("A4", "portrait");
        $this->dompdf->render();
        $filename = "Заказ_пользователя_".$this->session->userdata('email')."_от_".date("d.m.Y H:i").".pdf";
        $this->dompdf->stream($filename);
    }	

    //Очищаем корзину
    public function clearOrder($user){
        if ($this->session->userdata('id_user') == null){
            $cookie = get_cookie('id');
        } else {
            $cookie = $this->session->userdata('id_user');
        }
        $sql = 'DELETE FROM `cart` WHERE user = '.$this->db->escape($cookie);
        $this->db->query($sql);
        redirect($this->server, 'refresh');
    }

    //TODO проверку статуса заказа
    //TODO переделать скорей всего сейчас отображает заказ для каждой компании.
    //Получаем информацию по конкретному заказу.
    public function getOrdersAjax(){
		//TODO ." AND id_company = ".$this->db->escape($this->session->userdata('id_company'));
        $sql = "UPDATE `cart` SET `order_status` = 'Просмотрен'  WHERE `id_order` = (SELECT id_order FROM orders WHERE order_date ="
               .$this->db->escape($this->input->get('data'))." AND user =".$this->db->escape($this->input->get('id_user'))." AND order_status != 'Принят') AND id_company = ".$this->db->escape($this->session->userdata('id_company'));
        $this->db->query($sql);
        $sql = "SELECT *,product.name_country FROM cart NATURAL JOIN orders NATURAL JOIN product LEFT OUTER JOIN company USING (id_company)
               LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN country USING (id_country)
               LEFT OUTER JOIN currency USING (id_currency) WHERE user =  ".$this->db->escape($this->input->get('id_user')).
               " AND cart.id_order = orders.id_order AND order_date = ".$this->db->escape($this->input->get('data'))." AND cart.id_company = "
               .$this->db->escape($this->session->userdata('id_company'));
        $object = $this->db->query($sql);
        $allOrders = array();
        $order = array();
        foreach($object ->result_array() as $row){
            $order['id_user'] = $row['user'];
            $order['number_of_products'] = $row['number_of_products'];
            $order['price_cart'] = $row['price_cart'];
            $order['name_product'] = $row['name_product'];
            $order['at_the_end'] = $row['at_the_end'];
            $order['name_country'] = $row['name_country'];
            $order['icon_currency'] = $row['icon_currency'];
            $order['fio'] = $row['fio'];
            $order['phone'] = $row['phone'];
            $order['email'] = $row['email'];
            $order['order_date'] = $row['order_date'];
            $order['comment'] = $row['comment'];
            $order['data_shipping'] = $row['data_shipping'];
            $order['address'] = $row['address'];
            $order['id_order'] = $row['id_order'];
            $order['name'] = $row['name'];
            array_push($allOrders, $order);
        }
        print json_encode($allOrders);
    }

    public function getOrderOutAjax(){
        $sql = "SELECT *,product.name_country FROM cart NATURAL JOIN orders NATURAL JOIN product LEFT OUTER JOIN company USING (id_company)
               LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN country USING (id_country)
               LEFT OUTER JOIN currency USING (id_currency) WHERE user =  ".$this->db->escape($this->input->get('id_user')).
               " AND cart.id_order = orders.id_order AND order_date = ".$this->db->escape($this->input->get('data'));
        $object = $this->db->query($sql);
        $allOrders = array();
        $order = array();
        foreach($object ->result_array() as $row){
            $order['number_of_products'] = $row['number_of_products'];
            $order['price_cart'] = $row['price_cart'];
            $order['name_product'] = $row['name_product'];
            $order['at_the_end'] = $row['at_the_end'];
            $order['name_country'] = $row['name_country'];
            $order['icon_currency'] = $row['icon_currency'];
            $order['fio'] = $row['fio'];
            $order['phone'] = $row['phone'];
            $order['email'] = $row['email'];
            $order['order_date'] = $row['order_date'];
            $order['comment'] = $row['comment'];
            $order['data_shipping'] = $row['data_shipping'];
            $order['address'] = $row['address'];
            $order['id_order'] = $row['id_order'];
            $order['name'] = $row['name'];
            array_push($allOrders,$order);
        }
        print json_encode($allOrders);
    }

    //Получаем историю, это заказы у которых статус не равен Просмотрен или Не просмотрен
    public function getHistory($searchText = null){
        if ($this->session->userdata('id_user')!= null){
            $cookie = $this->session->userdata('id_user');
            $sql = "SELECT * FROM cart LEFT OUTER JOIN orders USING (user) WHERE cart.id_order = orders.id_order  AND id_company = "
                   .$this->db->escape($this->session->userdata('id_company'))." AND cart.id_order is not NULL AND
                   (order_status = 'Доставлен' OR order_status = 'Отказан') GROUP BY orders.id_order ORDER BY order_date DESC";
            $orders = $this->db->query($sql);
            $this->load->view('header', array('title'=> 'История заказов', 'keywords' => null, 'description' => null, 'title' => 'Регистрация'));
            $this->load->view('headerMenu', array('hide' => null, 'searchText' => null));
			$this->load->view('leftMenu', array('active'=>'orders'));
            $this->load->view('history_order', array('orders' => $orders));
            $this->load->view('footer');
        } else redirect($this->server, 'refresh');
    }

    //Получаем заказы исходящие от нас
    public function getOrdersOutgoing($searchText = null){
        if ($this->session->userdata('id_user')!= null){
            $cookie = $this->session->userdata('id_user');
            $sql = "SELECT * FROM cart LEFT OUTER JOIN orders USING (user) WHERE cart.id_order = orders.id_order  AND user = ".$this->db->escape($cookie).
                   " AND cart.id_order is not NULL AND (order_status = 'Просмотрен' OR order_status = 'Не просмотрен' OR order_status = 'Принят')
                   GROUP BY orders.id_order ORDER BY order_date DESC ";
            $orders = $this->db->query($sql);
            $this->load->view('header', array('title'=> 'Исходящие заказы', 'keywords' => null, 'description' => null, 'title' => 'Регистрация'));
            $this->load->view('headerMenu', array('hide' => null, 'searchText' => null));
			$this->load->view('leftMenu', array('active'=>'orders'));
            $this->load->view('outgoing_order', array('orders' => $orders));
            $this->load->view('footer');
        } else redirect($this->server, 'refresh');
    }

    //TODO Лучше по id заказа, по дате он сейчас тупить будет т.к. это не дата.
    //Такс, вот такой примерно запрос SELECT * FROM `cart` LEFT OUTER JOIN company USING (id_company) LEFT OUTER JOIN product USING (id_product) WHERE name = 'Ну ок 1'
    //id_customer - поле должно быть и в cart, изменить поле user на id_customer
    //Получаем заказы поступившие к нам с помощью запроса, а потом просто добавляем в конец с помощью джаваскрипта
    public function getOrdersIncoming($searchText = null){
        if ($this->session->userdata('id_user') != null){
            $sql = "SELECT * FROM cart LEFT OUTER JOIN orders USING (user) WHERE  cart.id_order = orders.id_order  AND id_company = "
                   .$this->db->escape($this->session->userdata('id_company'))." AND cart.id_order is not NULL
                   AND (order_status = 'Просмотрен' OR order_status = 'Не просмотрен' OR order_status = 'Принят')
                   GROUP BY orders.id_order ORDER BY order_date DESC";
            $orders = $this->db->query($sql);
            $this->load->view('header', array('title'=> 'Входящие заказы', 'keywords' => null, 'description' => null, 'title' => 'Регистрация'));
            $this->load->view('headerMenu', array('hide' => null, 'searchText' => null));
			$this->load->view('leftMenu', array('active'=>'orders'));
            $this->load->view('incoming_order', array('orders' => $orders));
            $this->load->view('footer');
        } else redirect($this->server, 'refresh');
    }

	//TODO проверить
    //Принять заказ
    public function acceptOrderAjax(){
        $sql = "UPDATE `cart` SET `order_status` = 'Принят'  WHERE `id_order` = ".$this->db->escape($this->input->get('id_order'))
			   ." AND id_company = ".$this->db->escape($this->session->userdata('id_company'));
        if ($this->db->query($sql)){
            echo 'true';
        } else echo 'false';

    }

	//TODO проверить
    //Отказаться от заказа
    public function declineOrderAjax(){
        $sql = "UPDATE `cart` SET `order_status` = 'Отказан'  WHERE `id_order` = ".$this->db->escape($this->input->get('id_order'))
			   ." AND id_company = ".$this->db->escape($this->session->userdata('id_company'));
        if ($this->db->query($sql)){
            echo 'true';
        } else echo 'false';
    }

    //Отредактировать
    public function editOrder(){

    }

    public function completeOrderAjax(){
        $sql = "UPDATE `cart` SET `order_status` = 'Доставлен'  WHERE `id_order` = ".$this->db->escape($this->input->get('id_order'));
        if ($this->db->query($sql)){
            echo 'true';
        } else echo 'false';
    }

    //TODO тестить что получилось
    //Возможно мы должны сделать отдельную таблицу с почтами и потом группировать по ним и отправлять почту
    //Отправляем сообщеня о том, что поступил заказ
    public function emailIncomingOrder($email, $id_order, $order_data, $name, $cart, $comment, $phone, $address, $price_cart, $company_name){
        if ($this->is_email($email)) {
            $products = null;
			$this->load->library('email');
			//Добавили
			$config['mailtype'] = 'html';
			$this->email->initialize($config);
			//Добавили
			$this->email->clear();
			//Задает адрес электронной почты и имя лица-отправителя:
			$this->email->from('info@zakup.asia', 'Администрация');
			//Устанавливает адрес для ответа, если не была указана информация в функции "from". Пример:
			$this->email->reply_to('info@zakup.asia', 'Администрация');
			//Задает адрес(а) получателя (получателей). Может быть указан один адрес электронной почты, список через запятую или массив:
			$list = array($email);
			$this->email->to($list);
			//Устанавливает тему письма
			$this->email->subject('Поступил онлайн-заказ  на Zakup.asia');
			//Устанавливает текст сообщения
            foreach ($cart->result_array() as $row) {
                if ($row['name'] == $company_name){
                    $products = $products.'<tr>
                                            <td width="60%" style="padding: 5px;text-align:left; font-size:12px;" align="left">
                                                <a href="http://zakup.asia/index.php/Products/getProduct/'.$row['id_product'].'">'.$row['name_product'].'</a>
                                            </td>
                                            <td width="20%"  style="padding: 5px;text-align:right; font-size:12px;" align="right">'.$row['number_of_products'].'</td>
                                            <td width="20%" style="padding: 5px;text-align:right; font-size:12px;" align="right">'.$row['price_cart'].'&nbsp;'.$row['icon_currency'].'.</td>
                                        </tr>
                                        ';
                }
            }
            $sendEmail = '<!DOCTYPE HTML>
            <html>
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>zakup.asia</title>

            <style type="text/css">
                .ReadMsgBody {width: 100%; background-color: #F0F2F5;}
                .ExternalClass {width: 100%; background-color: #F0F2F5;}
                body	 {width: 100%; background-color: #F0F2F5; margin:0; padding:0; -webkit-font-smoothing: antialiased;font-family: Tahoma, Arial, sans-serif}
                table {border-collapse: collapse;}

                @media only screen and (max-width: 400px)  {
                                    body[yahoo] [class=deviceWidth] {width:380px!important; padding:0;}
                                body[yahoo] [class=center] {text-align: center!important;}
                                img[class=emailwrapto100pc]
                                table[class="hide"], img[class="hide"], td[class="hide"], span[class="hide"] {
                                        display:none!important;
                                    }
                        }

            </style>
            </head>
            <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="font-family: Tahoma, Arial, sans-serif;background: #F0F2F5;" ><img src="http://zakup.asia/images/logo.png" border="0" width="1" height="1">
            <table class="deviceWidth"  cellpadding="0" cellspacing="0" border="0" width="100%" align="center" style="border:none;border-collapse:collapse; background: #F0F2F5;font-family:Arial,sans-serif;" bgcolor="#F0F2F5">
                <tr>
                    <td width="50%"></td>
                    <td>
                        <table class="deviceWidth" cellpadding="0" cellspacing="0" border="0" width="600" align="center" style="border: 1px solid #dedede;border-collapse:collapse; background: #ffffff;" bgcolor="#ffffff">
                            <tr>
                                <td style="border:none;">
                                    <!-- HEADER -->
                                    <table class="deviceWidth" cellpadding="0" cellspacing="0" border="0" width="600" align="center" style="border:none;border-collapse:collapse;" bgcolor="#ffffff">
                                        <tr>
                                            <td>
                                                <!-- 2 -->
                                                <table class="deviceWidth" width="100%" style="width:600px;border:none; border-collapse:collapse;padding:0px; margin:0px;" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="border:none; padding:0px; margin:0px;">
                                                                <a target="_blank" href="http://zaku.asia">
                                                                    <img  hspace="0" vspace="0" src="http://zakup.asia/images/logo.png" border="0" width="88" style="width:88px; text-align:center; font-family:Arial,Tahoma,sans-serif; border:none; text-decoration:none; line-height:0; vertical-align:top; display:block; padding:0px; margin:15px auto;" hspace="0" vspace="0" alt="zakup.asia">
                                                                </a>
                                                            </td>
                                                        </tr>
                                                </table>
                                                <!-- #2 -->
                                                <!-- 4 -->
                                                <table style="width:600px; height:1px; border:none; border-collapse:collapse; padding:0px; margin:0px; mso-table-lspace:0pt; mso-table-rspace:0pt;" width="100%" height="1" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="border:none; padding:0px; margin:0px;">
                                                                <img  hspace="0" vspace="0" src="http://mailings.lmcdn.ru/template/header/20150615/header_men_no_balls_dot_2016.jpg" border="0" width="100%" style="width:600px;text-align:center; font-family:Arial,Tahoma,sans-serif; border:none; text-decoration:none; line-height:0; vertical-align:top; display:block; padding:0px; margin:0px;outline:none;" hspace="0" vspace="0" alt="">
                                                            </td>
                                                        </tr>
                                                </table>
                                                <!-- #4 -->
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- #header-end-wrapper -->
                                    <!-- #HEADER -->
                                    <br/>
                <table width="590"  class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" >
                    <tr>
                        <td height="37" valign="middle" style="height: 37px; border-bottom-color: #d6d6d6; border-bottom-width:1px; border-bottom-style: solid; vertical-align: middle; font-family:tahoma,Arial,sans-serif;">
                            <table width="100%"  class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="left" bgcolor="#ffffff" >
                                <tr>
                                    <td style="border:none; padding:5px; margin:0px;" align="left">
                                        <span align="left" style="font-weight: bold; font-size: 14px; margin: 0; padding: 0; color: #243a5a; text-align: left;">Вам поступил заказ №'.$id_order.' на общую сумму '.$price_cart.'тг. </span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <table width="590"  class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" >
                    <tr>
                        <td style="border:none; padding:0px; margin:0px;">
                            <table width="100%"  class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="left" bgcolor="#ffffff" >
                                <tr>
                                    <td style="border:none; padding:5px; margin:0px;" align="left">
                                        <span style="text-align:left; font-size: 13px;  color: #000000; line-height:16px; font-family: Tahoma, Arial, sans-serif;">
                                            <br/>
                                            <span style="text-transform: capitalize;">Здравствуйте!</span>
                                            <br/><br/>
                                            <span style="font-family:Arial,Tahoma,sans-serif; font-size:13px;">Вам поступил заказ на zakup.asia! </span><br>
                                <!--Вы можете <a href="http://zakup.asia/index.php/cart/getOrdersIncoming">принять заказ</a> или <a href="http://zakup.asia/index.php/cart/getOrdersIncoming">отказаться</a>-->
                                            <br/><br>
                                            <span style="font-family:Arial,Tahoma,sans-serif; font-size: 13px;">Также Вы можете в любой момент увидеть все детали заказа в <a href="http://zakup.asia/index.php/cart/getOrdersIncoming">личном кабинете</a>.
                                        </span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <br/>

                <table width="590"  class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="padding: 0px; border: none; ">
                    <tr>
                        <td style="border:none; line-height:0px; font-size:0;padding:0px; margin:0px; ">
                            <!-- Таблица -->
                            <table width="100%" class="deviceWidth" align="center" border="1" bordercolor="#E9EEF2" cellpadding="0" cellspacing="0" bgcolor="#ffffff" style="border-collapse:collapse; padding: 0px; border: 1px solid #E9EEF2;line-height: 1em; font-size: 13px; font-family:tahoma,arial,sans-serif;">
                                <tr height="30" style="vertical-align: middle;" bgcolor="#f8fafc">
                                    <td width="60%"  style="padding: 5px;text-align:left;" align="left">Наименование</td>
                                    <td width="20%"  style="padding: 5px;text-align:right;" align="right">Кол<span class="hide">ичест</span>во</td>
                                    <td width="20%"  style="padding: 5px;text-align:right;" align="right">Сумма</td>
                                </tr>'.$products.
                '</table>
                        </td>
                    </tr>
                </table>

                <!-- Итого -->
                <table width="590" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" bgcolor="#ffffff" style="font-size: 13px;font-family:Arial;text-align:left;border-collapse:collapse;">
                    <tr>
                        <td style="border:none; padding:0px; margin:0px;" width="200" class="hide">&nbsp;
                        </td>
                        <td style="border:none; padding:0px; margin:0px;" width="390">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" style="font-size: 12px;font-family:Arial;text-align:left;border-collapse:collapse;">
                                <tr style="vertical-align: middle;font-size:12px;" align="right">
                                    <td width="250" align="right" >
                                        <b>Итого</b>
                                    </td>
                                    <td width="140" align="right" >
                                        <b>'.$price_cart.'&nbsp;тг.</b>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>

                <br/>


                <table style="border: 1px solid #E9EEF2;"  border="1" cellpadding="0" cellspacing="0"  align="center" width="590" class="deviceWidth" >
                    <tr>
                        <td style="border:none; padding: 5px 9px 6px; margin:0px;line-height:16px; font-size:13px; text-align:left; color:#000000;font-family:tahoma,arial,sans-serif; font-weight:bold;" bgcolor="#f8fafc">
                                    Покупатель
                        </td>
                    </tr>

                    <tr>
                        <td style="border:none; padding: 5px 9px 6px; margin:0px;line-height:16px; font-size:13px;text-align:left; color:#000000;font-family:tahoma,arial,sans-serif;"  bgcolor="#ffffff">

                                   '.$name.'<br>
                            <a href="mailto:'.$email.'">'.$email.'</a> <br />
                            ' .$phone. '<br><br>
                                '.$address.'<br />'.$comment.'</td>
                    </tr>
                </table>
                <br/>
                <table width="590"  class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" >
                    <tr>
                        <td style="border:none; padding:5px; margin:0px;font-size:13px;color: #000000; line-height:16px;font-family:Arial;text-align:left;border-collapse:collapse;" >
                            <span style="font-family:Arial,tahoma,sans-serif; font-size:13px;">
                                    Следите за состоянием заказа в <a target="_blank" href="http://zakup.asia/index.php/cart/getHistory">личном кабинете<a>.
                                <br/>
                                Команда zakup.asia.
                            </span>
                        </td>
                    </tr>
                </table>
            <!-- #FOOTER -->
            <table cellpadding="0" cellspacing="0" border="0" width="600" align="center" style="border:none; font-size:13px; font-family:Arial,sans-serif; text-align:left; border-collapse:collapse;">
            </table>
            <!-- FOOTER-bottom-wrapper -->
                    </td>
                </tr>
            </table>
            &nbsp;<br />
            <br />
            </td>
            <td width="50%">
            </td>
            </tr>
            </table>
            </body>
            </html>
            <!-- #FOOTER -->';
            $this->email->message($sendEmail);
			//Отправка самого письма
			$this->email->send();
		}
    }

    //TODO
    //Пишем сами себе о том, что поступил заказ
    public function alertZakup($email, $id_order, $order_data, $name, $cart, $comment, $phone, $address, $price_cart){
        $products = null;
        $MyUser = array();
        $this->load->library('email');
        //Добавили
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        //Добавили
        $this->email->clear();
        //Задает адрес электронной почты и имя лица-отправителя:
        $this->email->from('center@zakup.asia', 'Администрация');
        //Устанавливает адрес для ответа, если не была указана информация в функции "from". Пример:
        $this->email->reply_to('center@zakup.asia', 'Администрация');
        //Задает адрес(а) получателя (получателей). Может быть указан один адрес электронной почты, список через запятую или массив:
        $this->email->to('info@zakup.asia');
        //Устанавливает тему письма
        $this->email->subject('Поступил онлайн-заказ  на Zakup.asia');
        //Устанавливает тему письма
        foreach ($cart->result_array() as $row) {
                $products = $products.'<tr>
                                            <td width="60%" style="padding: 5px;text-align:left; font-size:12px;" align="left">
                                                <a href="http://zakup.asia/index.php/Products/getProduct/'.$row['id_product'].'">'.$row['name_product'].'</a>
                                            </td>
                                            <td width="20%"  style="padding: 5px;text-align:right; font-size:12px;" align="right">'.$row['name'].'</td>
                                            <td width="20%"  style="padding: 5px;text-align:right; font-size:12px;" align="right">'.$row['number_of_products'].'</td>
                                            <td width="20%" style="padding: 5px;text-align:right; font-size:12px;" align="right">'.$row['price_cart'].'&nbsp;'.$row['icon_currency'].'.</td>
                                        </tr>
                                        ';
        }
        $sendEmail = '<!DOCTYPE HTML>
            <html>
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>zakup.asia</title>

            <style type="text/css">
                .ReadMsgBody {width: 100%; background-color: #F0F2F5;}
                .ExternalClass {width: 100%; background-color: #F0F2F5;}
                body	 {width: 100%; background-color: #F0F2F5; margin:0; padding:0; -webkit-font-smoothing: antialiased;font-family: Tahoma, Arial, sans-serif}
                table {border-collapse: collapse;}

                @media only screen and (max-width: 400px)  {
                                    body[yahoo] [class=deviceWidth] {width:380px!important; padding:0;}
                                body[yahoo] [class=center] {text-align: center!important;}
                                img[class=emailwrapto100pc]
                                table[class="hide"], img[class="hide"], td[class="hide"], span[class="hide"] {
                                        display:none!important;
                                    }
                        }

            </style>
            </head>
            <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="font-family: Tahoma, Arial, sans-serif;background: #F0F2F5;" ><img src="http://zakup.asia/images/logo.png" border="0" width="1" height="1">
            <table class="deviceWidth"  cellpadding="0" cellspacing="0" border="0" width="100%" align="center" style="border:none;border-collapse:collapse; background: #F0F2F5;font-family:Arial,sans-serif;" bgcolor="#F0F2F5">
                <tr>
                    <td width="50%"></td>
                    <td>
                        <table class="deviceWidth" cellpadding="0" cellspacing="0" border="0" width="600" align="center" style="border: 1px solid #dedede;border-collapse:collapse; background: #ffffff;" bgcolor="#ffffff">
                            <tr>
                                <td style="border:none;">
                                    <!-- HEADER -->
                                    <table class="deviceWidth" cellpadding="0" cellspacing="0" border="0" width="600" align="center" style="border:none;border-collapse:collapse;" bgcolor="#ffffff">
                                        <tr>
                                            <td>
                                                <!-- 2 -->
                                                <table class="deviceWidth" width="100%" style="width:600px;border:none; border-collapse:collapse;padding:0px; margin:0px;" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="border:none; padding:0px; margin:0px;">
                                                                <a target="_blank" href="http://zaku.asia">
                                                                    <img  hspace="0" vspace="0" src="http://zakup.asia/images/logo.png" border="0" width="88" style="width:88px; text-align:center; font-family:Arial,Tahoma,sans-serif; border:none; text-decoration:none; line-height:0; vertical-align:top; display:block; padding:0px; margin:15px auto;" hspace="0" vspace="0" alt="zakup.asia">
                                                                </a>
                                                            </td>
                                                        </tr>
                                                </table>
                                                <!-- #2 -->
                                                <!-- 4 -->
                                                <table style="width:600px; height:1px; border:none; border-collapse:collapse; padding:0px; margin:0px; mso-table-lspace:0pt; mso-table-rspace:0pt;" width="100%" height="1" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="border:none; padding:0px; margin:0px;">
                                                                <img  hspace="0" vspace="0" src="http://mailings.lmcdn.ru/template/header/20150615/header_men_no_balls_dot_2016.jpg" border="0" width="100%" style="width:600px;text-align:center; font-family:Arial,Tahoma,sans-serif; border:none; text-decoration:none; line-height:0; vertical-align:top; display:block; padding:0px; margin:0px;outline:none;" hspace="0" vspace="0" alt="">
                                                            </td>
                                                        </tr>
                                                </table>
                                                <!-- #4 -->
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- #header-end-wrapper -->
                                    <!-- #HEADER -->
                                    <br/>
                <table width="590"  class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" >
                    <tr>
                        <td height="37" valign="middle" style="height: 37px; border-bottom-color: #d6d6d6; border-bottom-width:1px; border-bottom-style: solid; vertical-align: middle; font-family:tahoma,Arial,sans-serif;">
                            <table width="100%"  class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="left" bgcolor="#ffffff" >
                                <tr>
                                    <td style="border:none; padding:5px; margin:0px;" align="left">
                                        <span align="left" style="font-weight: bold; font-size: 14px; margin: 0; padding: 0; color: #243a5a; text-align: left;">Поступил заказ №'.$id_order.' на общую сумму '.$price_cart.'тг. </span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <table width="590"  class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" >
                    <tr>
                        <td style="border:none; padding:0px; margin:0px;">
                            <table width="100%"  class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="left" bgcolor="#ffffff" >
                                <tr>
                                    <td style="border:none; padding:5px; margin:0px;" align="left">
                                        <span style="text-align:left; font-size: 13px;  color: #000000; line-height:16px; font-family: Tahoma, Arial, sans-serif;">
                                            <br/>
                                            <span style="text-transform: capitalize;">Здравствуйте!</span>
                                            <br/><br/>
                                            <span style="font-family:Arial,Tahoma,sans-serif; font-size:13px;">Поступил заказ на zakup.asia! </span><br>
                                <!--Вы можете <a href="http://zakup.asia/index.php/cart/getOrdersIncoming">принять заказ</a> или <a href="http://zakup.asia/index.php/cart/getOrdersIncoming">отказаться</a>-->
                                            <br/><br>
                                        </span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

                <br/>

                <table width="590"  class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" style="padding: 0px; border: none; ">
                    <tr>
                        <td style="border:none; line-height:0px; font-size:0;padding:0px; margin:0px; ">
                            <!-- Таблица -->
                            <table width="100%" class="deviceWidth" align="center" border="1" bordercolor="#E9EEF2" cellpadding="0" cellspacing="0" bgcolor="#ffffff" style="border-collapse:collapse; padding: 0px; border: 1px solid #E9EEF2;line-height: 1em; font-size: 13px; font-family:tahoma,arial,sans-serif;">
                                <tr height="30" style="vertical-align: middle;" bgcolor="#f8fafc">
                                    <td width="40%"  style="padding: 5px;text-align:left;" align="left">Наименование</td>
                                    <td width="20%"  style="padding: 5px;text-align:right;" align="right">Компания</td>
                                    <td width="20%"  style="padding: 5px;text-align:right;" align="right">Кол<span class="hide">ичест</span>во</td>
                                    <td width="20%"  style="padding: 5px;text-align:right;" align="right">Сумма</td>
                                </tr>'.$products.
            '</table>
                        </td>
                    </tr>
                </table>

                <!-- Итого -->
                <table width="590" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" bgcolor="#ffffff" style="font-size: 13px;font-family:Arial;text-align:left;border-collapse:collapse;">
                    <tr>
                        <td style="border:none; padding:0px; margin:0px;" width="200" class="hide">&nbsp;
                        </td>
                        <td style="border:none; padding:0px; margin:0px;" width="390">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" style="font-size: 12px;font-family:Arial;text-align:left;border-collapse:collapse;">
                                <tr style="vertical-align: middle;font-size:12px;" align="right">
                                    <td width="250" align="right" >
                                        <b>Итого</b>
                                    </td>
                                    <td width="140" align="right" >
                                        <b>'.$price_cart.'&nbsp;тг.</b>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>

                <br/>


                <table style="border: 1px solid #E9EEF2;"  border="1" cellpadding="0" cellspacing="0"  align="center" width="590" class="deviceWidth" >
                    <tr>
                        <td style="border:none; padding: 5px 9px 6px; margin:0px;line-height:16px; font-size:13px; text-align:left; color:#000000;font-family:tahoma,arial,sans-serif; font-weight:bold;" bgcolor="#f8fafc">
                                    Покупатель
                        </td>
                    </tr>

                    <tr>
                        <td style="border:none; padding: 5px 9px 6px; margin:0px;line-height:16px; font-size:13px;text-align:left; color:#000000;font-family:tahoma,arial,sans-serif;"  bgcolor="#ffffff">

                                   '.$name.'<br>
                            <a href="mailto:'.$email.'">'.$email.'</a> <br />
                            ' .$phone. '<br><br>
                                '.$address.'<br />'.$comment.'</td>
                    </tr>
                </table>
                <br/>
                <table width="590"  class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#ffffff" >
                    <tr>
                        <td style="border:none; padding:5px; margin:0px;font-size:13px;color: #000000; line-height:16px;font-family:Arial;text-align:left;border-collapse:collapse;" >
                            <span style="font-family:Arial,tahoma,sans-serif; font-size:13px;">
                                    Следите за состоянием заказа в <a target="_blank" href="http://zakup.asia/index.php/cart/getHistory">личном кабинете<a>.
                                <br/>
                                Команда zakup.asia.
                            </span>
                        </td>
                    </tr>
                </table>
            <!-- #FOOTER -->
            <table cellpadding="0" cellspacing="0" border="0" width="600" align="center" style="border:none; font-size:13px; font-family:Arial,sans-serif; text-align:left; border-collapse:collapse;">
            </table>
            <!-- FOOTER-bottom-wrapper -->
                    </td>
                </tr>
            </table>
            &nbsp;<br />
            <br />
            </td>
            <td width="50%">
            </td>
            </tr>
            </table>
            </body>
            </html>
            <!-- #FOOTER -->';
        $this->email->message($sendEmail);
        //Отправка самого письма
        $this->email->send();
    }
	
	 //Пример использования: if ( !is_email($url) ) echo 'E-mail not valid';
    //Проверка емайла на валидность
    private function is_email($email) {
        return preg_match("/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)*\.([a-zA-Z]{2,6})$/", $email);
    }
}
//TODO очередная замена статусов
//UPDATE `cart` SET `order_status`='Доставлен' WHERE `order_status`='Статус : Доставлен'
//UPDATE `cart` SET `order_status`='Не просмотрен' WHERE `order_status`='Статус : Не просмотрен'
//UPDATE `cart` SET `order_status`='Отказан' WHERE `order_status`='Статус : Отказан'
//UPDATE `cart` SET `order_status`='Принят' WHERE `order_status`='Статус : Принят'
//UPDATE `cart` SET `order_status`='Просмотрен' WHERE `order_status`='Статус : Просмотрен'