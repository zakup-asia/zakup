<div class="wrapper reg" id="panel">
    <header>
        <div class="header-reg">
            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>" class="logo-reg"><img src="/../../images/logo.png" alt="Zakup"></a>
        </div>
    </header>	
    <div class="container">
        <div class="row">
            <div class="reg-wrapper lrg">
                <div class="reg-top">
                    <h1>Регистрация</h1>
                    <div class="reg-top-right">
                        <span>Уже зарегистрированы?</span>
                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/login/">Войти</a>
                    </div>
                </div>

				<div class="col-sm-6 col-md-5 col-lg-6 reg-col">
					<center>
						<img src="/../../images/customer.png" alt="Покупатель" class="reg-type-img"/>
						<h3>Покупатель</h3>
					</center>					
					<?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/user/registration/', array('class' => "reg-form form-horizontal")) ?>
						<input type="hidden" class="form-control" name="goal" value="0">
						<div class="form-group">
							<label for="email" class="control-label col-xs-4"></label>
							<div class="col-sm-8 col-sm-offset-2 col-xs-offset-2 col-xs-8 reg-input">
								<input type="text" class="form-control" id="email" name="email" placeholder="Введите e-mail или телефон" value="<?php echo set_value('email'); ?>">
								<?php echo form_error('email', '<div class="css-error">', '</div>'); ?>
							</div>
						</div>
						<div class="form-group">
							<label for="pass" class="control-label col-xs-4"></label>
							<div class="col-sm-8 col-sm-offset-2 col-xs-offset-2 col-xs-8 reg-input">
								<input class="form-control" id="pass" type="password" name="password" placeholder="Введите новый пароль" value="<?php echo set_value('password'); ?>">
								<?php echo form_error('password', '<div class="css-error">', '</div>'); ?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2 col-xs-offset-2 col-xs-8 reg-sub">
								<input type="submit" class="form-control red-button" value="Зарегистрироваться">
							</div>
						</div>
					</form>				
				</div>
				
				<div class="col-sm-6 col-md-5 col-lg-6 reg-col">
					<center>
						<img src="/../../images/saler.png" alt="Продавец" class="reg-type-img"/>
						<h3>Продавец</h3>
					</center>	
					<?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/user/registration/', array('class' => "reg-form form-horizontal")) ?>
						<input type="hidden" class="form-control" name="goal" value="1">
						<div class="form-group">
							<label for="email" class="control-label col-xs-4"></label>
							<div class="col-sm-8 col-sm-offset-2 col-xs-offset-2 col-xs-8 reg-input">
								<input type="text" class="form-control" id="email" name="email" placeholder="Введите e-mail или телефон" value="<?php echo set_value('email'); ?>">
								<?php echo form_error('email', '<div class="css-error">', '</div>'); ?>
							</div>
						</div>
						<div class="form-group">
							<label for="pass" class="control-label col-xs-4"></label>
							<div class="col-sm-8 col-sm-offset-2 col-xs-offset-2 col-xs-8 reg-input">
								<input class="form-control" id="pass" type="password" name="password" placeholder="Введите новый пароль" value="<?php echo set_value('password'); ?>">
								<?php echo form_error('password', '<div class="css-error">', '</div>'); ?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8 col-sm-offset-2 col-xs-offset-2 col-xs-8 reg-sub">
								<input type="submit" class="form-control red-button" value="Зарегистрироваться">
							</div>
						</div>
					</form>				
				</div>
				
            </div>
        </div>

    </div>
</div>

<!--
    <section>
        <?php echo validation_errors(); ?>
        <?php echo form_open_multipart('user/registration/') ?>
            <label>
                email
                <input type="email" name="email" value="<?php echo set_value('email'); ?>"><br>
            </label>
            <br>
            <label>
                пароль
                <input type="password" name="password" value="<?php echo set_value('password'); ?>"><br>
            </label>
            <br>
            <input type="submit" value="Зарегестрироваться">
        </form>
    </section>
-->