<section class="two-col-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="two-col">
                    <div class="profile-menu open hidden-sm hidden-xs" id="profile-menu">
                        <ul>
                            <li ><a id="orders" href="<?php echo SITE_NAME ?>index.php/cart/getOrdersOutgoing">Заказы</a></li>
                            <?php if($this->session->userdata('goal') == TRUE ) : ?>
                                <li><a id="productsIcon" href="<?php echo SITE_NAME ?>index.php/Products/myProducts">Товары</a></li>
                                <li><a id="stock" href="#">Акции</a></li>
                                <ul>
                                </ul>
                                <li><a id="news" href="#">Новости</a></li>
                            <?php endif; ?>
                            <?php if($this->session->userdata('goal') == FALSE ) :?>
                                <ul>
                                </ul>
                            <?php endif; ?>
                            <li><a id="statistics" href="#">Статистика</a></li>
                            <li><a id="favorites" href="#">Избранное</a></li>
                        </ul>
                    </div><!-- /.profile-menu -->
                    <div class="content profile">
                        <div class="row">
                            <div class="col-lg-3 col-lg-push-9 col-sm-4 col-sm-push-8">
                                <div class="dop-menu">
                                    <nav class="navbar">
                                        <div class="navbar-header" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#dop-menu-collapse" aria-expanded="false">
                                            <button type="button" class="navbar-toggle collapsed">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                            <a class="navbar-brand" href="#">Меню</a>
                                        </div>

                                        <div class="collapse navbar-collapse" id="dop-menu-collapse">
                                            <ul class="nav navbar-nav">
                                                <li class="active"><a href="<?php echo SITE_NAME ?>index.php/user/main/">Основное</a></li>
                                                <li><a href="<?php echo SITE_NAME ?>index.php/user/contactinformation/">Контактная информация</a></li>
                                                <li><a href="<?php echo SITE_NAME ?>index.php/user/conditions/">Условия заказа</a></li>
                                                <li><a href="<?php echo SITE_NAME ?>index.php/user/requisites/">Реквизиты</a></li>
                                                <li><a href="<?php echo SITE_NAME ?>index.php/user/workers/">Сотрудники</a></li>
                                                <li><a href="<?php echo SITE_NAME ?>index.php/user/loginAndPassword/">Логин и пароль</a></li>
                                            </ul>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>