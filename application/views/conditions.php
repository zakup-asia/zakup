<?php $i = 0;?>
<input type="hidden" id="id_my_company" value="<?php echo $this->session->userdata('id_company')?>">

    <?php foreach ($conditions->result_array() as $row): ?>
                                        <div class="content profile">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="profile-content">
                                    <div class="profile-content tabs-wrapper order">
                                        <div class="row css-tabs-wrapper">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/main/">
                                                    <div class="col-md-2 col-sm-2 col-xs-2 text-center css-top-menu">
                                                        Основные
                                                    </div>
                                                </a>
                                                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/contactinformation/">
                                                    <div class="col-md-2 col-sm-2 col-xs-2 text-center css-top-menu">
                                                        Контакты
                                                    </div>
                                                </a>
                                                <?php if($this->session->userdata('goal') == TRUE ) : ?>
                                                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/conditions/">
                                                        <div class="col-md-3 col-sm-3 col-xs-3 text-center active css-top-menu">
                                                            Условия заказа
                                                        </div>
                                                    </a>
                                                <?php endif;?>
                                                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/requisites/">
                                                    <div class="col-md-2 col-sm-2 col-xs-2 text-center css-top-menu">
                                                        Реквизиты
                                                    </div>
                                                </a>
                                                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/loginAndPassword/">
                                                    <div class="col-md-3 col-sm-3 col-xs-3 text-center css-top-menu">
                                                        Логин и пароль
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    <div class="profile-content">
                                        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/user/conditions/', array('class' => 'form-horizontal')) ?>
                                        <div class="profile-form">
                                           <?php if ($emails == null):?>
                                                    <div class="form-group all-email">
                                                        <input type="hidden" id="email_count" name="email_count" value="1">
                                                        <label for="full-desc" class="col-xs-3 control-label">Email для оповещений:</label>
                                                        <div class="col-xs-8 email-input">
                                                            <input type="text" class="form-control" id="email" name="email0" value="<?php echo $row['email']?>" placeholder="Введите email">
                                                        </div>
                                                        <div class="col-xs-1">
                                                            <span class="add-email" onclick="insertAlertEmail('0')"></span>
                                                        </div>
                                                    </div>
                                                <?php endif;?>
                                                <?php if ($emails != null):?>
                                                    <?php foreach($emails->result_array() as $email):?>
                                                        <?php if($i == 0):?>
                                                            <div class="form-group all-email">
                                                                <input type="hidden" id="email_count" name="email_count" value="1">
                                                                <label for="full-desc" class="col-xs-3 control-label">Email для оповещений:</label>
                                                                <div class="col-xs-8 email-input">
                                                                    <input type="text" class="form-control" id="email" name="email0" value="<?php echo $email['company_email']?>" placeholder="Введите email">
                                                                    <input type="hidden" name="id_alert_email" value="<?php echo $email['id_alert_email']?>">
                                                                </div>
                                                                <div class="col-xs-1">
                                                                    <span class="add-email"></span>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                        <?php if($i != 0):?>
                                                            <div class="form-group">
                                                                <div class="col-xs-8 email-input col-xs-offset-3">
                                                                    <input type="text" class="form-control" name="email<?php echo $i?>" value="<?php echo $email['company_email']?>" onclick="deleteEmail('<?php echo $email['id_alert_email']?>')" onBlur="insertAlertEmail('<?php echo $i?>')">
                                                                </div>
                                                                <div class="col-xs-1">
                                                                    <span onclick="addAlertEmail()" class="add-mobile-phone css-add-phone"></span>
                                                                    <span class="delete-home-phone css-delete-phone" onclick="deleteEmail(<?php echo "'".$email['id_alert_email']."'";?>)"></span>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                        <?php $i++; ?>
                                                    <?php endforeach; ?>
                                                <?php endif?>
                                                <input type="hidden" class="input-alert-email">
                                                <input name="email_first" type="hidden" value="<?php echo $i?>">
                                                <br>
                                                 <div class="form-group">
                                                    <label for="min-summa" class="col-xs-3 control-label">Мин. сумма заказа:*</label>
                                                    <div class="col-xs-5 width-175">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" id="min-summa" name="min_amount" value="<?php $min_amount = set_value('min_amount'); if($row['min_amount'] == null) $row['min_amount'] = 0; echo $min_amount = !empty($min_amount) ? $min_amount : $row['min_amount']; ?>">
                                                            <?php if($row['icon_currency'] != null):?>
                                                                <div class="input-group-addon"> <?php echo $row['icon_currency']?></div>
                                                            <?php endif ?>
                                                        </div>
                                                        <?php if($row['icon_currency'] == null):?>
                                                            <?php echo '<div class="css-error">Не установили страну в разделе Контакты</div>'?>
                                                        <?php endif ?>
                                                        <?php echo form_error('min_amount', '<div class="css-error">', '</div>'); ?>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="pickup" class="col-xs-3 control-label">Самовывоз:</label>
                                                    <div class="col-xs-5 width-175">
                                                        <select class="form-control" id="pickup" name="pickup">
                                                            <option value="1" <?php if ($row['pickup'] == '1') echo 'selected="selected"'?>>Есть</option>
                                                            <option value="0" <?php if ($row['pickup'] == '0') echo 'selected="selected"'?>>Нет</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="delivery" class="col-xs-3 control-label">Хранение:</label>
                                                    <div class="col-xs-5 width-175">
                                                        <select class="form-control" id="delivery" name="storing">
                                                            <option value="1" <?php if ($row['storing'] == '1') echo 'selected="selected"'?>>Есть</option>
                                                            <option value="0" <?php if ($row['storing'] == '0') echo 'selected="selected"'?>>Нет</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="consign" class="col-xs-3 control-label">Консигнация:</label>
                                                    <div class="col-xs-5 width-175">
                                                        <select class="form-control" id="consign" name="сonsignment">
                                                            <option value="1" <?php if ($row['сonsignment'] == '1') echo 'selected="selected"'?>>Есть</option>
                                                            <option value="0" <?php if ($row['сonsignment'] == '0') echo 'selected="selected"'?>>Нет</option>
                                                            <option value="2" <?php if ($row['сonsignment'] == '2') echo 'selected="selected"'?>>По договору</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="consign" class="col-xs-3 control-label">Проба:</label>
                                                    <div class="col-xs-5 width-175">
                                                        <select class="form-control" id="is_test" name="is_test">
                                                            <option value="1" <?php if ($row['is_test'] == '1') echo 'selected="selected"'?>>Есть</option>
                                                            <option value="0" <?php if ($row['is_test'] == '0') echo 'selected="selected"'?>>Нет</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="payment" class="col-xs-3 control-label">Способ оплаты:</label>
                                                    <div class="col-xs-5 width-175">
                                                        <select class="form-control" id="payment" name="payment_methods">
                                                            <option>Нал./безнал.</option>
                                                            <option>Наличный</option>
                                                            <option>Безналичный</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group horiz-line">
                                                    <label for="nds" class="col-xs-3 control-label">НДС:</label>
                                                    <div class="col-xs-5 width-175">
                                                        <input type="text" class="form-control" id="nds" name="nds" value="<?php $nds = set_value('nds'); echo $nds = !empty($nds) ? $nds : $row['nds']; ?>">
                                                    </div>
                                                    <div class="col-xs-2 width-90">
                                                        <select class="form-control" name="id_nds">
                                                            <option>%</option>
                                                            <option>тг.</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="delivery" class="col-xs-3 control-label">Доставка:</label>
                                                    <div class="col-xs-5 width-175">
                                                        <select class="form-control" id="delivery" name="shipping">
                                                            <option value="1" <?php if ($row['shipping'] == '1') echo 'selected="selected"'?>>Есть</option>
                                                            <option value="0" <?php if ($row['shipping'] == '0') echo 'selected="selected"'?>>Нет</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="delivery" class="col-xs-3 control-label">Бесплатная доставка:</label>
                                                    <div class="col-xs-4 width-175">
                                                        <select class="form-control" id="delivery" name="free_shipping">
                                                            <option value="1" <?php if ($row['free_shipping'] == '1') echo 'selected="selected"'?>>Есть</option>
                                                            <option value="0" <?php if ($row['free_shipping'] == '0') echo 'selected="selected"'?>>Нет</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-1 ">
                                                        <span>От</span>
                                                    </div>
                                                    <div class="col-xs-2 width-300">
                                                        <input type="text" class="form-control" id="free-delivery" name="free_shipping_from" value="<?php $free_shipping_from = set_value('free_shipping_from'); echo $free_shipping_from = !empty($free_shipping_from) ? $free_shipping_from : $row['free_shipping_from']; ?>">
                                                    </div>
                                                </div>

                                                <div class="form-group horiz-line">
                                                    <label for="delivery" class="col-xs-3 control-label">Стоимость доставки:</label>
                                                    <div class="col-xs-5 width-175">
                                                        <input type="text" class="form-control" id="cost-delivery" name="cost_shipping"  value="<?php $cost_shipping = set_value('cost_shipping'); echo $cost_shipping = !empty($cost_shipping) ? $cost_shipping : $row['cost_shipping']; ?>">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="short-desc" class="col-lg-3 col-sm-5 control-label">Расписание доставки:</label>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-lg-5 col-xs-6 col-lg-offset-1">
                                                        <div class="work-day">
                                                            <span>ПН</span>
                                                            <input type="text" class="time-work form-control" name="shippingday1from" value="<?php $shippingday1from = set_value('shippingday1from'); echo $shippingday1from = !empty($shippingday1from) ? $shippingday1from : $row['shippingday1from']; ?>">
                                                            <span> - </span>
                                                            <input type="text" class="time-work form-control" name="shippingday1to" value="<?php $shippingday1to = set_value('shippingday1to'); echo $shippingday1to = !empty($shippingday1to) ? $shippingday1to : $row['shippingday1to']; ?>">
                                                            <span class="check-day"></span>
                                                        </div>
                                                        <div class="work-day">
                                                            <span>ВТ</span>
                                                            <input type="text" class="time-work form-control" name="shippingday2from" value="<?php $shippingday2from = set_value('shippingday2from'); echo $shippingday2from = !empty($shippingday2from) ? $shippingday2from : $row['shippingday2from']; ?>">
                                                            <span> - </span>
                                                            <input type="text" class="time-work form-control" name="shippingday2to" value="<?php $shippingday2to = set_value('shippingday2to'); echo $shippingday2to = !empty($shippingday2to) ? $shippingday2to : $row['shippingday2to']; ?>">
                                                            <span class="check-day"></span>
                                                        </div>
                                                        <div class="work-day">
                                                            <span>СР</span>
                                                            <input type="text" class="time-work form-control" name="shippingday3from" value="<?php $shippingday3from = set_value('shippingday3from'); echo $shippingday3from = !empty($shippingday3from) ? $shippingday3from : $row['shippingday3from']; ?>">
                                                            <span> - </span>
                                                            <input type="text" class="time-work form-control" name="shippingday3to" value="<?php $shippingday3to = set_value('shippingday3to'); echo $shippingday3to = !empty($shippingday3to) ? $shippingday3to : $row['shippingday3to']; ?>">
                                                            <span class="check-day"></span>
                                                        </div>
                                                        <div class="work-day">
                                                            <span>ЧТ</span>
                                                            <input type="text" class="time-work form-control" name="shippingday4from" value="<?php $shippingday4from = set_value('shippingday4from'); echo $shippingday4from = !empty($shippingday4from) ? $shippingday4from : $row['shippingday4from']; ?>">
                                                            <span> - </span>
                                                            <input type="text" class="time-work form-control" name="shippingday4to" value="<?php $shippingday4to = set_value('shippingday4to'); echo $shippingday4to = !empty($shippingday4to) ? $shippingday4to : $row['shippingday4to']; ?>">
                                                            <span class="check-day"></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-5 col-xs-6">
                                                        <div class="work-day">
                                                            <span>ПТ</span>
                                                            <input type="text" class="time-work form-control" name="shippingday5from" value="<?php $shippingday5from = set_value('shippingday5from'); echo $shippingday5from = !empty($shippingday5from) ? $shippingday5from : $row['shippingday5from']; ?>">
                                                            <span> - </span>
                                                            <input type="text" class="time-work form-control" name="shippingday5to" value="<?php $shippingday5to = set_value('shippingday5to'); echo $shippingday5to = !empty($shippingday5to) ? $shippingday5to : $row['shippingday5to']; ?>">
                                                            <span class="check-day"></span>
                                                        </div>
                                                        <div class="work-day">
                                                            <span>СБ</span>
                                                            <input type="text" class="time-work form-control" name="shippingday6from" value="<?php $shippingday6from = set_value('shippingday6from'); echo $shippingday6from = !empty($shippingday6from) ? $shippingday6from : $row['shippingday6from']; ?>">
                                                            <span> - </span>
                                                            <input type="text" class="time-work form-control" name="shippingday6to" value="<?php $shippingday6to = set_value('shippingday6to'); echo $shippingday6to = !empty($shippingday6to) ? $shippingday6to : $row['shippingday6to']; ?>">
                                                            <span class="check-day"></span>
                                                        </div>
                                                        <div class="work-day">
                                                            <span>ВС</span>
                                                            <input type="text" class="time-work form-control" name="shippingday7from" value="<?php $shippingday7from = set_value('shippingday7from'); echo $shippingday7from = !empty($shippingday7from) ? $shippingday7from : $row['shippingday7from']; ?>">
                                                            <span> - </span>
                                                            <input type="text" class="time-work form-control" name="shippingday7to" value="<?php $shippingday7to = set_value('shippingday7to'); echo $shippingday7to = !empty($shippingday7to) ? $shippingday7to : $row['shippingday7to']; ?>">
                                                            <span class="check-day"></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-xs-offset-5 col-xs-3">
                                                        <button type="submit" class="btn btn-profile">Сохранить</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>


                        </div><!-- /.content-->
                    </div><!-- /.two-col -->
                </div>
            </div>
        </div><!-- /.container -->
    </section>
    <?php endforeach; ?>


<script>
    function save(){
        setWorkDays();
        setConditions();
    }
    function setConditions(){
        var formData = new FormData(document.forms.upload);
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
            }
        };
        xhttp.open("POST", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/' ?>index.php/user/Conditions/");
        xhttp.send(formData);
    }
    function setWorkDays(){
        var formData = new FormData(document.forms.upload);
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
            }
        };
        xhttp.open("POST", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/' ?>index.php/user/setWorkDays/");
        xhttp.send(formData);
    }
</script>

