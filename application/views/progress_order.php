<link href="/../../css/style2.css" rel="stylesheet">
<section class="two-col-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="two-col">
                    <div class="profile-menu open hidden-sm hidden-xs" id="profile-menu">
                        <ul>
                            <li ><a id="orders" href="<?php echo SITE_NAME ?>index.php/cart/getOrdersOutgoing">Заказы</a></li>
                            <?php if($this->session->userdata('goal') == TRUE ) : ?>
                                <li><a id="productsIcon" href="<?php echo SITE_NAME ?>index.php/Products/myProducts">Товары</a></li>
                                <li><a id="stock" href="#">Акции</a></li>
                                <ul>
                                </ul>
                                <li><a id="news" href="#">Новости</a></li>
                            <?php endif; ?>
                            <?php if($this->session->userdata('goal') == FALSE ) :?>
                                <ul>
                                </ul>
                            <?php endif; ?>
                            <li><a id="statistics" href="#">Статистика</a></li>
                            <li><a id="favorites" href="<?php echo SITE_NAME?>index.php/Favorites/getFavoritesCompanies">Избранное</a></li>
                        </ul>
                    </div><!-- /.profile-menu -->

                    <div class="megamenu-left close hidden-sm hidden-xs" id="megamenu">

                    </div><!-- /.megamenu-left -->
                    <div class="content profile">
                        <div class="row">
                            <div class="col-lg-3 col-lg-push-9 col-sm-4 col-sm-push-8">
                                <div class="dop-menu">
                                    <nav class="navbar">
                                        <div class="navbar-header" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#dop-menu-collapse" aria-expanded="false">
                                            <button type="button" class="navbar-toggle collapsed">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                            <a class="navbar-brand" href="#">Меню</a>
                                        </div>

                                        <div class="collapse navbar-collapse" id="dop-menu-collapse">
                                            <ul class="nav navbar-nav">
                                                <?php if($this->session->userdata('goal') == TRUE ) : ?>
                                                    <li ><a href="<?php echo SITE_NAME ?>index.php/cart/getOrdersIncoming">Входящие заказы</a></li>
                                                    <li class="active"><a href="<?php echo SITE_NAME ?>index.php/cart/inProgress">Обрабатываемые заказы</a></li>
                                                <?php endif;?>
                                                <li><a href="<?php echo SITE_NAME ?>index.php/cart/getOrdersOutgoing">Исходящие заказы</a></li>
                                                <li ><a href="<?php echo SITE_NAME ?>index.php/cart/getHistory">История заказов</a></li>
                                            </ul>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                            <div class="tabs">
                                <div class="col-lg-9 col-lg-pull-3 col-sm-8 col-sm-pull-4">
                                    <div class="profile-content tabs-wrapper order">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <?php if($this->session->userdata('goal') == TRUE ) : ?>
                                                <li role="presentation">
                                                    <a href="<?php echo SITE_NAME ?>index.php/cart/getOrdersIncoming">
                                                        <span class="hidden-xs">Входящие</span>
                                                        <span class="visible-xs-inline">Входящие</span>
                                                        <span>&nbsp;</span>
                                                    </a>
                                                </li>
                                                <li role="presentation" class="active">
                                                    <a href="<?php echo SITE_NAME ?>index.php/cart/inProgress">
                                                        <span class="hidden-xs">Обрабатываются</span>
                                                        <span class="visible-xs-inline">Обрабатываются</span>
                                                        <span>&nbsp;</span>
                                                    </a>
                                                </li>
                                            <?php endif; ?>
                                            <li role="presentation" >
                                                <a href="<?php echo SITE_NAME ?>index.php/cart/getOrdersOutgoing">
                                                    <span class="hidden-xs">Исходящие</span>
                                                    <span class="visible-xs-inline">Исходящие</span>
                                                    <span>&nbsp;</span>
                                                </a>
                                            </li>
                                            <li role="presentation">
                                                <a href="<?php echo SITE_NAME ?>index.php/cart/getHistory">
                                                    <span class="hidden-xs">История</span>
                                                    <span class="visible-xs-inline">История</span>
                                                    <span>&nbsp;</span>
                                                </a>
                                            </li>
                                        </ul>

                                        <section id="content1">
                                            <div class="bs-example" id="products">
                                                <div class="panel-group" id="accordion">
                                                    <?php foreach ($orders->result_array() as $row): ?>
                                                        <div class="panel panel-default">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $row['id_order']?>" class="accordion-btn" onclick="getOrderOut(<?php echo "'".$row['user']."'" ?>,<?php echo "'".$row['order_date']."'" ?>,<?php echo "'".$row['id_order']."'" ?>)">
                                                                <div class="panel-heading panel-first">
                                                                    <div class="panel-title">
                                                                        <div class="row">
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                                                                    <div class="toggle-btn"><?php echo $row['user'] ?></div>
                                                                                </div>
                                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                                                                    <span>Заказ № <?php echo $row['id_order'] ?></span>
                                                                                </div>
                                                                                <div class="col-lg-3 col-md-4 col-sm-3 col-xs-6">
                                                                                    <span><?php echo $row['order_date'] ?></span>
                                                                                </div>
                                                                                <div class="col-lg-3 col-md-2 col-sm-3 col-xs-6">
                                                                                    <?php if($row['order_status'] != null): ?>
                                                                                        <button class="change-btn"><?php echo $row['order_status']?></button>
                                                                                    <?php endif; ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                            <div id="collapse<?php echo $row['id_order']?>" class="panel-collapse collapse">
                                                                <div id="panel-body<?php echo $row['id_order']?>" class="panel-body">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endforeach ?>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>