
                    <div class="content profile">
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="profile-content">
                                    <ul class="breadcrumb">
                                        <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/product/myProducts">Мои товары </a></li>
                                        <li class="active">Редактировать товар</li>
                                    </ul>
                                    <div class="profile-form">
                                        <?php echo form_open_multipart('http://'.$_SERVER['SERVER_NAME'].'/product/editProduct/', array('class' => "form-horizontal", 'name' => 'upload')) ?>
                                        <?php if ($info != null) : ?>
                                            <?php foreach ($product->result_array() as $row) : ?>
                                                <input type="hidden" value="<?php echo $row['id_product']?>" name="id_product">
                                                <div class="form-group">
                                                    <label for="name" class="col-xs-3 control-label">Наименование товара:</label>
                                                    <div class="col-xs-8">
                                                        <input type="text" class="form-control" id="name" name="name_product" maxlength="120" value='<?php $name_product = set_value('name_product'); echo $name_product = !empty($name_product) ? $name_product : $row['name_product']; ?>'>
                                                        <?php echo form_error('name_product', '<div class="css-error">', '</div>'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="name" class="col-xs-3 control-label">Описание товара:</label>
                                                    <div class="col-xs-8">
                                                        <textarea name="product_meta" class="form-control" id="product_meta"><?php $product_meta = set_value('product_meta'); echo $product_meta = !empty($product_meta) ? $product_meta : $row['product_meta']; ?></textarea>
                                                        <?php echo validation_errors(); ?>
                                                    </div>
                                                </div>
												<div class="form-group">
                                                    <label for="name" class="col-xs-3 control-label">Штрих код:</label>
                                                    <div class="col-xs-8">
                                                        <input type="text" class="form-control" id="barcode" name="barcode" value="<?php $barcode = set_value('barcode'); echo $barcode = !empty($barcode) ? $pbarcode : $row['barcode']; ?>">
                                                    </div>
                                                </div>
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label">Страна производитель:</label>
                                                <div class="col-xs-6">
                                                    <input type="text" class="form-control" name="name_country" value="<?php $name_country = set_value('name_country'); echo $name_country = !empty($name_country) ? $name_country : $row['name_country'];?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label" for="price">Цена:</label>
                                                <div class="input-group price">
                                                    <input type="text" class="form-control" id="price" name="price" value="<?php $price = set_value('price'); echo $price = !empty($price) ? $price : $row['price']; ?>">
                                                    <div class="input-group-addon"><?php echo $row['icon_currency'] ?></div>
                                                </div>
                                                <?php echo form_error('price', '<div class="css-error">', '</div>'); ?>
                                            </div>
											<div class="form-group" >
                                                <div class="col-xs-offset-3 col-xs-8">
                                                    <div class="row">
                                                                <input type="checkbox" value="1" name="in_stock" <?php if($row['in_stock'] == 1 ) echo 'checked'?>>
                                                        <p class="col-md-6 col-sm-6 col-xs-6">В наличии</p>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group" >
                                                <div class="col-xs-offset-3 col-xs-8">
                                                    <div class="row">
                                                                <input type="checkbox" value="1" name="at_the_end" <?php if($row['at_the_end'] == 1 ) echo 'checked'?>>
                                                        <p class="col-md-6 col-sm-6 col-xs-6">Товар на исходе</p>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group" >
                                                <div class="col-xs-offset-3 col-xs-8">
                                                    <div class="row">
                                                                <input type="checkbox" value="1" name="sample" <?php if($row['sample'] == 1 ) echo 'checked'?>> 
                                                        <p class="col-md-6 col-sm-6 col-xs-6">Доступна проба товара</p>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group" >
                                                <div class="col-xs-offset-3 col-xs-8">
                                                    <div class="row">
                                                                <input type="checkbox" value="1" name="stock" <?php if($row['stock'] == 1 ) echo 'checked'?>>
                                                        <p class="col-md-6 col-sm-6 col-xs-6">Акция</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label">Фотография товара <span class="grey">(200px*200px)</span>:</label>
                                                <div class="col-xs-8">
                                                    <div class="logo-img" id="main">
                                                        <?php if ($row['path'] != null) : ?>
                                                            <img class='css-pointer' onclick="$('#file').click()" src="/../../images/mini/<?php $path = set_value('path'); echo $path = !empty($path) ? $path : $row['path']; ?>" alt="">
                                                            <a onclick="deleteImage()"><span class="glyphicon glyphicon-remove"></span></a>
                                                            <input type="hidden" id="path" name="path" value="<?php $path = set_value('path'); echo $path = !empty($path) ? $path : $row['path']; ?>">
                                                        <?php endif; ?>
                                                        <?php if ($row['path'] == null) : ?>
                                                            <img class='css-pointer' onclick="$('#file').click()" src="/../../images/download-image.png" alt="">
                                                        <?php endif; ?>
                                                    </div>
                                                    <input type="file" id="file" class="css-hide" name="userfile" size="20" onchange="loadImage()"/>
                                                </div>
                                            </div>
                                                <br>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <div class="form-group">
                                            <div class="col-xs-offset-4 col-xs-3">
                                                <button type="submit" class="btn btn-profile">Сохранить</button>
                                            </div>
                                        </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>

<script>
    function loadImage(){
		var file = document.getElementById('file');
        var formData = new FormData(document.forms.upload);
        var response = document.getElementById('info');
		var file2 = '"#file"';
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                var jsonText = JSON.parse(xhttp.responseText);
                for (key in jsonText) {
                    console.log(key);
                    main.innerHTML = "<img class='css-pointer' onclick='$("+file2+").click()' src='/../../images/mini/"+jsonText['file_name']+"' alt=''> <a onclick='deleteImage()'><span class='glyphicon glyphicon-remove css-pointer'></span></a> <input type='hidden' id='path' name='path' value='"+jsonText['file_name']+"'>";
                }
				file.value = '';
            }
        };
        xhttp.open("POST", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/' ?>index.php/image/do_upload/");
        xhttp.send(formData);
    }

    function deleteImage(){
        var path = document.getElementById('path').value;
		var file2 = "'#file'";
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                main.innerHTML = '<img class="css-pointer" onclick="$('+file2+').click()" src="/../../images/download-image.png" alt="">';
            }
        };
        xhttp.open("GET", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/' ?>index.php/image/deleteImage/"+path);
        xhttp.send();
    }
</script>
