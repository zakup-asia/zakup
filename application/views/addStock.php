				
					<div class="content profile">
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="profile-content">
                                    <ul class="breadcrumb">
                                        <li><a href="http://<?php echo $_SERVER['SERVER_NAME']?>/StockAndNews/getMyStock">Мои акции</a></li>
                                        <li class="active">Новая акция</li>
                                    </ul>
                                    <div class="profile-form">
                                        <?php echo form_open_multipart('http://'.$_SERVER['SERVER_NAME'].'/stockAndNews/addStock/', array('class' => 'form-horizontal', 'name' => 'upload')) ?>
                                            <div class="form-group">
                                                <label for="name" class="col-xs-3 control-label">Название акции:*</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="name_stock" name="name_stock">
													<span id="name_stockcharsLeft" class="grey"></span>&nbsp;<span class="grey">знаков осталось</span>
                                                    <?php echo form_error('name_stock', '<div class="css-error">', '</div>'); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="name" class="col-xs-3 control-label">Описание акции:*</label>
                                                <div class="col-xs-8">
                                                    <textarea class="form-control" id="description_stock" name="description_stock"></textarea>
													<span id="description_stockcharsLeft" class="grey"></span>&nbsp;<span class="grey">знаков осталось</span>
                                                    <?php echo form_error('description_stock', '<div class="css-error">', '</div>'); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="name" class="col-xs-3 control-label">Дата начала:</label>
                                                <div class="col-xs-8">
                                                    <input type="date" class="form-control" id="begin_stock" name="begin_stock" value="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="name" class="col-xs-3 control-label">Дата окончания:</label>
                                                <div class="col-xs-8">
                                                    <input type="date" class="form-control" id="end_stock" name="end_stock" value="">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label">Фотография акции</label>
                                                <div class="col-xs-8">
                                                    <div class="logo-img" id="main">
                                                        <img class="css-pointer" onclick="$('#file').click()" src="/../../images/download-image.png" alt="">
                                                    </div>
                                                    <input class="css-hide" type="file" id="file" name="userfile" size="20" onchange="loadImage()"/>
                                                    <div id="info">
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <br>
                                            <div class="form-group">
                                                <div class="col-xs-offset-4 col-xs-3">
                                                    <button type="submit" class="btn btn-profile">Сохранить</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
</section>

<script>
    function loadImage(){
		var file = document.getElementById('file');
		var file2 = '"#file"';
        var formData = new FormData(document.forms.upload);
        var response = document.getElementById('info');
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                var jsonText = JSON.parse(xhttp.responseText);
                for (key in jsonText) {
                    console.log(key);
                    main.innerHTML = "<img class='css-pointer' onclick='$("+file2+").click()' src='/../../images/mini/"+jsonText['file_name']+"' alt=''> <a onclick='deleteImage()'><span class='glyphicon glyphicon-remove css-pointer'></span></a> <input type='hidden' id='path'name='path' value='"+jsonText['file_name']+"'>";
                }
				file.value = '';
            }
        };
        xhttp.open("POST", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/' ?>index.php/image/do_upload/");
        xhttp.send(formData);
    }

    function deleteImage(){
        var path = document.getElementById('path').value;
		var file2 = "'#file'";
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                main.innerHTML = '<img class="css-pointer" onclick="$('+file2+').click()" src="/../../images/download-image.png" alt="">';
            }
        };
        xhttp.open("GET", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/' ?>index.php/image/deleteImage/"+path);
        xhttp.send();
    }
</script>