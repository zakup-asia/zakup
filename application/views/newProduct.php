
                    <div class="content profile">
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="profile-content">
                                    <ul class="breadcrumb">
                                        <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/product/myProducts">Мои товары</a></li>
                                        <li class="active">Новый товар</li>
                                    </ul>
                                    <div class="profile-form">
                                        <form class="form-horizontal">
                                            <div class="form-group">
                                                <label for="name" class="col-xs-3 control-label">Наименование товара:</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="name">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label">Раздел товара:</label>
                                                <div class="col-xs-6">
                                                    <select type="text" class="form-control">
                                                        <option>Раздел 1</option>
                                                        <option>Раздел 2</option>
                                                        <option>Раздел 3</option>
                                                        <option>Раздел 4</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label">Категория товара:</label>
                                                <div class="col-xs-6">
                                                    <select type="text" class="form-control">
                                                        <option>Категория 1</option>
                                                        <option>Категория 2</option>
                                                        <option>Категория 3</option>
                                                        <option>Категория 4</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label">Подкатегория товара:</label>
                                                <div class="col-xs-6">
                                                    <select type="text" class="form-control">
                                                        <option>Подкатегория 1</option>
                                                        <option>Подкатегория 2</option>
                                                        <option>Подкатегория 3</option>
                                                        <option>Подкатегория 4</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label">Страна производитель:</label>
                                                <div class="col-xs-6">
                                                    <select type="text" class="form-control">
                                                        <option>Страна 1</option>
                                                        <option>Страна 2</option>
                                                        <option>Страна 3</option>
                                                        <option>Страна 4</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label" for="price">Цена:</label>
                                                <div class="input-group price">
                                                    <input type="text" class="form-control" id="price">
                                                    <div class="input-group-addon">Тг.</div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-offset-3 col-xs-6">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" checked=""> В наличии
                                                        </label>
                                                        <label>
                                                            <input type="checkbox"> Товар на исходе
                                                        </label>
                                                        <label>
                                                            <input type="checkbox"> Доступна проба товара
                                                        </label>
                                                        <label>
                                                            <input type="checkbox"> Акция
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label">Фотография товара <span class="grey">(200px*200px)</span>:</label>
                                                <div class="col-xs-8">
                                                    <div class="product-img">
                                                        <img src="/../../images/add-image-big.jpg" alt="">
                                                        <div class="small-img">
                                                            <img src="/../../images/add-image-small.jpg" alt="" >
                                                            <img src="/../../images/add-image-small.jpg" alt="" >
                                                        </div>
                                                    </div>
                                                    <input type="file" name="logo_img" accept="image/*">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-offset-4 col-xs-3">
                                                    <button type="submit" class="btn btn-profile">Сохранить</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="action-menu">
                                    <ul class="nav">
                                        <li><a href="#">Сохранить</a></li>
                                        <li><a href="#">Отмена</a></li>
                                        <li><a href="#">Сохранить и добавить еще</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>