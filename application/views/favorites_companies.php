
                    <div class="content results">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="results-tab">
                                    <div class="tabs-wrapper">
                                        <div class="row">
                                            <div class="col-sm-8">

                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation"  class="active">
                                                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/Favorites/getFavoritesCompanies">
                                                            <span class="hidden-xs">Компании</span>
                                                            <span class="visible-xs-inline">Компании</span>
                                                            <span>&nbsp;</span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/Favorites/getFavoritesProducts">
                                                            <span class="hidden-xs">Товары</span>
                                                            <span class="visible-xs-inline">Товары</span>
                                                            <span>&nbsp;</span>
                                                        </a>
                                                    </li>
                                                </ul>

                                                <ul class="nav nav-tabs">
                                                    <li>
                                                        <a href="#providers" aria-controls="providers" role="tab" data-toggle="tab"></a>
                                                    </li>
                                                </ul> <!-- /.nav-tabs -->

                                                <div class="tab-content">
                                                    <div class="tab-pane products-wrapper active" id="products">
                                                        <div class="products">
                                                            <?php foreach ($favorites->result_array() as $row) : ?>
                                                                <div class="row">
                                                                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/Suppliers/getSupplier/<?php echo $row['id_company']?>">
                                                                        <?php if($row['path'] != null): ?>
                                                                            <div class="col-md-4">
                                                                                <img  width='120' src="/../../images/mini/<?php $path = set_value('path'); echo $path = !empty($path) ? $path : $row['path']; ?>" alt="">
                                                                            </div>
                                                                        <?php endif; ?>
                                                                        <?php if ($row['path'] == null) : ?>
                                                                            <div class="col-md-4">
                                                                                <img width='120' src="/../../images/best-action.jpg" alt="">
                                                                            </div>
                                                                        <?php endif; ?>

                                                                        <div class="col-md-7">
                                                                            <p><?php echo $row['name'] ?></p>
                                                                            <div class="provider-cats">
                                                                                <?php if($row['short_description'] != null):?>
                                                                                    <p><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/Suppliers/getSupplier/<?php echo $row['id_company']?>"><?php echo $row['short_description'] ?></a></p>
                                                                                <?php endif; ?>
                                                                            </div>
                                                                            <div class="provider-info">
                                                                                <?php if($row['payment_methods'] != null):?>
                                                                                    <span class="small">Способы оплаты:</span>
                                                                                    <span class="small"><?php echo $row['payment_methods'] ?></span>
                                                                                <?php endif; ?>
                                                                                <br>
                                                                                <?php if($row['min_amount'] != null):?>
                                                                                    <span class="small">Мин. сумма заказа:</span>
                                                                                    <span class="small"><?php echo $row['min_amount'].' '?></span>
                                                                                <?php endif; ?>
                                                                                <br>
                                                                                <?php if($row['shipping'] != null):?>
                                                                                    <span class="small">Доставка:</span>
                                                                                    <span class="small"><?php if($row['shipping'] == '1') echo 'есть'; else echo 'нет'  ?></span>
                                                                                <?php endif; ?>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            <?php endforeach; ?>
                                                        </div>
                                                        <nav>
                                                            <ul class="pagination">
                                                                <li>
                                                                    <a href="#" aria-label="Previous">
                                                                        <span aria-hidden="true">&laquo;</span>
                                                                    </a>
                                                                </li>
                                                                <li><a href="#">1</a></li>
                                                                <li>
                                                                    <a href="#" aria-label="Next">
                                                                        <span aria-hidden="true">&raquo;</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </nav>
                                                    </div><!-- / #products -->
                                                    <div class="tab-pane providers-wrapper" id="providers">
                                                        <div class="products">

                                                        </div>
                                                    </div><!-- / #products -->
                                                </div><!-- /.tab-content  -->
                                            </div>

                                        </div>


                                    </div>	<!-- /.tabs-wrapper-->
                                </div><!-- /.povider-tab -->
                            </div>
                        </div>

                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>

