<section class="two-col-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="two-col">
                    <div class="profile-menu open " style="display: block;" id="profile-menu">
                        <ul>
                            <?php if($this->session->userdata('goal') == TRUE ) : ?>
                                <li><a id="orders" <?php if ($active == 'orders') echo 'class="css-bold"'?> href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/getOrdersIncoming">Заказы<span id="vOrders"></span></a></li>
                            <?php endif; ?>
                            <?php if($this->session->userdata('goal') == FALSE ) : ?>
                                <li><a id="orders" <?php if ($active == 'orders') echo 'class="css-bold"'?> href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/getOrdersOutgoing">Заказы</a></li>
                            <?php endif; ?>
                            <?php if($this->session->userdata('goal') == TRUE ) : ?>
                                <li><a id="productsIcon" <?php if ($active == 'products') echo 'class="css-bold"'?> href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/product/myProducts">Товары<span id="vProducts"></span></a></li>
                                <li><a id="stock" <?php if ($active == 'stock') echo 'class="css-bold"'?> href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/stockAndNews/getMyStock">Акции<span id="vStocks"></span></a></li>
                                <ul>
                                </ul>
                                <li><a id="news" <?php if ($active == 'news') echo 'class="css-bold"'?> href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/stockAndNews/getMyNews">Новости<span id="vNews"></span></a></li>
                            <?php endif; ?>
                            <?php if($this->session->userdata('goal') == FALSE ) :?>
                                <ul>
                                </ul>
                            <?php endif; ?>
                            <li><a id="statistics" <?php if ($active == 'statistics') echo 'class="css-bold"'?> href="#">Статистика</a></li>
                            <li><a id="favorites" <?php if ($active == 'favorites') echo 'class="css-bold"'?> href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/favorites/getFavoritesCompanies">Избранное</a></li>
							<ul></ul>
							<li><a id="cssoptions" <?php if ($active == 'main') echo 'class="css-bold"'?> href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/main/">Настройки</a></li>
                        </ul>
                    </div><!-- /.profile-menu -->