
                    <div class="content profile">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="profile-content">
                                    <ul class="breadcrumb">
                                        <li class="active">Мои новости</li>
                                        <div class="add-product-link"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/StockAndNews/addNew">Добавить новость</a></div>
                                    </ul>
                                    <div class="profile-products">
                                        <?php if ($news != null) : ?>
                                            <?php foreach ($news->result_array() as $row) : ?>
                                                <div class="profile-product">
                                                    <div class="product-name">
                                                        <p><?php echo $row['name_new']?></p>
                                                        <span class="country"><?php echo $row['short_discription_new']?></span>
                                                        <div class="product-price"><?php echo $row['data_new']?></div>
                                                    </div>
                                                    <ul class="product-actions">
                                                        <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/StockAndNews/deleteNew?id_new=<?php echo $row['id_new']?>" class="delete" onclick="decNotViewedNews()"></a></li>
                                                        <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/StockAndNews/editNew?id_new=<?php echo $row['id_new']?>" class="edit"></a></li>
                                                        <li><input type="checkbox"></li>
                                                        <li>
                                                            <a type="button" class="more dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#">Action</a></li>
                                                                <li><a href="#">Another action</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>