                    <div class="content profile">
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="profile-content">
                                    <ul class="breadcrumb">
                                        <li><a href="http://<?php echo $_SERVER['SERVER_NAME']?>/StockAndNews/getMyNews">Мои новости</a></li>
                                        <li class="active">Новая новость</li>
                                    </ul>
                                    <div class="profile-form">
                                        <?php echo form_open_multipart('http://'.$_SERVER['SERVER_NAME'].'/StockAndNews/addNew/', array('class' => "form-horizontal", 'name' => 'upload')) ?>
                                            <input type="hidden" name="id_company" value="<?php echo $this->session->userdata('id_company')?>">
                                            <div class="form-group">
                                                <label for="name" class="col-xs-3 control-label">Название новости:*</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="name_new" name="name_new">
													<span id="name_newcharsLeft" class="grey"></span>&nbsp;<span class="grey">знаков осталось</span>
                                                    <?php echo form_error('name_new', '<div class="css-error">', '</div>'); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="name" class="col-xs-3 control-label">Описание новости:*</label>
                                                <div class="col-xs-8">
                                                    <textarea class="form-control" id="description_new" name="description_new"></textarea>
													<span id="description_newcharsLeft" class="grey"></span>&nbsp;<span class="grey">знаков осталось</span>
                                                    <?php echo form_error('description_new', '<div class="css-error">', '</div>'); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label">Фотография новости</label>
                                                <div class="col-xs-8">
                                                    <div class="logo-img" id="main">
                                                        <img class="css-pointer" onclick="$('#file').click()" src="/../../images/download-image.png" alt="">
                                                    </div>
                                                    <input class="css-hide" type="file" id="file" name="userfile" size="20" onchange="loadImage()" />
                                                    <div id="info">
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <br>
                                            <div class="form-group">
                                                <div class="col-xs-offset-4 col-xs-3">
                                                    <button type="submit" class="btn btn-profile" onclick="incNotViewedNews()">Сохранить</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
</section>

<script>
    function loadImage(){
		var file = document.getElementById('file');
		var file2 = '"#file"';
        var formData = new FormData(document.forms.upload);
        var response = document.getElementById('info');
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                var jsonText = JSON.parse(xhttp.responseText);
                for (key in jsonText) {
                    console.log(key);
                    main.innerHTML = "<img class='css-pointer' onclick='$("+file2+").click()' src='/../../images/mini/"+jsonText['file_name']+"' alt=''> <a onclick='deleteImage()'><span class='glyphicon glyphicon-remove css-pointer'></span></a> <input type='hidden' id='path'name='path' value='"+jsonText['file_name']+"'>";
                }
				file.value = '';
            }
        };
        xhttp.open("POST", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/' ?>index.php/image/do_upload/");
        xhttp.send(formData);
    }

    function deleteImage(){
        var path = document.getElementById('path').value;
		var file2 = "'#file'";
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                main.innerHTML = '<img class="css-pointer" onclick="$('+file2+').click()" src="/../../images/download-image.png" alt="">';
            }
        };
        xhttp.open("GET", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/' ?>index.php/image/deleteImage/"+path);
        xhttp.send();
    }
</script>
