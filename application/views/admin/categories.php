<?php if($this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) :?>
    <nav class="navbar navbar-default">
        <ul class="nav navbar-nav">
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSection">Разделы</a>
            </li>
            <li class="active">
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCategory">Категории</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSubcategory">Подкатегории</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCountry">Страны</a>
            </li>
            <li >
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllRegion">Регионы</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCity">Города</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCurrency">Валюты</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getSliders">Слайды</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCompanies">Продавцы</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/User/getAllUsers">Покупатели</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllOrders"> Заказы</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/logoutAdmin"> Выйти </a>
            </li>

        </ul>
    </nav>

        <div class="col-xs-4">
            <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/addCategory') ?>
            <select name="id_section_one">
                <option></option>
                <?php foreach($section->result_array() as $row):?>
                    <option value="<?php echo $row['id_section_one']?>"> <?php echo $row['name_section'] ?> </option>
                <?php endforeach?>
            </select>
            <input type="text" name="name_category">
            <input type="submit" value="Добавить">
            </form>
        </div>

        <table class="table table-striped ">
            <caption><h4>Таблица категорий</h4></caption>
            <tr>
                <th>№</th>
                <th>Название категории</th>
                <th>Относится к</th>
                <th>Действие</th>
            </tr>
            <?php foreach($categories->result_array() as $row):?>
                <tr>
                    <td>
                        <?php echo $row['id_category']?>
                    </td>
                    <td>
                        <?php echo $row['name_category'] ?>
                    </td>
                    <td>
                        <?php echo $row['name_section']?>
                    </td>
                    <td>
                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/editCategory/<?php echo $row['id_category']?>"> Редактировать </a>
                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/addSubcategoryForC/<?php echo $row['id_category']?>"> Добавить подкатегорию</a>
                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/deleteCategory/<?php echo $row['id_category']?>"> Удалить </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
<?php endif; ?>
<?php if($this->session->userdata('admin') == null) : ?>
    <!--
        Форма входа и кнопочка выхода.
    -->
    <div class="padding-divs">
        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/loginAdmin', array('class' => "form-horizontal")) ?>
        <p> Логин админа 123</p>
        <input type="text" name="nickname">
        <p> Пароль админа 123</p>
        <input type="password" name="password">
        <br><br>
        <input type="submit" value="Войти">
        </form>
    </div>
<?php endif; ?>

