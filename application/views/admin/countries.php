<?php if($this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) :?>
    <div class="padding-divs">

        <nav class="navbar navbar-default">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/AdminPanel/getAllSection">Разделы</a>
                </li>
                <li >
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/AdminPanel/getAllCategory">Категории</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/AdminPanel/getAllSubcategory">Подкатегории</a>
                </li>
                <li class="active">
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/AdminPanel/getAllCountry">Страны</a>
                </li>
                <li >
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/AdminPanel/getAllRegion">Регионы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/AdminPanel/getAllCity">Города</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/AdminPanel/getCurrency">Валюты</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/AdminPanel/getSliders">Слайды</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/AdminPanel/getCompanies">Продавцы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/User/getAllUsers">Покупатели</a>
                </li>
                <li >
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/AdminPanel/getAllOrders"> Заказы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/AdminPanel/logoutAdmin"> Выйти </a>
                </li>

            </ul>
        </nav>

        <?php //TODO Вывод доступных валют ?>
        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/addCountry', array()) ?>
            <input type="text" name="name_country" placeholder="Название страны">
            <input type="text" name="mask_country" placeholder="Маска">
            <select name="id_currency">
                <option value="null"></option>
                <?php foreach ($currency->result_array() as $row) :?>
                    <option value="<?php $id_currency = set_value('id_currency'); echo $id_currency = !empty($id_currency) ? $id_currency : $row['id_currency']; ?>"> <?php $name_currency = set_value('name_currency'); echo $name_currency = !empty($name_currency) ? $name_currency : $row['name_currency']; ?></option>
                <?php endforeach; ?>
            </select>
            <input type="submit" value="Добавить">
        </form>


        <table class="table table-striped ">
            <caption><h4>Таблица стран</h4></caption>
            <tr>
                <th>№</th>
                <th>Название страны</th>
                <th>Валюта страны</th>
                <th>Маска</th>
                <th>Действие</th>
            </tr>
            <?php foreach($countries->result_array() as $row):?>
                <tr>
                    <td>
                        <?php echo $row['id_country']?>
                    </td>
                    <td>
                       <?php echo $row['name_country']?>
                    </td>
                    <td>
                        <?php echo $row['name_currency'] ?>
                    </td>
                    <td>
                        <?php echo $row['mask_country'] ?>
                    </td>
                    <td>
                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/editCountry/<?php echo $row['id_country']?>"> Редактировать </a>
                        <a href="#"> Добавить </a>
                        <?php if($row['name_currency'] == null): ?>
                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/addCurrencyFromC"> Добавить валюту</a>
                            <a href="#">Выбрать валюту</a>
                        <?php endif; ?>
                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/deleteCountry/<?php echo $row['id_country']?>"> Удалить </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
<?php endif; ?>
<?php if($this->session->userdata('admin') == null) : ?>
    <!--
        Форма входа и кнопочка выхода.
    -->
    <div class="padding-divs">
        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/loginAdmin', array('class' => "form-horizontal")) ?>
        <p> Логин админа 123</p>
        <input type="text" name="nickname">
        <p> Пароль админа 123</p>
        <input type="password" name="password">
        <br><br>
        <input type="submit" value="Войти">
        </form>
    </div>
<?php endif; ?>