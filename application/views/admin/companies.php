<?php if($this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) :?>
    <div class="padding-divs">

        <nav class="navbar navbar-default">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSection">Разделы</a>
                </li>
                <li >
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCategory">Категории</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSubcategory">Подкатегории</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCountry">Страны</a>
                </li>
                <li >
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllRegion">Регионы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCity">Города</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCurrency">Валюты</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getSliders">Слайды</a>
                </li>
                <li class="active">
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCompanies">Продавцы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/User/getAllUsers">Покупатели</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllOrders"> Заказы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/logoutAdmin"> Выйти </a>
                </li>

            </ul>
        </nav>

        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/addRegion', array()) ?>
            <input type="text" name="id_region">
            <input type="text" name="name_region">
            <input type="text" name="id_country">
            <input type="submit" value="Добавить">
        </form>

        <table class="table table-striped ">
            <caption><h4>Таблица зарегестрированных компаний</h4></caption>
            <tr>
                <th>№</th>
                <th>Лого</th>
                <th>Название компании</th>
                <th>Логин и пароль</th>
                <th>Доп. инфо.</th>
                <th>Телефоны</th>
                <th>Активна</th>
                <th>Действие</th>
            </tr>
            <?php foreach($companies->result_array() as $row):?>
                <tr>
                    <td>
                        <?php echo $row['id_company']?>
                    </td>
                    <td>
                        <?php if($row['path'] != null){?>
                            <img width="98" src="/../../images/mini/<?php $path = set_value('path'); echo $path = !empty($path) ? $path : $row['path']; ?>" alt="">
                        <?php } else { ?>
                            <img width="98" src="/../../images/best-action.jpg" alt="">
                        <?php }?>
                    </td>
                    <td>
                       <?php echo $row['name']?><br>
                    </td>
                    <td>
                        <?php echo $row['email']?><br>
                        <?php echo $row['password']?>
                    </td>
                    <td>
                        Дата регистрации:<br>
                        <?php echo $row['data_created']?>
                        <br>Город:<br>
                        <?php echo $row['name_city']?>
                    </td>
                    <td>
                        Домашние:<br>
                        <?php echo $row['home_phone0'].'<br>' ?>
                        <?php echo $row['home_phone1'].'<br>' ?>
                        <?php echo $row['home_phone2'] ?>
                        Мобильные:<br>
                        <?php echo $row['mobile_phone0'].'<br>' ?>
                        <?php echo $row['mobile_phone1'].'<br>' ?>
                        <?php echo $row['mobile_phone2'] ?>
                    </td>
                    <td>
                        <?php if ($row['show_company'] == '0'){
                                echo 'Нет';
                            } else {
                                echo 'Да';
                            }
                        ?>
                    </td>
                    <td>
                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/suppliers/showSupplier/<?php echo $row['id_company']?>?email=<?php echo $row['email']?>&name_translit=<?php echo $row['name_translit']?>" onclick="setActive(<?php echo "'".$row['id_user']."'"?>)">Показать</a><br>
                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/suppliers/hideSupplier/<?php echo $row['id_company']?>" onclick="setNonActive(<?php echo "'".$row['id_user']."'"?>)">Скрыть</a><br>
                        <a href="#"> Редактировать </a><br>
                        <a href="#"> Добавить </a><br>
                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/suppliers/deleteSupplier?id_company=<?php echo $row['id_company']?>"> Удалить </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
<?php endif; ?>
<?php if($this->session->userdata('admin') == null) : ?>
    <!--?
        Форма входа и кнопочка выхода.
    -->
    <div class="padding-divs">
        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/loginAdmin', array('class' => "form-horizontal")) ?>
        <p> Логин админа 123</p>
        <input type="text" name="nickname">
        <p> Пароль админа 123</p>
        <input type="password" name="password">
        <br><br>
        <input type="submit" value="Войти">
        </form>
    </div>
<?php endif; ?>