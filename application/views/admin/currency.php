<?php if($this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) :?>
    <div class="padding-divs">

        <nav class="navbar navbar-default">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSection">Разделы</a>
                </li>
                <li >
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCategory">Категории</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSubcategory">Подкатегории</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCountry">Страны</a>
                </li>
                <li >
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllRegion">Регионы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCity">Города</a>
                </li>
                <li class="active">
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCurrency">Валюты</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getSliders">Слайды</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCompanies">Продавцы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/User/getAllUsers">Покупатели</a>
                </li>
                <li >
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllOrders"> Заказы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/logoutAdmin"> Выйти </a>
                </li>

            </ul>
        </nav>

        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/addCurrency/', array()) ?>
            <label>Название валюты</label>
            <input type="text" name="name_currency">
            <label>Сокращённое название</label>
            <input type="text" name="icon_currency">
            <input type="submit" value="Добавить">
        </form>

        <table class="table table-striped ">
            <caption><h4>Таблица подкатегорий</h4></caption>
            <tr>
                <th>№</th>
                <th>Название подкатегории</th>
                <th>Сокращённое название</th>
                <th>Действие</th>
            </tr>
            <?php foreach($currency->result_array() as $row):?>
                <tr>
                    <td>
                        <?php echo $row['id_currency']?>
                    </td>
                    <td>
                        <?php echo $row['name_currency'] ?>
                    </td>
                    <td>
                        <?php echo $row['icon_currency'] ?>
                    </td>
                    <td>
                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/updateCurrency/<?php echo $row['id_currency']?>"> Редактировать </a>
                        <a href="#"> Добавить </a>
                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/deleteCurrency/<?php echo $row['id_currency']?>"> Удалить </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
<?php endif; ?>
<?php if($this->session->userdata('admin') == null) : ?>
    <!--
        Форма входа и кнопочка выхода.
    -->
    <div class="padding-divs">
        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/loginAdmin', array('class' => "form-horizontal")) ?>
        <p> Логин админа 123</p>
        <input type="text" name="nickname">
        <p> Пароль админа 123</p>
        <input type="password" name="password">
        <br><br>
        <input type="submit" value="Войти">
        </form>
    </div>
<?php endif; ?>