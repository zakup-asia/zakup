<?php if($this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) :?>

<?php
	include_once '/var/www/vhosts/zakup.asia/httpdocs/order_test/db.php';
	$db = new DB();
?>
    <nav class="navbar navbar-default">
        <ul class="nav navbar-nav">
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSection">Разделы</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCategory">Категории</a>
            </li>
            <li class="active">
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/changeOrder">Изменить порядок</a>
            </li>			
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSubcategory">Подкатегории</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCountry">Страны</a>
            </li>
            <li >
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllRegion">Регионы</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCity">Города</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCurrency">Валюты</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getSliders">Слайды</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCompanies">Продавцы</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/User/getAllUsers">Покупатели</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllOrders"> Заказы</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/logoutAdmin"> Выйти </a>
            </li>

        </ul>
    </nav>

        <div class="col-xs-4">
			<link href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/order_test/style.css" rel="stylesheet" type="text/css" />

			<style>
			/* ------------- Контейнер с адаптивными блоками------------- */
			.masonry {
				margin: 100px auto;
				padding: 0;
				column-gap: 1.5em; /* Общее расстояние между колонками */
				font-size: 12px;
				-moz-column-gap: 1.5em; /* Расстояние между колонками для Firefox */
				-webkit-column-gap: 1.5em; /* Расстояние между колонками  для Safari, Chrome и iOS */
				height: 445px;
				width: 975px;
				column-fill: auto;
				border: 1px solid #000;
				overflow: hidden;
				clear: both;
			}
			 
			/* Элементы в виде плиток с содержанием */
			.item {
				display: inline-block;
				width: 100%;
				box-sizing: border-box; /* Изменения алгоритма расчета ширины и высоты элемента.*/
				-moz-box-sizing: border-box; /* Для Firefox */ 
				-webkit-box-sizing: border-box; /* Для Safari, Chrome, iOS иAndroid */ 
			   
			}
			 
			.item a:last-child {
				margin-bottom: 10px;
			}
				
			.masonry a {
				font-size: 12px;
			}	
			 
			/* Медиа-запросы для различных размеров адаптивного макета */
			@media only screen and (min-width: 400px) {
				.masonry {
					-moz-column-count: 2;
					-webkit-column-count: 2;
					column-count: 2;
				}
			}
			 
			@media only screen and (min-width: 700px) {
				.masonry {
					-moz-column-count: 6;
					-webkit-column-count: 6;
					column-count: 6;
					height: 459px;
				}
			}
			 
			@media only screen and (min-width: 900px) {
				.masonry {
					-moz-column-count: 6;
					-webkit-column-count: 6;
					column-count: 6;
				}
			}
			 
			@media only screen and (min-width: 1100px) {
				.masonry {
					-moz-column-count: 6;
					-webkit-column-count: 6;
					column-count: 6;
					
				}
			}

			.megamenu-left {
				height: 445px;
				border-radius: 5px;
			}

			.megamenu-items-wrapper {
				height: 445px;
				border-radius: 5px;	
			}

			.two-col, .two-col-wrapper {
				height: 445px;
			}

			.two-col-wrapper {
				overflow: hidden;
			}

			</style>	
			
	<select id="sections" name="sections">
        <option>выберите родительскую категорию</option>
		<?php 
			//Fetch all images from database
			$items = $db->getSections();
			if(!empty($items)){
				foreach($items as $row){
		?>
            <option value="<?php echo $row['id_section_one']; ?>"><?php echo $row['name_section']; ?></option>
        <?php } } ?>	
	</select>	
		<br><br>

		<a href="javascript:void(0);" class="btn outlined mleft_no reorder_link" id="save_reorder">Поменять порядок категорий</a><br>
    <div id="reorder-helper" class="light_box" style="display:none;">1. Перемещайте элементы мышью.<br>2. Нажмите СОХРАНИТЬ ПОРЯДОК после утверждения порядка.</div><br><br>			
			
			
        </div><br><br>

    <div class="masonry">

        </div><br><br>
    </div>
<?php endif; ?>
<?php if($this->session->userdata('admin') == null) : ?>
    <!--
        Форма входа и кнопочка выхода.
    -->
    <div class="padding-divs">
        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/loginAdmin', array('class' => "form-horizontal")) ?>
        <p> Логин админа 123</p>
        <input type="text" name="nickname">
        <p> Пароль админа 123</p>
        <input type="password" name="password">
        <br><br>
        <input type="submit" value="Войти">
        </form>
    </div>
<?php endif; ?>

