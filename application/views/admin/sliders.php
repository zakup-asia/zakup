<?php if($this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) :?>
<div class="padding-divs">

    <nav class="navbar navbar-default">
        <ul class="nav navbar-nav">
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSection">Разделы</a>
            </li>
            <li >
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCategory">Категории</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSubcategory">Подкатегории</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCountry">Страны</a>
            </li>
            <li >
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllRegion">Регионы</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCity">Города</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCurrency">Валюты</a>
            </li>
            <li class="active">
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getSliders">Слайды</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCompanies">Продавцы</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/User/getAllUsers">Покупатели</a>
            </li>
            <li >
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllOrders"> Заказы</a>
            </li>
            <li>
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/logoutAdmin"> Выйти </a>
            </li>
        </ul>
    </nav>

    <div>
        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/addSliders') ?>
        <label>Добавить слайды для города/региона/страны</label>
        <select name="id_city">
            <option></option>
            <?php foreach($city->result_array() as $row): ?>
                <option value="<?php $id_city = set_value('id_city'); echo $id_city = !empty($id_city) ? $id_city : $row['id_city']; ?>"> <?php $name_city = set_value('name_city'); echo $name_city = !empty($name_city) ? $name_city : $row['name_city']; ?></option>
            <?php endforeach; ?>
        </select>
        <select name="id_region">
            <option></option>
            <?php foreach($region->result_array() as $row): ?>
                <option value="<?php $id_region = set_value('id_region'); echo $id_region = !empty($id_region) ? $id_region : $row['id_region']; ?>"> <?php $name_region = set_value('name_region'); echo $name_region = !empty($name_region) ? $name_region : $row['name_region']; ?></option>
            <?php endforeach; ?>
        </select>
        <select name="id_country">
            <option></option>
            <?php foreach($country->result_array() as $row): ?>
                <option value="<?php $id_country = set_value('id_country'); echo $id_country = !empty($id_country) ? $id_country : $row['id_country']; ?>"> <?php $name_country = set_value('name_country'); echo $name_country = !empty($name_country) ? $name_country : $row['name_country']; ?></option>
            <?php endforeach; ?>
        </select>
        <input type="submit" value="Добавить">
        </form>
        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/deleteSliders') ?>
        <label>Удалить слайды для города/региона/страны</label>
        <select name="id_city">
            <option></option>
            <?php foreach($city->result_array() as $row): ?>
                <option value="<?php $id_city = set_value('id_city'); echo $id_city = !empty($id_city) ? $id_city : $row['id_city']; ?>"> <?php $name_city = set_value('name_city'); echo $name_city = !empty($name_city) ? $name_city : $row['name_city']; ?></option>
            <?php endforeach; ?>
        </select>
        <select name="id_region">
            <option></option>
            <?php foreach($region->result_array() as $row): ?>
                <option value="<?php $id_region = set_value('id_region'); echo $id_region = !empty($id_region) ? $id_region : $row['id_region']; ?>"> <?php $name_region = set_value('name_region'); echo $name_region = !empty($name_region) ? $name_region : $row['name_region']; ?></option>
            <?php endforeach; ?>
        </select>
        <select name="id_country">
            <option></option>
            <?php foreach($country->result_array() as $row): ?>
                <option value="<?php $id_country = set_value('id_country'); echo $id_country = !empty($id_country) ? $id_country : $row['id_country']; ?>"> <?php $name_country = set_value('name_country'); echo $name_country = !empty($name_country) ? $name_country : $row['name_country']; ?></option>
            <?php endforeach; ?>
        </select>
        <input type="submit" value="Удалить">
        </form>
    </div>

    <div class="row">
    <?php foreach($sliders->result_array() as $row):?>
        <div class="col-xs-4">
            <?php if ($row['name_city'] != null) : ?>
                <h4> Город - <?php echo $row['name_city'] ?> </h4>
            <?php endif;?>
            <?php if ($row['name_country'] != null) : ?>
                <h4> Страна - <?php echo $row['name_country'] ?> </h4>
            <?php endif;?>
            <?php if ($row['name_region'] != null) : ?>
                <h4> Регион - <?php echo $row['name_region'] ?> </h4>
            <?php endif;?>
            <?php echo form_open_multipart('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/updateSlider', array( "id" => $row['id_slide'] )) ?>
                <input type="hidden" name="id_slide" value="<?php echo $row['id_slide']?>">
                <input type="text" name="name_slide" value="<?php echo $row['name_slide']?>" readonly>
                <div class="logo-img" id="main<?php echo $row['id_slide']?>">
                    <?php if ($row['path'] != null) : ?>
                        <img src="/../../images/mini/<?php $path = set_value('path'); echo $path = !empty($path) ? $path : $row['path']; ?>" alt="">
                        <a onclick="deleteImage(<?php echo $row['id_slide']?>)"><span class="glyphicon glyphicon-remove"></span></a>
                        <input type="hidden" id="path" name="path<?php echo $row['id_slide']?>" value="<?php $path = set_value('path'); echo $path = !empty($path) ? $path : $row['path']; ?>">
                    <?php endif; ?>
                    <?php if ($row['path'] == null) : ?>
                        <img src="/../../images/best-action.jpg" alt="">
                    <?php endif; ?>
                </div>
                <input type="file" id="file" name="userfile" size="200" onchange="loadImage(<?php echo $row['id_slide']?>)" onclick="setClass(<?php echo $row['id_slide']?>)"/>
                <input type="button" value="Загрузить" onclick="loadImage(<?php echo $row['id_slide']?>)">
                <input type="button" value="Удалить" onclick="deleteImage(<?php echo $row['id_slide']?>)">
                <input type="submit" value="Сохранить">
            </form>
            <!--
                Можем удалить картинку, но не можем удалить слайд
                Также будет необходимо узнать каких размеров должен быть слайд
                С первого по четвёртый - 630x330
                С пятого по шестой - 300x160
                Она на опыте знаеть ишь ты какая опытная, кажется у неё слишьком много свободного времени.
            -->
            <?php ?>
        </div>
        <br>
    <?php endforeach; ?>
</div>

<script>

    //Установка класса
    function setClass(id){
        var ConcretForm = document.getElementById(id);
        ConcretForm.setAttribute('name','upload');
    }

    function setMain(){

    }

    //
    //Загрука изображения
    function loadImage(id){
        var server_port = document.getElementById('SERVER_PORT').value;
        var server_name = document.getElementById('SERVER_NAME').value;
        if (server_port != '80'){
            server_name = server_name + ':' + server_port;
        }
		var file = document.getElementById('file');
        var formData = new FormData(document.forms.upload);
        var find = 'main'+id;
        console.log(find);
        var main = document.getElementById(find);
        console.log(main);
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                var jsonText = JSON.parse(xhttp.responseText);
                for (key in jsonText) {
                    main.innerHTML = "<img src='/../../images/mini/"+jsonText['file_name']+"' alt=''> <input type='hidden' id='path' name='path' value='"+jsonText['file_name']+"'>";
                }
                var ConcretForm = document.getElementById(id);
                ConcretForm.removeAttribute('name');
				file.value = '';
            }
        };
        xhttp.open("POST", "http://"+server_name+"/index.php/image/do_upload/");
        xhttp.send(formData);
    }

    //Возможно стоит вешать разные атрибуты для добавления и удаления. Ведь иначе мы можем получить баг. Вообще в данном коде мы очень
    //имеем очень большие шансы получить баг
    //Удаляем картинку
    function deleteImage(id){
        var server_port = document.getElementById('SERVER_PORT').value;
        var server_name = document.getElementById('SERVER_NAME').value;
        if (server_port != '80'){
            server_name = server_name + ':' + server_port;
        }
        var find = 'main'+id;
        console.log(find);
        var main = document.getElementById(find);
        console.log(main);
        var path = document.getElementById('path').value;
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                main.innerHTML = '<img src="/../../images/best-action.jpg" alt="">';
                var ConcretForm = document.getElementById(id);
                ConcretForm.removeAttribute('name');
            }
        };
        xhttp.open("GET", "http://"+server_name+"/index.php/image/deleteImage/"+path);
        xhttp.send();
    }

    //Сохраняем картинку.
    function seveImage(){

    }

</script>
      </div>
<?php endif; ?>
<?php if($this->session->userdata('admin') == null) : ?>
    <!--
        Форма входа и кнопочка выхода.
    -->
    <div class="padding-divs">
        <?php echo form_open('AdminPanel/loginAdmin', array('class' => "form-horizontal")) ?>
        <p> Логин админа 123</p>
        <input type="text" name="nickname">
        <p> Пароль админа 123</p>
        <input type="password" name="password">
        <br><br>
        <input type="submit" value="Войти">
        </form>
    </div>
<?php endif; ?>