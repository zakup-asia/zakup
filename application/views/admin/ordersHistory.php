<nav class="navbar navbar-default">
    <ul class="nav navbar-nav">
        <li>
            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSection">Разделы</a>
        </li>
        <li >
            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCategory">Категории</a>
        </li>
        <li>
            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSubcategory">Подкатегории</a>
        </li>
        <li>
            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCountry">Страны</a>
        </li>
        <li >
            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllRegion">Регионы</a>
        </li>
        <li>
            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCity">Города</a>
        </li>
        <li>
            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCurrency">Валюты</a>
        </li>
        <li>
            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getSliders">Слайды</a>
        </li>
        <li>
            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCompanies">Продавцы</a>
        </li>
        <li>
            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/User/getAllUsers">Покупатели</a>
        </li>
        <li class="active">
            <a  href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllOrders"> Заказы</a>
        </li>
        <li>
            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/logoutAdmin"> Выйти </a>
        </li>
    </ul>
</nav>


<link href="/../../css/style2.css" rel="stylesheet">
<section class="two-col-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                    <div class="content profile">
                        <div class="row">
                            <div class="tabs">
                                <div class="col-lg-12 col-lg-pull-3 col-sm-8 col-sm-pull-4">
                                    <div class="profile-content tabs-wrapper order">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation">
                                                <a href="<?php echo SITE_NAME ?>index.php/AdminPanel/getAllOrders">
                                                    <span class="hidden-xs">Заказы</span>
                                                    <span class="visible-xs-inline">Заказы</span>
                                                    <span>&nbsp;</span>
                                                </a>
                                            </li>
                                            <li role="presentation" class="active">
                                                <a href="<?php echo SITE_NAME ?>index.php/AdminPanel/getOrdersHistory">
                                                    <span class="hidden-xs">История</span>
                                                    <span class="visible-xs-inline">История</span>
                                                    <span>&nbsp;</span>
                                                </a>
                                            </li>
                                        </ul>

                                        <section id="content1">
                                            <div class="bs-example" id="products">
                                                <div class="panel-group" id="accordion">
                                                    <?php foreach ($orders->result_array() as $row): ?>
                                                        <div class="panel panel-default">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $row['id_order']?>" class="accordion-btn" onclick="getOrderOut(<?php echo "'".$row['user']."'" ?>,<?php echo "'".$row['order_date']."'" ?>,<?php echo "'".$row['id_order']."'" ?>)">
                                                                <div class="panel-heading panel-first">
                                                                    <div class="panel-title">
                                                                        <div class="row">
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                                                                    <div class="toggle-btn"><?php echo $row['user'] ?></div>
                                                                                </div>
                                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                                                                    <span>Заказ № <?php echo $row['id_order'] ?></span>
                                                                                </div>
                                                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                                                    <span><?php echo $row['order_date'] ?></span>
                                                                                </div>
                                                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                                                    <span><?php echo $row['name'] ?></span>
                                                                                </div>
                                                                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                                                    <?php if($row['order_status'] != null): ?>
                                                                                        <button class="change-btn"><?php echo $row['order_status']?></button>
                                                                                    <?php endif; ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                            <div id="collapse<?php echo $row['id_order']?>" class="panel-collapse collapse">
                                                                <div id="panel-body<?php echo $row['id_order']?>" class="panel-body">

                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endforeach ?>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>