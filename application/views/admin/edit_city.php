<?php if($this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) :?>
    <div class="padding-divs">
        <div class="col-xs-4">
            <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/editCity') ?>
                <?php foreach ($city->result_array() as $row2) :?>
                    <input type="hidden" name="id_city" value="<?php echo $id_city ?>">
                    <input type="text" name="name_city" value="<?php $name_city = set_value('name_city'); echo $name_city = !empty($name_city) ? $name_city : $row2['name_city']; ?>">
                    <select name="id_country">
                        <option></option>
                        <?php foreach ($country->result_array() as $row) :?>
                            <option value="<?php $id_country = set_value('id_country'); echo $id_country = !empty($id_country) ? $id_country : $row['id_country']; ?>" <?php if($row2['id_country'] == $row['id_country']) echo 'selected="selected"' ?>> <?php $name_country = set_value('name_country'); echo $name_country = !empty($name_country) ? $name_country : $row['name_country']; ?></option>
                        <?php endforeach ?>
                    </select>
                    <input type="text" name="mask_sity" value="<?php $mask_sity = set_value('mask_sity'); echo $mask_sity = !empty($mask_sity) ? $mask_sity : $row2['mask_sity']; ?>">
                    <select name="id_region">
                        <option></option>
                        <?php foreach ($region->result_array() as $row) :?>
                            <option value="<?php $id_region = set_value('id_region'); echo $id_region = !empty($id_region) ? $id_region : $row['id_region']; ?>" <?php if($row2['id_region'] == $row['id_region']) echo 'selected="selected"' ?>> <?php $name_region = set_value('name_region'); echo $name_region = !empty($name_region) ? $name_region : $row['name_region']; ?></option>
                        <?php endforeach ?>
                    </select>
                    <input type="submit" value="Сохранить">
                <?php endforeach; ?>
            </form>
        </div>
    </div>
<?php endif; ?>
<?php if($this->session->userdata('admin') == null) : ?>
    <!--
        Форма входа и кнопочка выхода.
    -->
    <div class="padding-divs">
        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/loginAdmin', array('class' => "form-horizontal")) ?>
        <p> Логин админа 123</p>
        <input type="text" name="nickname">
        <p> Пароль админа 123</p>
        <input type="password" name="password">
        <br><br>
        <input type="submit" value="Войти">
        </form>
    </div>
<?php endif; ?>