<?php if($this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) :?>
    <div class="padding-divs">

        <nav class="navbar navbar-default">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSection">Разделы</a>
                </li>
                <li class="active">
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCategory">Категории</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSubcategory">Подкатегории</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCountry">Страны</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllRegion">Регионы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCity">Города</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCurrency">Валюты</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getSliders">Слайды</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCompanies">Компании</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/logoutAdmin"> Выйти </a>
                </li>
            </ul>
        </nav>

        <div class="col-xs-4">
            <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/addCategoryForS') ?>
                <input type="hidden" name="id_section" value="<?php echo $id_section ?>">
                <input type="text" name="name_category">
                <input type="submit" value="Добавить">
            </form>
        </div>
        <br><br>

    </div>
<?php endif; ?>
<?php if($this->session->userdata('admin') == null) : ?>
    <!--
        Форма входа и кнопочка выхода.
    -->
    <div class="padding-divs">
        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/loginAdmin', array('class' => "form-horizontal")) ?>
        <p> Логин админа 123</p>
        <input type="text" name="nickname">
        <p> Пароль админа 123</p>
        <input type="password" name="password">
        <br><br>
        <input type="submit" value="Войти">
        </form>
    </div>
<?php endif; ?>

