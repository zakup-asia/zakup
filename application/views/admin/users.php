<?php if($this->session->userdata('admin') != null) : ?>
    <div class="padding-divs">
        <nav class="navbar navbar-default">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSection">Разделы</a>
                </li>
                <li >
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCategory">Категории</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSubcategory">Подкатегории</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCountry">Страны</a>
                </li>
                <li >
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllRegion">Регионы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCity">Города</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCurrency">Валюты</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getSliders">Слайды</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCompanies">Продавцы</a>
                </li>
                <li class="active">
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/User/getAllUsers">Покупатели</a>
                </li>
                <li >
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllOrders"> Заказы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/logoutAdmin"> Выйти </a>
                </li>
            </ul>
        </nav>

        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/addSection', array()) ?>
            <input type="text" name="name_section">
            <input type="submit" value="Добавить">
        </form>

        <table class="table table-striped ">
            <caption><h4>Таблица пользователей</h4></caption>
            <tr>
                <th>№</th>
                <th>email</th>
                <th>Роль</th>
                <th>Название компании</th>
                <th>Действие</th>
            </tr>
            <?php foreach($object->result_array() as $row):?>
                <tr>
                    <td>
                        <?php echo $row['id_user']?>
                    </td>
                    <td>
                        <?php echo $row['email'] ?>
                    </td>
                    <td>
                        <?php echo $row['name_role'] ?>
                    </td>
                    <td>
                        <?php echo $row['name']?>
                    </td>
                    <td>
                        <?php if ($row['name_role'] == null | $row['name_role'] == 'standart') : ?>
                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/user/setAdmin?id_user=<?php echo $row['id_user']?>"> Сделать администратором </a>
                        <?php endif; ?>
                        <?php if ($row['name_role'] == 'admin') : ?>
                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/unsetAdmin?id_user=<?php echo $row['id_user']?>"> Забрать права </a>
                        <?php endif; ?>
                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/deleteUser?id_user=<?php echo $row['id_user']?>">Удалить</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
<?php endif; ?>
<?php if($this->session->userdata('admin') == null) : ?>
    <!--
        Форма входа и кнопочка выхода.
    -->
    <div class="padding-divs">
        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/loginAdmin', array('class' => "form-horizontal")) ?>
        <p> Логин админа 123</p>
        <input type="text" name="nickname">
        <p> Пароль админа 123</p>
        <input type="password" name="password">
        <br><br>
        <input type="submit" value="Войти">
        </form>
    </div>
<?php endif; ?>