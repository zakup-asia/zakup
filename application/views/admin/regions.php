<?php if($this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) :?>
    <div class="padding-divs">

        <nav class="navbar navbar-default">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?php echo SITE_NAME ?>/AdminPanel/getAllSection">Разделы</a>
                </li>
                <li >
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCategory">Категории</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSubcategory">Подкатегории</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCountry">Страны</a>
                </li>
                <li class="active">
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllRegion">Регионы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCity">Города</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCurrency">Валюты</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getSliders">Слайды</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCompanies">Продавцы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/User/getAllUsers">Покупатели</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllOrders"> Заказы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/logoutAdmin"> Выйти </a>
                </li>
            </ul>
        </nav>

        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/addRegion', array()) ?>
            <input type="text" name="name_region" placeholder="Название региона">
            <input type="text" name="mask_region" placeholder="Маска">
            <select name="id_country">
                <option></option>
                <?php foreach($country->result_array() as $row) :?>
                    <option value="<?php $id_country = set_value('id_country'); echo $id_country = !empty($id_country) ? $id_country : $row['id_country']; ?>"> <?php $name_country = set_value('name_country'); echo $name_country = !empty($name_country) ? $name_country : $row['name_country']; ?></option>
                <?php endforeach; ?>
            </select>
            <input type="submit" value="Добавить">
        </form>

        <table class="table table-striped ">
            <caption><h4>Таблица регионов</h4></caption>
            <tr>
                <th>№</th>
                <th>Название региона</th>
                <th>Страна региона</th>
                <th>Маска</th>
                <th>Действие</th>
            </tr>
            <?php foreach($region->result_array() as $row):?>
                <tr>
                    <td>
                        <?php echo $row['id_region']?>
                    </td>
                    <td>
                       <?php echo $row['name_region']?>
                    </td>
                    <td>
                        <?php echo $row['name_country'] ?>
                    </td>
                    <td>
                        <?php echo $row['mask_region'] ?>
                    </td>
                    <td>
                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/editRegion/<?php echo $row['id_region']?>"> Редактировать </a>
                        <a href="#"> Добавить </a>
                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/deleteRegion/<?php echo $row['id_region']?>"> Удалить </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
<?php endif; ?>
<?php if($this->session->userdata('admin') == null) : ?>
    <!--
        Форма входа и кнопочка выхода.
    -->
    <div class="padding-divs">
        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/loginAdmin', array('class' => "form-horizontal")) ?>
            <p> Логин админа 123</p>
            <input type="text" name="nickname">
            <p> Пароль админа 123</p>
            <input type="password" name="password">
            <br><br>
            <input type="submit" value="Войти">
        </form>
    </div>
<?php endif; ?>




