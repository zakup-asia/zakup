<?php if($this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) :?>
    <div class="padding-divs">
        <div class="col-xs-4">
            <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/editCountry', array()) ?>
                <?php foreach ($country->result_array() as $row2) :?>
                    <input type="hidden" name="id_country" value="<?php echo $id_country;?>">
                    <input type="text" name="name_country" value="<?php $name_country = set_value('name_country'); echo $name_country = !empty($name_country) ? $name_country : $row2['name_country']; ?>">
                    <input type="text" name="mask_country" value="<?php $mask_country = set_value('mask_country'); echo $mask_country = !empty($mask_country) ? $mask_country : $row2['mask_country']; ?>">
                    <select name="id_currency">
                        <option></option>
                        <?php foreach ($currency->result_array() as $row) :?>
                            <option value="<?php $id_currency = set_value('id_currency'); echo $id_currency = !empty($id_currency) ? $id_currency : $row['id_currency']; ?>" <?php if($row2['id_currency'] == $row['id_currency']) echo 'selected="selected"' ?>> <?php $name_currency = set_value('name_currency'); echo $name_currency = !empty($name_currency) ? $name_currency : $row['name_currency']; ?></option>
                        <?php endforeach; ?>
                    </select>
                <?php endforeach; ?>
            <input type="submit" value="Добавить">
            </form>
        </div>
    </div>
<?php endif; ?>
<?php if($this->session->userdata('admin') == null) : ?>
    <!--
        Форма входа и кнопочка выхода.
    -->
    <div class="padding-divs">
        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/loginAdmin', array('class' => "form-horizontal")) ?>
        <p> Логин админа 123</p>
        <input type="text" name="nickname">
        <p> Пароль админа 123</p>
        <input type="password" name="password">
        <br><br>
        <input type="submit" value="Войти">
        </form>
    </div>
<?php endif; ?>