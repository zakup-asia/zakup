<?php if($this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) :?>
    <div class="padding-divs">

        <nav class="navbar navbar-default">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSection">Разделы</a>
                </li>
                <li >
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCategory">Категории</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSubcategory">Подкатегории</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCountry">Страны</a>
                </li>
                <li >
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllRegion">Регионы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCity">Города</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCurrency">Валюты</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getSliders">Слайды</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCompanies">Продавцы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/User/getAllUsers">Покупатели</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllOrders"> Заказы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/logoutAdmin"> Выйти </a>
                </li>
            </ul>
        </nav>

        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/addSection', array()) ?>
            <input type="text" name="name_section">
            <input type="submit" value="Добавить">
        </form>

        <table class="table table-striped ">
            <caption><h4>Таблица разделов</h4></caption>
            <tr>
                <th>№</th>
                <th>Название раздела</th>
                <th>Действие</th>
            </tr>
            <?php foreach($section_one->result_array() as $row):?>
                <tr>
                    <td>
                        <?php echo $row['id_section_one']?>
                    </td>
                    <td>
                        <?php echo $row['name_section'] ?>
                    </td>
                    <td>
                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/editSection/<?php echo $row['id_section_one']?>"> Редактировать </a>
                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/addCategoryForS/<?php echo $row['id_section_one']?>"> Добавить категорию</a>
                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/deleteSection/<?php echo $row['id_section_one']?>"> Удалить </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
<?php endif; ?>
<?php if($this->session->userdata('admin') == null) : ?>
    <!--
        Форма входа и кнопочка выхода.
    -->
    <div class="padding-divs">
        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/loginAdmin', array('class' => "form-horizontal")) ?>
        <p> Логин админа 123</p>
        <input type="text" name="nickname">
        <p> Пароль админа 123</p>
        <input type="password" name="password">
        <br><br>
        <input type="submit" value="Войти">
        </form>
    </div>
<?php endif; ?>