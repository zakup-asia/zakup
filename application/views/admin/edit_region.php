<?php if($this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) :?>
    <div class="padding-divs">
        <div class="col-xs-4">
            <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/editRegion', array()) ?>
                <?php foreach ($region->result_array() as $row2) :?>
                    <input type="hidden" name="id_region" value="<?php echo $id_region;?>">
                    <input type="text" name="name_region" value="<?php $name_region = set_value('name_region'); echo $name_region = !empty($name_region) ? $name_region : $row2['name_region']; ?>">
                    <input type="text" name="mask_region" value="<?php $mask_region = set_value('mask_region'); echo $mask_region = !empty($mask_region) ? $mask_region : $row2['mask_region']; ?>">
                    <select name="id_country">
                        <option></option>
                        <?php foreach ($country->result_array() as $row) :?>
                            <option value="<?php $id_country = set_value('id_country'); echo $id_country = !empty($id_country) ? $id_country : $row['id_country']; ?>" <?php if($row2['id_country'] == $row['id_country']) echo 'selected="selected"' ?>> <?php $name_country = set_value('name_country'); echo $name_country = !empty($name_country) ? $name_country : $row['name_country']; ?></option>
                        <?php endforeach; ?>
                    </select>
                <?php endforeach; ?>
                <input type="submit" value="Сохранить">
            </form>
        </div>
    </div>
<?php endif; ?>
<?php if($this->session->userdata('admin') == null) : ?>
    <!--
        Форма входа и кнопочка выхода.
    -->
    <div class="padding-divs">
        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/loginAdmin', array('class' => "form-horizontal")) ?>
        <p> Логин админа 123</p>
        <input type="text" name="nickname">
        <p> Пароль админа 123</p>
        <input type="password" name="password">
        <br><br>
        <input type="submit" value="Войти">
        </form>
    </div>
<?php endif; ?>