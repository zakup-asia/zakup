<?php if($this->session->userdata('admin') != null | $this->session->userdata('name_role') != 'standart' & $this->session->userdata('name_role') != null) :?>
    <div class="padding-divs">

        <nav class="navbar navbar-default">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSection">Разделы</a>
                </li>
                <li >
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCategory">Категории</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllSubcategory">Подкатегории</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCountry">Страны</a>
                </li>
                <li >
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllRegion">Регионы</a>
                </li>
                <li class="active">
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllCity">Города</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCurrency">Валюты</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getSliders">Слайды</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getCompanies">Продавцы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/User/getAllUsers">Покупатели</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/getAllOrders"> Заказы</a>
                </li>
                <li>
                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/AdminPanel/logoutAdmin"> Выйти </a>
                </li>

            </ul>
        </nav>

        <div >
            <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/addCity') ?>
            <input type="text" name="name_city" placeholder="Название города">
            <input type="text" name="mask_sity" placeholder="Маска">
            <select name="id_region">
                <option></option>
                <?php foreach($region->result_array() as $row): ?>
                    <option value="<?php $id_region = set_value('id_region'); echo $id_region = !empty($id_region) ? $id_region : $row['id_region']; ?>"> <?php $name_region = set_value('name_region'); echo $name_region = !empty($name_region) ? $name_region : $row['name_region']; ?></option>
                <?php endforeach; ?>
            </select>
            <input type="submit" value="Добавить">
            </form>
        </div>

        <table class="table table-striped ">
            <caption><h4>Таблица городов</h4></caption>
            <tr>
                <th>№</th>
                <th>Название города</th>
                <th>Регион города</th>
                <th>Страна города</th>
                <th>Маска</th>
                <th>Действие</th>
            </tr>
            <?php foreach($cities->result_array() as $row):?>
                <tr>
                    <td>
                        <?php echo $row['id_city']?>
                    </td>
                    <td>
                        <?php echo $row['name_city']?>
                    </td>
                    <td>
                        <?php echo $row['name_region'] ?>
                    </td>
                    <td>
                        <?php echo $row['name_country'] ?>
                    </td>
                    <td>
                        <?php echo $row['mask_sity']?>
                    </td>
                    <td>
                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/editCity/<?php echo $row['id_city']?>"> Редактировать </a>
                        <a href="#"> Добавить </a>
                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/AdminPanel/deleteCity/<?php echo $row['id_city']?>"> Удалить </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
<?php endif; ?>
<?php if($this->session->userdata('admin') == null) : ?>
    <!--
        Форма входа и кнопочка выхода.
    -->
    <div class="padding-divs">
        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/AdminPanel/loginAdmin', array('class' => "form-horizontal")) ?>
        <p> Логин админа 123</p>
        <input type="text" name="nickname">
        <p> Пароль админа 123</p>
        <input type="password" name="password">
        <br><br>
        <input type="submit" value="Войти">
        </form>
    </div>
<?php endif; ?>


<?php ?>

