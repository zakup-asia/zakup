<?php foreach ($user->result_array() as $row) : ?>
				<div class="content profile">
					<div class="row">
						<div class="col-xs-12">
							<div class="profile-content">
								<div class="profile-content tabs-wrapper order">
									<div class="row css-tabs-wrapper">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/main/">
                                                <div class="col-md-2 col-sm-2 col-xs-2 text-center css-top-menu">
                                                    Основные
                                                </div>
                                            </a>
                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/contactinformation/">
                                                <div class="col-md-2 col-sm-2 col-xs-2 text-center css-top-menu">
                                                    Контакты
                                                </div>
                                            </a>
                                            <?php if($this->session->userdata('goal') == TRUE ) : ?>
												<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/conditions/">
													<div class="col-md-3 col-sm-3 col-xs-3 text-center css-top-menu">
														Условия заказа
													</div>
												</a>
											<?php endif;?>
                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/requisites/">
                                                <div class="col-md-2 col-sm-2 col-xs-2 text-center css-top-menu">
                                                    Реквизиты
                                                </div>
                                            </a>
                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/loginAndPassword/">
                                                <div class="col-md-3 col-sm-3 col-xs-3 text-center active css-top-menu">
                                                    Логин и пароль
                                                </div>
                                            </a>
                                        </div>
                                    </div>
								<style>
									.show-password-link {
										top: -1px!important;
									}
									.password-showing {
										width: 90%;
										border-radius: 3px;
										height: 30px;
									}
								</style>
								<div class="profile-content">
                                    <div class="profile-form">
                                        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/User/LoginAndPassword/', array('class' => "form-horizontal")) ?>
                                            <div class="form-group">
                                                <label for="name" class="col-xs-3 control-label">Логин:</label>
                                                <div class="col-xs-5">
                                                    <input type="email" class="form-control" id="name" name="email" placeholder="Введите email" value="<?php $email = set_value('email'); echo $email = !empty($email) ? $email : $row['email']; ?>"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="phone" class="col-xs-3 control-label">Введите новый пароль:</label>
                                                    <div class="col-xs-5">
                                                        <input type="password" class="form-control" id="phone" name="password" placeholder="Введите пароль" value="<?php $password = set_value('password'); echo $password = !empty($password) ? $password : $row['password']; ?>" >
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-xs-offset-5 col-xs-3">
                                                        <button type="submit" class="btn btn-profile">Сохранить</button>
                                                    </div>
                                                </div>
                                        </form>

                                    </div>

                                </div>
								</div>
                            </div>
                        </div>

                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>
<?php endforeach; ?>