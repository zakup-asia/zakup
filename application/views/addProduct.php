 <?php
    $icon_currency = array();
    foreach($currency->result_array() as $row2){
        if($row2['icon_currency'] != null) $icon_currency['icon_currency'] = $row2['icon_currency'];
    }
?>

                    <div class="content profile">
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="profile-content">
                                    <ul class="breadcrumb">
                                        <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/product/myProducts">Мои товары </a></li>
                                        <li class="active">Новый товар</li>
                                    </ul>
                                    <div class="profile-form">
                                        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/product/addProduct/', array('class' => "form-horizontal", 'name' => 'upload')) ?>
                                        <?php if ($info != null) : ?>
                                            <div class="form-group">
                                                <label for="name" class="col-xs-3 control-label">Наименование товара:*</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="name_product" name="name_product" maxlength="120">
													<span id="name_productcharsLeft" class="grey"></span>&nbsp;<span class="grey">знаков осталось</span>
                                                    <?php echo form_error('name_product', '<div class="css-error">', '</div>'); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="name" class="col-xs-3 control-label">Описание товара:</label>
                                                <div class="col-xs-8">
                                                    <textarea name="product_meta" class="form-control" id="product_meta"></textarea>
													<span id="product_metacharsLeft" class="grey"></span>&nbsp;<span class="grey">знаков осталось</span>
                                                </div>
                                            </div>
											<div class="form-group">
                                                <label for="name" class="col-xs-3 control-label">Штрих код:</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="barcode" name="barcode">
                                                </div>
                                            </div>
                                            <!--
                                                <div class="form-group">
                                                    <label class="col-xs-3 control-label">Раздел товара:</label>
                                                    <div class="col-xs-6">
                                                        <select type="text" class="form-control" name="id_section_one">
                                                            <?php foreach ($info->result_array() as $row) : ?>
                                                                <option value="<?php echo $row['id_section_one']?>"><?php echo $row['name_section']?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            -->
                                            <!--
                                                <div class="form-group">
                                                    <label class="col-xs-3 control-label">Категория товара:</label>
                                                    <div class="col-xs-6">
                                                        <select type="text" class="form-control" name="id_category">
                                                            <?php foreach ($info2->result_array() as $row) : ?>
                                                                <option value="<?php echo $row['id_category']?>"><?php echo $row['name_category']?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            -->
                                            <!--
                                                <div class="form-group">
                                                    <label class="col-xs-3 control-label">Подкатегория товара:</label>
                                                    <div class="col-xs-6">
                                                        <select type="text" class="form-control" name="id_subcategory">
                                                            <?php foreach ($info3->result_array() as $row) : ?>
                                                                <option value="<?php echo $row['id_subcategory']?>"><?php echo $row['name_subcategory']?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            -->
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label">Страна производитель:</label>
                                                <div class="col-xs-6">
                                                    <input type="text" class="form-control" name="name_country">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label" for="price">Цена:*</label>
                                                    <div class="input-group price">
                                                        <input type="text" class="form-control" id="price" name="price">
                                                        <div class="input-group-addon"><?php echo $icon_currency['icon_currency'] ?></div>
                                                    </div>
                                                <?php echo form_error('price', '<div class="css-error">', '</div>'); ?>
                                            </div>
											<div class="form-group" >
                                                <div class="col-xs-offset-3 col-xs-8">
                                                    <div class="row">
                                                        <input type="checkbox" value="1" name="in_stock" checked>
                                                        <p class="col-md-6 col-sm-6 col-xs-6">В наличии</p>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group" >
                                                <div class="col-xs-offset-3 col-xs-8">
                                                    <div class="row">
                                                        <input type="checkbox" value="1" name="at_the_end">
                                                        <p class="col-md-6 col-sm-6 col-xs-6">Товар на исходе</p>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group" >
                                                <div class="col-xs-offset-3 col-xs-8">
                                                    <div class="row">
                                                        <input type="checkbox" value="1" name="sample"> 
                                                        <p class="col-md-6 col-sm-6 col-xs-6">Доступна проба товара</p>
                                                    </div>
                                                </div>
                                            </div>
											<div class="form-group" >
                                                <div class="col-xs-offset-3 col-xs-8">
                                                    <div class="row">
                                                        <input type="checkbox" value="1" name="stock">
                                                        <p class="col-md-6 col-sm-6 col-xs-6">Акция</p>
                                                    </div>
                                                </div>
                                            </div>
											<!--
                                            <div class="form-group">
                                                <div class="col-xs-offset-3 col-xs-6">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" value="1" name="in_stock" checked> В наличии
                                                        </label>
                                                        <label>
                                                            <input type="checkbox" value="1" name="at_the_end"> Товар на исходе
                                                        </label>
                                                        <label>
                                                            <input type="checkbox" value="1" name="sample"> Доступна проба товара
                                                        </label>
                                                        <label>
                                                            <input type="checkbox" value="1" name="stock"> Акция
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
											-->
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label">Фотография товара <span class="grey">(200px*200px)</span>:</label>
                                                <div class="col-xs-8">
                                                    <div class="logo-img" id="main">
                                                        <img class='css-pointer' onclick="$('#file').click()" src="/../../images/download-image.png" alt="">
                                                    </div>
                                                    <input class="css-hide" type="file" id="file" name="userfile" size="20" onchange="loadImage()" />
                                                    <div id="info">
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                            <br>
                                            <div class="form-group">
                                                <div class="col-xs-offset-4 col-xs-3">
                                                    <button type="submit" class="btn btn-profile">Сохранить</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>


<script>
    function loadImage(){
		var file = document.getElementById('file');
		var file2 = '"#file"';
        var formData = new FormData(document.forms.upload);
        var response = document.getElementById('info');
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                var jsonText = JSON.parse(xhttp.responseText);
                for (key in jsonText) {
                    console.log(key);
                    main.innerHTML = "<img class='css-pointer' onclick='$("+file2+").click()' src='/../../images/mini/"+jsonText['file_name']+"' alt=''> <a onclick='deleteImage()'><span class='glyphicon glyphicon-remove css-pointer'></span></a> <input type='hidden' id='path'name='path' value='"+jsonText['file_name']+"'>";
                }
				file.value = '';
            }
        };
        xhttp.open("POST", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/' ?>index.php/image/do_upload/");
        xhttp.send(formData);
    }

    function deleteImage(){
        var path = document.getElementById('path').value;
		var file2 = "'#file'";
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                main.innerHTML = '<img class="css-pointer" onclick="$('+file2+').click()" src="/../../images/download-image.png" alt="">';
            }
        };
        xhttp.open("GET", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/' ?>index.php/image/deleteImage/"+path);
        xhttp.send();
    }
</script>
