<section class="two-col-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="two-col">

                    <div class="megamenu-left" style="display: block" >
                        <ul class="megamenu-items-wrapper hidden-sm hidden-xs">
                            <?php foreach($section->result_array() as $row) : ?>
                                <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/products/findProducts/?searchText=<?php echo $row['name_section']?>"><?php echo $row['name_section']?></a>
                                    <div class="hidden-item">
                                        <div>
                                            <?php foreach ($category->result_array() as $row2) :?>
                                                <div class="hidden-menu-column">
                                                    <?php if($row2['id_section_one'] == $row['id_section_one']):?>
                                                        <a class="menu-parent-item" href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/products/findProducts/?searchText=<?php echo $row2['name_category']?>"><?php echo $row2['name_category']?></a>
                                                        <?php foreach ($subcategory->result_array() as $row3) :?>
                                                            <?php if($row3['id_category'] == $row2['id_category']):?>
                                                                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/products/findProducts/?searchText=<?php echo $row3['name_subcategory']?>" class="menu-cat-item"><?php echo $row3['name_subcategory']?></a>
                                                            <?php endif;?>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </div>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>

                    <div class="content profile">
                        <div class="row">
                            <div class="col-lg-3 col-lg-push-9 col-sm-4 col-sm-push-8">
                                <div class="cart-wrapper">
                                    <div class="cart-result">
                                        Корзина <span><span id="productCount"></span>/<span id="productPrice"></span> тг</span>
                                        <a href="#" class="double-arrow"></a>
                                    </div>
                                    <div id="all_product" class="all-products">
                                    </div><!-- /.all-products -->
                                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/sendOrder" class="cart-btn">Оформить заказ</a>
                                </div>
                            </div>
                            <div class="col-lg-9 col-lg-pull-3 col-sm-8 col-sm-pull-4">
                                <div class="product-content">
                                    <?php foreach($product->result_array() as $row): ?>
                                        <div class="row">
                                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                                <ul class="product-ul custom-breadcrumb breadcrumb">
                                                    <?php
													    if (isset($_SERVER['HTTP_REFERER'])){
                                                        if (strpos($_SERVER['HTTP_REFERER'], 'search')){
                                                            echo '<li><a href="'.$_SERVER['HTTP_REFERER'].'">Поиск <span class="count"></span></a></li>';
                                                        } else if(strpos($_SERVER['HTTP_REFERER'], 'findProducts')){
                                                            echo '<li><a href="'.$_SERVER['HTTP_REFERER'].'">Поиск <span class="count"></span></a></li>';
                                                        } else {
                                                            echo '<li><a href="'.$_SERVER['HTTP_REFERER'].'">Компания <span class="count"></span></a></li>';
                                                        }
}
                                                    ?>
                                                    <li class="active"><a href="#"><?php echo $row['name_product'] ?></a></li>
                                                    <input type="hidden" id="productFavorite" value="<?php echo $row['id_product']?>">
                                                    <span id="actionFavorite">
                                                        <?php if($this->session->userdata('id_user') != null && $row["(SELECT COUNT(*) FROM favorites WHERE id_product = ".$this->db->escape($row['id_product'])." AND favorites.id_user = ".$this->db->escape($this->session->userdata('id_user')).")"] == 0): ?>
                                                            <a class="favoriteProduct favoriteThis" onclick="favoriteProduct(<?php echo "'".$row['id_product']."'"?>)">В избранное</a>
                                                        <?php endif; ?>
                                                        <?php if($this->session->userdata('id_user') != null && $row["(SELECT COUNT(*) FROM favorites WHERE id_product = ".$this->db->escape($row['id_product'])." AND favorites.id_user = ".$this->db->escape($this->session->userdata('id_user')).")"] != 0): ?>
                                                            <a class="favoriteProduct favoriteThis" onclick="unfavoriteProduct(<?php echo "'".$row['id_product']."'"?>)">Из избранного</a>
                                                        <?php endif; ?>
                                                    </span>
                                                </ul>

                                            </div>
                                        </div>
                                        <!--screen product-->
                                        <div class="row product-screen">
                                            <div class="col-lg-3 col-sm-4 col-md-4 col-xs-4">
                                                <div class="item-images">
                                                    <div class="big-item-pic">
                                                        <?php if ($row['path'] != null) : ?>
                                                            <img style="max-height:134px;" src="/../../images/mini/<?php echo $row['path']; ?>" alt="">
                                                        <?php endif; ?>
														<?php if ($row['path'] == null):?>
															<img width="134px" src="/../../images/add-image-big.jpg" alt="">
														<?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-8 item-full" id="item<?php echo $row['id_product']?>">
                                                <div class="item-content">
                                                    <div class="row item-title">
                                                        <div class="col-lg-7 col-sm-6 col-xs-6">
                                                            <label class="item-gr" ><?php echo $row['price'] ?><span><?php if(isset($row['icon_currency'])) echo $row['icon_currency'] ?></span></label>
                                                            <div style="display: none" id="price<?php echo $row['id_product']?>">
                                                                <?php echo $row['price'] ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-4 col-sm5 col-xs-5">
                                                            <div style="float:right;">
                                                                <input type="hidden" id="view" value="view">
                                                                <b class="my-class" id="<?php echo $row['id_product']?>">
                                                                    <button onclick="addToCart(<?php echo "'".$this->session->userdata('__ci_last_regenerate')."'" ?>, <?php echo "'".$row['id_product']."'" ?>, <?php echo "'".$row['id_company']."'"?>, <?php echo "'".$row['price']."'"?>)">купить</button>
                                                                </b>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row item-description">
                                                        <div class="col-lg-12 col-sm-12 col-xs-12">
                                                            <ul>
                                                                <li>
                                                                    Наличие:
                                                                    <?php if($row['in_stock'] != null) :?>
                                                                        <span>в наличии</span>
                                                                    <?php endif; ?>
                                                                    <?php if($row['in_stock'] == null) : ?>
                                                                        <span>нет</span>
                                                                    <?php endif; ?>
                                                                </li>
                                                                <li>
                                                                    Поставщик: <span><?php echo $row['name'] ?></span>
                                                                </li>
                                                                <li>
                                                                    Проба:
                                                                    <?php if($row['sample'] != null) :?>
                                                                        <span>Есть</span>
                                                                    <?php endif; ?>
                                                                    <?php if($row['sample'] == null) : ?>
                                                                        <span>нет</span>
                                                                    <?php endif; ?>
                                                                </li>
                                                                <li>Производитель: <span><?php ?></span></li>
                                                                <li>Страна производитель:<span> <?php echo $row['name_country'] ?></span></li>
                                                                <li>Категория: <span><?php ?></span></li>
																<li>Штрих-код: <span><?php echo $row['barcode'] ?></span></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                    <div class="item-action action-block desc">
                                                        <p>
                                                            <?php echo $row['product_meta']?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--mobile product-->
                                        <div class="row product-mobile">
                                            <div class="row">
                                                <div class="col-sm-9 col-xs-9 col-sm-offset-3 col-xs-offset-3">
                                                    <div class="item-images">
                                                        <a href="#">
                                                            <div class="big-item-pic">
                                                                <?php if ($row['path'] != null) : ?>
                                                                    <img src="/../../images/mini/<?php echo $row['path']; ?>" alt="">
                                                                <?php endif; ?>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12 col-sm-12 col-xs-12 item-full">
                                                    <div class="item-content">
                                                        <div class="row item-title">
                                                            <div class="col-lg-7 col-sm-7 col-xs-6">
                                                                <label class="item-gr"><?php echo $row['price'] ?><span><?php if($row['icon_currency'] != null) echo $row['icon_currency'] ?></span></label>
                                                            </div>
                                                            <div class="col-lg-5 col-sm-5 col-xs-6">
                                                                <a ><button class="buy-item"><div>КУПИТЬ</div></button></a>
                                                            </div>
                                                        </div>
                                                        <div class="row item-description">
                                                            <div class="col-lg-12 col-sm-12 col-xs-12">
                                                                <ul>
                                                                    <li>
                                                                        Наличие:
                                                                        <?php if($row['in_stock'] != null) :?>
                                                                            <span>в наличии</span>
                                                                        <?php endif; ?>
                                                                        <?php if($row['in_stock'] == null) : ?>
                                                                            <span>нет</span>
                                                                        <?php endif; ?>
                                                                    </li>
                                                                    <li>
                                                                        Поставщик: <span><?php echo $row['name'] ?></span>
                                                                    </li>
                                                                    <li>
                                                                        Проба:
                                                                        <?php if($row['sample'] != null) :?>
                                                                            <span>Есть</span>
                                                                        <?php endif; ?>
                                                                        <?php if($row['sample'] == null) : ?>
                                                                            <span>нет</span>
                                                                        <?php endif; ?>
                                                                    </li>
                                                                    <li>Производитель: <span><?php ?></span></li>
                                                                    <li>Страна производитель:<span> <?php echo $row['name_country'] ?></span></li>
                                                                    <li>Категория: <span><?php ?></span></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12 col-sm-12 col-xs-12">
                                                    <div class="item-action action-block desc">
                                                        <p>
                                                            <?php echo $row['product_meta']?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!--/mobile product-->
                                    <?php endforeach; ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>

<script>
    window.onload = function () {
        updateCart(<?php
            if ($this->session->userdata('id_user') == null){
                $cookie = get_cookie('id');
            } else {
                $cookie = $this->session->userdata('id_user');
            }
        echo "'".$cookie."'"
        ?>);
    }
</script>
<!--
    Что-то я сейчас даже представить не могу чего он не отпрабатывает
-->