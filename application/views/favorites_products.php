
                    <div class="content results">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="results-tab">
                                    <div class="tabs-wrapper">
                                        <div class="row">
                                            <div class="col-sm-8">

                                                <ul class="nav nav-tabs" role="tablist">
                                                    <li role="presentation">
                                                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/Favorites/getFavoritesCompanies">
                                                            <span class="hidden-xs">Компании</span>
                                                            <span class="visible-xs-inline">Компании</span>
                                                            <span>&nbsp;</span>
                                                        </a>
                                                    </li>
                                                    <li role="presentation" class="active" >
                                                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/Favorites/getFavoritesProducts">
                                                            <span class="hidden-xs">Товары</span>
                                                            <span class="visible-xs-inline">Товары</span>
                                                            <span>&nbsp;</span>
                                                        </a>
                                                    </li>
                                                </ul>

                                                <div class="tab-content">
                                                    <div class="tab-pane products-wrapper active" id="products">
                                                        <div class="products">
                                                            <?php foreach($favorites->result_array() as $row): ?>
                                                                <div id="item<?php echo $row['id_product']?>">
                                                                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/Products/getProduct/<?php echo $row['id_product']?>/<?php echo $row['id_company'] ?>"><?php echo $row['name_product']?></a>
                                                                    <p class="small"><a href="#"><?php echo $row['name_country']?></a> | <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/Suppliers/getSupplier/<?php echo $row['id_company'] ?>"></a></p>
                                                                    <!-- Отлично, только как-то всё не продуманно всё-равно. Надо идти опять в туалет, чтобы убрать сопли и тогда уже
                                                                        поговорит о моём здоровье, потом будем думать что и как. Хз что-то, надо об этом подумать.
                                                                        <div>
                                                                            <button class="my-minus"></button><input class="my-count" type="text" value="1"><button class="my-plus"></button>
                                                                        </div>
                                                                    -->
                                                                    <div id="<?php echo 'rem'.$row['id_product']?>" class="price-buy">
                                                                        <div class="price">
                                                                            <p id="price<?php echo $row['id_product']?>"><?php echo $row['price']?></p>
                                                                            <a href="#">отложить</a>
                                                                        </div>
                                                                        <div class="buy">
                                                                            <b id="<?php echo $row['id_product']?>">
                                                                                <button onclick="addToCart(<?php echo "'".$this->session->userdata('__ci_last_regenerate')."'" ?>, <?php echo "'".$row['id_product']."'" ?>, <?php echo "'".$row['id_company']."'"?>, <?php echo "'".$row['price']."'"?>)">купить</button>
                                                                            </b>
                                                                            <?php if($row['in_stock'] == null ) : ?>
                                                                                <span>отсутствует</span>
                                                                            <?php endif ?>
                                                                            <?php if($row['at_the_end'] != null ) : ?>
                                                                                <span>на исходе</span>
                                                                            <?php endif ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            <?php endforeach; ?>
                                                        </div>
                                                        <nav>
                                                            <ul class="pagination">
                                                                <li>
                                                                    <a href="#" aria-label="Previous">
                                                                        <span aria-hidden="true">&laquo;</span>
                                                                    </a>
                                                                </li>
                                                                <li><a href="#">1</a></li>
                                                                <li>
                                                                    <a href="#" aria-label="Next">
                                                                        <span aria-hidden="true">&raquo;</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </nav>
                                                    </div><!-- / #products -->
                                                    <div class="tab-pane providers-wrapper" id="providers">
                                                        <div class="products">
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div><!-- / #products -->
                                                </div><!-- /.tab-content  -->
                                            </div>
                                            <div class="col-sm-4">
                                                <div id="myAffix" data-spy="affix" data-offset-top="110" class="cart-wrapper">
                                                    <div class="cart-result">
                                                        Корзина <span><span id="productCount"></span>/<span id="productPrice"></span> тг</span>
                                                        <a href="#" class="double-arrow"></a>
                                                    </div>
                                                    <div id="all_product" class="all-products">

                                                    </div><!-- /.all-products -->
                                                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/sendOrder" class="cart-btn">Оформить заказ</a>
                                                </div>
                                            </div>
                                        </div>


                                    </div>	<!-- /.tabs-wrapper-->
                                </div><!-- /.povider-tab -->
                            </div>
                        </div>

                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>

<script>
    window.onload = function () {
        updateCart(<?php
            if ($this->session->userdata('id_user') == null){
                $cookie = get_cookie('id');
            } else {
                $cookie = $this->session->userdata('id_user');
            }
        echo "'".$cookie."'"
        ?>);
    }
</script>
