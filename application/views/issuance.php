<section class="two-col-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="two-col">
                    <div class="profile-menu open hidden-sm hidden-xs" id="profile-menu">
                        <ul>
                            <li ><a id="orders" href="<?php echo SITE_NAME ?>index.php/cart/getOrdersOutgoing">Заказы</a></li>
                            <?php if($this->session->userdata('goal') == TRUE ) : ?>
                                <li><a id="productsIcon" href="<?php echo SITE_NAME ?>index.php/Products/myProducts">Товары</a></li>
                                <li><a id="stock" href="#">Акции</a></li>
                                <ul>
                                </ul>
                                <li><a id="news" href="#">Новости</a></li>
                            <?php endif; ?>
                            <?php if($this->session->userdata('goal') == FALSE ) :?>
                                <ul>
                                </ul>
                            <?php endif; ?>
                            <li><a id="statistics" href="#">Статистика</a></li>
                            <li><a id="favorites" href="#">Избранное</a></li>
                        </ul>
                    </div><!-- /.profile-menu -->
                    <div class="megamenu-left close hidden-sm hidden-xs" id="megamenu">

                    </div><!-- /.megamenu-left -->
                    <div class="content results">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="results-filter-xs">
                                    <a href="#" class="open-filter off-filter">Открыть фильтр</a>
                                    <div class="filter-sorting-xs">
                                        <a id="filter-sorting-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            Сортировать
                                            <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu " aria-labelledby="filter-sorting-xs">
                                            <li><a href="#">По названию</a></li>
                                            <li><a href="#">По цене</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="results-filter">
                                    <form class="filter" action="/" method="post">
                                        <div class="filter-price">
                                            <p>Цена:</p>
                                            <input type="text" name="min_price" value="0">
                                            <span>-</span>
                                            <input type="text" name="max_price" value="5000">
                                        </div>
                                        <div class="filter-cats-country">
                                            <div class="filter-cats">
                                                <a id="filter-cats" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    Все категории
                                                    <span class="caret"></span>
                                                </a>
                                                <ul class="dropdown-menu " aria-labelledby="filter-cats">
                                                    <li><a href="#">Мясо</a></li>
                                                    <li><a href="#">Хлебо-булочные изделия</a></li>
                                                    <li><a href="#">Кулинария</a></li>
                                                    <li><a href="#">Алкоголь</a></li>
                                                    <li><a href="#">Бакалея</a></li>
                                                </ul>
                                            </div>
                                            <div class="filter-country">
                                                <a id="filter-country" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    Все страны
                                                    <span class="caret"></span>
                                                </a>
                                                <ul class="dropdown-menu " aria-labelledby="filter-country">
                                                    <li><a href="#">Россия</a></li>
                                                    <li><a href="#">США</a></li>
                                                    <li><a href="#">Великобритания</a></li>
                                                    <li><a href="#">Китай</a></li>
                                                </ul>
                                            </div>
                                            <div class="filter-provider">
                                                <a id="filter-provider" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    Все поставщики
                                                    <span class="caret"></span>
                                                </a>
                                                <ul class="dropdown-menu " aria-labelledby="filter-provider">
                                                    <li><a href="#">Coca-Cola</a></li>
                                                    <li><a href="#">Restobrothers</a></li>
                                                </ul>
                                            </div>
                                            <div class="filter-fabricator">
                                                <a id="filter-fabricator" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    Производитель
                                                    <span class="caret"></span>
                                                </a>
                                                <ul class="dropdown-menu " aria-labelledby="filter-fabricator">
                                                    <li><a href="#">Coca-Cola</a></li>
                                                    <li><a href="#">Restobrothers</a></li>
                                                </ul>
                                            </div>
                                            <div class="search-buttons">
                                                <button class="check-button">Наличие</button>
                                                <button>Доставка</button>
                                                <button>Самовывоз</button>
                                                <button>От избранных</button>
                                                <button>На исходе</button>
                                                <button>Бонус</button>
                                                <button>Проба</button>
                                                <button>Скидки</button>
                                                <button>Консигнация</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="results-tab">
                                    <div class="tabs-wrapper">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#products" aria-controls="products" role="tab" data-toggle="tab">
                                                            Товары <span>14</span>
                                                        </a></li>
                                                    <li><a href="#providers" aria-controls="providers" role="tab" data-toggle="tab">
                                                            Поставщики <span>25</span>
                                                        </a></li>
                                                    <div class="filter-sorting">
                                                        <a id="filter-sorting" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                            Сортировать
                                                            <span class="caret"></span>
                                                        </a>
                                                        <ul class="dropdown-menu " aria-labelledby="filter-sorting">
                                                            <li><a href="#">По названию</a></li>
                                                            <li><a href="#">По цене</a></li>
                                                        </ul>
                                                    </div>
                                                </ul> <!-- /.nav-tabs -->

                                                <div class="tab-content">
                                                    <div class="tab-pane products-wrapper active" id="products">
                                                        <div class="products">
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <nav>
                                                            <ul class="pagination">
                                                                <li>
                                                                    <a href="#" aria-label="Previous">
                                                                        <span aria-hidden="true">&laquo;</span>
                                                                    </a>
                                                                </li>
                                                                <li><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li>
                                                                <li><a href="#">3</a></li>
                                                                <li><a href="#">4</a></li>
                                                                <li><a href="#">5</a></li>
                                                                <li>
                                                                    <a href="#" aria-label="Next">
                                                                        <span aria-hidden="true">&raquo;</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </nav>
                                                    </div><!-- / #products -->
                                                    <div class="tab-pane providers-wrapper" id="providers">
                                                        <div class="products">
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!-- / #products -->
                                                </div><!-- /.tab-content  -->
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="cart-wrapper">
                                                    <div class="cart-result">
                                                        Корзина <span>3 / 24800 тг</span>
                                                        <a href="#" class="double-arrow"></a>
                                                    </div>
                                                    <div class="all-products">
                                                        <div class="company-products">
                                                            <p><span>Restobrothers</span></p>
                                                            <ul>
                                                                <li>Лимон, Африка 1кг*20шт. кислый, Чайный, сладкий<span>8450 тг</span></li>
                                                                <li>Сахар-рафинад ГОСТ, Чайный, сладкий, Чайный, сладкий<span>8450 тг</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="company-products">
                                                            <p><span>Сoca - cola & Co</span></p>
                                                            <ul>
                                                                <li>Лимон, Африка 1кг*20шт. кислый<span>8450 тг</span></li>
                                                                <li>Сахар-рафинад ГОСТ, Чайный, сладкий<span>8450 тг</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="company-products">
                                                            <p><span>Restobrothers</span></p>
                                                            <ul>
                                                                <li>Лимон, Африка 1кг*20шт. кислый <span>8450 тг</span></li>
                                                                <li>Сахар-рафинад ГОСТ, Чайный, сладкий<span>8450 тг</span></li>
                                                                <li>Шоколад AlpenGold 100гр.<span>8450 тг</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="company-products">
                                                            <p><span>Restobrothers</span></p>
                                                            <ul>
                                                                <li>Лимон, Африка 1кг*20шт. кислый</li>
                                                                <li>Сахар-рафинад ГОСТ, Чайный, сладкий</li>
                                                            </ul>
                                                        </div>
                                                    </div><!-- /.all-products -->
                                                    <a href="#" class="cart-btn">Оформить заказ</a>
                                                </div>
                                            </div>
                                        </div>


                                    </div>	<!-- /.tabs-wrapper-->
                                </div><!-- /.povider-tab -->
                            </div>
                        </div>

                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>