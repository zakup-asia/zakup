<?php
    $company = null;
    $product = null;
?>
<section class="two-col-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="two-col">

                    <div class="megamenu-left" style="display: block" >
                        <ul class="megamenu-items-wrapper hidden-sm hidden-xs">
                            <?php foreach($section->result_array() as $row) : ?>
                                <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/product/findProducts/?searchText=<?php echo $row['name_section']?>"><?php echo $row['name_section']?></a>
                                    <div class="hidden-item">
                                        <div>
                                            <?php foreach ($category->result_array() as $row2) :?>
                                                <div class="hidden-menu-column">
                                                    <?php if($row2['id_section_one'] == $row['id_section_one']):?>
                                                        <a class="menu-parent-item" href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/product/findProducts/?searchText=<?php echo $row2['name_category']?>"><?php echo $row2['name_category']?></a>
                                                        <?php foreach ($subcategory->result_array() as $row3) :?>
                                                            <?php if($row3['id_category'] == $row2['id_category']):?>
                                                                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>product/findProducts/?searchText=<?php echo $row3['name_subcategory']?>" class="menu-cat-item"><?php echo $row3['name_subcategory']?></a>
                                                            <?php endif;?>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </div>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>

                    </div><!-- /.megamenu-left -->
                    <div class="content profile">
                        <div class="row">
                            <div class="col-lg-3 col-lg-push-9 col-sm-4 col-sm-push-8">
                                <div class="dop-menu">
                                    <nav class="navbar">
                                        <div class="navbar-header" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#dop-menu-collapse" aria-expanded="false">
                                            <button type="button" class="navbar-toggle collapsed">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                            <a class="navbar-brand" href="#">Меню</a>
                                        </div>

                                        <div class="collapse navbar-collapse" id="dop-menu-collapse">
                                            <ul class="nav navbar-nav">
                                                <li class="active"><a href="#">Корзина</a></li>
                                                <?php if($this->session->userdata('id_user') != null) : ?>
                                                    <li><a href="#">Избранные товары</a></li>
                                                    <li><a href="#">Закуп</a></li>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-lg-9 col-lg-pull-3 col-sm-8 col-sm-pull-4">
                                <div class="profile-content">
                                    <div class="breadcrumb cart-header">
                                        <div>Корзина</div>
                                        <div>товаров <span id="productCount"><?php foreach($count->result_array() as $row) echo $row['count'] ?></span></div>
                                        <div>сумма <span><span id="summa1"><?php foreach($summa->result_array() as $row) echo $row['SUMMA']?></span>  тг.</span></div>
                                    </div>
                                    <?php if ($cart != null): ?>
                                        <?php foreach($cart->result_array() as $row):?>

                                            <?php if($company != $row['name']): ?>
                                                <div class="cart-provider identy<?php echo $row['id_company']?>" idUserCompany="<?php echo $row['id_user']?>">
                                                    <?php $company = $row['name']?>
                                                    <div class="provider-name">
                                                        <span class="company-name"><?php echo $row['name']?></span>
                                                        <span>Дополнительно</span>
                                                    </div>
                                                    <?php foreach($cart->result_array() as $row2):?>
                                                        <?php if($row['name'] == $row2['name']):?>
                                                            <div class="cart-product" id="cartPD<?php echo $row2['id_product']?>">
                                                                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/deleteFromCart?id_user=<?php echo $this->session->userdata('id_user')?>&id_cart=<?php echo $row2['id_cart']?>" class="delete"></a>
                                                                <div class="product-info">
                                                                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/product/getProduct/<?php echo $row['id_product']?>/<?php echo $row['id_company'] ?>" class="product-name"><?php echo $row2['name_product'] ?></a>
                                                                    <p>
                                                                        <span><?php echo $row2['name_country'] ?></span>
                                                                        <?php if($row2['at_the_end'] != null ) : ?>
                                                                            <span>на исходе</span>
                                                                        <?php endif ?>
                                                                    </p>
                                                                    <p id="price<?php echo $row2['id_product']?>" style="display:none"><?php echo $row2['price']?></p>
                                                                </div>
                                                                <div class="cart-product-right">
                                                                    <div class="count">
                                                                        <input type="hidden" id="id_company<?php echo $row2['id_product']?>" value="<?php echo $row2['id_company']?>">
                                                                        <input id="product<?php echo $row2['id_product']?>" type="hidden" value="<?php echo $row2['id_cart']?>">
                                                                        <button onclick="decSend('<?php echo $row2['id_product']?>')" class="minus"></button>
                                                                        <input id="upd<?php echo $row2['id_product']?>" onchange="updateSend('<?php echo $row2['id_product']?>')" type="text" value="<?php echo $row2['number_of_products']?>">
                                                                        <button onclick="incSend('<?php echo $row2['id_product']?>')" class="plus"></button>
                                                                    </div>
                                                                    <div class="price" id="summa2">
                                                                        <input id="price<?php echo $row2['id_product'] ?>" type="hidden" value="<?php echo $row2['price'] ?>">
                                                                        <span id="cartP<?php echo $row2['id_product'] ?>"><?php echo $row2['price_cart']?></span> Тг.
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php endforeach;?>
                                                </div>
                                            <?php endif;?>
                                        <?php endforeach; ?>
                                    <?php endif;?>
                                    <?php  foreach($count->result_array() as $row){
                                        if($row['count'] == '0')echo '<div class="text-center"><h1>Корзина пуста</h1></div>';
                                    }?>
                                    <div class="cart-total">
                                        <span>Итого</span>
                                        <div class="cart-count-right">
                                            <div class="count-provider"><?php foreach($company_count->result_array() as $row) {echo $row['count']; if($row['count'] > '1') echo ' поставщика'; else echo ' поставщик'; } ?></div>
                                            <div class="count"><span id="productCount2"><?php foreach($count->result_array() as $row) echo $row['count'];?></span> товаров</div>
                                            <div class="price"><span id="summa3"><?php foreach($summa->result_array() as $row) echo $row['SUMMA'] ?></span> Тг.</div>
                                        </div>
                                    </div>
                                    <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/cart/sendOrder/'.$this->session->userdata('__ci_last_regenerate'), array('class' => "form-horizontal", 'id' => 'form')) ?>
                                    <?php if ($user != null ){?>
                                    <?php foreach($user->result_array() as $userInformation) : ?>

                                    <input type="hidden" id="data"  name="order_date" value="<?php echo (date("Y-m-j H:i:s")); ?>">
                                    <input type="hidden" id="id_user" value="<?php if($this->session->userdata('id_user') != null) echo $this->session->userdata('id_user'); else echo get_cookie('id');?>">
                                    <input type="hidden" value="<?php echo $this->session->userdata('__ci_last_regenerate')?>" name="id_user">
                                        <div class="client-data">
                                            <div class="my-client-data">
                                                <div>
                                                    <div>
                                                        <div class="col-xs-8">
                                                            <p>Ваши данные:</p>
                                                            <div class="form-group">
                                                                <label class="col-xs-3 control-label"> Наименование:</label>
                                                                <div class="col-xs-9">
                                                                    <input class="form-control" type="text" name="client_name" placeholder="Наименование" value="<?php $name = set_value('client_name'); echo $name = !empty($name) ? $name : $userInformation['name']; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-xs-3 control-label">Телефон : *</label>
                                                                <div class="col-xs-9">
                                                                    <input class="form-control" type="text" name="client_phone" placeholder="Телефон" value="<?php $client_phone = set_value('client_phone'); echo $client_phone = !empty($client_phone) ? $client_phone : $userInformation['mobile_phone0']; ?>">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-xs-3 control-label">Емайл : </label>
                                                                <div class="col-xs-9">
                                                                    <input class="form-control" type="text" name="client_email" placeholder="email" value="<?php $email = set_value('client_email'); echo $email = !empty($email) ? $email : $userInformation['email']; ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br><br><br><br><br><br><br><br><br><br><br>
                                                </div>
                                            </div>
                                                <div class="client-address">
                                                    <div class="form-group">
                                                        <label class="col-xs-3 control-label">Адрес доставки:</label>
                                                        <div class="col-xs-8">
                                                            <input class="form-control" type="text" value="<?php $address = set_value('address'); echo $address = !empty($address) ? $address : $userInformation['actual_address']; ?>" name="address" >
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="date-time">
                                                <div class="form-group">
                                                    <label for="region" class="col-xs-3 control-label">Дата доставки:</label>
                                                    <div class="col-xs-4">
                                                        <input class="form-control" type="date" name="data_shipping" value="<?php echo set_value('data_shipping')?>">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="region" class="col-xs-3 control-label">Комментарий к заказу:</label>
                                                    <div class="col-xs-8">
                                                        <input type="text" class="form-control" id="region" name="comment" value="<?php echo set_value('comment')?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cart-action">
                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/clearOrder/<?php echo $this->session->userdata('__ci_last_regenerate') ?>" class="clear-order text-center" onclick="">Очистить корзину</a>
                                            <input type="submit"  class="do css-red-input" value="Оформить заказ" onclick="setSuppliers()">
                                        </div>
                                </div>
                                <?php endforeach; ?>
                                <?php } else {?>
                                    <input type="hidden" id="data"  name="order_date" value="<?php echo (date("Y-m-j H:i:s")); ?>">
                                    <input type="hidden" id="id_user" value="<?php if($this->session->userdata('id_user') != null) echo $this->session->userdata('id_user'); else echo get_cookie('id');?>">
                                    <input type="hidden" value="<?php echo $this->session->userdata('__ci_last_regenerate')?>" name="id_user">
                                    <div class="client-data">
                                        <div class="my-client-data">
                                            <div>
                                                <div>
                                                    <div class="col-xs-8">
                                                        <p>Ваши данные:</p>
                                                        <div class="form-group">
                                                            <label class="col-xs-3 control-label"> Наименование:</label>
                                                            <div class="col-xs-9">
                                                                <input class="form-control" type="text" name="client_name" placeholder="Наименование" value="<?php set_value('client_name')?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-xs-3 control-label">Телефон : *</label>
                                                            <div class="col-xs-9">
                                                                <input class="form-control" type="text" name="client_phone" placeholder="Телефон" value="<?php set_value('client_phone')?>">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-xs-3 control-label">Емайл : </label>
                                                            <div class="col-xs-9">
                                                                <input class="form-control" type="text" name="client_email" placeholder="email" value="<?php set_value('client_email')?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br><br><br><br><br><br><br><br><br><br><br>
                                            </div>
                                        </div>
                                        <div class="client-address">
                                            <div class="form-group">
                                                <label class="col-xs-3 control-label">Адрес доставки:</label>
                                                <div class="col-xs-8">
                                                    <input class="form-control" type="text" value="<?php set_value('address')?>" name="address" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="date-time">
                                            <div class="form-group">
                                                <label for="region" class="col-xs-3 control-label">Дата доставки:</label>
                                                <div class="col-xs-4">
                                                    <input class="form-control" type="date" name="data_shipping" value="<?php echo set_value('data_shipping')?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="region" class="col-xs-3 control-label">Комментарий к заказу:</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="region" name="comment" value="<?php echo set_value('comment')?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        <div class="cart-action">
                                            <a href="#<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/clearOrder/<?php echo $this->session->userdata('__ci_last_regenerate') ?>" class="clear-order text-center">Очистить корзину</a>
                                            <input type="submit"  class="do css-red-input" value="Оформить заказ" onclick="setSuppliers()">
                                        </div>
                                    </div>
                                <?php } ?>
                                </form>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>

<script>
    //$(document).ready(function () {
    //    document.getElementById('data').value = NowData();
    //});

    function getAllClass(){
        var companyClass = document.getElementsByClassName('company-name');
        var companyClass2;
        for(i=0; i<companyClass.length; i++){
            console.log(companyClass[i].innerHTML);
        }
    }

</script>