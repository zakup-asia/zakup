<link href="/../../css/style2.css" rel="stylesheet">

<style>
	.printSelected div {display: none } /* скрываем весь контент на странице */
	.printSelected div.printSelection .toggle-btn {display: none }
	.printSelected div.printSelection .clear-order {display: none }
	.printSelected div.printSelection {display: block; } /* делаем видимым только тот блок, который подготовлен для печати */
	.printSelected div.printSelection div {display: block; } /* показываем всех его потомков, которые были скрыты первой строкой */
</style>

<script>
function printBlock(id)
{
    productDesc = $('#order_content_'+id+'').html();//забираем контент нужного нам блока (в моем случае ссылка на печать находится внути его)
    $('body').addClass('printSelected');//добавляем класс к body
    $('body').append('<div class="printSelection">' + productDesc + '</div>');//создаем новый блок внутри body
    window.print();//печатаем
    
    window.setTimeout(pageCleaner, 0); //очищаем нашу страницу от "мусора"
    
    return false;//баним переход по ссылке, чтобы она не пыталась перейти по адресу, указанному внутри аттрибута href
}

function pageCleaner()
{
    $('body').removeClass('printSelected');//убираем класс у body
    $('.printSelection').remove();//убиваем наш только что созданный блок для печати
}
</script>
                    <div class="content profile">
                        <div class="row">
                            <div class="col-lg-3 col-lg-push-9 col-sm-4 col-sm-push-8">
                                <div class="dop-menu">
                                    <nav class="navbar">
                                        <div class="navbar-header" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#dop-menu-collapse" aria-expanded="false">
                                            <button type="button" class="navbar-toggle collapsed">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                            <a class="navbar-brand" href="#">Меню</a>
                                        </div>

                                        <div class="collapse navbar-collapse" id="dop-menu-collapse">
                                            <ul class="nav navbar-nav">
                                                <?php if($this->session->userdata('goal') == TRUE ) : ?>
                                                    <li ><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/getOrdersIncoming">Входящие заказы</a></li>
                                                <?php endif;?>
                                                <li class="active"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/getOrdersOutgoing">Исходящие заказы</a></li>
                                                <li ><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/getHistory">История заказов</a></li>
                                            </ul>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                            <div class="tabs">
                                <div class="col-lg-9 col-lg-pull-3 col-sm-8 col-sm-pull-4">
                                    <div class="profile-content tabs-wrapper order">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <?php if($this->session->userdata('goal') == TRUE ) : ?>
                                                <li role="presentation">
                                                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/getOrdersIncoming">
                                                        <span class="hidden-xs">Входящие</span>
                                                        <span class="visible-xs-inline">Входящие</span>
                                                        <span>&nbsp;</span>
                                                    </a>
                                                </li>
                                            <?php endif; ?>
                                            <li role="presentation" class="active" >
                                                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/getOrdersOutgoing">
                                                    <span class="hidden-xs">Исходящие</span>
                                                    <span class="visible-xs-inline">Исходящие</span>
                                                    <span>&nbsp;</span>
                                                </a>
                                            </li>
                                            <li role="presentation">
                                                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/getHistory">
                                                    <span class="hidden-xs">История</span>
                                                    <span class="visible-xs-inline">История</span>
                                                    <span>&nbsp;</span>
                                                </a>
                                            </li>
                                        </ul>

                                        <section id="content1">
                                            <div class="bs-example" id="products">
                                                <div class="panel-group" id="accordion">
                                                    <?php foreach ($orders->result_array() as $row): ?>
                                                        <div class="panel panel-default" id="order_content_<?php echo $row['id_order']?>">
                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $row['id_order']?>" class="accordion-btn" onclick="getOrderOut(<?php echo "'".$row['user']."'" ?>,<?php echo "'".$row['order_date']."'" ?>,<?php echo "'".$row['id_order']."'" ?>)">
                                                                <div class="panel-heading panel-first">
                                                                    <div class="panel-title">
                                                                        <div class="row">
                                                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                                                                    <div class="toggle-btn"><?php echo $row['user'] ?></div>
                                                                                </div>
                                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                                                                    <span>Заказ № <?php echo $row['id_order'] ?></span>
                                                                                </div>
                                                                                <div class="col-lg-3 col-md-4 col-sm-3 col-xs-6">
                                                                                    <span><?php echo $row['order_date'] ?></span>
                                                                                </div>
                                                                                <div class="col-lg-3 col-md-2 col-sm-3 col-xs-6">
                                                                                    <?php if($row['order_status'] != null): ?>
                                                                                        <button class="change-btn"><?php echo $row['order_status']?></button>
                                                                                    <?php endif; ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                            <div id="collapse<?php echo $row['id_order']?>" class="panel-collapse collapse">
                                                                <div id="panel-body<?php echo $row['id_order']?>" class="panel-body">

                                                                </div>
																<div class="panel-body">
																	<a onclick="printBlock(<?php echo $row['id_order']?>); return false;" class="clear-order text-center">Распечатать заказ</a>
																		
																</div>
																
                                                            </div>
                                                        </div>
                                                    <?php endforeach ?>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>