<?php
    $this->session->unset_userdata('mask_city');
    $array = array('mask_city' => $city);
    $this->session->set_userdata($array);
?>
<style>
/* ------------- Контейнер с адаптивными блоками------------- */
.masonry {
    margin: 1.5em 0;
    padding: 0;
    column-gap: 1.5em; /* Общее расстояние между колонками */
    font-size: 12px;
    -moz-column-gap: 1.5em; /* Расстояние между колонками для Firefox */
    -webkit-column-gap: 1.5em; /* Расстояние между колонками  для Safari, Chrome и iOS */
	height: 445px;

	column-fill: auto;
}
 
.masonry a {
	font-size: 12px;
}	
	
/* Элементы в виде плиток с содержанием */
.item {
    display: inline-block;
    width: 100%;
    box-sizing: border-box; /* Изменения алгоритма расчета ширины и высоты элемента.*/
    -moz-box-sizing: border-box; /* Для Firefox */ 
    -webkit-box-sizing: border-box; /* Для Safari, Chrome, iOS иAndroid */ 
   
}
 
.item a:last-child {
	margin-bottom: 10px;
}
 
/* Медиа-запросы для различных размеров адаптивного макета */
@media only screen and (min-width: 400px) {
    .masonry {
        -moz-column-count: 2;
        -webkit-column-count: 2;
        column-count: 2;
    }
}
 
@media only screen and (min-width: 700px) {
    .masonry {
        -moz-column-count: 5;
        -webkit-column-count: 5;
        column-count: 5;
		height: 459px;
    }
}
 
@media only screen and (min-width: 900px) {
    .masonry {
        -moz-column-count: 6;
        -webkit-column-count: 6;
        column-count: 6;
    }
}
 
@media only screen and (min-width: 1100px) {
    .masonry {
        -moz-column-count: 6;
        -webkit-column-count: 6;
        column-count: 6;
		
    }
}



</style>
    <section class="two-col-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="two-col">
                        <div class="profile-menu" id="profile-menu">

                        </div><!-- /.profile-menu -->
                        <div class="megamenu-left" style="display: block" >
                            <ul class="megamenu-items-wrapper hidden-sm hidden-xs">
                                <?php foreach($section->result_array() as $row) : ?>
                                    <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/search/<?php echo $row['name_section_translit']?>"><?php echo $row['name_section']?></a>
                                        <div class="hidden-item">
                                            <div class="masonry">
                                                <?php foreach ($category->result_array() as $row2) :?>
												<?php if ($subcategory->result_array()) {?>
                                                    <div class="hidden-menu-column item">
                                                        <?php if($row2['id_section_one'] == $row['id_section_one'] ):?>
                                                            <a class="menu-parent-item" href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/search/<?php echo $row2['name_category_translit']?>"><?php echo $row2['name_category']?></a>
                                                            <?php foreach ($subcategory->result_array() as $row3) :?>
                                                                <?php if($row3['id_category'] == $row2['id_category']):?>
                                                                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/search/<?php echo $row3['name_subcategory_translit']?>" class="menu-cat-item"><?php echo $row3['name_subcategory']?></a>
                                                                <?php endif;?>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </div>
												<?php } ?>
                                                <?php endforeach;?>
                                            </div>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>

                        <div class="megamenu-right">
                            <div class="slider-banners hidden-xs">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div id="carousel-example-generic" class="carousel slide block-shadow" data-ride="carousel">
                                            <!-- Indicators -->
                                            <ol class="carousel-indicators">
                                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                                <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                            </ol>

                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner" role="listbox">
                                                <!-- Проверку первый ли это слайд, если первый, значит он активный -->
                                                <?php foreach($sliders->result_array() as $row): ?>
                                                    <?php if($row['name_slide'] == 'Первый слайд' | $row['name_slide'] == 'Второй слайд' | $row['name_slide'] == 'Третий слайд' | $row['name_slide'] == 'Четвёртый слайд') {?>
                                                        <div class="item <?php if($row['name_slide'] == 'Первый слайд') echo 'active' ?>">
                                                            <div class="slide">
                                                                <img src='/../../images/upload/<?php echo $row['path'] ?>' alt="...">
                                                            </div>
                                                        </div>
                                                    <?php } else if($row == null) {?>
                                                        <div class="item <?php if($row['name_slide'] == 'Первый слайд') echo 'active' ?>">
                                                            <div class="slide">
                                                                <img src='/../../images/upload/img.png' alt="...">
                                                            </div>
                                                        </div>
                                                    <?php }?>
                                                <?php endforeach; ?>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="main-actions-wrapper">
                                            <?php foreach($sliders->result_array() as $row): ?>
                                                <?php if($row['name_slide'] == 'Пятый слайд' | $row['name_slide'] == 'Шестой слайд') :?>
                                                    <a class="action-wrapper block-shadow">
                                                        <div class="action" style="background-image: url();">
                                                            <img src='/../../images/upload/<?php echo $row['path'] ?>' alt="">
                                                        </div>
                                                    </a>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- /.slider-banners-->
                            <div class="privilleges">
                                <div class="row">
                                    <div class="col-md-4 col-xs-3">
                                        <div class="privillege block-shadow">
                                            <img src="/../../images/privillege1.png" alt="">
                                            <p class="hidden-sm hidden-xs">Найдите самые <span>низкие цены </span>и <span>лучшие условия поставок</span></p>
                                            <p class="visible-sm-block visible-xs-block">Поиск лучших цен</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-3">
                                        <div class="privillege block-shadow">
                                            <img src="/../../images/privillege2.png" alt="">
                                            <p class="hidden-sm hidden-xs">Найдите надежного поставщика за <span>3 минуты</span></p>
                                            <p class="visible-sm-block visible-xs-block">Быстро и удобно</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-xs-3">
                                        <div class="privillege block-shadow">
                                            <img src="/../../images/privillege3.png" alt="">
                                            <p class="hidden-sm hidden-xs">Полностью <span>бесплатно</span> и доступно <span>без регистрации</span></p>
                                            <p class="visible-sm-block visible-xs-block">Всегда бесплатно</p>
                                        </div>
                                    </div>
                                    <div class="col-xs-3 visible-xs-block visible-sm-block">
                                        <div class="privillege block-shadow">
                                            <img src="/../../images/menu.png" alt="">
                                            <p class="visible-sm-block visible-xs-block">Все категории</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div><!-- /.megamenu-right-->
                    </div><!-- /.two-col -->
                </div>
            </div>
        </div><!-- /.container -->
    </section>

    <section class="main-tab">
        <div class="tab-line"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="tabs-wrapper">
                        <a id="All" href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/Suppliers/getAllSuppliers" class="all-actions"><span>Все поставщики</span></a>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#providers" aria-controls="providers" role="tab" data-toggle="tab" onclick="AllCompanies()">
                                    <span class="hidden-xs">Поставщики</span>
                                    <span class="visible-xs-inline">Поставщики</span>
                                    <span>&nbsp;</span>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#actions" aria-controls="actions" role="tab" data-toggle="tab" onclick="AllAction()">
                                    <span class="hidden-xs">Акции</span>
                                    <span class="visible-xs-inline">Акции</span>
                                    <span>&nbsp;</span>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#new" aria-controls="new" role="tab" data-toggle="tab" onclick="AllNews()">
                                    <span class="hidden-xs">Новости и статьи</span>
                                    <span class="visible-xs-inline">Новости</span>
                                    <span>&nbsp;</span>
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#products" aria-controls="products" role="tab" data-toggle="tab" onclick="AllProducts()">
                                    <span class="hidden-xs">Товары</span>
                                    <span class="visible-xs-inline">Товары</span>
                                    <span>&nbsp;</span>
                                </a>
                            </li>
                        </ul>


                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane" id="actions">
                                <?php //TODO акции ?>
                                <?php foreach($stock->result_array() as $row): ?>
                                    <div class="best-actions">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="best-action">
                                                    <?php if($row['path'] != null):?>
                                                        <img width="72" src="/../../images/mini/<?php echo $row['path']?>" alt="">
                                                    <?php endif; ?>
                                                    <?php if($row['path'] == null):?>
                                                        <img width="72" src="/../../images/best-action.jpg<?php echo $row['path']?>" alt="">
                                                    <?php endif; ?>
                                                    <div class="action-desc">
                                                        <p><?php echo $row['description_stock']?></p>
														<br>
                                                        <p class="action-company">
                                                            <span><?php echo $row['name']?></span>
                                                            <span> до <?php echo $row['end_stock']?></span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- .best-actions -->
                                <?php endforeach;?>
                            </div>
                            <div role="tabpanel" class="tab-pane active" id="providers">
                                <div class="best-providers">
                                    <div class="row">
                                        <!-- Tab panes -->
                                        <?php foreach ($companies->result_array() as $row) :?>
                                            <div class="col-md-6">
                                                <div class="best-provider block-shadow">
                                                    <?php if($row['path'] != null):?>
                                                        <img width="72" src="/../../images/mini/<?php echo $row['path']?>" alt="">
                                                    <?php endif; ?>
                                                    <?php if($row['path'] == null):?>
                                                        <img width="72" src="/../../images/best-action.jpg<?php echo $row['path']?>" alt="">
                                                    <?php endif; ?>
                                                    <div class="provider-desc">
                                                        <p>
                                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/<?php echo $row['name_translit'] ?>">
                                                                <?php echo $row['name']?>
                                                            </a>
                                                        </p>
                                                        <div class="provider-cats">
                                                            <?php if($row['short_description'] != null):?>
                                                                <p><?php echo $row['short_description']?></p>
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="provider-info">
                                                            <?php if($row['payment_methods'] != null):?>
                                                                <span class="small">Способы оплаты:</span>
                                                                <span class="small"><?php echo $row['payment_methods'] ?></span>
                                                            <?php endif; ?>
                                                            <br>
                                                            <?php if($row['min_amount'] != null):?>
                                                                <span class="small">Мин. сумма заказа:</span>
                                                                <span class="small"><?php echo $row['min_amount'].' '?></span>
                                                            <?php endif; ?>
                                                            <br>
                                                            <?php if($row['shipping'] != null):?>
                                                                <span class="small">Доставка:</span>
                                                                <span class="small"><?php if($row['shipping'] == '1') echo 'есть'; else echo 'нет'  ?></span>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div><!-- .best-actions -->
                            </div>
                            <div role="tabpanel" class="tab-pane" id="new">
                                <div class="best-actions">
                                    <div class="row">
                                        <?php //TODO новости ?>
                                        <?php foreach($news->result_array() as $row):?>
                                            <div class="col-md-6">
                                                <div class="best-action">
                                                    <?php if($row['path'] != null):?>
                                                        <img width="72" src="/../../images/mini/<?php echo $row['path']?>" alt="">
                                                    <?php endif; ?>
                                                    <?php if($row['path'] == null):?>
                                                        <img width="72" src="/../../images/best-action.jpg<?php echo $row['path']?>" alt="">
                                                    <?php endif; ?>
                                                    <div class="action-desc">
                                                        <p><?php echo $row['description_stock']?></p>
                                                           <br>
                                                           <p class="action-company">
                                                                <a href="#"><?php echo $row['name']?></a>
                                                                <span>до <?php echo $row['end_stock']?></span>
                                                          </p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach;?>
                                    </div>
                                </div><!-- .best-actions -->
                            </div><!-- /.tab-content  -->
                            <div role="tabpanel" class="tab-pane" id="products">
                                <div class="best-providers">
                                    <div class="row">
                                        <?php foreach($products->result_array() as $row): ?>
                                            <div class="col-md-6">
                                                <div class="best-action">
                                                    <?php if($row['path'] != null): ?>
                                                        <img width="72" src="/../../images/mini/<?php echo $row['path'] ?>">
                                                    <?php endif;?>
                                                    <?php if($row['path'] == null): ?>
                                                        <img width="72" src="/../../images/best-action.jpg" alt="">
                                                    <?php endif; ?>
                                                    <div class="action-desc">
                                                        <p>
                                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/product/getProduct/<?php echo $row['id_product']?>/<?php echo $row['id_company'] ?>">
                                                                <?php echo $row['name_product']?>
                                                            </a>
                                                        </p>
                                                        <p class="action-company">
                                                            <span>
                                                                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/<?php echo $row['name_translit'] ?>">
                                                                    <?php echo $row['name']?>
                                                                </a>
                                                            </span>
                                                            <span>&nbsp;</span>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div><!-- .best-actions -->
                            </div>
                        </div>	<!-- /.tabs-wrapper-->
                    </div>
                </div>
            </div>
    </section>

    <section class="main-informer">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="main-summa">
                        <p>Сегодня на Zakup.asia совершили</p>
                        <span><?php foreach($random->result_array() as $row) echo $row['random'];?> заказов</span>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="main-subscribe">
                        <p>Хотите получать <span>выгодные предложения</span> и первыми узнавать о <span>новых акциях</span> и <span>скидках</span> лучших поставщиков? Подписывайтесь на рассылку!</p>
                        <form class="form-inline">
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Введите Ваш E-mail">
                            </div>
                            <div class="form-group">
                                <input type="tel" class="form-control" placeholder="Введите Ваш телефон">
                            </div>
                            <button type="submit" class="btn">Подписаться</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>




<script>
    function AllProducts(){
        var all = document.getElementById('All');
        all.setAttribute('href', '<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/AllProducts');
        all.innerHTML = '<span>Все товары</span>';
    }

    function AllCompanies(){
        var all = document.getElementById('All');
        all.setAttribute('href', '<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/Suppliers/getAllSuppliers');
        all.innerHTML = '<span>Все компании</span>';
    }

    function AllAction(){
        var all = document.getElementById('All');
        all.setAttribute('href', '#');
        all.innerHTML = '<span>Все акции</span>';
    }

    function AllNews(){
        var all = document.getElementById('All');
        all.setAttribute('href', '#');
        all.innerHTML = '<span>Все новости</span>';
    }

</script>