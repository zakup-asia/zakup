
                    <div class="content profile">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="profile-content">
                                    <ul class="breadcrumb">
                                        <li class="active">Мои акции</li>
                                        <div class="add-product-link"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/StockAndNews/addStock">Добавить акцию</a></div>
                                    </ul>
                                    <div class="profile-products">
                                        <?php if ($stock != null) : ?>
                                            <?php foreach ($stock->result_array() as $row) : ?>
                                                <div class="profile-product">
                                                    <div class="product-name">
                                                        <p><?php echo $row['name_stock']?></p>
                                                        <span class="country"><?php echo $row['short_description_stock']?></span>
                                                        <div class="product-price"><?php echo $row['begin_stock']?> | <?php echo $row['end_stock']?></div>
                                                    </div>
                                                    <ul class="product-actions">
                                                        <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/StockAndNews/deleteStock?id_stock=<?php echo $row['id_stock']?>" class="delete"></a></li>
                                                        <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/StockAndNews/editStock?id_stock=<?php echo $row['id_stock']?>" class="edit"></a></li>
                                                        <li><input type="checkbox"></li>
                                                        <li>
                                                            <a type="button" class="more dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#">Action</a></li>
                                                                <li><a href="#">Another action</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>