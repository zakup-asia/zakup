<!-- Кнопка, вызывающее модальное окно -->
<a href="#MyModal" data-toggle="modal"  class="btn btn-primary">Модальное окно</a>

<!-- HTML-код модального окна -->
<div id="MyModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title">Выберите город</h2>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 css-first css">
                        <h3><small><strong>Страна</strong></small></h3>
                        <a href=""><h4  class="abutton-red"><strong>Казахстан</strong></h4></a>
                        <a href=""><h4><strong>Казахстан</strong></h4></a>
                        <a href=""><h4><strong>Казахстан</strong></h4></a>
                        <a href=""><h4><strong>Казахстан</strong></h4></a>
                        <a href=""><h4><strong>Казахстан</strong></h4></a>
                    </div>
                    <div class="col-md-4 css-second css">
                        <h3><small><strong>Область</strong></small></h3>
                        <a href=""><h4><strong>Караганданская</strong></h4></a>
                        <a href=""><h4><strong>Караганданская</strong></h4></a>
                        <a href=""><h4><strong>Караганданская</strong></h4></a>
                        <a href=""><h4><strong>Караганданская</strong></h4></a>
                        <a href=""><h4><strong>Караганданская</strong></h4></a>
                    </div>
                    <div class="col-md-4 css-third css">
                        <h3><small><strong>Город</strong></small></h3>
                        <a href=""><h4><strong>Караганда</strong></h4></a>
                        <a href=""><h4><strong>Караганда</strong></h4></a>
                        <a href=""><h4><strong>Караганда</strong></h4></a>
                        <a href=""><h4><strong>Караганда</strong></h4></a>
                        <a href=""><h4><strong>Караганда</strong></h4></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--

-->