        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <ul class="footer-nav">
                            <li><a href="#">О сервисе</a></li>
                            <li><a href="#">Вопрос-ответ</a></li>
                            <li><a href="#">Обратная связь</a></li>
                            <li><a href="#">Контакты</a></li>
                            <li><a href="#">IOS</a></li>
                            <li><a href="#">Android</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <div class="footer-right">
                            <span>Zakup.asia © 2016</span>
                            <div class="social-icons">
                                <a href="#"><img src="/../../images/vk-icon.png" alt=""></a>
                                <a href="#"><img src="/../../images/facebook-icon.png" alt=""></a>
                                <a href="#"><img src="/../../images/google-icon.png" alt=""></a>
                                <a href="#"><img src="/../../images/twitter-icon.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        </div>
		<?php
			function getUrl() {
			  $url  = @( $_SERVER["HTTPS"] != 'on' ) ? 'http://'.$_SERVER["SERVER_NAME"] :  'https://'.$_SERVER["SERVER_NAME"];
			  $url .= ( $_SERVER["SERVER_PORT"] != 80 ) ? ":".$_SERVER["SERVER_PORT"] : "";
			  $url .= $_SERVER["REQUEST_URI"];
			  return $url;
			}  	
			
			if ( getUrl() == 'http://zakup.asia/AdminPanel/changeOrder' )
			{
		?>		
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
			<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
			<script type="text/javascript">
			$(document).ready(function(){
				
				$('#sections').click(function(){
								$.ajax({  
								type: "POST",  
								url: "http://"+window.location.hostname+"/order_test/get_childrens.php",  
								data: "parent_id="+$(this).val(),  
								success: function(html){  
									$(".masonry").html(html);  
									
								}  
							});

						});	
				
				
				$('.reorder_link').on('click',function(){
					$(".masonry").sortable({ tolerance: 'pointer' });
					$('.reorder_link').html('Сохранить порядок');
					$('.reorder_link').attr("id","save_reorder");
					$('#reorder-helper').slideDown('slow');
					$('.image_link').attr("href","javascript:void(0);");
					$('.image_link').css("cursor","move");
					$("#save_reorder").click(function( e ){
						if( !$("#save_reorder i").length ){
							$(this).html('').prepend('<img src="http://<?php echo $_SERVER['SERVER_NAME'] ?>/order_test/images/refresh-animated.gif"/>');
							$("ul.reorder-photos-list").sortable('destroy');
							$("#reorder-helper").html( "Изменение порядка категорий - Это может занять некоторое время. Пожалуйста, оставайтесь на странице до окончания сохранения порядка." ).removeClass('light_box').addClass('notice notice_error');
				
							var h = [];
							$(".hidden-menu-column").each(function() {  h.push($(this).attr('id').substr(9));  });
							
							$.ajax({
								type: "POST",
								url: "http://"+window.location.hostname+"/order_test/orderUpdate.php",
								data: {ids: " " + h + ""},
								success: function(html){
									window.location.reload();
								}
							});	
							return false;
						}	
						e.preventDefault();		
					});
				});
			});
			</script>	
		<?php
			}
			else
			{	
		?>
        		<script src="/../../js/jquery.min.js"></script>
		<?php
			}
		?>
		
        <script src="/../../js/bootstrap.min.js"></script>
        <script src="/../../js/slideout.min.js"></script>
        <script src="/../../js/scripts.js"></script>
        <script src="/../../js/scripts2.js"></script>
        <script src="/../../js/jquery.maskedinput.min.js"></script>
        <script src="/../../js/nikiScripts.js"></script>
		<script src="/../../js/websocketNiki.js"></script>
		<?php
			if ( getUrl() == 'http://zakup.asia/AdminPanel/changeOrder' )
			{
		?>		
	
		<?php
			}
			else
			{	
		?>
			<script src="/../../js/jquery.showPassword.js" type="text/javascript"></script>
			<script src="/../../js/jquery.spzoom.js"></script>
			<script type="text/javascript" src="/../../js/tooltipster.bundle.min.js"></script>        		
			<script type="text/javascript" src="/../../js/jquery.limit.js"></script>
		<?php
			}
		?>
		<!-- Yandex.Metrika counter --> 
		<script type="text/javascript"> 
			(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter37948930 = new Ya.Metrika({ id:37948930, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); 
		</script>
		<noscript><div><img src="https://mc.yandex.ru/watch/37948930" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
    </body>
</html>
