<section class="two-col-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="two-col">

                    <div class="megamenu-left" style="display: block" >
                        <ul class="megamenu-items-wrapper hidden-sm hidden-xs">
                            <?php foreach($section->result_array() as $row) : ?>
                                <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/search/<?php echo $row['name_section_translit']?>"><?php echo $row['name_section']?></a>
                                    <div class="hidden-item">
                                        <div class="masonry">
                                            <?php foreach ($category->result_array() as $row2) :?>
                                                <?php if ($subcategory->result_array()) {?>
                                                    <div class="hidden-menu-column item">
                                                        <?php if($row2['id_section_one'] == $row['id_section_one'] ):?>
                                                            <a class="menu-parent-item" href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/search/<?php echo $row2['name_category_translit']?>"><?php echo $row2['name_category']?></a>
                                                            <?php foreach ($subcategory->result_array() as $row3) :?>
                                                                <?php if($row3['id_category'] == $row2['id_category']):?>
                                                                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/search/<?php echo $row3['name_subcategory_translit']?>" class="menu-cat-item"><?php echo $row3['name_subcategory']?></a>
                                                                <?php endif;?>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                <?php } ?>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
					<script src="/../../js/jquery.min.js"></script>


                    <div class="content results">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="results-tab">
                                    <div class="tabs-wrapper">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#products" aria-controls="products" role="tab" data-toggle="tab">
                                                            Товары <span><?php foreach($count->result_array() as $row) echo $row['count'] ?></span>
                                                        </a></li>
                                                    <div class="filter-sorting">
                                                        <a id="filter-sorting" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                            Сортировать
                                                            <span class="caret"></span>
                                                        </a>
                                                        <ul class="dropdown-menu " aria-labelledby="filter-sorting">
                                                            <li><a href="#">По названию</a></li>
                                                            <li><a href="#">По цене</a></li>
                                                        </ul>
                                                    </div>
                                                </ul> <!-- /.nav-tabs -->

                                                <div class="tab-content">
                                                    <div class="tab-pane products-wrapper active" id="products">
                                                        <div class="products">
                                                            <?php foreach($products->result_array() as $row):?>
                                                                <div id="item<?php echo $row['id_product']?>">
                                                                    <div class="row">
                                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                            <div class="col-md-9 col-sm-8 col-xs-8">
                                                                                <?php if ( $row['path'] ) {?>
                                                                                    <span class="products-image">
																						<a href="/images/upload/<?php echo $row['path']?>" title="<?php echo $row['name'] ?>" data-spzoom data-target-page="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/product/getProduct/<?php echo $row['id_product']?>/<?php echo $row['id_company'] ?>">
																							<img src="/images/mini/<?php echo $row['path']?>" alt="<?php echo $row['name'] ?>" align="center"/>
																						</a>
                                                                                                
                                                                                            </span>
                                                                                <?php } else { ?>
                                                                                    <span class="products-image">
                                                                                            <img src="/../../images/best-action.jpg" alt="<?php echo $row['name'] ?>"  align="center"/>
                                                                                        </span>
                                                                                <?php } ?>
                                                                                <a class="css-black" href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/product/getProduct/<?php echo $row['id_product']?>"><?php echo $row['name_product']?></a>
                                                                                <p class="small"><a href="#"><?php echo $row['name_country']?></a> | <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/product/getProduct/<?php echo $row['id_product']?>"><?php echo $row['name']?></a></p>
                                                                            </div>
                                                                            <div class="col-md-3 col-sm-4 col-xs-4">
                                                                                <div id="<?php echo 'rem'.$row['id_product']?>" class="price-buy">
                                                                                    <div class="buy">
                                                                                        <b id="<?php echo $row['id_product']?>">
                                                                                            <button onclick="addToCart(<?php echo "'".$this->session->userdata('__ci_last_regenerate')."'" ?>, <?php echo "'".$row['id_product']."'" ?>, <?php echo "'".$row['id_company']."'"?>, <?php echo "'".$row['price']."'"?>)">купить</button>
                                                                                        </b>
                                                                                        <?php if($row['in_stock'] == null ) : ?>
                                                                                            <span>отсутствует</span>
                                                                                        <?php endif ?>
                                                                                        <?php if($row['at_the_end'] != null ) : ?>
                                                                                            <span>на исходе</span>
                                                                                        <?php endif ?>
                                                                                    </div>
                                                                                    <div class="price">
                                                                                        <p id="price<?php echo $row['id_product']?>"><?php echo $row['price']?>  <?php echo $row['icon_currency']?></p><!--<p class="css-icon-currency"><?php echo $row['icon_currency']?> </p>-->
                                                                                        <a href="#">отложить</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>													
                                                            <?php endforeach; ?>
                                                        </div>
                                                        <nav>
                                                            <ul class="pagination">
                                                                <li>
                                                                    <a href="#" aria-label="Previous">
                                                                        <span aria-hidden="true">&laquo;</span>
                                                                    </a>
                                                                </li>
                                                                <li><a href="#">1</a></li>
                                                                <li>
                                                                    <a href="#" aria-label="Next">
                                                                        <span aria-hidden="true">&raquo;</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </nav>
                                                    </div><!-- / #products -->
                                                    <div class="tab-pane providers-wrapper" id="providers">
                                                        <div class="products">
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div><!-- / #products -->
                                                </div><!-- /.tab-content  -->
                                            </div>
                                                <div class="col-sm-4">
                                                    <div id="myAffix" data-spy="affix" data-offset-top="110" class="cart-wrapper">
                                                        <div class="cart-result">
                                                            Корзина <span><span id="productCount"></span>/<span id="productPrice"></span> тг</span>
                                                            <a href="#" class="double-arrow"></a>
                                                        </div>
                                                        <div id="all_product" class="all-products">

                                                        </div><!-- /.all-products -->
                                                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/sendOrder" class="cart-btn">Оформить заказ</a>
                                                    </div>
                                                </div>


                                        </div>




                                    </div>	<!-- /.tabs-wrapper-->
                                </div><!-- /.povider-tab -->
                            </div>
                        </div>

                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>
<script>
    window.onload = function () {
        updateCart(<?php
            if ($this->session->userdata('id_user') == null){
                $cookie = get_cookie('id');
            } else {
                $cookie = $this->session->userdata('id_user');
            }
        echo "'".$cookie."'"
        ?>);
    }
</script>