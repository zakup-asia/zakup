<section class="two-col-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="two-col">

                    <div class="megamenu-left" style="display: block" >
                        <ul class="megamenu-items-wrapper hidden-sm hidden-xs">
                            <?php foreach($section->result_array() as $row) : ?>
                                <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/products/findProducts/?searchText=<?php echo $row['name_section']?>"><?php echo $row['name_section']?></a>
                                    <div class="hidden-item">
                                        <div>
                                            <?php foreach ($category->result_array() as $row2) :?>
                                                <div class="hidden-menu-column">
                                                    <?php if($row2['id_section_one'] == $row['id_section_one']):?>
                                                        <a class="menu-parent-item" href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/products/findProducts/?searchText=<?php echo $row2['name_category']?>"><?php echo $row2['name_category']?></a>
                                                        <?php foreach ($subcategory->result_array() as $row3) :?>
                                                            <?php if($row3['id_category'] == $row2['id_category']):?>
                                                                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/products/findProducts/?searchText=<?php echo $row3['name_subcategory']?>" class="menu-cat-item"><?php echo $row3['name_subcategory']?></a>
                                                            <?php endif;?>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </div>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>

                    <div class="megamenu-left close hidden-sm hidden-xs" id="megamenu">

                    </div><!-- /.megamenu-left -->

                    <div class="content results">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="results-filter-xs">
                                    <a href="#" class="open-filter off-filter">Открыть фильтр</a>
                                    <div class="filter-sorting-xs">
                                        <a id="filter-sorting-xs" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            Сортировать
                                            <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu " aria-labelledby="filter-sorting-xs">
                                            <li><a href="#">По названию</a></li>
                                            <li><a href="#">По цене</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <!--
                                <div class="results-filter">
                                    <form class="filter" action="/" method="post">
                                        <div class="filter-price">
                                            <p>Цена:</p>
                                            <input type="text" name="min_price" value="0">
                                            <span>-</span>
                                            <input type="text" name="max_price" value="5000">
                                        </div>
                                        <div class="filter-cats-country">
                                            <div class="filter-cats">
                                                <a id="filter-cats" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    Все категории
                                                    <span class="caret"></span>
                                                </a>
                                                <ul class="dropdown-menu " aria-labelledby="filter-cats">
                                                    <li><a href="#">Мясо</a></li>
                                                    <li><a href="#">Хлебо-булочные изделия</a></li>
                                                    <li><a href="#">Кулинария</a></li>
                                                    <li><a href="#">Алкоголь</a></li>
                                                    <li><a href="#">Бакалея</a></li>
                                                </ul>
                                            </div>
                                            <div class="filter-country">
                                                <a id="filter-country" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    Все страны
                                                    <span class="caret"></span>
                                                </a>
                                                <ul class="dropdown-menu " aria-labelledby="filter-country">
                                                    <li><a href="#">Россия</a></li>
                                                    <li><a href="#">США</a></li>
                                                    <li><a href="#">Великобритания</a></li>
                                                    <li><a href="#">Китай</a></li>
                                                </ul>
                                            </div>
                                            <div class="filter-provider">
                                                <a id="filter-provider" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    Все поставщики
                                                    <span class="caret"></span>
                                                </a>
                                                <ul class="dropdown-menu " aria-labelledby="filter-provider">
                                                    <li><a href="#">Coca-Cola</a></li>
                                                    <li><a href="#">Restobrothers</a></li>
                                                </ul>
                                            </div>
                                            <div class="filter-fabricator">
                                                <a id="filter-fabricator" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                    Производитель
                                                    <span class="caret"></span>
                                                </a>
                                                <ul class="dropdown-menu " aria-labelledby="filter-fabricator">
                                                    <li><a href="#">Coca-Cola</a></li>
                                                    <li><a href="#">Restobrothers</a></li>
                                                </ul>
                                            </div>
                                            <div class="search-buttons">
                                                <button class="check-button">Наличие</button>
                                                <button>Доставка</button>
                                                <button>Самовывоз</button>
                                                <button>От избранных</button>
                                                <button>На исходе</button>
                                                <button>Бонус</button>
                                                <button>Проба</button>
                                                <button>Скидки</button>
                                                <button>Консигнация</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                -->
                            </div>
                            <div class="col-sm-12">
                                <div class="results-tab">
                                    <div class="tabs-wrapper">
                                        <div class="row">
                                            <div class="col-sm-8">
                                                <ul class="nav nav-tabs">
                                                    <li><a href="#providers" aria-controls="providers" role="tab" data-toggle="tab">
                                                            <?php if ($count != null):?>
                                                                <?php foreach ($count->result_array() as $row) : ?>
                                                                    Поставщики <span><?php echo $row['count'] ?></span>
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
                                                        </a></li>
                                                    <div class="filter-sorting">
                                                        <a id="filter-sorting" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                            Сортировать
                                                            <span class="caret"></span>
                                                        </a>
                                                        <ul class="dropdown-menu " aria-labelledby="filter-sorting">
                                                            <li><a href="#">По названию</a></li>
                                                            <li><a href="#">По цене</a></li>
                                                        </ul>
                                                    </div>
                                                </ul> <!-- /.nav-tabs -->

                                                <div class="tab-content">
                                                    <div class="tab-pane products-wrapper active" id="products">
                                                        <div class="products">
                                                            <?php foreach ($suppliers->result_array() as $row) : ?>
                                                                <div class="row">
                                                                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/suppliers/getSupplier/<?php echo $row['id_company']?>">
                                                                            <?php if($row['path'] != null): ?>
                                                                                <div class="col-md-4">
                                                                                    <img  width='120' src="/../../images/mini/<?php $path = set_value('path'); echo $path = !empty($path) ? $path : $row['path']; ?>" alt="">
                                                                                </div>
                                                                            <?php endif; ?>
                                                                            <?php if ($row['path'] == null) : ?>
                                                                                <div class="col-md-4">
                                                                                    <img width='120' src="/../../images/best-action.jpg" alt="">
                                                                                </div>
                                                                            <?php endif; ?>

                                                                        <div class="col-md-7">
                                                                            <p><?php echo $row['name'] ?></p>
                                                                            <div class="provider-cats">
                                                                                <?php if($row['short_description'] != null):?>
                                                                                    <p><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/suppliers/getSupplier/<?php echo $row['id_company']?>"><?php echo $row['short_description'] ?></a></p>
                                                                                <?php endif; ?>
                                                                            </div>
                                                                            <div class="provider-info">
                                                                                <?php if($row['payment_methods'] != null):?>
                                                                                    <span class="small">Способы оплаты:</span>
                                                                                    <span class="small"><?php echo $row['payment_methods'] ?></span>
                                                                                <?php endif; ?>
                                                                                <br>
                                                                                <?php if($row['min_amount'] != null):?>
                                                                                    <span class="small">Мин. сумма заказа:</span>
                                                                                    <span class="small"><?php echo $row['min_amount'].' '?></span>
                                                                                <?php endif; ?>
                                                                                <br>
                                                                                <?php if($row['shipping'] != null):?>
                                                                                    <span class="small">Доставка:</span>
                                                                                    <span class="small"><?php if($row['shipping'] == '1') echo 'есть'; else echo 'нет'  ?></span>
                                                                                <?php endif; ?>
                                                                            </div>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            <?php endforeach; ?>
                                                            <!--
                                                            Сначала мы должны вывести картинк
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            -->
                                                        </div>
                                                        <nav>
                                                            <ul class="pagination">
                                                                <li>
                                                                    <a href="#" aria-label="Previous">
                                                                        <span aria-hidden="true">&laquo;</span>
                                                                    </a>
                                                                </li>
                                                                <li><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li>
                                                                <li><a href="#">3</a></li>
                                                                <li><a href="#">4</a></li>
                                                                <li><a href="#">5</a></li>
                                                                <li>
                                                                    <a href="#" aria-label="Next">
                                                                        <span aria-hidden="true">&raquo;</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </nav>
                                                    </div><!-- / #products -->
                                                    <div class="tab-pane providers-wrapper" id="providers">
                                                        <div class="products">
                                                            <!--
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <a href="#">Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</a>
                                                                <p class="small"><a href="#">Россия</a> | <a href="#">Restobrothers</a></p>
                                                                <div class="price-buy">
                                                                    <div class="price">
                                                                        <p>286970 тг.</p>
                                                                        <a href="#">отложить</a>
                                                                    </div>
                                                                    <div class="buy">
                                                                        <button>купить</button>
                                                                        <span>на исходе</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            -->
                                                        </div>
                                                    </div><!-- / #products -->
                                                </div><!-- /.tab-content  -->
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="cart-wrapper">
                                                    <div class="cart-result">
                                                        Корзина <span><span id="productCount"></span>/<span id="productPrice"></span> тг</span>
                                                        <a href="#" class="double-arrow"></a>
                                                    </div>
                                                    <div id="all_product" class="all-products">
                                                        <!--
                                                        <div class="company-products">
                                                            <p><span>Restobrothers</span></p>
                                                            <ul>
                                                                <li>Лимон, Африка 1кг*20шт. кислый, Чайный, сладкий<span>8450 тг</span></li>
                                                                <li>Сахар-рафинад ГОСТ, Чайный, сладкий, Чайный, сладкий<span>8450 тг</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="company-products">
                                                            <p><span>Сoca - cola & Co</span></p>
                                                            <ul>
                                                                <li>Лимон, Африка 1кг*20шт. кислый<span>8450 тг</span></li>
                                                                <li>Сахар-рафинад ГОСТ, Чайный, сладкий<span>8450 тг</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="company-products">
                                                            <p><span>Restobrothers</span></p>
                                                            <ul>
                                                                <li>Лимон, Африка 1кг*20шт. кислый <span>8450 тг</span></li>
                                                                <li>Сахар-рафинад ГОСТ, Чайный, сладкий<span>8450 тг</span></li>
                                                                <li>Шоколад AlpenGold 100гр.<span>8450 тг</span></li>
                                                            </ul>
                                                        </div>
                                                        <div class="company-products">
                                                            <p><span>Restobrothers</span></p>
                                                            <ul>
                                                                <li>Лимон, Африка 1кг*20шт. кислый</li>
                                                                <li>Сахар-рафинад ГОСТ, Чайный, сладкий</li>
                                                            </ul>
                                                        </div>
                                                        -->
                                                    </div><!-- /.all-products -->
                                                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/sendOrder/<?php echo $this->session->userdata('id_user') ?>" class="cart-btn">Оформить заказ</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>	<!-- /.tabs-wrapper-->
                                </div><!-- /.povider-tab -->
                            </div>
                        </div>

                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>
<script>
    window.onload = function () {
        updateCart(<?php echo "'".$this->session->userdata('id_user')."'"?>);
    }
</script>
