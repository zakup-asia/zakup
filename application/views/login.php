<div class="wrapper reg" id="panel">
    <header>
        <div class="header-reg">
            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/' ?>" class="logo-reg"><img src="/../../images/logo.png" alt="Zakup"></a>
        </div>
    </header>
    <div class="container">
        <div class="row">
            <div class="reg-wrapper">
                <div class="reg-top">
                    <h1>Вход</h1>
                    <div class="reg-top-right">
                        <span>Еще не зарегистрированы?</span>
                        <?= anchor('http://'.$_SERVER['SERVER_NAME'].'/user/registration', 'Регистрация'); ?>
                    </div>
                </div>
                <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/user/login/', array('class' => "reg-form form-horizontal")) ?>
                <div class="form-group">
                    <label for="email" class="control-label col-xs-4"></label>
                    <div class="col-sm-5 col-xs-9 reg-input">
                        <input type="text" class="form-control" id="email" name="email" placeholder="Введите e-mail или телефон" value="<?php echo set_value('email'); ?>" autofocus>
                        <?php echo form_error('email', '<div class="css-error">', '</div>'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="pass" class="control-label col-xs-4"></label>
                    <div class="col-sm-5 col-xs-9 reg-input">
                        <input type="password" class="form-control" id="pass" name="password" placeholder="Введите пароль" value="<?php echo set_value('password'); ?>">
                        <?php echo form_error('password', '<div class="css-error">', '</div>'); ?>
                    </div>
                </div>
                <div class="grey-line"></div>
                <div class="form-group">
                    <div class="col-sm-5 col-sm-offset-4 col-xs-9 reg-sub">
                        <input type="submit" class="form-control red-button" value="Войти">
                    </div>
                </div>
                </form>
            </div>
        </div>

    </div>
</div>