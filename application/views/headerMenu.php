<input type="hidden" id="connUserId" value="<?php if($this->session->userdata('id_user') != null) echo $this->session->userdata('id_user'); ?>">
<div class="wrapper" id="panel">
    <div class="over"></div>
    <div class="mobile-nav navbar  hidden-lg hidden-md">
        <button type="button" class="toggle-button navbar-toggle">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">ZAKUP</a>
        <div class="dop-menu">
            <a href="#" id="toggle-search"><img src="/../../images/search-btn.png" alt=""></a>
            <a href="#"><img src="/../../images/alert-icon-white.png" alt=""></a>
            <a href="#"><img src="/../../images/cart-icon-white.png" alt=""></a>
        </div>
        <div class="mobile-search">
            <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/product/findProducts/', array('class' => "form-inline search", 'method' => 'get')) ?>
                <div class="form-group">
                    <button type="submit"><img src="/../../images/arrow-back.png" alt="search"></button>
                    <input type="text" name="searchText" onfocus="clearSearch()" onblur="setNormalSearch()" class="form-control" placeholder="Я ищу">
                </div>
                <div class="form-group search-filter dropdown">
                    <span id="dropdownFilter-mobile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Товар</span>
                    <ul class="dropdown-menu" aria-labelledby="dropdownFilter-mobile">
                        <li><a href="#">Товар</a></li>
                        <li><a href="#">Поставщик</a></li>
                        <li><a href="#">Тендер</a></li>
                    </ul>
                </div>
            </form>
        </div>
    </div> <!-- /.mobile-nav -->

    <header>
        <div class="container visible-xs-block">
            <a href="#" style="background-image: url(images/mobile-banner.jpg);" class="mobile-banner" ></a>
        </div>
        <div class="header hidden-sm hidden-xs">
            <div class="container">
                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>" class="logo"><img src="/../../images/logo.png" alt="Zakup"></a>
                <!--<button type="button"  id="left-menu-toggle" class="top-menu-button img-circle button-click"></button>-->
                <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/product/findProducts/', array('class' => "form-inline search", 'method' => 'get')) ?>
                    <div class="form-group">
                        <button id="submitSearch" type="submit"><img src="/../../images/search-button.png" alt="search"></button>
                        <input type="text" class="form-control" onfocus="clearSearch()" onblur="setNormalSearch()" placeholder="Я ищу" id="searchText" name="searchText" value="<?php if($searchText != null) echo $searchText ?>">
                    </div>
                    <div class="form-group search-filter">
                        <button type="submit"><img src="/../../images/search-button-2.png"></button>
                    </div>
                </form>
                <div class="top-menu-right">
                    <ul class="nav navbar-nav">
                        <li class="change-city">
                            <button href="#MyModal" id="MyCity" data-toggle="modal" class="main-city"></button>
                            <ul class="dropdown-menu" id="cities">
                                <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/geolocation">Выбрать город</a></li>
                            </ul>
                        </li>
                        <?php if($this->session->userdata('id_user') == null): ?>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/sendOrder" class="top-cart"></a></li>
                            <li>
                                <div class="form-control red-button my-color">
                                    <a class="my-cart-btn" href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/login/">Войти</a>
                                </div>
                            </li>
                        <?php endif; ?>
                        <?php if($this->session->userdata('id_user') != null): ?>
                            <li><a href="#" class="top-alert"></a></li>
                            <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/sendOrder" class="top-cart"></a></li>
                            <li>
                                <a href="#"  class="dropdown-toggle head-image" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img id="MyProfile" class="img-radius" src="/../../images/mini/<?php echo $this->session->userdata('path')?>"></a>
                                <ul id="Profile" class="dropdown-menu">
                                    <?php if($this->session->userdata('goal') == TRUE ): ?>
                                        <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/<?php echo $this->session->userdata('name_translit')?>">Моя компания</a></li>
                                        <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/getOrdersIncoming">Мои заказы</a></li>
                                    <?php endif ?>
                                    <?php if($this->session->userdata('goal') == FALSE ) : ?>
                                        <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/getOrdersOutgoing">Мои заказы</a></li>
                                    <?php endif; ?>
                                    <li class="divider"></li>
                                    <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/main/">Настройки</a></li>
                                    <li><a href="#">Помощь</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/logout/1">Выйти</a></li>
                                </ul>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>  <!-- /.container -->
        </div><!-- /.header -->
    </header>
    <div id="companyInTheModeration" style="display:none">Ваша компания находится на модерации. Заполните все поля отмеченные звёздочкой в разделе Настройки</div>
    <!-- HTML-код модального окна -->
    <div id="MyModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Заголовок модального окна -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h2 class="modal-title">Выберите город</h2>
                </div>
                <!-- Основное содержимое модального окна -->
                <div class="modal-body">
                    <div class="row">
                        <div id="first" class="col-md-4 css-first css">
                            <h3><small><strong>Страна</strong></small></h3>
                        </div>
                        <div id="second" class="col-md-4 css-second css">
                            <h3><small><strong>Область</strong></small></h3>
                        </div>
                        <div id="third" class="col-md-4 css-third css">
                            <h3><small><strong>Город</strong></small></h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
    <script type="text/javascript">
        ymaps.ready(init);

        function init() {
            var MyCity = document.getElementById('MyCity');
            // Данные о местоположении, определённом по IP
            var geolocation = ymaps.geolocation,
            // координаты
                coords = [geolocation.latitude, geolocation.longitude],
                myMap = new ymaps.Map('map', {
                    center: coords,
                    zoom: 10
                });
            //MyCity.innerHTML = geolocation.city;
            //getAllCityFromCountry(geolocation['country']);
            var tochka = new ymaps.Placemark(
                coords,
                {
                    // В балуне: страна, город, регион.
                    balloonContentHeader: geolocation.country,
                    balloonContent: geolocation.city,
                    balloonContentFooter: geolocation.region
                }
            )

            myMap.geoObjects.add(tochka);
            //В итоге нам нужно парсить вот это, оно содержит всё нам необходимое.
            console.log(geolocation);
            console.log(geolocation['country'], geolocation['region'], geolocation['city']);
			var server_name = document.getElementById('SERVER_NAME').value;
			var name = document.getElementById('SERVER_NAME').value;
			name = name.split(".", 1);
			name[0] = name[0] + '.';
			var text;
			xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function () {
				if (xhttp.readyState==4 && xhttp.status == 200) {
					text = xhttp.responseText;
					console.log(text);
					if (name[0] != text && getCookie('check') === undefined){
						Cookie.set('check', 'true');
						window.location.href = "http://"+text+"zakup.asia/index.php";
					}
				}
			};
			xhttp.open("GET", "http://"+server_name+"/index.php/country/Compare?city="+geolocation['city']);
			xhttp.send();
        }

        //TODO не забывай, что нельзя использовать русские буквы в строке. Это ведёт к печальным последствиям.
        //Делаем запрос на получение городов, а затем обрабатываем его
        function getAllCityFromCountry (country){
            var cities = document.getElementById('cities');
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState==4 && xhttp.status == 200) {
                    console.log(xhttp.responseText);
                    var jsonText = JSON.parse(xhttp.responseText);
                    var jsonText2;
                    for (key in jsonText) {
                        if (jsonText.hasOwnProperty(key)) {
                            jsonText2 = jsonText[key];
                            var div = document.createElement('li');
                            div.setAttribute('value', jsonText2['id_city']);
                            div.innerHTML = "<a href='<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'?>index.php/user/registration'>"+jsonText2['name_city']+"</a>";
                            cities.appendChild(div);
                        }
                    }
                }
            };
            xhttp.open("GET", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'?>index.php/country/getallcityfromcountry?name_country="+country);
            xhttp.send();
        }
		
		function getCookie(name) {
		  var matches = document.cookie.match(new RegExp(
			"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
		  ));
		  return matches ? decodeURIComponent(matches[1]) : undefined;
		}
		
		var Cookie ={
		   set: function(name, value, days)
		   {
			  var domain, domainParts, date, expires, host;

			  if (days)
			  {
				 date = new Date();
				 date.setTime(date.getTime()+(days*24*60*60*1000));
				 expires = "; expires="+date.toGMTString();
			  }
			  else
			  {
				 expires = "";
			  }

			  host = location.host;
			  if (host.split('.').length === 1)
			  {
				 // no "." in a domain - it's localhost or something similar
				 document.cookie = name+"="+value+expires+"; path=/";
			  }
			  else
			  {
				 // Remember the cookie on all subdomains.
				  //
				 // Start with trying to set cookie to the top domain.
				 // (example: if user is on foo.com, try to set
				 //  cookie to domain ".com")
				 //
				 // If the cookie will not be set, it means ".com"
				 // is a top level domain and we need to
				 // set the cookie to ".foo.com"
				 domainParts = host.split('.');
				 domainParts.shift();
				 domain = '.'+domainParts.join('.');

				 document.cookie = name+"="+value+expires+"; path=/; domain="+domain;

				 // check if cookie was successfuly set to the given domain
				 // (otherwise it was a Top-Level Domain)
				 if (Cookie.get(name) == null || Cookie.get(name) != value)
				 {
					// append "." to current domain
					domain = '.'+host;
					document.cookie = name+"="+value+expires+"; path=/; domain="+domain;
				 }
			  }
		   },

		   get: function(name)
		   {
			  var nameEQ = name + "=";
			  var ca = document.cookie.split(';');
			  for (var i=0; i < ca.length; i++)
			  {
				 var c = ca[i];
				 while (c.charAt(0)==' ')
				 {
					c = c.substring(1,c.length);
				 }

				 if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
			  }
			  return null;
		   },

		   erase: function(name)
		   {
			  Cookie.set(name, '', -1);
		   }
		};
    </script>
    <div id="map" style="width: 0px; height: 0px; display: block;"></div>
