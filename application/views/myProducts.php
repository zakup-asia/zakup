
                    <div class="content profile">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="profile-content">
                                    <ul class="breadcrumb">
                                        <li class="active">Мои товары</li>
                                        <div class="add-product-link"><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/product/addProduct">Добавить товар</a></div>
                                    </ul>
                                    <div id="example-1-1" class="profile-products">
										<ul class="sortable-list ui-sortable">
											<?php if ($products != null) : ?>
												<?php foreach ($products->result_array() as $row) : ?>
													<li class="sortable-item">
														<div id="item<?php echo $row['id_product']?>" draggable="true" ondragstart="return dragStart(event)" class="profile-product <?php if($row['in_stock'] == null ) echo 'not-available'?> droppable" myid="<?php echo $row['id_product']?>">
															<div class="product-name">
																<p><?php echo $row['name_product']?></p>
																<span class="country"><?php echo $row['name_country']?></span>
																<?php if($row['at_the_end'] != null ) : ?>
																	<span class="status">на исходе</span>
																<?php endif ?>
																<?php if($row['in_stock'] == null ) : ?>
																	<span class="status">нет в наличии</span>
																<?php endif ?>
																<div class="product-price"><?php echo $row['price'].' '.$row['icon_currency']?></div>
															</div>
															<ul class="product-actions">
																<li><a href="#Modal2" data-toggle="modal" data-parameter="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/product/deleteProduct?id_product=<?php echo $row['id_product']?>"  class="activeModal delete" ></a></li>
																<li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/product/editProduct?id_product=<?php echo $row['id_product']?>" class="edit"></a></li>
																<li>
																	<a type="button" class="more dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
																	<ul class="dropdown-menu">
																		<li><a href="#">Action</a></li>
																		<li><a href="#">Another action</a></li>
																	</ul>
																</li>
															</ul>
														</div>
													</li>	
												<?php endforeach; ?>
											<?php endif; ?>
										</ul>	
                                    </div>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>

<!-- HTML-код модального окна -->
<div id="Modal2" class="modal fade" style="display: none">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Заголовок модального окна -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <!-- Основное содержимое модального окна -->
            <div class="modal-body">
                <h4 class="modal-title text-center">Вы уверены что хотите удалить этот товар?</h4>
                <br>
                <!-- Форма входа -->
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <a id="deleteProduct" class="btn btn-lg btn-block css-grey">Да</a>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6">
                        <a data-dismiss="modal" class="btn btn-lg btn-block css-grey">Нет</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form id="person">
</form>
<script>

    function dragStart(ev) {
        ev.dataTransfer.effectAllowed='move';
        ev.dataTransfer.setData("Text", ev.target.getAttribute('id'));
        ev.dataTransfer.setDragImage(ev.target,10,10);
        return true;
    }

    function dragEnter(ev) {
        event.preventDefault();
        return true;
    }

    function dragOver(ev) {
        event.preventDefault();
    }

    function dragDrop(ev) {
		var server_name = document.getElementById('SERVER_NAME').value;
        var elem = document.elementFromPoint(event.clientX, event.clientY);
        // найти ближайший сверху droppable
        var targetElement = elem.closest('.droppable');
        var check = '#'+targetElement.getAttribute('id');
        var data = ev.dataTransfer.getData("Text");
        var id = '#'+ev.target.getAttribute('id');
        var dataid = '#'+ data;
        $(check).after($('#'+data));
        console.log($(check).attr('myid'));
        console.log($(dataid).attr('myid'));
        var position = getMyProducts();
        console.log(position);
        var formData = new FormData(document.forms.person);
        formData.append("position", position);
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                console.log(xhttp.responseText);
            }
        };
        xhttp.open("POST", "http://"+server_name+"/index.php/product/changePosition");
        xhttp.send(formData);
        ev.stopPropagation();
        return false;
    }
    //Почти забавно даже
	
	//Создаём запрос для обновления позиций продуктов
    function getMyProducts(){
        var myProduxts =$('.profile-product');
        var stroka = '';
        for (i=0; i<myProduxts.length; i++){
            stroka = stroka + ' when id_product = '+ myProduxts[i].getAttribute('myid') + ' then ' + i;
        }
        stroka = stroka + ' end WHERE id_product in (';
        for (i=0; i<myProduxts.length; i++) {
            if ( i == 0) stroka = stroka + myProduxts[i].getAttribute('myid');
            else stroka = stroka + ',' +myProduxts[i].getAttribute('myid');
        }
        stroka = stroka + ')';
        return stroka;
    }
	
	function MyMouseUp(){
        var server_port = document.getElementById('SERVER_PORT').value;
        var server_name = document.getElementById('SERVER_NAME').value;
        if (server_port != '80'){
            server_name = server_name + ':' + server_port;
        }
        var position = getMyProducts();
        console.log(position);
        var formData = new FormData(document.forms.person);
        formData.append("position", position);
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                console.log(xhttp.responseText);
            }
        };
        xhttp.open("POST", "http://"+server_name+"/index.php/product/changePosition");
        xhttp.send(formData);
    }
</script>