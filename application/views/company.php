<?php
    $icon_currency = null;
    foreach($currency->result_array() as $row2){

        if($row2['icon_currency'] != null){
            $icon_currency = array();
            $icon_currency['icon_currency'] = $row2['icon_currency'];
        }
    }
?>


<section class="two-col-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="two-col">

                    <div class="megamenu-left" style="display: block" >
                        <ul class="megamenu-items-wrapper hidden-sm hidden-xs">
                            <?php foreach($section->result_array() as $row) : ?>
                                <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/search/<?php echo $row['name_section_translit']?>"><?php echo $row['name_section']?></a>
                                    <div class="hidden-item">
                                        <div class="masonry">
                                            <?php foreach ($category->result_array() as $row2) :?>
                                                <?php if ($subcategory->result_array()) {?>
                                                    <div class="hidden-menu-column item">
                                                        <?php if($row2['id_section_one'] == $row['id_section_one'] ):?>
                                                            <a class="menu-parent-item" href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/search/<?php echo $row2['name_category_translit']?>"><?php echo $row2['name_category']?></a>
                                                            <?php foreach ($subcategory->result_array() as $row3) :?>
                                                                <?php if($row3['id_category'] == $row2['id_category']):?>
                                                                    <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/search/<?php echo $row3['name_subcategory_translit']?>" class="menu-cat-item"><?php echo $row3['name_subcategory']?></a>
                                                                <?php endif;?>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                <?php } ?>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>

                    </div><!-- /.megamenu-left -->
                    <div class="content provider">
                        <div class="provider-top-lg hidden-xs"> <!--для больших и средних экранов -->
                            <?php if ($suppliers->result_array() != null){?>
                            <?php foreach ($suppliers->result_array() as $row) : ?>
                            <div class="row">
                                <div class="left col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="info">
                                                <div class="name">
                                                    <h1><?php echo $row['name'] ?></h1>
                                                    <div class="status">
                                                        <?php if($row['is_supplier'] != null):?>
                                                            <span>Поставщик</span>
                                                        <?php endif; ?>
                                                        <?php if($row['is_seller'] != null):?>
                                                            <span>Дистрибьютор</span>
                                                        <?php endif; ?>
                                                        <?php if($row['is_manufacturer'] != null):?>
                                                            <span>Производитель</span>
                                                        <?php endif; ?>
                                                        <?php if($row['is_importer'] != null):?>
                                                            <span>Импортёр</span>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="params">
                                                    <div class="row">
                                                        <div class="col-sm-9">
                                                            <?php if($row['path'] != null): ?>
                                                                <img width="98" src="/../../images/mini/<?php $path = set_value('path'); echo $path = !empty($path) ? $path : $row['path']; ?>" alt="">
                                                            <?php endif; ?>
                                                            <?php if ($row['path'] == null) : ?>
                                                                <img width="98" src="/../../images/best-action.jpg" alt="">
                                                            <?php endif; ?>
                                                            <ul class="params-left">
                                                                <li>
                                                                    <span>Мин. сумма заказа</span>
                                                                    <span><?php echo $row['min_amount'].' '?> <?php if($icon_currency['icon_currency'] != null) echo $icon_currency['icon_currency']; ?> </span>
                                                                </li>
                                                                <li>
                                                                    <span>Способ оплаты</span>
                                                                    <span><?php echo $row['payment_methods'] ?></span>
                                                                </li>
                                                                <li>
                                                                    <span>Доставка</span>
                                                                    <span><?php if($row['shipping'] == '1') echo 'есть'; else echo 'нет'  ?><span data-toggle="tooltip" data-placement="right" title="Поставщик сам доставляет товар покупателю">?</span></span>
                                                                </li>
                                                                <li>
                                                                    <span>Консигнация</span>
                                                                    <span><?php  if($row['сonsignment'] == '1') echo 'есть'; else if ($row['сonsignment'] == '0') echo 'нет'; else echo 'По договору' ?><span data-toggle="tooltip" data-placement="right" title="Поставка товара с отсрочкой платежа">?</span></span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <ul class="params-right">
                                                                <li class="red">
                                                                    <span></span>
                                                                    <span></span>
                                                                </li>
                                                                <li>
                                                                    <span>Хранение</span>
                                                                    <span <?php if($row['storing'] == '1') echo 'class="yes"'; else echo 'class="no"'  ?>></span>
                                                                </li>
                                                                <li>
                                                                    <span>Проба</span>
                                                                    <span <?php if($row['is_test'] == '1') echo 'class="yes"'; else echo 'class="no"'  ?>></span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>	<!-- /.params -->
                                            </div> <!-- /.info -->
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-lg-3 col-sm-4">
                                            <div class="rating-actions">
                                                <div class="rating">
                                                    <?php if ($row['company_rating'] > 4):?>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '1')">
                                                            <img src="/../../images/starsTrue.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '2')">
                                                            <img src="/../../images/starsTrue.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '3')">
                                                            <img src="/../../images/starsTrue.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '4')">
                                                            <img src="/../../images/starsTrue.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '5')">
                                                            <img src="/../../images/starsTrue.png">
                                                        </a>
                                                    <?php endif; ?>
                                                    <?php if ($row['company_rating'] > 3 && $row['company_rating']  <= 4):?>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '1')">
                                                            <img src="/../../images/starsTrue.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '2')">
                                                            <img src="/../../images/starsTrue.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '3')">
                                                            <img src="/../../images/starsTrue.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '4')">
                                                            <img src="/../../images/starsTrue.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '5')">
                                                            <img src="/../../images/starsFalse.png">
                                                        </a>
                                                    <?php endif; ?>
                                                    <?php if ($row['company_rating'] > 2 && $row['company_rating']  <= 3):?>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '1')">
                                                            <img src="/../../images/starsTrue.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '2')">
                                                            <img src="/../../images/starsTrue.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '3')">
                                                            <img src="/../../images/starsTrue.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '4')">
                                                            <img src="/../../images/starsFalse.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '5')">
                                                            <img src="/../../images/starsFalse.png">
                                                        </a>
                                                    <?php endif; ?>
                                                    <?php if ($row['company_rating'] > 1 && $row['company_rating']  <= 2):?>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '1')">
                                                            <img src="/../../images/starsTrue.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '2')">
                                                            <img src="/../../images/starsTrue.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '3')">
                                                            <img src="/../../images/starsFalse.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '4')">
                                                            <img src="/../../images/starsFalse.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '5')">
                                                            <img src="/../../images/starsFalse.png">
                                                        </a>
                                                    <?php endif; ?>
                                                    <?php if ($row['company_rating'] > 0 && $row['company_rating']  <= 1):?>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '1')">
                                                            <img src="/../../images/starsTrue.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '2')">
                                                            <img src="/../../images/starsFalse.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '3')">
                                                            <img src="/../../images/starsFalse.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '4')">
                                                            <img src="/../../images/starsFalse.png">
                                                        </a>
                                                        <a class="rating-company" onclick="setRating(<?php echo "'".$row['id_company']."'"?>, '5')">
                                                            <img src="/../../images/starsFalse.png">
                                                        </a>
                                                    <?php endif; ?>
                                                    <span id="rating"><?php echo number_format($row['company_rating'], 1)?></span>
                                                </div>
                                                <div class="actions">
                                                    <input type="hidden" id="companyFavorite" value="<?php echo $row['id_company']?>">
                                                    <span id="actionFavorite">
                                                        <?php if($this->session->userdata('id_user') != null && $row["count"] != 0): ?>
                                                            <a class="favoriteThis" onclick="unFavorite(<?php echo "'".$row['id_company']."'"?>)">Из избранного</a>
                                                        <?php endif; ?>
                                                        <?php if($this->session->userdata('id_user') != null && $row["count"] == 0): ?>
                                                            <a class="favoriteThis" onclick="favorite(<?php echo "'".$row['id_company']."'"?>)">В избранное</a>
                                                        <?php endif; ?>
                                                    </span>
                                                    <a class="notesThis" href="#">Заметки</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-9 col-sm-8">
                                            <div class="desc">
                                                <p><?php echo $row['description']?></p>
                                                <a href="#" class="expand close-type">Развернуть <span class="glyphicon glyphicon-menu-down"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.left-->
                                <div class="right col-sm-3">
                                    <div class="all-contacts">
                                        <div class="time-work">
                                            <a id="time-work" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                <span>Сегодня</span> <span id="today2">с 11:00 до 23:00</span>
                                                <span class="caret"></span>
                                            </a>
                                            <div class="dropdown-menu days" aria-labelledby="time-work">
                                                <p>Время работы :</p>
                                                <div id="Понедельник">
                                                    <span>ПН</span>
                                                    <?php if ($row['workday1from'] != null & $row['workday1to'] != null):?>
                                                        <span id="Понедельник1"><?php echo $row['workday1from']?></span>
                                                        <span id="Понедельник2"><?php echo $row['workday1to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['workday1from'] == null & $row['workday1to'] == null):?>
                                                        <span id="Понедельник1">&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span id="Понедельник2">&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div id="Вторник">
                                                    <span>ВТ</span>
                                                    <?php if ($row['workday2from'] != null & $row['workday2to'] != null):?>
                                                        <span id="Вторник1"><?php echo $row['workday2from']?></span>
                                                        <span id="Вторник2"><?php echo $row['workday2to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['workday2from'] == null & $row['workday2to'] == null):?>
                                                        <span id="Вторник1">&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span id="Вторник2">&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div id="Среда">
                                                    <span>СР</span>
                                                    <?php if ($row['workday3from'] != null & $row['workday3to'] != null):?>
                                                        <span id="Среда1"><?php echo $row['workday3from']?></span>
                                                        <span id="Среда2"><?php echo $row['workday3to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['workday3from'] == null & $row['workday3to'] == null):?>
                                                        <span id="Среда1">&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span id="Среда2">&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div id="Четверг">
                                                    <span>ЧТ</span>
                                                    <?php if ($row['workday4from'] != null & $row['workday4to'] != null):?>
                                                        <span id="Четверг1"><?php echo $row['workday4from']?></span>
                                                        <span id="Четверг2"><?php echo $row['workday4to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['workday4from'] == null & $row['workday4to'] == null):?>
                                                        <span id="Четверг1">&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span id="Четверг2">&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div id="Пятница">
                                                    <span>ПТ</span>
                                                    <?php if ($row['workday5from'] != null & $row['workday5to'] != null):?>
                                                        <span id="Пятница1"><?php echo $row['workday5from']?></span>
                                                        <span id="Пятница2"><?php echo $row['workday5to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['workday5from'] == null & $row['workday5to'] == null):?>
                                                        <span id="Пятница1">&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span id="Пятница2">&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div id="Суббота">
                                                    <span>СБ</span>
                                                    <?php if ($row['workday6from'] != null & $row['workday6to'] != null):?>
                                                        <span id="Суббота1"><?php echo $row['workday6from']?></span>
                                                        <span id="Суббота2"><?php echo $row['workday6to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['workday6from'] == null & $row['workday6to'] == null):?>
                                                        <span id="Суббота1">&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span id="Суббота2">&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div id="Воскресенье">
                                                    <span>ВС</span>
                                                    <?php if ($row['workday7from'] != null & $row['workday7to'] != null):?>
                                                        <span id="Воскресенье1"><?php echo $row['workday7from']?></span>
                                                        <span id="Воскресенье2"><?php echo $row['workday7to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['workday7from'] == null & $row['workday7to'] == null):?>
                                                        <span id="Воскресенье1">&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span id="Воскресенье2">&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <p>Время доставки :</p>
                                                <div>
                                                    <span>ПН</span>
                                                    <?php if ($row['shippingday1from'] != null & $row['shippingday1to'] != null):?>
                                                        <span><?php echo $row['shippingday1from']?></span>
                                                        <span><?php echo $row['shippingday1to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['shippingday1from'] == null & $row['shippingday1to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <span>ВТ</span>
                                                    <?php if ($row['shippingday2from'] != null & $row['shippingday2to'] != null):?>
                                                        <span><?php echo $row['shippingday2from']?></span>
                                                        <span><?php echo $row['shippingday2to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['shippingday2from'] == null & $row['shippingday2to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <span>СР</span>
                                                    <?php if ($row['shippingday3from'] != null & $row['shippingday3to'] != null):?>
                                                        <span><?php echo $row['shippingday3from']?></span>
                                                        <span><?php echo $row['shippingday3to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['shippingday3from'] == null & $row['shippingday3to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <span>ЧТ</span>
                                                    <?php if ($row['shippingday4from'] != null & $row['shippingday4to'] != null):?>
                                                        <span><?php echo $row['shippingday4from']?></span>
                                                        <span><?php echo $row['shippingday4to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['shippingday4from'] == null & $row['shippingday4to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <span>ПТ</span>
                                                    <?php if ($row['shippingday5from'] != null & $row['shippingday5to'] != null):?>
                                                        <span><?php echo $row['shippingday5from']?></span>
                                                        <span><?php echo $row['shippingday5to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['shippingday5from'] == null & $row['shippingday5to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <span>СБ</span>
                                                    <?php if ($row['shippingday6from'] != null & $row['shippingday6to'] != null):?>
                                                        <span><?php echo $row['shippingday6from']?></span>
                                                        <span><?php echo $row['shippingday6to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['shippingday6from'] == null & $row['shippingday6to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <span>ВС</span>
                                                    <?php if ($row['shippingday7from'] != null & $row['shippingday7to'] != null):?>
                                                        <span><?php echo $row['shippingday7from']?></span>
                                                        <span><?php echo $row['shippingday7to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['shippingday7from'] == null & $row['shippingday7to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div><!-- /.time-work -->

                                        <div class="contacts">
                                            <a id="contacts" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                Адрес
                                                <span class="caret"></span>
                                            </a>
                                            <div class="dropdown-menu full-contacts" aria-labelledby="contacts">
                                                <?php if($row['name_country'] != null) :?>
                                                    <p>Страна - <?php echo $row['name_country'] ?></p>
                                                <?php endif; ?>
                                                <p>
                                                    <?php foreach($region->result_array() as $go){
                                                        if($go['name_region'] != null) echo 'Область - '.$go['name_region'];
                                                    }
                                                    ?>
                                                </p>
                                                <?php if($row['name_city'] != null) :?>
                                                    <p>Город - <?php echo $row['name_city'] ?></p>
                                                <?php endif; ?>
                                            </div>
                                        </div>	<!-- /.contacts -->
                                    </div><!-- /.all-contacts -->
                                </div>	<!-- /.right-->
                                <div class="clearfix"></div>
                            </div>
                            <?php endforeach; ?>
                            <?php } else { echo 'Такой компании нет';}?>
                        </div><!-- /.provider-top-lg--> <!--для больших и средних экранов -->
                        <?php //TODO ?>
                        <div class="provider-top-xs visible-xs-block">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="name">
                                        <?php if($row['path'] != null): ?>
                                            <img width="98" src="/../../images/mini/<?php $path = set_value('path'); echo $path = !empty($path) ? $path : $row['path']; ?>" alt="">
                                        <?php endif; ?>
                                        <?php if ($row['path'] == null) : ?>
                                            <img width="98" src="/../../images/best-action.jpg" alt="">
                                        <?php endif; ?>
                                        <p><?php echo $row['name'] ?></p>
                                        <div class="status">
                                            <?php if($row['is_seller'] != null):?>
                                                <span>Магазин</span>
                                            <?php endif; ?>
                                            <?php if($row['is_manufacturer'] != null):?>
                                                <span>Производитель</span>
                                            <?php endif; ?>
                                            <?php if($row['is_importer'] != null):?>
                                                <span>Импортёр</span>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-6">
                                    <div class="rating-actions">
                                        <div class="rating">
                                            <img src="/../../images/stars.png" alt="">
                                            <span>3.2</span>
                                        </div>
                                        <div class="actions">
                                            <a href="#">Написать</a>
                                            <a href="#">В избранное</a>
                                            <a href="#">Заметки</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="all-contacts">
                                        <div class="time-work">
                                            <a id="time-work" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                <span>Сегодня</span> <span id="today">с 11:00 до 23:00</span>
                                                <span class="caret"></span>
                                            </a>
                                            <div class="dropdown-menu days" aria-labelledby="time-work">
                                                <p>Время работы :</p>
                                                <div>
                                                    <span>ПН</span>
                                                    <?php if ($row['workday1from'] != null & $row['workday1to'] != null):?>
                                                        <span><?php echo $row['workday1from']?></span>
                                                        <span id="Понедельник2"><?php echo $row['workday1to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['workday1from'] == null & $row['workday1to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <span>ВТ</span>
                                                    <?php if ($row['workday2from'] != null & $row['workday2to'] != null):?>
                                                        <span><?php echo $row['workday2from']?></span>
                                                        <span><?php echo $row['workday2to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['workday2from'] == null & $row['workday2to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <span>СР</span>
                                                    <?php if ($row['workday3from'] != null & $row['workday3to'] != null):?>
                                                        <span><?php echo $row['workday3from']?></span>
                                                        <span><?php echo $row['workday3to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['workday3from'] == null & $row['workday3to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <span>ЧТ</span>
                                                    <?php if ($row['workday4from'] != null & $row['workday4to'] != null):?>
                                                        <span><?php echo $row['workday4from']?></span>
                                                        <span><?php echo $row['workday4to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['workday4from'] == null & $row['workday4to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <span>ПТ</span>
                                                    <?php if ($row['workday5from'] != null & $row['workday5to'] != null):?>
                                                        <span><?php echo $row['workday5from']?></span>
                                                        <span><?php echo $row['workday5to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['workday5from'] == null & $row['workday5to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <span>СБ</span>
                                                    <?php if ($row['workday6from'] != null & $row['workday6to'] != null):?>
                                                        <span><?php echo $row['workday6from']?></span>
                                                        <span><?php echo $row['workday6to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['workday6from'] == null & $row['workday6to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <span>ВС</span>
                                                    <?php if ($row['workday7from'] != null & $row['workday7to'] != null):?>
                                                        <span><?php echo $row['workday7from']?></span>
                                                        <span><?php echo $row['workday7to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['workday7from'] == null & $row['workday7to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <p>Время доставки :</p>
                                                <div>
                                                    <span>ПН</span>
                                                    <?php if ($row['shippingday1from'] != null & $row['shippingday1to'] != null):?>
                                                        <span><?php echo $row['shippingday1from']?></span>
                                                        <span><?php echo $row['shippingday1to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['shippingday1from'] == null & $row['shippingday1to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <span>ВТ</span>
                                                    <?php if ($row['shippingday2from'] != null & $row['shippingday2to'] != null):?>
                                                        <span><?php echo $row['shippingday2from']?></span>
                                                        <span><?php echo $row['shippingday2to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['shippingday2from'] == null & $row['shippingday2to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <span>СР</span>
                                                    <?php if ($row['shippingday3from'] != null & $row['shippingday3to'] != null):?>
                                                        <span><?php echo $row['shippingday3from']?></span>
                                                        <span><?php echo $row['shippingday3to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['shippingday3from'] == null & $row['shippingday3to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <span>ЧТ</span>
                                                    <?php if ($row['shippingday4from'] != null & $row['shippingday4to'] != null):?>
                                                        <span><?php echo $row['shippingday4from']?></span>
                                                        <span><?php echo $row['shippingday4to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['shippingday4from'] == null & $row['shippingday4to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <span>ПТ</span>
                                                    <?php if ($row['shippingday5from'] != null & $row['shippingday5to'] != null):?>
                                                        <span><?php echo $row['shippingday5from']?></span>
                                                        <span><?php echo $row['shippingday5to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['shippingday5from'] == null & $row['shippingday5to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <span>СБ</span>
                                                    <?php if ($row['shippingday6from'] != null & $row['shippingday6to'] != null):?>
                                                        <span><?php echo $row['shippingday6from']?></span>
                                                        <span><?php echo $row['shippingday6to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['shippingday6from'] == null & $row['shippingday6to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <span>ВС</span>
                                                    <?php if ($row['shippingday7from'] != null & $row['shippingday7to'] != null):?>
                                                        <span><?php echo $row['shippingday7from']?></span>
                                                        <span><?php echo $row['shippingday7to']?></span>
                                                    <?php endif; ?>
                                                    <?php if ($row['shippingday7from'] == null & $row['shippingday7to'] == null):?>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                        <span>&mdash;&nbsp;:&nbsp;&mdash;</span>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div><!-- /.time-work -->

                                        <div class="contacts">
                                            <a id="contacts" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                Адрес
                                                <span class="caret"></span>
                                            </a>
                                            <div class="dropdown-menu full-contacts" aria-labelledby="contacts">
                                                <?php if($row['name_country'] != null) :?>
                                                    <p>Страна - <?php echo $row['name_country'] ?></p>
                                                <?php endif; ?>
                                                <p>
                                                    <?php foreach($region->result_array() as $go){
                                                        if($go['name_region'] != null) echo 'Область - '.$go['name_region'];
                                                    }
                                                    ?>
                                                </p>
                                                <?php if($row['name_city'] != null) :?>
                                                    <p>Город - <?php echo $row['name_city'] ?></p>
                                                <?php endif; ?>
                                            </div>
                                        </div>	<!-- /.contacts -->
                                    </div><!-- /.all-contacts -->
                                </div>
                                <div class="col-xs-12">
                                    <div class="params">
                                        <div class="row">
                                            <div class="col-xs-8">
                                                <ul class="params-left">
                                                    <li>
                                                        <span>Мин. сумма</span>
                                                        <span><?php echo $row['min_amount'] ?> <?php echo $row['id_currency'] ?></span>
                                                    </li>
                                                    <li>
                                                        <span>Оплата</span>
                                                        <span><?php echo $row['payment_methods'] ?></span>
                                                    </li>
                                                    <li>
                                                        <span>Доставка</span>
                                                        <span><?php echo $row['pickup'] ?><span data-toggle="tooltip" data-placement="bottom" title="Поставщик сам доставляет товар покупателю">?</span></span>
                                                    </li>
                                                    <li>
                                                        <span>Консигн.</span>
                                                        <span><?php echo $row['сonsignment']?><span data-toggle="tooltip" data-placement="bottom" title="Поставка товара с отсрочкой платежа">?</span></span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-xs-4">
                                                <ul class="params-right">
                                                    <li class="red">
                                                        <span>Бонус</span>
                                                        <span>0.3%</span>
                                                    </li>
                                                    <li>
                                                        <span>Хранение</span>
                                                        <span class="no"></span>
                                                    </li>
                                                    <li>
                                                        <span>Проба</span>
                                                        <span class="yes"></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>	<!-- /.params -->
                                </div>
                                <div class="col-xs-12">
                                    <div class="cats">
                                    </div> <!-- /.cats-->
                                </div>
                                <div class="col-xs-12">
                                    <div class="desc">
                                        <p><?php echo $row['description']?></p>
                                        <a href="#" class="expand close-type">Развернуть <span class="glyphicon glyphicon-menu-down"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="provider-bottom">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="provider-tab">
                                        <div class="tabs-wrapper">

                                            <ul class="nav nav-tabs">
                                                <li class="active"><a href="#price" aria-controls="price" role="tab" data-toggle="tab">
                                                        Прайс
                                                            <span>
                                                                <?php foreach ($count_product->result_array() as $row){
                                                                    echo $row['count'];
                                                                } ?>
                                                            </span>
                                                    </a></li>
                                                <li><a href="#actions" aria-controls="actions" role="tab" data-toggle="tab">Акции <span><?php $count = 0;  if ($stock != null) {foreach($stock->result_array() as $rw) echo $rw['count'];}else echo $count?></span>
                                                    </a></li>
                                                <li><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">Отзывы <span>0</span>
                                                    </a></li>
                                                <li><a href="#new" aria-controls="new" role="tab" data-toggle="tab">Новости <span><?php $count = 0; foreach($news->result_array() as $rw) if ($rw['count'] != null) echo $rw['count'];  else echo $count ?></span>
                                                    </a></li>
                                            </ul> <!-- /.nav-tabs -->

                                            <div class="row">
                                                <div class="col-sm-8 left-col">
                                                    <div class="tab-content">
                                                        <div class="tab-pane price-wrapper active" id="price">
                                                            <form class="filter" action="/" method="post">
                                                                <div class="filter-price">
                                                                    <p>Цена:</p>
                                                                    <input type="text" name="min_price" value="0">
                                                                    <span>-</span>
                                                                    <input type="text" name="max_price" value="5000">
                                                                </div>
                                                                <div class="filter-cats-country">
                                                                    <div class="filter-cats">
                                                                        <a id="filter-cats" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                            Все категории
                                                                            <span class="caret"></span>
                                                                        </a>
                                                                        <ul class="dropdown-menu " aria-labelledby="filter-cats">
                                                                            <li><a href="#">Мясо</a></li>
                                                                            <li><a href="#">Хлебо-булочные изделия</a></li>
                                                                            <li><a href="#">Кулинария</a></li>
                                                                            <li><a href="#">Алкоголь</a></li>
                                                                            <li><a href="#">Бакалея</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="filter-country">
                                                                        <a id="filter-country" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                            Все страны
                                                                            <span class="caret"></span>
                                                                        </a>
                                                                        <ul class="dropdown-menu " aria-labelledby="filter-country">
                                                                            <li><a href="#">Россия</a></li>
                                                                            <li><a href="#">США</a></li>
                                                                            <li><a href="#">Великобритания</a></li>
                                                                            <li><a href="#">Китай</a></li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="search-buttons">
                                                                        <button class="check-button">Наличие</button>
                                                                        <button>На исходе</button>
                                                                        <button>Бонус</button>
                                                                        <button>Проба</button>
                                                                        <button>Скидки</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                            <div class="products">
                                                                <?php if ($products != null) : ?>
                                                                    <?php foreach ($products->result_array() as $row) : ?>
                                                                        <div  id="item<?php echo $row['id_product']?>" class="profile-product <?php if($row['in_stock'] == null ) echo 'not-available'?>">
																			
                                                                            <div class="row">
                                                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                                    <div class="col-md-9 col-sm-8 col-xs-8">
																						<?php if ( $row['path'] ) {?>
																							<span class="products-image">
																								<a href="/images/upload/<?php echo $row['path']?>" title="<?php echo $row['name'] ?>" data-spzoom data-target-page="<?php echo 'http://'.$_SERVER['SERVER_NAME'] ?>/product/getProduct/<?php echo $row['id_product']?>/<?php echo $row['id_company'] ?>">
																									<img src="/images/mini/<?php echo $row['path']?>" alt="<?php echo $row['name'] ?>" align="center" />
																								</a>																							
																							</span>
																						<?php } else { ?>
																						<span class="products-image">
																							<img src="/../../images/best-action.jpg" alt="<?php echo $row['name'] ?>"  align="center"/>
																						</span>	
																						<?php } ?>
                                                                                        <a class="css-black" href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/product/getProduct/<?php echo $row['id_product']?>/<?php echo $row['id_company'] ?>"><?php echo $row['name_product']?></a>
                                                                                        <p class="small"><a href="#"><?php echo $row['name_country']?></a> | <a href="#"><?php echo $row['name'] ?></a></p>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-4 col-xs-4">
                                                                                        <div id="<?php echo 'rem'.$row['id_product']?>" class="price-buy">
                                                                                            <div class="buy">
                                                                                                <b id="<?php echo $row['id_product']?>">
                                                                                                    <button onclick="addToCart(<?php echo "'".$this->session->userdata('__ci_last_regenerate')."'" ?>, <?php echo "'".$row['id_product']."'" ?>, <?php echo "'".$row['id_company']."'"?>, <?php echo "'".$row['price']."'"?>)">купить</button>
                                                                                                </b>
                                                                                                <?php if($row['in_stock'] == null ) : ?>
                                                                                                    <span>отсутствует</span>
                                                                                                <?php endif ?>
                                                                                                <?php if($row['at_the_end'] != null ) : ?>
                                                                                                    <span> на исходе </span>
                                                                                                <?php endif ?>
                                                                                            </div>
                                                                                            <div class="price">
                                                                                                <p  ><?php echo $row['price'].' '.$icon_currency['icon_currency']?></p>
                                                                                                <a href="#">отложить</a>
                                                                                                <div style="display: none" id="price<?php echo $row['id_product']?>">
                                                                                                    <?php echo $row['price']?>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    <?php endforeach; ?>
                                                                <?php endif; ?>
																<?php foreach ($suppliers->result_array() as $row) : ?>
																	<a class="fa fa-file-excel-o" href="http://<?php echo $_SERVER['HTTP_HOST']?>/index.php/Suppliers/getSuppliersPrice/<?php echo $row['id_company']?>" title="Скачать прайс <?php echo $row['name']?> в формате Excel">Скачать прайс <?php echo $row['name']?> в формате Excel</a>
																<?php endforeach; ?>
                                                            </div>
                                                        </div><!-- / #price -->

                                                        <div class="tab-pane" id="actions">
                                                            <?php foreach ($stock->result_array() as $row) :?>
                                                                <?php if($row['description_stock'] != null) : ?>
                                                                    <div class="provider-actions">
                                                                        <div class="clearfix">
                                                                            <?php if($row['path'] != null){?>
                                                                                <img width="72" src="/../../images/mini/<?php echo $row['path']?>" alt="">
                                                                            <?php } else {?>
                                                                                <img width="72" src="/../../images/best-action.jpg<?php echo $row['path']?>" alt="">
                                                                            <?php } ?>
                                                                            <div class="action-desc">
                                                                                <p><?php echo $row['description_stock']?></p>
                                                                                <br>
                                                                                <p class="action-company">
                                                                                    <a href="#"><?php echo $row['name']?></a>
                                                                                    <span>до <?php echo $row['end_stock']?></span>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php endif;?>
                                                            <?php endforeach; ?>
                                                        </div><!-- / #actions -->
                                                        <div class="tab-pane" id="reviews">
                                                        </div><!-- / #reviews -->
                                                        <div class="tab-pane" id="new">
															<?php foreach ($news->result_array() as $row) :?>
                                                                <?php if($row['description_new'] != null) : ?>
                                                                    <div class="provider-actions">
                                                                        <div class="clearfix">
                                                                            <?php if($row['path'] != null){?>
                                                                                <img width="72" src="/../../images/mini/<?php echo $row['path']?>" alt="">
                                                                            <?php } else {?>
                                                                                <img width="72" src="/../../images/best-action.jpg<?php echo $row['path']?>" alt="">
                                                                            <?php } ?>
                                                                            <div class="action-desc">
                                                                                <p><?php echo $row['description_new']?></p>
                                                                                <p class="action-company">
                                                                                    <a href="#"><?php echo $row['name']?></a>
                                                                                    <span><?php echo $row['data_new']?></span>
                                                                                </p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php endif;?>
                                                            <?php endforeach;?>
                                                        </div><!-- / #news -->
                                                    </div><!-- /.tab-content  -->
                                                </div>
                                                <div class="col-sm-4 right-col" >
                                                    <div id="myAffix" data-spy="affix" data-offset-top="510" class="cart-wrapper">
                                                        <div class="cart-result">
                                                            Корзина <span><span id="productCount"></span>/<span id="productPrice"></span> тг</span>
                                                            <a href="#" class="double-arrow"></a>
                                                        </div>
                                                        <div id="all_product" class="all-products">

                                                        </div><!-- /.all-products -->
                                                        <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/sendOrder/<?php echo $this->session->userdata('id_user') ?>" class="cart-btn">Оформить заказ</a>
                                                    </div>

                                                </div>
                                            </div>

                                        </div>	<!-- /.tabs-wrapper-->
                                    </div><!-- /.povider-tab -->
                                </div>
                            </div>
                        </div> <!-- /.provider-bottom -->

                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>

<script>
	
    /**
     * Получить название текущего дня недели
     * @return {String}
     */
    function getWeekDay(date) {
        date = date || new Date();
        var days = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
        var day = date.getDay();
        return days[day];
    }

    //Установка сегодняшнего дня типо D
    function getDay(){
        var date = new Date();
        var weekDay = getWeekDay(date);
        console.log(weekDay);
        var day = document.getElementById(weekDay.toString());
        var today = document.getElementById('today');
        var today2 = document.getElementById('today2');
        today2.innerHTML = 'c ' + document.getElementById(weekDay + '1').innerText + ' до ' + document.getElementById(weekDay + '2').innerText;
        today.innerHTML = 'c ' + document.getElementById(weekDay + '1').innerText + ' до ' + document.getElementById(weekDay + '2').innerText;
        console.log(today.innerHTML);
        day.setAttribute('class', 'today');
    }


		

    window.onload = function () {
		updateCart(<?php echo "'".$this->session->userdata('id_user')."'"?>);
        getDay();
    };

    //Добавляем в избранное
    function favorite(id_company){
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                console.log(xhttp.responseText);
                document.getElementById('actionFavorite').innerHTML = xhttp.responseText;
            }
        };
        xhttp.open("GET", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'?>index.php/Favorites/setFavorite?id_company="+id_company);
        xhttp.send();
    }

    //Удаляем из избранного
    function unFavorite(id_company){
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                console.log(xhttp.responseText);
                document.getElementById('actionFavorite').innerHTML = xhttp.responseText
            }
        };
        xhttp.open("GET", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'?>index.php/Favorites/unsetFavorite?id_company="+id_company);
        xhttp.send();
    }

    function setRating(id_company, company_rating){
        var raing = document.getElementById('rating');
        var component = document.getElementsByClassName('rating-company');
        var intRat = 0;
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                intRat = parseInt(xhttp.responseText);
                raing.innerHTML = xhttp.responseText;
                if (intRat > 0 && intRat <= 1){
                    component[0].innerHTML = '<img src="/../../images/starsTrue.png">';
                    component[1].innerHTML = '<img src="/../../images/starsFalse.png">';
                    component[2].innerHTML = '<img src="/../../images/starsFalse.png">';
                    component[3].innerHTML = '<img src="/../../images/starsFalse.png">';
                    component[4].innerHTML = '<img src="/../../images/starsFalse.png">';
                }
                if (intRat > 1 && intRat <= 2){
                    component[0].innerHTML = '<img src="/../../images/starsTrue.png">';
                    component[1].innerHTML = '<img src="/../../images/starsTrue.png">';
                    component[2].innerHTML = '<img src="/../../images/starsFalse.png">';
                    component[3].innerHTML = '<img src="/../../images/starsFalse.png">';
                    component[4].innerHTML = '<img src="/../../images/starsFalse.png">';
                }
                if (intRat > 2 && intRat <= 3){
                    component[0].innerHTML = '<img src="/../../images/starsTrue.png">';
                    component[1].innerHTML = '<img src="/../../images/starsTrue.png">';
                    component[2].innerHTML = '<img src="/../../images/starsTrue.png">';
                    component[3].innerHTML = '<img src="/../../images/starsFalse.png">';
                    component[4].innerHTML = '<img src="/../../images/starsFalse.png">';
                }

                if (intRat > 3 && intRat <= 4){
                    component[0].innerHTML = '<img src="/../../images/starsTrue.png">';
                    component[1].innerHTML = '<img src="/../../images/starsTrue.png">';
                    component[2].innerHTML = '<img src="/../../images/starsTrue.png">';
                    component[3].innerHTML = '<img src="/../../images/starsTrue.png">';
                    component[4].innerHTML = '<img src="/../../images/starsFalse.png">';
                }

                if (intRat > 4){
                    component[0].innerHTML = '<img src="/../../images/starsTrue.png">';
                    component[1].innerHTML = '<img src="/../../images/starsTrue.png">';
                    component[2].innerHTML = '<img src="/../../images/starsTrue.png">';
                    component[3].innerHTML = '<img src="/../../images/starsTrue.png">';
                    component[4].innerHTML = '<img src="/../../images/starsTrue.png">';
                }
            }
        };
        xhttp.open("GET", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'?>index.php/rating/setRating?id_company="+id_company+"&company_rating="+company_rating);
        xhttp.send();
    }

</script>