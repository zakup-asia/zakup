<div class="wrapper" id="panel">
    <div class="over"></div>
    <div class="mobile-nav navbar  hidden-lg hidden-md">
        <button type="button" class="toggle-button navbar-toggle">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">ZAKUP</a>
        <div class="dop-menu">
            <a href="#" id="toggle-search"><img src="/../../images/search-btn.png" alt=""></a>
            <a href="#"><img src="/../../images/alert-icon-white.png" alt=""></a>
            <a href="#"><img src="/../../images/cart-icon-white.png" alt=""></a>
        </div>
        <div class="mobile-search">
            <?php echo form_open('Products/findProducts/', array('class' => "form-inline search", 'method' => 'get')) ?>
            <div class="form-group">
                <button type="submit"><img src="/../../images/arrow-back.png" alt="search"></button>
                <input type="text" name="searchText" class="form-control" placeholder="Я ищу">
            </div>
            <div class="form-group search-filter dropdown">
                <span id="dropdownFilter-mobile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Товар</span>
                <ul class="dropdown-menu" aria-labelledby="dropdownFilter-mobile">
                    <li><a href="#">Товар</a></li>
                    <li><a href="#">Поставщик</a></li>
                    <li><a href="#">Тендер</a></li>
                </ul>
            </div>
            </form>
        </div>
    </div> <!-- /.mobile-nav -->

    <header>
        <div class="container visible-xs-block">
            <a href="#" style="background-image: url(images/mobile-banner.jpg);" class="mobile-banner" ></a>
        </div>
        <div class="header hidden-sm hidden-xs">
            <div class="container">
                <a href="<?php echo SITE_NAME ?>" class="logo"><img src="/../../images/logo.png" alt="Zakup"></a>
                <!--<button type="button"  id="left-menu-toggle" class="top-menu-button img-circle button-click"></button>-->
                <?php echo form_open('Products/findProducts/', array('class' => "form-inline search", 'method' => 'get')) ?>
                <div class="form-group">
                    <button id="submitSearch" type="submit"><img src="/../../images/search-button.png" alt="search"></button>
                    <input type="text" class="form-control" placeholder="Я ищу" id="searchText" name="searchText" value="<?php if($searchText != null) echo $searchText ?>">
                </div>
                <div class="form-group search-filter">
                    <button type="submit"><img src="/../../images/search-button-2.png"></button>
                </div>
                </form>
                <div class="top-menu-right">
                    <ul class="nav navbar-nav">
                        <li class="change-city">
                            <button href="#" id="MyCity" class="main-city dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                            <ul class="dropdown-menu" id="cities">
                                <li><a href="<?php echo SITE_NAME?>index.php/user/geolocation">Выбрать город</a></li>
                            </ul>
                        </li>
                        <?php if($this->session->userdata('id_user') == null): ?>
                            <li><a href="<?php echo SITE_NAME ?>index.php/cart/sendOrder" class="top-cart"></a></li>
                            <li>
                                <div class="form-control red-button my-color">
                                    <a class="my-cart-btn" href="<?php echo SITE_NAME ?>index.php/user/login/">Войти</a>
                                </div>
                            </li>
                        <?php endif; ?>
                        <?php if($this->session->userdata('id_user') != null): ?>
                            <li><a href="#" class="top-alert"></a></li>
                            <li><a href="<?php echo SITE_NAME ?>index.php/cart/sendOrder" class="top-cart"></a></li>
                            <li>
                                <a href="#" id="MyProfile" class="top-profile dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                <ul id="Profile" class="dropdown-menu">
                                    <?php if($this->session->userdata('goal') == TRUE ) : ?>
                                        <li><a href="<?php echo SITE_NAME ?>index.php/cart/getOrdersIncoming">Моя компания</a></li>
                                    <?php endif; ?>
                                    <?php if($this->session->userdata('goal') == FALSE ) : ?>
                                        <li><a href="<?php echo SITE_NAME ?>index.php/cart/getOrdersOutgoing">Моя компания</a></li>
                                    <?php endif; ?>
                                    <li class="divider"></li>
                                    <li><a href="<?php echo SITE_NAME?>index.php/user/main/">Настройки</a></li>
                                    <li><a href="#">Помощь</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<?php echo SITE_NAME?>index.php/user/logout/1">Выйти</a></li>
                                </ul>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>  <!-- /.container -->
        </div><!-- /.header -->
    </header>
    <?php
    if($this->session->userdata('id_user') != null){
        if($this->session->userdata('show') == 0 && $this->session->userdata('goal') == 1){
            echo '<div id="companyInTheModeration">Ваша компания находится на модерации</div>';
        }
    }
    ?>
    <script src="https://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru-RU" type="text/javascript"></script>
    <script type="text/javascript">
        ymaps.ready(init);

        function init() {
            document.getElementById('map').innerHTML = '';
            var MyCity = document.getElementById('MyCity');
            var MyCity2 = document.getElementById('MyCity2');
            // Данные о местоположении, определённом по IP
            var geolocation = ymaps.geolocation,
            // координаты
                coords = [geolocation.latitude, geolocation.longitude],
                myMap = new ymaps.Map('map', {
                    center: coords,
                    zoom: 10
                });
            MyCity.innerHTML = geolocation.city;
            MyCity2.value = geolocation.city;
            getAllCityFromCountry(geolocation['country']);
            var tochka = new ymaps.Placemark(
                coords, {
                    // В балуне: страна, город, регион.
                    balloonContentHeader: geolocation.country,
                    balloonContent: geolocation.city,
                    balloonContentFooter: geolocation.region
                }
            );
            myMap.geoObjects.add(tochka);
            //В итоге нам нужно парсить вот это, оно содержит всё нам необходимое.
            console.log(geolocation);
            console.log(geolocation['country'], geolocation['region'], geolocation['city']);
        }

        function findCity(){
            document.getElementById('map').innerHTML = '';
            ymaps.geocode(document.getElementById('MyCity2').value, { results: 1 }).then(function (res) {
                // Выбираем первый результат геокодирования.
                var firstGeoObject = res.geoObjects.get(0),
                // Создаем карту с нужным центром.
                    myMap = new ymaps.Map("map", {
                        center: firstGeoObject.geometry.getCoordinates(),
                        zoom: 10
                    } , {
                    searchControlProvider: 'yandex#search'
                });
                myMap.container.fitToViewport();
                //Пробуем найти именно место

                // Поиск станций метро.
                ymaps.geocode(myMap.getCenter(), {
                    /**
                     * Опции запроса
                     * @see https://api.yandex.ru/maps/doc/jsapi/2.1/ref/reference/geocode.xml
                     */
                    // Ищем только станции метро.
                    kind: 'locality',
                    // Запрашиваем не более 20 результатов.
                    results: 1
                }).then(function (res) {
                    // Задаем изображение для иконок меток.
                    res.geoObjects.options.set('preset', 'islands#redCircleIcon');
                    // Добавляем коллекцию найденных геообъектов на карту.
                    myMap.geoObjects.add(res.geoObjects);
                });


            }, function (err) {
                // Если геокодирование не удалось, сообщаем об ошибке.
                alert(err.message);
            });
        }

        //TODO не забывай, что нельзя использовать русские буквы в строке. Это ведёт к печальным последствиям.
        //Делаем запрос на получение городов, а затем обрабатываем его
        function getAllCityFromCountry (country){
            console.log(country);
            var cities = document.getElementById('cities');
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState==4 && xhttp.status == 200) {
                    console.log(xhttp.responseText);
                    var jsonText = JSON.parse(xhttp.responseText);
                    var jsonText2;
                    for (key in jsonText) {
                        if (jsonText.hasOwnProperty(key)) {
                            jsonText2 = jsonText[key];
                            var div = document.createElement('li');
                            div.setAttribute('value', jsonText2['id_city']);
                            div.innerHTML = "<a href='#'>"+jsonText2['name_city']+"</a>";
                            cities.appendChild(div);
                        }
                    }
                }
            };
            xhttp.open("GET", "<?php echo SITE_NAME ?>index.php/country/getallcityfromcountry?name_country="+country);
            xhttp.send();
        }

        $(document).ready(function () {
            $('#MyCity').mouseover(function() {
                $('#cities').show();
            })
            $('#MyCity').mouseout(function() {
                t = setTimeout(function() {
                    $('#cities').hide();
                }, 500);
                $('#cities').on('mouseenter', function() {
                    $('#cities').show();
                    clearTimeout(t);
                }).on('mouseleave', function() {
                    $('#cities').hide();
                })
            })
            $('#MyProfile').mouseover(function() {
                $('#Profile').show();
            })
            $('#MyProfile').mouseout(function() {
                t = setTimeout(function() {
                    $('#Profile').hide();
                }, 500);
                $('#Profile').on('mouseenter', function() {
                    $('#Profile').show();
                    clearTimeout(t);
                }).on('mouseleave', function() {
                    $('#Profile').hide();
                })
            })
            $('#dropdownFilter').mouseover(function() {
                $('#filterContent').show();
            })
            $('#dropdownFilter').mouseout(function() {
                t = setTimeout(function() {
                    $('#filterContent').hide();
                }, 500);
                $('#filterContent').on('mouseenter', function() {
                    $('#filterContent').show();
                    clearTimeout(t);
                }).on('mouseleave', function() {
                    $('#filterContent').hide();
                })
            })
        });
    </script>
    <div class="megamenu-left">

        <div class="complete-order">
                <div id="map" class="col-md-12 yandexMap">

                </div>
                <div class="form-group " >
                    <div class="col-xs-offset-3 col-xs-8" id="change-city" >
                        <div class="col-md-1">
                            <h4>Город</h4>
                        </div>
                        <div class="col-md-3">
                            <input id="MyCity2" type="text" value="" placeholder="Ваш город" class="form-control" onchange="findCity()">
                        </div>
                        <div>
                            <button class="btn btn-profile" onclick="init()">Определить автоматически</button>
                        </div>
                    </div>
                    <?php //TODO редирект по нужной ссылке ?>
                </div>
                <div class="col-xs-offset-5 col-xs-2">
                    <button class="btn btn-profile">Сохранить</button>
                </div>
    </div>
        <!-- Остаётся только верить, что копыта я не отброшу. Необходимо подумать о том, как ты будешь переводить всё это дело с русского
            Вот так вот и живём.
            Такс каким образом это всё провернуть? Даже и не знаю. Будем верить, что я смогу как-то убедить, что всё будет готово до сколькита
            часов ночи, а затем уже сделать это. Поспать и сделать. Уверен, что я смогу.
            Да уж, это будет действительно трудно.
            По идее мы можем парсить сам днс сайта, но это не очень здравая идея.
            Ну и чего смешного? То, что я всю ночь не спал и припёрся на работу.
            Как вспомнишь так вздрогнешь, отличные у них шуточки были.
            Похоже это всё-таки будет более весело, чем я думал. Впрочем, просто тебе никогда и не было.
            надо как-то схитрить и временно провести шефа? Такая себе идея. Работать
            Я уверен, что смогу убрать потом, но как это обьяснить. Вот это более интересно.
        -->