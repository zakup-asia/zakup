<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php if (isset($title)) echo '<title>'.$title.'</title>';?>
    <link href="/../../css/bootstrap.min.css" rel="stylesheet">
    <link href="/../../css/style.css" rel="stylesheet">
    <link href="/../../css/my-style.css" rel="stylesheet">
    <link href="/../../css/css.css" rel="stylesheet">
	<link href="/../../css/jquery.spzoom.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/../../css/tooltipster.bundle.css" />
	<link rel="stylesheet" type="text/css" href="/../../css/plugins/tooltipster/sideTip/themes/tooltipster-sideTip-light.min.css" />
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->
    <link rel="shortcut icon" href="/../../images/favicon.ico" type="image/x-icon">
    <meta http-equiv="Cache-Control" content="no-cache">
    <?php if (isset($keywords)) echo '<meta name="keywords" content="'.$keywords.'">';?>
    <?php  if (isset($description)) echo '<meta name="description" content="'.$description.'">';?>
    <?php if (isset($canonical)) echo '<link rel="canonical" href="'.$canonical.'" />';?>
</head>

<body onload="chekCity()">
<?php if ($_SERVER['SERVER_PORT'] != 80){
    echo '<input type="hidden" id="SERVER_NAME" value="'.$_SERVER['SERVER_NAME'].'">';
    echo '<input type="hidden" id="SERVER_PORT" value="'.$_SERVER['SERVER_PORT'].'">';
    $_SERVER['SERVER_NAME'] = $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'];
} else {
    echo '<input type="hidden" id="SERVER_NAME" value="'.$_SERVER['SERVER_NAME'].'">';
    echo '<input type="hidden" id="SERVER_PORT" value="'.$_SERVER['SERVER_PORT'].'">';
}
?>

