<?php foreach ($requisites->result_array() as $row): ?>
<!--
                    <div class="content profile">
                        <div class="row">
                            <div class="col-lg-3 col-lg-push-9 col-sm-4 col-sm-push-8">
                               
                            </div>
                            <div class="col-lg-9 col-lg-pull-3 col-sm-8 col-sm-pull-4">
								-->
				<div class="content profile">
					<div class="row">
						<div class="col-xs-12">
							<div class="profile-content">
								<div class="profile-content tabs-wrapper order">
                                    <div class="row css-tabs-wrapper">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/main/">
                                                <div class="col-md-2 col-sm-2 col-xs-2 text-center css-top-menu">
                                                    Основные
                                                </div>
                                            </a>
                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/contactinformation/">
                                                <div class="col-md-2 col-sm-2 col-xs-2 text-center css-top-menu">
                                                    Контакты
                                                </div>
                                            </a>
                                            <?php if($this->session->userdata('goal') == TRUE ) : ?>
												<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/conditions/">
													<div class="col-md-3 col-sm-3 col-xs-3 text-center css-top-menu">
														Условия заказа
													</div>
												</a>
											<?php endif;?>
                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/requisites/">
                                                <div class="col-md-2 col-sm-2 col-xs-2 text-center active css-top-menu">
                                                    Реквизиты
                                                </div>
                                            </a>
                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/loginAndPassword/">
                                                <div class="col-md-3 col-sm-3 col-xs-3 text-center css-top-menu">
                                                    Логин и пароль
                                                </div>
                                            </a>
                                        </div>
                                    </div>
								
                                <div class="profile-content">
                                    <div class="profile-form">
                                        <!-- заполняем с помощью foreach, а затем -->
                                        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/user/requisites/', array('class' => "form-horizontal")) ?>

                                            <div class="form-group">
                                                <label for="name" class="col-xs-3 control-label">Название компании:</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="name" name="name_company" value="<?php $name_company = set_value('name_company'); echo $name_company = !empty($name_company) ? $name_company : $row['name_company']; ?>">
                                                    <?php echo validation_errors(); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="name-head" class="col-xs-3 control-label">Тип компании:</label>
                                                <div class="col-xs-8">
                                                    <select class="form-control" name="type_company" >
                                                        <option>ИП</option>
                                                        <option>ТОО</option>
                                                        <option>ОАО</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="name-head" class="col-xs-3 control-label">ФИО руководителя:</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="name-head" name="FIO_director" value="<?php $FIO_director = set_value('FIO_director'); echo $FIO_director = !empty($FIO_director) ? $FIO_director : $row['FIO_director']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="legal-address" class="col-xs-3 control-label">Юридический адрес:</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="legal-address" name="business_address" value="<?php $business_address = set_value('business_address'); echo $business_address = !empty($business_address) ? $business_address : $row['business_address']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="iin" class="col-xs-3 control-label">ИИН:</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="iin" name="IIN" value="<?php $IIN = set_value('IIN'); echo $IIN = !empty($IIN) ? $IIN : $row['IIN']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group horiz-line">
                                                <label for="kpp" class="col-xs-3 control-label">КПП:</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="kpp" name="KPP" value="<?php $KPP = set_value('KPP'); echo $KPP = !empty($KPP) ? $KPP : $row['KPP']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="short-desc" class="col-lg-3 col-sm-5 control-label">Банковские реквизиты:</label>
                                            </div>
                                            <div class="form-group">
                                                <label for="bank" class="col-xs-3 control-label">Банк:</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="bank" name="bank" value="<?php $bank = set_value('bank'); echo $bank = !empty($bank) ? $bank : $row['bank']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="account" class="col-xs-3 control-label">Расчетный счет:</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="account" name="account" value="<?php $account = set_value('account'); echo $account = !empty($account) ? $account : $row['account']; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="bik" class="col-xs-3 control-label">БИК:</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="bik" name="BIK" value="<?php $BIK = set_value('BIK'); echo $BIK = !empty($BIK) ? $BIK : $row['BIK']; ?>">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-offset-5 col-xs-3">
                                                    <button type="submit" class="btn btn-profile">Сохранить</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>
								</div>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>
<?php endforeach; ?>
<!-- twenty m for go th -->