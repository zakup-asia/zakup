<section class="two-col-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="two-col">

                    <div class="megamenu-left" style="display: block" >
                        <ul class="megamenu-items-wrapper hidden-sm hidden-xs">
                            <?php foreach($section->result_array() as $row) : ?>
                                <li><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/Products/findProducts/?searchText=<?php echo $row['name_section']?>"><?php echo $row['name_section']?></a>
                                    <div class="hidden-item">
                                        <div>
                                            <?php foreach ($category->result_array() as $row2) :?>
                                                <div class="hidden-menu-column">
                                                    <?php if($row2['id_section_one'] == $row['id_section_one']):?>
                                                        <a class="menu-parent-item" href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/Products/findProducts/?searchText=<?php echo $row2['name_category']?>"><?php echo $row2['name_category']?></a>
                                                        <?php foreach ($subcategory->result_array() as $row3) :?>
                                                            <?php if($row3['id_category'] == $row2['id_category']):?>
                                                                <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/Products/findProducts/?searchText=<?php echo $row3['name_subcategory']?>" class="menu-cat-item"><?php echo $row3['name_subcategory']?></a>
                                                            <?php endif;?>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </div>
                                            <?php endforeach;?>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>

                    <div class="megamenu-right">

                        <div class="complete-order">
                            <div class="row">
                                <div class="col-md-9">
                                    <h2 class="complete-order-background">Спасибо за использование сайта Zakup.asia.</h2>
                                    <h1><small>Ваша заявка находится в обработке. В течение нескольких минут оператор свяжется с Вами для подтверждения заказа.</small></h1>
                                </div>
                                <div class="col-md-3 bottom-op">
                                    <img src="/../../images/Zakaz1.jpg" alt="">
                                </div>
                            </div>
                        </div>

                        <div class="privilleges">
                            <div class="row">
                                <div class="col-md-4 col-xs-3">
                                    <div class="privillege block-shadow">
                                        <img src="/../../images/privillege1.png" alt="">
                                        <p class="hidden-sm hidden-xs">Найдите самые <span>низкие цены </span>и <span>лучшие условия поставок</span></p>
                                        <p class="visible-sm-block visible-xs-block">Поиск лучших цен</p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-3">
                                    <div class="privillege block-shadow">
                                        <img src="/../../images/privillege2.png" alt="">
                                        <p class="hidden-sm hidden-xs">Найдите надежного поставщика за <span>3 минуты</span></p>
                                        <p class="visible-sm-block visible-xs-block">Быстро и удобно</p>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-3">
                                    <div class="privillege block-shadow">
                                        <img src="/../../images/privillege3.png" alt="">
                                        <p class="hidden-sm hidden-xs">Полностью <span>бесплатно</span> и доступно <span>без регистрации</span></p>
                                        <p class="visible-sm-block visible-xs-block">Всегда бесплатно</p>
                                    </div>
                                </div>
                                <div class="col-xs-3 visible-xs-block visible-sm-block">
                                    <div class="privillege block-shadow">
                                        <img src="/../../images/menu.png" alt="">
                                        <p class="visible-sm-block visible-xs-block">Все категории</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div><!-- /.megamenu-right-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>
