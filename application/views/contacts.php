<?php foreach ($contactInformation->result_array() as $row) :?>
                  <div class="content profile">
					<div class="row">
						<div class="col-xs-12">
							<div class="profile-content">
								<div class="profile-content tabs-wrapper order">
									<div class="row css-tabs-wrapper">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/main/">
                                                <div class="col-md-2 col-sm-2 col-xs-2 text-center css-top-menu">
                                                    Основные
                                                </div>
                                            </a>
                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/contactinformation/">
                                                <div class="col-md-2 col-sm-2 col-xs-2 text-center active css-top-menu">
                                                    Контакты
                                                </div>
                                            </a>
                                            <?php if($this->session->userdata('goal') == TRUE ) : ?>
												<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/conditions/">
													<div class="col-md-3 col-sm-3 col-xs-3 text-center css-top-menu">
														Условия заказа
													</div>
												</a>
											<?php endif;?>
                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/requisites/">
                                                <div class="col-md-2 col-sm-2 col-xs-2 text-center css-top-menu">
                                                    Реквизиты
                                                </div>
                                            </a>
                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/loginAndPassword/">
                                                <div class="col-md-3 col-sm-3 col-xs-3 text-center css-top-menu">
                                                    Логин и пароль
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                <div class="profile-content">
                                    <div class="profile-form">
                                        <?php echo form_open('http://'.$_SERVER['SERVER_NAME'].'/user/contactInformation/', array('class' => "form-horizontal")) ?>
                                                <div class="form-group">
                                                    <label for="pickup" class="col-xs-3 control-label"> Страна *</label>
                                                    <div class="col-xs-8">
                                                        <select class="form-control" id="id_country" name="id_country" onclick="getAllRegions()">
                                                            <option></option>
                                                            <?php foreach ($info4->result_array() as $notRow) : ?>
                                                                <option value="<?php echo $notRow['id_country']?>" <?php if($row['id_country'] == $notRow['id_country']) echo 'selected="selected"'?>><?php echo $notRow['name_country']?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <?php echo form_error('id_country', '<div class="css-error">', '</div>'); ?>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="pickup" class="col-xs-3 control-label"> Регион *</label>
                                                    <div class="col-xs-8">
                                                        <select class="form-control" id="id_region" name="id_region" onclick="getAllCity()">
                                                            <option value="<?php $id_region = set_value('id_region'); echo $id_region = !empty($id_region) ? $id_region : $row['id_region']; ?>"> <?php $name_region = set_value('name_region'); echo $name_region = !empty($name_region) ? $name_region : $row['name_region']; ?></option>
                                                        </select>
                                                        <div id="add_region"></div>
                                                        <?php echo form_error('id_region', '<div class="css-error">', '</div>'); ?>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="pickup" class="col-xs-3 control-label"> Город *</label>
                                                    <div class="col-xs-8">
                                                        <select class="form-control" id="id_city" name="id_city" >
                                                            <option value="<?php $id_city = set_value('id_city'); echo $id_city = !empty($id_city) ? $id_city : $row['id_city']; ?>"> <?php $name_city = set_value('name_city'); echo $name_city = !empty($name_city) ? $name_city : $row['name_city']; ?></option>
                                                        </select>
                                                        <?php echo form_error('id_city', '<div class="css-error">', '</div>'); ?>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="index" class="col-xs-3 control-label">Индекс:</label>
                                                    <div class="input-index">
                                                        <input type="text" class="form-control" id="index" name="indexs" placeholder="______" value="<?php $indexs = set_value('indexs'); echo $indexs = !empty($indexs) ? $indexs : $row['indexs']; ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group horiz-line">
                                                    <label for="address" class="col-xs-3 control-label">Фактический адрес:</label>
                                                    <div class="col-xs-8">
                                                        <input type="text" class="form-control" id="address" name="actual_address" value="<?php $actual_address = set_value('actual_address'); echo $actual_address = !empty($actual_address) ? $actual_address : $row['actual_address']; ?>">
                                                   
                                                    </div>
                                                </div>

                                                <div class="form-group all-home-phone">
                                                    <label for="short-desc" class="col-xs-3 control-label">Телефоны:</label>
                                                    <div class="input-phone homep">
                                                        <input type="text" class="form-control"  name="home_phone0" value="<?php $home_phone0 = set_value('home_phone0'); echo $home_phone0 = !empty($home_phone0) ? $home_phone0 : $row['home_phone0']; ?>">
                                                        <span class="glyphicon glyphicon-phone-alt"></span>
                                                    </div>
                                                    <div class="col-xs-1">
                                                        <span class="add-home-phone"></span>
                                                    </div>
                                                </div>
                                                <?php if ($row['home_phone1'] != null ) : ?>
                                                    <div class="form-group">
                                                        <div class="input-phone homep col-xs-offset-3">
                                                            <input type="text" class="form-control"  name="home_phone0" value="<?php $home_phone1 = set_value('home_phone1'); echo $home_phone1 = !empty($home_phone1) ? $home_phone1 : $row['home_phone1']; ?>">
                                                            <span class="glyphicon glyphicon-phone-alt"></span>
                                                        </div>
                                                        <div class="col-xs-1">
                                                            <span onclick="addHomePhone()" class="add-mobile-phone css-add-phone"></span>
                                                            <span class="delete-home-phone css-delete-phone"></span>
                                                        </div>
                                                    </div>
                                                <?php endif ?>
                                                <?php if ($row['home_phone2'] != null ) : ?>
                                                    <div class="form-group">
                                                        <div class="input-phone homep col-xs-offset-3">
                                                            <input type="text" class="form-control"  name="home_phone0" value="<?php $home_phone2 = set_value('home_phone2'); echo $home_phone2 = !empty($home_phone2) ? $home_phone2 : $row['home_phone2']; ?>">
                                                            <span class="glyphicon glyphicon-phone-alt"></span>
                                                        </div>
                                                        <div class="col-xs-1">
                                                            <span onclick="addHomePhone()" class="add-mobile-phone css-add-phone"></span>
                                                            <span class="delete-home-phone css-delete-phone"></span>
                                                        </div>
                                                    </div>
                                                <?php endif ?>
                                                <input type="hidden" class="add-home-phone-before">
                                                <div class="form-group all-mobile-phone">
                                                    <div class="col-xs-offset-3 input-phone mobilep">
                                                        <input type="text" class="form-control"  name="mobile_phone0" value="<?php $mobile_phone0 = set_value('mobile_phone0'); echo $mobile_phone0 = !empty($mobile_phone0) ? $mobile_phone0 : $row['mobile_phone0']; ?>">
                                                        <span class="glyphicon glyphicon-phone"></span>
                                                    </div>
                                                    <div class="col-xs-1">
                                                        <span class="add-mobile-phone"></span>
                                                    </div>
                                                </div>
                                                <?php if ($row['mobile_phone1'] != null ) : ?>
                                                    <div class="form-group">
                                                        <div class="input-phone mobilep col-xs-offset-3">
                                                            <input type="text" class="form-control"  name="mobile_phone1" value="<?php $mobile_phone1 = set_value('mobile_phone1'); echo $mobile_phone1 = !empty($mobile_phone1) ? $mobile_phone1 : $row['mobile_phone1']; ?>">
                                                            <span class="glyphicon glyphicon-phone"></span>
                                                        </div>
                                                        <div class="col-xs-1">
                                                            <span onclick="addMobilePhone()" class="add-mobile-phone css-add-phone" ></span>
                                                            <span class="delete-mobile-phone css-delete-phone"></span>
                                                        </div>
                                                    </div>
                                                <?php endif ?>
                                                <?php if ($row['mobile_phone2'] != null ) : ?>
                                                    <div class="form-group">
                                                        <div class="input-phone mobilep col-xs-offset-3">
                                                            <input type="text" class="form-control"  name="mobile_phone2" value="<?php $mobile_phone2 = set_value('mobile_phone2'); echo $mobile_phone2 = !empty($mobile_phone2) ? $mobile_phone2 : $row['mobile_phone2']; ?>">
                                                            <span class="glyphicon glyphicon-phone"></span>
                                                        </div>
                                                        <div class="col-xs-1">
                                                            <span onclick="addMobilePhone()" class="add-mobile-phone css-add-phone" ></span>
                                                            <span class="delete-mobile-phone css-delete-phone"></span>
                                                        </div>
                                                    </div>
                                                <?php endif ?>
                                                <input type="hidden" class="add-mobile-phone-before">

                                                <div class="form-group all-email">
                                                    <label for="full-desc" class="col-xs-3 control-label">Email:</label>
                                                    <div class="col-xs-8 email-input">
                                                        <input type="text" class="form-control" id="email" name="email1" value="<?php $email1 = set_value('email1'); echo $email1 = !empty($email1) ? $email1 : $row['email1']; ?>">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-xs-offset-5 col-xs-3">
                                                        <button type="submit" class="btn btn-profile">Сохранить</button>
                                                    </div>
                                                </div>

                                        </form>
                                    </div>

                                </div>
								</div>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>
<?php endforeach; ?>

<script>
    function getAllRegions(){
        var country = document.getElementById('id_country').value;
        var region = document.getElementById('id_region');
        region.innerHTML = '<option></option>';
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                var jsonText = JSON.parse(xhttp.responseText);
                var jsonText2;
                for (key in jsonText) {
                    if (jsonText.hasOwnProperty(key)) {
                        jsonText2 = jsonText[key];
                        var div = document.createElement('option');
                        div.setAttribute('value', jsonText2['id_region']);
                        div.innerHTML = jsonText2['name_region'];
                        region.appendChild(div);
                    }
                }
            }
        };
        xhttp.open("GET", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/' ?>index.php/country/getAllRegions/"+country);
        xhttp.send();
    }

    function getAllCity(){
        var region = document.getElementById('id_region').value;
        var city = document.getElementById('id_city');
        city.innerHTML = '<option></option>';
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                var jsonText = JSON.parse(xhttp.responseText);
                var jsonText2;
                for (key in jsonText) {
                    if (jsonText.hasOwnProperty(key)) {
                        jsonText2 = jsonText[key];
                        var div = document.createElement('option');
                        div.setAttribute('value', jsonText2['id_city']);
                        div.innerHTML = jsonText2['name_city'];
                        city.appendChild(div);
                    }
                }
            }
        };
        xhttp.open("GET", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/' ?>index.php/country/getAllCity/"+region);
        xhttp.send();
    }


</script>
