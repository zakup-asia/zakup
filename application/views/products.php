<section class="two-col-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="two-col">
                    <div class="profile-menu open hidden-sm hidden-xs" id="profile-menu">
                        <ul>
                            <li ><a id="orders" href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/cart/getOrdersOutgoing">Заказы</a></li>
                            <?php if($this->session->userdata('goal') == TRUE ) : ?>
                                <li><a id="productsIcon" href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/products/myProducts">Товары</a></li>
                                <li><a id="stock" href="#">Акции</a></li>
                                <ul>
                                </ul>
                                <li><a id="news" href="#">Новости</a></li>
                            <?php endif; ?>
                            <?php if($this->session->userdata('goal') == FALSE ) :?>
                                <ul>
                                </ul>
                            <?php endif; ?>
                            <li><a id="statistics" href="#">Статистика</a></li>
                            <li><a id="favorites" href="#">Избранное</a></li>
                        </ul>
                    </div><!-- /.profile-menu -->

                    <div class="megamenu-left close hidden-sm hidden-xs" id="megamenu">

                    </div><!-- /.megamenu-left -->

                    <div class="content profile">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="profile-content">
                                    <ul class="breadcrumb">
                                        <li class="active">Мои товары 42 (12)</li>
                                        <div class="add-product-link"><a href="#">Добавить товар</a></div>
                                    </ul>
                                    <div class="profile-products">
                                        <div class="profile-product">
                                            <div class="product-name">
                                                <p>Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</p>
                                                <span class="country">Россия</span>
                                                <span class="status">на исходе</span>
                                                <div class="product-price">286970 Тг.</div>
                                            </div>
                                            <ul class="product-actions">
                                                <li><a href="#" class="delete"></a></li>
                                                <li><a href="#" class="edit"></a></li>
                                                <li><input type="checkbox"></li>
                                                <li>
                                                    <a type="button" class="more dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#">Action</a></li>
                                                        <li><a href="#">Another action</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="profile-product not-available">
                                            <div class="product-name">
                                                <p>Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</p>
                                                <span class="country">Россия</span>
                                                <span class="status">нет в наличии</span>
                                                <div class="product-price">286970 Тг.</div>
                                            </div>
                                            <ul class="product-actions">
                                                <li><a href="#" class="delete"></a></li>
                                                <li><a href="#" class="edit"></a></li>
                                                <li><input type="checkbox"></li>
                                                <li>
                                                    <a type="button" class="more dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#">Action</a></li>
                                                        <li><a href="#">Another action</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="profile-product">
                                            <div class="product-name">
                                                <p>Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</p>
                                                <span class="country">Россия</span>
                                                <span class="status">на исходе</span>
                                                <div class="product-price">286970 Тг.</div>
                                            </div>

                                            <ul class="product-actions">
                                                <li><a href="#" class="delete"></a></li>
                                                <li><a href="#" class="edit"></a></li>
                                                <li><input type="checkbox"></li>
                                                <li>
                                                    <a type="button" class="more dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#">Action</a></li>
                                                        <li><a href="#">Another action</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="profile-product not-available">
                                            <div class="product-name">
                                                <p>Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</p>
                                                <span class="country">Россия</span>
                                                <span class="status">нет в наличии</span>
                                                <div class="product-price">286970 Тг.</div>
                                            </div>
                                            <ul class="product-actions">
                                                <li><a href="#" class="delete"></a></li>
                                                <li><a href="#" class="edit"></a></li>
                                                <li><input type="checkbox"></li>
                                                <li>
                                                    <a type="button" class="more dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#">Action</a></li>
                                                        <li><a href="#">Another action</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="profile-product">
                                            <div class="product-name">
                                                <p>Сахар-рафинад ГОСТ Чайкофский, 1кг*20шт</p>
                                                <span class="country">Россия</span>
                                                <span class="status">на исходе</span>
                                                <div class="product-price">286970 Тг.</div>
                                            </div>

                                            <ul class="product-actions">
                                                <li><a href="#" class="delete"></a></li>
                                                <li><a href="#" class="edit"></a></li>
                                                <li><input type="checkbox"></li>
                                                <li>
                                                    <a type="button" class="more dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#">Action</a></li>
                                                        <li><a href="#">Another action</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>