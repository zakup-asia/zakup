<?php foreach ($company->result_array() as $row): ?>
				<div class="content profile">
					<div class="row">
						<div class="col-xs-12">
							<div class="profile-content">
                                <div class="profile-content tabs-wrapper order">
                                    <div class="row css-tabs-wrapper">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/main/">
                                                <div class="col-md-2 col-sm-2 col-xs-2 text-center active css-top-menu">
                                                    Основные
                                                </div>
                                            </a>
                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/contactinformation/">
                                                <div class="col-md-2 col-sm-2 col-xs-2 text-center css-top-menu">
                                                    Контакты
                                                </div>
                                            </a>
											<?php if($this->session->userdata('goal') == TRUE ) : ?>
												<a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/conditions/">
													<div class="col-md-3 col-sm-3 col-xs-3 text-center css-top-menu">
														Условия заказа
													</div>
												</a>
											<?php endif;?>
                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/requisites/">
                                                <div class="col-md-2 col-sm-2 col-xs-2 text-center css-top-menu">
                                                    Реквизиты
                                                </div>
                                            </a>
                                            <a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/user/loginAndPassword/">
                                                <div class="col-md-3 col-sm-3 col-xs-3 text-center css-top-menu">
                                                    Логин и пароль
                                                </div>
                                            </a>
                                        </div>
                                    </div>
   
								<div class="profile-content">
                                    <div class="profile-form">
                                        <?php echo form_open_multipart('http://'.$_SERVER['SERVER_NAME'].'/user/main/', array('class' => "form-horizontal", "name" => "upload")) ?>
                                        <?php //if($company!=null):?>
                                        <?php //endif; ?>
                                            <div class="form-group">
                                                <label for="name" class="col-xs-3 control-label">Наименование:*</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="name" name="name" value ="<?php $name = set_value('name'); echo $id_user = !empty($name) ? $name : $row['name']; ?>">

													<span id="namecharsLeft" class="grey"></span>&nbsp;<span class="grey">знаков осталось</span>
													<?php echo form_error('name', '<div class="css-error">', '</div>'); ?>
                                                </div>
                                            </div>
									        <div class="form-group">
                                                <label for="name" class="col-xs-3 control-label">Урл компании:*</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="name_translit" name="name_translit" value ="<?php $name_translit = set_value('name_translit'); echo $id_user = !empty($name_translit) ? $name_translit : $row['name_translit']; ?>">
													<span id="namecharsLeft" class="grey"></span>&nbsp;<span class="grey">знаков осталось</span>
													<div id="error_url">

													</div>
                                                </div>
                                            </div>
                                            <div class="form-group" id="is_supplier">
                                                <div class="col-xs-offset-3 col-xs-8">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" value="1" id="is_supplier1" name="is_supplier" <?php if($row['is_supplier'] == 1 ) echo 'checked'?> > Я поставщик
                                                        </label>
														<span class="tooltip2 question" title="Осуществляет поставку продукции потребителю"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" id="is_seller">
                                                <div class="col-xs-offset-3 col-xs-8">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" value="1" id="is_seller1" name="is_seller" <?php if($row['is_seller'] == 1 ) echo 'checked'?> > Я дистрибьютор
                                                        </label>
														<span class="tooltip2 question" title="Осуществляет поставки товара от лица компании-производителя"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" id="is_manufacturer">
                                                <div class="col-xs-offset-3 col-xs-8">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" value="1" id="is_manufacturer1" name="is_manufacturer" <?php if($row['is_manufacturer'] == 1 ) echo 'checked'?> > Я производитель
                                                        </label>
														<span class="tooltip2 question" title="Организация, занимающаяся изготовлением какого-либо товара/продукции"></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" id="is_importer">
                                                <div class="col-xs-offset-3 col-xs-8">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" value="1" id="is_importer1" name="is_importer" <?php if($row['is_importer'] == 1 ) echo 'checked'?> > Я импортёр
                                                        </label>
														<span class="tooltip2 question" title="Занимается ввозом товара из-за границы с последующей его реализацией"></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="short-desc" class="col-xs-3 control-label">Краткое описание:*</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" id="short-desc" name="short_description" value="<?php $short_description = set_value('short_description'); echo $short_description = !empty($short_description) ? $short_description : $row['short_description']; ?>">
													<span id="shortcharsLeft" class="grey"></span>&nbsp;<span class="grey">знаков осталось</span>
													<?php echo form_error('short_description', '<div class="css-error">', '</div>'); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="full-desc" class="col-xs-3 control-label">Полное описание:*</label>
                                                <div class="col-xs-8">
                                                    <textarea class="form-control" id="full-desc" rows="3" name="description"><?php $description = set_value('description'); echo $description = !empty($description) ? $description : $row['description']; ?></textarea>					
                                                    <?php echo form_error('description', '<div class="css-error">', '</div>'); ?>
                                                </div>
                                            </div>
                                            <div class="form-group horiz-line">
                                                <label class="col-xs-3 control-label">Логотип <span class="grey">(200px*200px)</span>:</label>
                                                <div class="col-xs-8">
                                                    <div class="logo-img" id="main">
                                                        <?php if ($row['path'] != null) : ?>
                                                            <img class='css-pointer' onclick="$('#file').click()" src="/../../images/mini/<?php $path = set_value('path'); echo $path = !empty($path) ? $path : $row['path']; ?>" alt="">
                                                            <a onclick="deleteImage()"><span class="glyphicon glyphicon-remove"></span></a>
                                                            <input type="hidden" id="path" name="path" value="<?php $path = set_value('path'); echo $path = !empty($path) ? $path : $row['path']; ?>">
                                                        <?php endif; ?>
                                                        <?php if ($row['path'] == null) : ?>
                                                            <img class='css-pointer' onclick="$('#file').click()" src="/../../images/download-image.png" alt="">
                                                        <?php endif; ?>
                                                    </div>
                                                    <input class="css-hide" type="file" id="file" name="userfile" size="20" onchange="loadImage()" />
                                                    <br>
													<!-- Старая загрузка изображения через кнопку
														<div class="form-group">
															<div class="col-xs-offset-1 col-xs-3">
																<input type="button" class="btn btn-profile" onclick="loadImage()" value="Загрузить">
															</div>
														</div>
													-->
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="short-desc" class="col-lg-3 col-sm-5 control-label">Время работы:</label>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-5 col-xs-6 col-lg-offset-1">
                                                    <div class="work-day">
                                                        <span>ПН</span>
                                                        <input type="text" class="time-work form-control" name="workday1from" value="<?php $workday1from = set_value('workday1from'); echo $workday1from = !empty($workday1from) ? $workday1from : $row['workday1from']; ?>">
                                                        <span> - </span>
                                                        <input type="text" class="time-work form-control" name="workday1to" value="<?php $workday1to = set_value('workday1to'); echo $workday1to = !empty($workday1to) ? $workday1to : $row['workday1to']; ?>">
                                                        <span class="check-day"></span>
                                                    </div>
                                                    <div class="work-day">
                                                        <span>ВТ</span>
                                                        <input type="text" class="time-work form-control" name="workday2from" value="<?php $workday2from = set_value('workday2from'); echo $workday2from = !empty($workday2from) ? $workday2from : $row['workday2from']; ?>">
                                                        <span> - </span>
                                                        <input type="text" class="time-work form-control" name="workday2to" value="<?php $workday2to = set_value('workday2to'); echo $workday2to = !empty($workday2to) ? $workday2to : $row['workday2to']; ?>">
                                                        <span class="check-day"></span>
                                                    </div>
                                                    <div class="work-day">
                                                        <span>СР</span>
                                                        <input type="text" class="time-work form-control" name="workday3from" value="<?php $workday3from = set_value('workday3from'); echo $workday3from = !empty($workday3from) ? $workday3from : $row['workday3from']; ?>">
                                                        <span> - </span>
                                                        <input type="text" class="time-work form-control" name="workday3to" value="<?php $workday3to = set_value('workday3to'); echo $workday3to = !empty($workday3to) ? $workday3to : $row['workday3to']; ?>">
                                                        <span class="check-day"></span>
                                                    </div>
                                                    <div class="work-day">
                                                        <span>ЧТ</span>
                                                        <input type="text" class="time-work form-control" name="workday4from" value="<?php $workday4from = set_value('workday4from'); echo $workday4from = !empty($workday4from) ? $workday4from : $row['workday4from']; ?>">
                                                        <span> - </span>
                                                        <input type="text" class="time-work form-control" name="workday4to" value="<?php $workday4to = set_value('workday4to'); echo $workday4to = !empty($workday4to) ? $workday4to : $row['workday4to']; ?>">
                                                        <span class="check-day"></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-5 col-xs-6">
                                                    <div class="work-day">
                                                        <span>ПТ</span>
                                                        <input type="text" class="time-work form-control" name="workday5from" value="<?php $workday5from = set_value('workday5from'); echo $workday5from = !empty($workday5from) ? $workday5from : $row['workday5from']; ?>">
                                                        <span> - </span>
                                                        <input type="text" class="time-work form-control" name="workday5to" value="<?php $workday5to = set_value('workday5to'); echo $workday5to = !empty($workday5to) ? $workday5to : $row['workday5to']; ?>">
                                                        <span class="check-day"></span>
                                                    </div>
                                                    <div class="work-day">
                                                        <span>СБ</span>
                                                        <input type="text" class="time-work form-control" name="workday6from" value="<?php $workday6from = set_value('workday6from'); echo $workday6from = !empty($workday6from) ? $workday6from : $row['workday6from']; ?>">
                                                        <span> - </span>
                                                        <input type="text" class="time-work form-control" name="workday6to" value="<?php $workday6to = set_value('workday6to'); echo $workday6to = !empty($workday6to) ? $workday6to : $row['workday6to']; ?>">
                                                        <span class="check-day"></span>
                                                    </div>
                                                    <div class="work-day">
                                                        <span>ВС</span>
                                                        <input type="text" class="time-work form-control" name="workday7from" value="<?php $workday7from = set_value('workday7from'); echo $workday7from = !empty($workday7from) ? $workday7from : $row['workday7from']; ?>">
                                                        <span> - </span>
                                                        <input type="text" class="time-work form-control" name="workday7to" value="<?php $workday7to = set_value('workday7to'); echo $workday7to = !empty($workday7to) ? $workday7to : $row['workday7to']; ?>">
                                                        <span class="check-day"></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-xs-offset-5 col-xs-3">
                                                    <button type="submit" class="btn btn-profile">Сохранить</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>
							</div>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                    </div><!-- /.content-->
                </div><!-- /.two-col -->
            </div>
        </div>
    </div><!-- /.container -->
</section>
<?php endforeach; ?>

<script>
    function loadImage(){
		var file = document.getElementById('file');
        var formData = new FormData(document.forms.upload);
        var response = document.getElementById('info');
		var file2 = '"#file"';
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                var jsonText = JSON.parse(xhttp.responseText);
                for (key in jsonText) {
                    console.log(key);
                    main.innerHTML = "<img class='css-pointer' onclick='$("+file2+").click()' src='/../../images/mini/"+jsonText['file_name']+"' alt=''> <a onclick='deleteImage()'><span class='glyphicon glyphicon-remove css-pointer'></span></a> <input type='hidden' id='path' name='path' value='"+jsonText['file_name']+"'>";
                }
				file.value = '';
            }
        };
        xhttp.open("POST", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/' ?>index.php/image/do_upload/");
        xhttp.send(formData);
    }

    function deleteImage(){
        var path = document.getElementById('path').value;
        xhttp = new XMLHttpRequest();
		var file2 = "'#file'";
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                main.innerHTML = '<img class="css-pointer" onclick="$('+file2+').click()" src="/../../images/download-image.png" alt="">';
            }
        };
        xhttp.open("GET", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/' ?>index.php/image/deleteImage/"+path);
        xhttp.send();
    }

    function CheckList(){
        if(document.getElementById('type_of').value == '1' && document.getElementById('type_of').value != null){
            document.getElementById('is_supplier').setAttribute('style', 'display:block');
            document.getElementById('is_seller').setAttribute('style', 'display:block');
            document.getElementById('is_manufacturer').setAttribute('style', 'display:block');
            document.getElementById('is_importer').setAttribute('style', 'display:block');
        } else {
            document.getElementById('is_supplier').setAttribute('style', 'display:none');
            document.getElementById('is_seller').setAttribute('style', 'display:none');
            document.getElementById('is_manufacturer').setAttribute('style', 'display:none');
            document.getElementById('is_importer').setAttribute('style', 'display:none');

            document.getElementById('is_supplier1').setAttribute('value', '0');
            document.getElementById('is_supplier1').removeAttribute('checked');
            document.getElementById('is_seller1').setAttribute('value', '0');
            document.getElementById('is_seller1').removeAttribute('checked');
            document.getElementById('is_manufacturer1').setAttribute('value', '0');
            document.getElementById('is_manufacturer1').removeAttribute('checked');
            document.getElementById('is_importer1').setAttribute('value', '0');
            document.getElementById('is_importer1').removeAttribute('checked');
        }
    }

    $(document).ready(function () {
        CheckList();
    });
/*
    function checkNameCompany(){
        //Сюда передаём ид компании и имя, которое хотим использовать.
        var name = document.getElementById("name_translit").value;
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {

            }
        };
        xhttp.open("GET", "<?php echo 'http://'.$_SERVER['SERVER_NAME']?>/index.php/user/checkName/"+name);
        xhttp.send();
    }*/
</script>
