            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <ul class="footer-nav">
                                <li><a href="#">О сервисе</a></li>
                                <li><a href="#">Вопрос-ответ</a></li>
                                <li><a href="#">Обратная связь</a></li>
                                <li><a href="#">Контакты</a></li>
                                <li><a href="#">IOS</a></li>
                                <li><a href="#">Android</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <div class="footer-right">
                                <span>Zakup.asia © 2016</span>
                                <div class="social-icons">
                                    <a href="#"><img src="/../../images/vk-icon.png" alt=""></a>
                                    <a href="#"><img src="/../../images/facebook-icon.png" alt=""></a>
                                    <a href="#"><img src="/../../images/google-icon.png" alt=""></a>
                                    <a href="#"><img src="/../../images/twitter-icon.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
		<input type="hidden" id="SERVER_NAME" value="<?php echo $_SERVER['SERVER_NAME']?>">
            <script src="/../../js/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="/../../js/bootstrap.min.js"></script>
        <script src="/../../js/scripts2.js"></script>
        <script src="/../../js/scripts.js"></script>
        <script src="/../../js/slideout.min.js"></script>
        <script src="/../../js/jquery.maskedinput.min.js"></script>
        <script src="/../../js/nikiScripts.js"></script>
    </body>
</html>