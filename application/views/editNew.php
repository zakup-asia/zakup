                    <div class="content profile">
                        <div class="row">
                            <div class="col-sm-9">
                                <div class="profile-content">
                                    <ul class="breadcrumb">
                                        <li><a href="http://<?php echo $_SERVER['SERVER_NAME']?>/StockAndNews/getMyNews">Мои новости </a></li>
                                        <li class="active">Редактировать новость</li>
                                    </ul>
                                    <div class="profile-form">
                                        <?php echo form_open_multipart('http://'.$_SERVER['SERVER_NAME'].'/stockAndNews/editNew/', array('class' => "form-horizontal", 'name' => 'upload')) ?>
                                            <?php foreach($new->result_array() as $row):?>
                                                <input type="hidden" name="id_new" value="<?php echo $row['id_new']?>">
                                                <input type="hidden" name="id_company" value="<?php echo $this->session->userdata('id_company')?>">
                                                <div class="form-group">
                                                    <label for="name" class="col-xs-3 control-label">Название новости:*</label>
                                                    <div class="col-xs-8">
                                                        <input type="text" class="form-control" id="name_new" name="name_new" value="<?php $name_new = set_value('name_new'); echo $name_new = !empty($name_new) ? $name_new : $row['name_new']; ?>">
                                                        <?php echo form_error('name_new', '<div class="css-error">', '</div>'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="name" class="col-xs-3 control-label">Описание новости:*</label>
                                                    <div class="col-xs-8">
                                                        <textarea class="form-control" id="description_new" name="description_new"><?php $description_new = set_value('description_new'); echo $description_new = !empty($description_new) ? $description_new : $row['description_new']; ?></textarea>
                                                        <?php echo form_error('description_new', '<div class="css-error">', '</div>'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-xs-3 control-label">Фотография новости</label>
                                                    <div class="col-xs-8">
                                                        <div class="logo-img" id="main">
                                                            <?php if ($row['path'] != null) : ?>
                                                                <img class='css-pointer' onclick="$('#file').click()" src="/../../images/mini/<?php $path = set_value('path'); echo $path = !empty($path) ? $path : $row['path']; ?>" alt="">
                                                                <a onclick="deleteImage()"><span class="glyphicon glyphicon-remove"></span></a>
                                                                <input type="hidden" id="path" name="path" value="<?php $path = set_value('path'); echo $path = !empty($path) ? $path : $row['path']; ?>">
                                                            <?php endif; ?>
                                                            <?php if ($row['path'] == null) : ?>
                                                                <img class='css-pointer' onclick="$('#file').click()" src="/../../images/download-image.png" alt="">
                                                            <?php endif; ?>
                                                        </div>
                                                        <input class="css-hide" type="file" id="file" name="userfile" size="20" onchange="loadImage()"/>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="form-group">
                                                    <div class="col-xs-offset-4 col-xs-3">
                                                        <input type="button" class="btn btn-profile" onclick="loadImage()" value="Загрузить">
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="form-group">
                                                    <div class="col-xs-offset-4 col-xs-3">
                                                        <button type="submit" class="btn btn-profile">Сохранить</button>
                                                    </div>
                                                </div>
                                            <?php endforeach;?>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
</section>

<script>
    function loadImage(){
		var file = document.getElementById('file');
		var file2 = '"#file"';
        var formData = new FormData(document.forms.upload);
        var response = document.getElementById('info');
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                var jsonText = JSON.parse(xhttp.responseText);
                for (key in jsonText) {
                    console.log(key);
                    main.innerHTML = "<img class='css-pointer' onclick='$("+file2+").click()' src='/../../images/mini/"+jsonText['file_name']+"' alt=''> <a onclick='deleteImage()'><span class='glyphicon glyphicon-remove css-pointer'></span></a> <input type='hidden' id='path'name='path' value='"+jsonText['file_name']+"'>";
                }
				file.value = '';
            }
        };
        xhttp.open("POST", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/' ?>index.php/image/do_upload/");
        xhttp.send(formData);
    }

    function deleteImage(){
        var path = document.getElementById('path').value;
		var file2 = "'#file'";
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState==4 && xhttp.status == 200) {
                main.innerHTML = '<img class="css-pointer" onclick="$('+file2+').click()" src="/../../images/download-image.png" alt="">';
            }
        };
        xhttp.open("GET", "<?php echo 'http://'.$_SERVER['SERVER_NAME'].'/' ?>index.php/image/deleteImage/"+path);
        xhttp.send();
    }
</script>
