<?php
class DB{
	//database configuration
	private $dbHost     = "localhost";
	private $dbUsername = "p-140_admin";
	private $dbPassword = "bYhd32~3";
	private $dbName     = "p-1400_asia";
	private $imgTbl     = 'category';
	
	function __construct(){
		if(!isset($this->db)){
            // Connect to the database
            $conn = new mysqli($this->dbHost, $this->dbUsername, $this->dbPassword, $this->dbName);
            if($conn->connect_error){
                die("Failed to connect with MySQL: " . $conn->connect_error);
            }else{
                $this->db = $conn;
            }
        }
	}
	
	function getRows(){
		$query = $this->db->query("SELECT * FROM ".$this->imgTbl." ORDER BY `order` ASC");
		if($query->num_rows > 0){
			while($row = $query->fetch_assoc()){
				$result[] = $row;
			}
		}else{
			$result = FALSE;
		}
		return $result;
	}
	
	function getRowsByParent($parent_id){
		$query = $this->db->query("SELECT * FROM ".$this->imgTbl." WHERE id_section_one=".$parent_id." ORDER BY `order` ASC");
		if($query->num_rows > 0){
			while($row = $query->fetch_assoc()){
				$result[] = $row;
			}
		}else{
			$result = FALSE;
		}
		return $result;
	}	
	
	function getSections(){
		$query = $this->db->query("SELECT * FROM section_one ORDER BY `id_section_one` ASC");
		if($query->num_rows > 0){
			while($row = $query->fetch_assoc()){
				$result[] = $row;
			}
		}else{
			$result = FALSE;
		}
		return $result;
	}	
	
	function getCategories($parent_id){
		$query = $this->db->query("SELECT * FROM subcategory WHERE id_category=".$parent_id." ORDER BY `id_subcategory` ASC");
		if($query->num_rows > 0){
			while($row = $query->fetch_assoc()){
				$result[] = $row;
			}
		}else{
			$result = FALSE;
		}
		return $result;
	}	
	
	function updateOrder($id_array){
		$count = 1;
		foreach ($id_array as $id){
			$update = $this->db->query("UPDATE ".$this->imgTbl." SET `order` = $count WHERE id_category = $id");
			$count ++;	
		}
		return TRUE;
		
	}
}
?>