<?php
namespace MyApp;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Chat implements MessageComponentInterface {

    protected $clients;

    //Соединение с базой данных
    private $mysqli;
    //Настройки для базы
    private $hostname = 'localhost';
    private $username = 'root';
    private $password = '';
    private $dbName   = 'p-1400_asia';

    //Создаём хранилище для клиентов
    //Создаём соединение с базой данных
    public function __construct() {
        $this->clients = new \SplObjectStorage;
    }

    //Событие открытия соединения
    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);
        echo "New connection! ({$conn->resourceId})\n";
    }

    //тут мы должны разбирать сообщение, определять его тип и в зависимости от типа сообщения отправлять соответсвующие данные
    //Событие получение сообщения
    public function onMessage(ConnectionInterface $from, $msg) {
        $obj = json_decode($msg, true);
        foreach($obj as $row){
            //Получаем информацию о пользователе после соединения.
            if ($row["type"] == "conn"){
                //Если ты хочешь использовать запросы, тогда тебе нужно делать коннекшн к базе и т.д.
                //Запрос выборка
                //TODO количество новых новостей, избранные товары, акции. Необходимо добавить новые таблицы, которые будут отвечать за
                //просмотры.
                $this->mysqli = mysqli_connect($this->hostname, $this->username, $this->password, $this->dbName);
                if ($this->mysqli->connect_errno) {
                    printf("Не удалось подключиться: %s\n", $this->mysqli->connect_error);
                    exit();
                }
                $sql = "SELECT *,(SELECT COUNT(*) FROM cart NATURAL JOIN orders NATURAL JOIN product LEFT OUTER JOIN
                       company USING (id_company) LEFT OUTER JOIN contact_information USING (id_company) LEFT OUTER JOIN country USING
                       (id_country) LEFT OUTER JOIN currency USING (id_currency) LEFT OUTER JOIN `user` USING (id_user) WHERE id_user =
                       ".$row["connUserId"]." AND cart.id_order = orders.id_order AND cart.order_status = 'Не просмотрен') AS
                       notViewOrders, (SELECT COUNT(*) FROM `stock` LEFT OUTER JOIN is_viewed_stock USING (id_stock) WHERE
                       (id_user != NULL AND id_user != ".$row["connUserId"].") OR id_user is NULL) AS notViewStocks, (SELECT COUNT(*)
                       FROM `news` LEFT OUTER JOIN is_viewed_new USING (id_new) WHERE (id_user != NULL AND id_user != ".$row["connUserId"].")
                       OR id_user is NULL) AS notViewNews, (SELECT COUNT(*) FROM product LEFT OUTER JOIN company USING (id_company)
                       LEFT OUTER JOIN `user` USING (id_user) WHERE id_user = ".$row["connUserId"]." AND deleted = FALSE) AS productCount
                       FROM `user` LEFT OUTER JOIN company USING (id_user) WHERE id_user =".$row["connUserId"];
                $result = mysqli_query($this->mysqli, $sql);
                $objects = array();
                $object = array();
                while ($rw = mysqli_fetch_assoc($result)) {
                    $object['type'] = 'checkAfterConn';
                    if (isset($rw['show_company'])) $object['show_company'] = $rw['show_company'];
                    if (isset($rw['notViewOrders'])) $object['notViewOrders'] = $rw['notViewOrders'];
                    if ($row["connUserId"] != null){
                        $object['notViewStocks'] = $rw['notViewStocks'];
                        $object['notViewNews'] = $rw['notViewNews'];
                        $object['productCount'] = $rw['productCount'];
                    }
                    $object['id'] = $row['connUserId'];
                    array_push($objects,$object);
                }
                //Тут мы должны проверить тот ли это юзер, который нам отправлял и отправляем ему сообщение.
                //echo json_encode($objects);
                foreach($this->clients as $client){
                    if ($from == $client){
                        $client->send(json_encode($objects));
                    }
                }
            }
            //Событие установка компании в активную/пассивную часть
            if ($row["type"] == "check"){
                $obj = json_encode($obj);
                echo $obj;
                foreach($this->clients as $client){
                    $client->send($msg);
                }
            }
            if ($row["type"] == "incrementOrders"){
                $obj = json_encode($obj);
                echo $obj;
                foreach($this->clients as $client){
                    $client->send($msg);
                }
            }
            if ($row['type'] == "incNVNews"){
                $obj = json_encode($obj);
                echo $obj;
                foreach($this->clients as $client){
                    $client->send($msg);
                }
            }
            if ($row['type'] == "decNVNews"){
                $obj = json_encode($obj);
                echo $obj;
                foreach($this->clients as $client){
                    $client->send($msg);
                }
            }
        }
        /*
        $numRecv = count($this->clients) - 1;
        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');
        foreach ($this->clients as $client) {
            if ($from !== $client) {
                // The sender is not the receiver, send to each client connected
                $client->send($msg);
            }
        }
        */
    }

    //Событие закрытия соединения
    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);
        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    //Событие ошибки
    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }
}
