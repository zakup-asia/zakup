/**
 * Created by DiZi on 19.01.2017.
 */
    var conn = new WebSocket('ws://localhost:8080');
    var count = null;
    var connUserId = null;

    //При открытии коннекта, мы должны отправлять специальный запрос на выборку данных для данного конекшена.
    //Открываем соединение
    conn.onopen = function(e) {
        connUserId = $("#connUserId").attr("value");
        console.log("Connection established!");
        //Json объект [{"атрибут":"значение"}]
        if (connUserId != null){
            conn.send('[{"type":"conn","connUserId":"'+connUserId+'"}]');
        }
    };

    //Получаем сообщение
    conn.onmessage = function(e) {
        console.log(e.data);
        //Тут мы должны обрабатывать сообщение в зависимости от типа сообщения
        //Получаем ид нашего пользователя
        var connUserId =  $('#connUserId').attr('value');
        //превращаем в джсон полученный текст
        var data = e.data.toString();
        var stringData = JSON.parse(data);
        for (key in stringData){
            data = stringData[key];
            if (data['type'] == 'check'){
                if(data['show_company'] == '1' && data['id'] == connUserId) $('#companyInTheModeration').attr('style','display:none');
                if (data['show_company'] == '0' && data['id'] == connUserId) $('#companyInTheModeration').attr('style','display:block');
            }
            if (data['type'] == 'checkAfterConn'){
                if (data['show_company'] == '1' && data['id'] == connUserId) $('#companyInTheModeration').attr('style','display:none');
                if (data['show_company'] == '0' && data['id'] == connUserId) $('#companyInTheModeration').attr('style','display:block');
                $("#vOrders").text(data['notViewOrders']).attr('class', 'css-icon-message');
                $("#vStocks").text(data['notViewStocks']).attr('class', 'css-icon-message-2');
                $("#vNews").text(data['notViewNews']).attr('class', 'css-icon-message-2');
                $("#vProducts").text(data['productCount']).attr('class', 'css-icon-message-2');
            }
            if(data['type'] == 'incrementOrders'){
                count = document.getElementById('vOrders').innerHTML;
                console.log(count);
                count = parseInt(count);
                count++;
                console.log(count);
                $("#vOrders").text(count);
            }
            if(data['type'] == 'decrementOrders'){
                count = $("#vOrders").text();
                console.log(count);
                count = parseInt(count);
                count--;
                console.log(count);
                $("#vOrders").text(count);
            }
            if(data['type'] == 'incNVNews' && data['id'] == connUserId){
                count = $("#vNews").text();
                console.log(count);
                count = parseInt(count);
                count++;
                console.log(count);
                $("#vNews").text(count);
            }
            if(data['type'] == 'decNVNews' && data['id'] == connUserId){
                count = $("#vNews").text();
                console.log(count);
                count = parseInt(count);
                count--;
                console.log(count);
                $("#vNews").text(count);
            }
            if(data['type'] == 'incNVStocks' && data['id'] == connUserId){
                count = $("#vStocks").text();
                console.log(count);
                count = parseInt(count);
                count++;
                console.log(count);
                $("#vStocks").text(count);
            }
            if(data['type'] == 'decNVStocks' && data['id'] == connUserId){
                count = $("#vStocks").text();
                console.log(count);
                count = parseInt(count);
                count--;
                console.log(count);
                $("#vStocks").text(count);
            }
        }
    };

    //Ошибка
    conn.onerror = function(e){
        console.log(e.data);
    };

    //Показываем компанию
    function setActive(id){
        var jsonMessage = '[{"type":"check", "id":'+id+', "show_company":"1"}]';
        conn.send(jsonMessage);
    }

    //Скрываем компанию
    function setNonActive(id){
        var jsonMessage = '[{"type":"check", "id":'+id+', "show_company":"0"}]';
        conn.send(jsonMessage);
    }

    //Отправляем фирмам весть о том, что заказов стало больше
    function setSuppliers(){
        var suppliers = document.getElementsByClassName('cart-provider');
        for(var i=0; i<suppliers.length; i++){
            var jsonMessage = '[{"type":"incrementOrders", "id":"'+suppliers[i].getAttribute('idUserCompany')+'"}]';
            conn.send(jsonMessage);
        }
    }

    //Событие, просмотр заказа
    function unsetSuppliers(){
        var suppliers = document.getElementsByClassName('cart-provider');
        for(var i=0; i<suppliers.length; i++){
            var jsonMessage = '[{"type":"decrementOrders", "id":"'+suppliers[i].getAttribute('idUserCompany')+'"}]';
            conn.send(jsonMessage);
        }
    }

    //Событие добавление новости
    function incNotViewedNews(){
        var jsonMessage = '[{"type":"incNVNews", "id":'+connUserId+'}]';
        conn.send(jsonMessage);
    }

    //У нас нет страницы новости и из-за этого нельзя сделать так, чтобы мы её просмотрели.
    //Нужно разделить по событиям удаление новости и её просмотр. Удаляется она для всех, а просмотрел её всего один человек
    //Соответственно просмотр использует ид, а удаление нет.
    //Событие просмотр новости/удаление новости
    function decNotViewedNews(){
        var jsonMessage = '[{"type":"decNVNews", "id":'+connUserId+'}]';
        conn.send(jsonMessage);
    }

    //Увеличение количества не просмотренных акций
    function incNotViewedStocks(){
        var jsonMessage = '[{"type":"incNVStocks", "id":'+connUserId+'}]';
        conn.send(jsonMessage);
    }

    //Уменьшение количества не просмотренных акций
    function decNotViewedStocks(){
        var jsonMessage = '[{"type":"decNVStocks", "id":'+connUserId+'}]';
        conn.send(jsonMessage);
    }


    //TODO доделать урл для компаний. Сделать JavaScript проверку на валидность (Нельзя использовать русские буквы и всякие знаки),
    //TODO а также проверка на уникальность.
    //Оптимизация
    //TODO переделать хранение товаров в корзине, использовать куки
    //TODO все панели, которые не показываются при начальной загрузке страницы нужно перевести на загрузку с помощью ajax
    //
    //TODO думаем дальше что будет и к чему.
