$(document).ready(function(){
    $(".panel-collapse").click(function(){
        $("#accordion").collapse({
            toggle: false
        });
    })
    $('#MyProfile').mouseover(function() {
        $('#Profile').show();
    });
	$('.XYZ').mouseenter(function(e){
       alert('Попал');
    });

    $('#MyProfile').mouseout(function() {
        t = setTimeout(function() {
            $('#Profile').hide();
        }, 500);
        $('#Profile').on('mouseenter', function() {
            $('#Profile').show();
            clearTimeout(t);
        }).on('mouseleave', function() {
            $('#Profile').hide();
        })
    });
    $('#img-radius').mouseover(function() {
        $('#Profile').show();
    });
    $('#img-radius').mouseout(function() {
        t = setTimeout(function() {
            $('#Profile').hide();
        }, 500);
        $('#Profile').on('mouseenter', function() {
            $('#Profile').show();
            clearTimeout(t);
        }).on('mouseleave', function() {
            $('#Profile').hide();
        })
    })
    $('#dropdownFilter').mouseover(function() {
        $('#filterContent').show();
    });
    $('#dropdownFilter').mouseout(function() {
        t = setTimeout(function() {
            $('#filterContent').hide();
        }, 500);
        $('#filterContent').on('mouseenter', function() {
            $('#filterContent').show();
            clearTimeout(t);
        }).on('mouseleave', function() {
            $('#filterContent').hide();
        })
    });
    $(".main-city").click(function() {
        $("#myModal").modal('show');
    });
	$(".activeModal").click(function() {
		document.getElementById('deleteProduct').setAttribute('href', this.getAttribute("data-parameter"));
        $("#MyModal2").modal('show');
    });
	$('.XYZ').mouseenter(function() {
        $('.XYZ').attr('style', '');
        $(this).attr('style', 'background:#ed4545;color: #fff;border-radius: 3px;padding: 5px 5px 5px 5px;');
    });
	var url = window.location.href;
	var host = window.location.host;
	if(url.indexOf('http://' + host + '/product/myProducts') != -1) {
		$('#example-1-1 .sortable-list').sortable({
			stop: function( event, ui ) {
				MyMouseUp();
			}
		});
	}		
})

$(function (){
    $('.accordion-btn').click(function (){
        $(this).toggleClass('off');
    });
})

$(function (){
    $('.panel-default').click(function (){
        $(this).toggleClass('non-active');
    });
})

$(function (){
    $('.panel-title').click(function (){
        $(this).toggleClass('off');
    });
})

$(function(){
    $('#myAffix').width($('#left').width());
    $(window).resize(function(){
        $('#myAffix').width($('#left').width());
    });
})

$(function(){
    // раскрытие полного описания компании
    $(".content .desc ").on('click', '.expand.close-type', function(e){
        e.preventDefault();
        $('.content .desc').css({'overflow': 'visible', 'height': 'auto'});
        $(".expand").html('Свернуть<span class="glyphicon glyphicon-menu-up"></span>');
        $(".expand").addClass('open-type').removeClass('close-type');
    });

    $(".content .desc").on('click', '.expand.open-type', function(e){
        e.preventDefault();
        $('.content .desc').css({'overflow': 'hidden', 'height': '105'});
        $(".expand").html('Развернуть<span class="glyphicon glyphicon-menu-down"></span>');
        $(".expand").addClass('close-type').removeClass('open-type');
    });
})

$(function(){

    $('#name').keyup(function(){
        translit();
        return false;
    });

    $('#name_translit').keyup(function(){
        checkUrl();
        return false;
    });

	$('.password-showing').change(function(){
        console.log($('.password-showing').attr('value'));  
    })	

    $('[data-toggle="tooltip"]').tooltip(); // поддержка подсказок при наведении

    $("#menu").css('display', 'block');

    /*   фильтр поиска    */
    $(".search-filter .dropdown-menu li a").click(function(){
        $("#dropdownFilter").html($(this).html());
    });

    $(".mobile-search .search-filter .dropdown-menu li a").click(function(){
        $("#dropdownFilter-mobile").html($(this).html());
    });

    $(".header .search input").focus(function(){
        $(".search").css('border', '1px solid #BEBFC0');
    });

    $(".header .search input").blur(function(){
        $(".search").css('border', '1px solid #F8FAFC');
    });

    $('.megamenu-items-wrapper li').hover(function() {
        $(this).find(".hidden-item").show();
    }, function() {
        $(this).find('.hidden-item').hide();
    });

    if($('#left-menu-toggle').hasClass('button-click')){
        $('#megamenu').show();
    }else{
        $('#megamenu').hide();
        $("#profile-menu").show();
    }

    $("#left-menu-toggle").click(function(){
        if($('#megamenu').hasClass('open')){
            $('#megamenu').hide();
            $('#megamenu').removeClass('open');
            $('#megamenu').addClass('close');
            $("#profile-menu").show();
            $("#profile-menu").removeClass('close');
            $("#profile-menu").addClass('open');
            $("#left-menu-toggle").removeClass('button-click');
        }else{
            $("#profile-menu").hide();
            $("#profile-menu").removeClass('open');
            $("#profile-menu").addClass('close');
            $('#megamenu').show();
            $('#megamenu').removeClass('close');
            $('#megamenu').addClass('open');
            $("#left-menu-toggle").addClass('button-click');
        }
    });

    // разворачивание мобильного поиска
    $('#toggle-search').on('click', function(){
        if($('.mobile-search').css('display') == 'block'){
            $('.mobile-search').slideUp();
            $(".over").css('display', 'none');
        }else{
            $('.mobile-search').slideDown();
            $(".over").css('display', 'block');
        }
    });
    $('.mobile-search button').on('click', function(){
        $('.mobile-search').slideUp();
        $(".over").css('display', 'none');
    });



    // выбор категории и страны в фильтре для товаров и выдачи
    $(".filter-cats .dropdown-menu li a").click(function(e){
        e.preventDefault();
        $("#filter-cats").html($(this).html() + '<span class="caret"></span>');
    });
    $(".filter-country .dropdown-menu li a").click(function(e){
        e.preventDefault();
        $("#filter-country").html($(this).html() + '<span class="caret"></span>');
    });
    $(".filter-provider .dropdown-menu li a").click(function(e){
        e.preventDefault();
        $("#filter-provider").html($(this).html() + '<span class="caret"></span>');
    });
    $(".filter-fabricator .dropdown-menu li a").click(function(e){
        e.preventDefault();
        $("#filter-fabricator").html($(this).html() + '<span class="caret"></span>');
    });

    // кнопки в фильтре поиска
    $(".search-buttons button").click(function(e){
        e.preventDefault();
        if($(this).hasClass("check-button")){
            $(this).removeClass("check-button");
        }else{
            $(this).addClass("check-button");
        }
    });

    // открытие и скрытие фильтра поиска на странице выдачи
    $('.results-filter-xs').on('click', '.open-filter.off-filter', function(e){ // если фильтр закрыт
        e.preventDefault();
        $(this).html('Закрыть фильтр').removeClass('off-filter').addClass('on-filter');
        $('.results-filter').slideDown();
    });

    $('.results-filter-xs').on('click', '.open-filter.on-filter', function(e){ // если фильтр открыт
        e.preventDefault();
        $(this).html('Открыть фильтр').removeClass('on-filter').addClass('off-filter');
        $('.results-filter').slideUp();
    });

    // маска для поля ввода времени работы
    $('.time-work').mask("99:99",{placeholder:"00:00"});
    $('.input-phone.home input').mask("+7(9999) 99-99-99",{placeholder:"+7(____) __-__-__"});
    $('.input-phone.mobile input').mask("+7(999) 999-99-99",{placeholder:"+7(___) ___-__-__"});
    $('#index').mask("999999",{placeholder:"______"});

    // чекбокс дней работы
    $('.profile-form').on('click', '.work-day span:last-child', function(e){
        if($(this).hasClass('check-day')){
            $(this).removeClass('check-day');
            $(this).siblings('input').attr('disabled', 'disabled').val('');

        }else{
            $(this).addClass('check-day');
            $(this).siblings('input').removeAttr('disabled');
        }
    });

    $('.add-email').click(function(){
        $('.input-alert-email').before('<div class="form-group"><div class="col-xs-8 email-input col-xs-offset-3"><input type="text" class="form-control" name="email'+$('.email-input input').size()+'" onchange="insertAlertEmail('+"'"+$('.email-input input').size()+"'"+')"></div><div class="col-xs-1"><span onclick="addAlertEmail()" class="add-home-phone" style="float:left; margin-right: 5px"></span><span class="delete-email" style="float:left"></span></div></div>');
        document.getElementById('email_count').value = $('.email-input input').size();
    });

$('.form-horizontal').on('click', '.delete-email', function(e){
        $(this).closest(".form-group").remove();
        document.getElementById('email_count').value = parseInt(document.getElementById('email_count').value) - 1;
    });

    // добавление полей для home-phone
    $('.add-home-phone').click(function(){

        if($('.input-phone.homep input').size() >= 4){
            alert("Вы добавили максимальное количество номеров!");
            return;
        }

        else if ($('.input-phone.homep input').size() >= 3 && $('.input-phone.homep input').size() <= 4  )
        {
            alert("Это максимальное количество номеров!");
            $('.add-home-phone-before').before('<div class="form-group"><div class="input-phone homep col-xs-offset-3"><input type="text" class="form-control" name="home_phone'+$('.input-phone.homep input').size()+'"><span class="glyphicon glyphicon-phone-alt"></span></div><div class="col-xs-1"><span onclick="addHomePhone()" class="add-home-phone" style="float:left; margin-right: 5px"></span><span class="delete-home-phone" style="float:left"></span></div></div>');
        }
        else
        {
            $('.add-home-phone-before').before('<div class="form-group"><div class="input-phone homep col-xs-offset-3"><input type="text" class="form-control" name="home_phone'+$('.input-phone.homep input').size()+'"><span class="glyphicon glyphicon-phone-alt"></span></div><div class="col-xs-1"><span onclick="addHomePhone()" class="add-home-phone" style="float:left; margin-right: 5px"></span><span class="delete-home-phone" style="float:left"></span></div></div>');
        }
    });

    $('.form-horizontal').on('click', '.delete-home-phone', function(e){
        $(this).closest(".form-group").remove();
    });

    // добавление полей для mobile-phone
    $('.add-mobile-phone').click(function(){

        if($('.input-phone.mobilep input').size() >= 4){
            alert("Вы добавили максимальное количество номеров!");
            return;
        }


        else if ($('.input-phone.mobilep input').size() >= 3 && $('.input-phone.mobilep input').size() <= 4 )
        {
            alert("Это максимальное количество номеров!");
            $('.add-mobile-phone-before').before('<div class="form-group"><div class="input-phone mobilep col-xs-offset-3"><input type="text" class="form-control" name="mobile_phone'+$('.input-phone.mobilep input').size()+'"><span class="glyphicon glyphicon-phone"></span></div><div class="col-xs-1"><span onclick="addMobilePhone()" class="add-mobile-phone" style="float:left; margin-right: 5px"></span><span class="delete-mobile-phone" style="float:left"></span></div></div>');
        }
        else
        {
            $('.add-mobile-phone-before').before('<div class="form-group"><div class="input-phone mobilep col-xs-offset-3"><input type="text" class="form-control" name="mobile_phone'+$('.input-phone.mobilep input').size()+'"><span class="glyphicon glyphicon-phone"></span></div><div class="col-xs-1"><span onclick="addMobilePhone()" class="add-mobile-phone" style="float:left; margin-right: 5px"></span><span class="delete-mobile-phone" style="float:left"></span></div></div>');;
        }

    });

    // удаление поля для mobile-phone
    $('.form-horizontal').on('click', '.delete-mobile-phone', function(e){
        $(this).closest(".form-group").remove();
    });

    //Добавление новой почты для получения оповещений
    $('.add-alert-email').click(function() {
        $('.all-alert-email').after('<div class="form-group"><div class="input-phone alert col-xs-offset-3"><input type="text" class="form-control" name="alert_email'+$('.input-phone.alert input')+'" onchange="insertAlertEmail('+"'"+$('.email-input input').size()+"'"+')"><span class="delete-email"></span></div>')
    });

  $(':password').showPassword({
    linkRightOffset: 10,
    linkTopOffset: 10
  });

	$('[data-spzoom]').spzoom({
	  width: 220,             
	  height: 220,            
	  position: 'right',      
	  margin: 20,             
	  showTitle: false,        
	  titlePosition: 'bottom'   
	});  
	
	$('.tooltip2').tooltipster({
		theme: 'tooltipster-light',
		side: 'right',
		arrow: false,
		maxWidth: 200,
		distance: 10,
		animation: 'fade'
	});
	
	
	var $lis = $('.products-image a');
	$lis.click(function(){	
		event.preventDefault();
		window.location = $(this).data('target-page');
	});	
	
	$('.show-password-link').mousedown(function(){	
		if ( $(this).hasClass('red-eye') ){
			$(this).removeClass('red-eye');
		}	
		else{
			$(this).addClass('red-eye');
		}	
	});
    var server_port = document.getElementById('SERVER_PORT').value;
    var server_name = document.getElementById('SERVER_NAME').value;
    if (server_port != '80'){
        server_name = server_name + ':' + server_port;
    }
	var url = window.location.href;
	var host = server_name;
	if(url.indexOf('http://' + host + '/StockAndNews/addStock') != -1) {
	   $('#name_stock').limit('70','#name_stockcharsLeft');
	   $('#description_stock').limit('100','#description_stockcharsLeft');
	}	
	else if (url.indexOf('http://' + host + '/user/main/') != -1)
	{
		$('#short-desc').limit('40','#shortcharsLeft');
		$('#name').limit('70','#namecharsLeft');	
	}		
	else if (url.indexOf('http://' + host + '/StockAndNews/addNew') != -1)
	{
		$('#description_new').limit('150','#description_newcharsLeft');
		$('#name_new').limit('70','#name_newcharsLeft');	
	}	
	else if (url.indexOf('http://' + host + '/product/addProduct') != -1)
	{
		$('#product_meta').limit('150','#product_metacharsLeft');
		$('#name_product').limit('70','#name_productcharsLeft');	
	}	
																										  
																										  
	$('.hidden-menu-column').filter(function(){
		return $(this).text().trim() === '';
	}).remove();																										  
																										  
																										  
});
